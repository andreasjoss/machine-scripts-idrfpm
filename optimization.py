#!/usr/bin/python

#Title : This script takes care of a single optimization convergence process
#Author: Andreas Joss

from __future__ import division
#import pyOpt
import nlopt
import numpy as np
import os, sys
import shutil
from prettytable import PrettyTable
#import threading
import argparse
import time
import sqlite3
import pylab as pl
import smtplib
import datetime

enable_multiple_operating_points = 1
#operating_point = 1 #ROUND LITZ
operating_point = 0 #SOLID BARS OR IDRFPM
enable_state_machine_operating_points = 1
enable_objective_constraints = 1
single_weight_optimization = 1
use_pyOpt = 0
use_nlopt = 1
start_time = time.time()
primary_key_start = 1
primary_key_upper_limit = -2
primary_key_lower_limit = -1
debug = 0
debug_f = 0
has_entered_opt_function = False
display_poly_from_this_iteration = -1
#display_poly_from_this_iteration = 125
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki','g','y','k']

#epsilon1 = 0.5e-3 #hierdie waarde vat so 15min vir 'n sekere weight om geoptimeer te wees
epsilon1 = 3e-2

start_time = time.time()

parser = argparse.ArgumentParser(description="Optimize weighted sum",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=1,help='primary_key to be optimized')
args = parser.parse_args()
primary_key = args.primary_key

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  #try:
  c.execute(string)
  return c.fetchone()[0]
  #except:
    #return 0

pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

pretty_variable_table = PrettyTable(['Variable','Value [p.u.]','Value','Unit'])
pretty_variable_table.align = 'l'
pretty_variable_table.border= True  

pretty_output_table = PrettyTable(['Variable','Value','Unit'])
pretty_output_table.align = 'l'
pretty_output_table.border= True  

#setup databse connection
#conn = sqlite3.connect('SORSPM.db')
conn = sqlite3.connect('IDRFPM.db')
c = conn.cursor()

#c.execute("ATTACH DATABASE 'SORSPM.db' as SORSPM")
pk=1

#ROUND LITZ
#p_mech_constraint = [[0,1,0],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
#torque_ripple_constraint = [[0,0,0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
#torque_constraint = [[0,0,0],db_input_select("output_constraints","average_torque_dq",pk)]
#power_factor_constraint = [[0,0,0],db_input_select("output_constraints","power_factor",pk)]
#total_mass_constraint = [[0,0,0],db_input_select("output_constraints","total_mass",pk)]
#magnet_mass_constraint = [[0,0,0],db_input_select("output_constraints","magnet_mass",pk)]
#efficiency_constraint = [[0,0,0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
#total_losses_constraint = [[0,0,0],db_input_select("output_constraints","total_losses",pk)]
#current_density_rms_constraint = [[0,0,0],db_input_select("output_constraints","current_density_rms",pk)]
#torque_density_constraint = [[0,0,0],db_input_select("output_constraints","torque_density",pk)] 

#SOLID BARS
#p_mech_constraint = [[1,0],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
#torque_ripple_constraint = [[0,0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
#torque_constraint = [[0,0],db_input_select("output_constraints","average_torque_dq",pk)]
#power_factor_constraint = [[0,0],db_input_select("output_constraints","power_factor",pk)]
#total_mass_constraint = [[0,0],db_input_select("output_constraints","total_mass",pk)]
#magnet_mass_constraint = [[0,0],db_input_select("output_constraints","magnet_mass",pk)]
#efficiency_constraint = [[0,0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
#total_losses_constraint = [[0,0],db_input_select("output_constraints","total_losses",pk)]
#current_density_rms_constraint = [[0,0],db_input_select("output_constraints","current_density_rms",pk)]
#torque_density_constraint = [[0,0],db_input_select("output_constraints","torque_density",pk)]

p_mech_constraint = [[1],db_input_select("output_constraints","p_out_dq_torque",pk)] #was previously [0,1,1]	#three constraint enables are used to represent each operating point
torque_ripple_constraint = [[0],db_input_select("output_constraints","torque_ripple_semfem",pk)]
torque_constraint = [[0],db_input_select("output_constraints","average_torque_dq",pk)]
power_factor_constraint = [[0],db_input_select("output_constraints","power_factor",pk)]
total_mass_constraint = [[0],db_input_select("output_constraints","total_mass",pk)]
magnet_mass_constraint = [[0],db_input_select("output_constraints","magnet_mass",pk)]
efficiency_constraint = [[0],db_input_select("output_constraints","efficiency_indirect_dq",pk)]
total_losses_constraint = [[0],db_input_select("output_constraints","total_losses",pk)]
current_density_rms_constraint = [[0],db_input_select("output_constraints","current_density_rms",pk)]
torque_density_constraint = [[0],db_input_select("output_constraints","torque_density",pk)]

#setup databse connection
conn.close()
conn = sqlite3.connect('optimise.db')
c = conn.cursor()

#obtain all the working points
operating_speeds = []
if enable_state_machine_operating_points:
  res = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
  for name in res:
    if name[0].startswith("machine_input_parameters_"):
      print name[0]
      operating_speeds.append(int(name[0][-3:]))
    
input_table_name = []
output_table_name = []

for i in range(0,len(operating_speeds)):
  input_table_name.append('machine_input_parameters' + '_rpm_%03d'%(operating_speeds[i]))
  output_table_name.append('machine_semfem_output' + '_rpm_%03d'%(operating_speeds[i]))

if single_weight_optimization == 1:
  iteration_number = 1
else:
  iteration_number = 0

c.execute("UPDATE machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))

#c.execute("ATTACH DATABASE 'best.db' as best")
#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(0,))
#c.execute("INSERT INTO best.info (variable_name,value_int) VALUES (?,?)",('have_already_commited_best_solution',0))
conn.commit()

#GLOBALS USED IN OBJ FUNC()
weight_eff = []
weight_pf = []
weight_mass_total = []
weight_torque_average = []
weight_torque_ripple = []
weight_torque_density = []
weight_pm_mass = []
weight_pmech = []
weight_total_losses = []
weight_J_rms = []
#read weights
for i in range(0,len(operating_speeds)):
  weight_eff.append(db_input_select(input_table_name[i],"weight_eff",primary_key))
  weight_pf.append(db_input_select(input_table_name[i],"weight_pf",primary_key))
  weight_mass_total.append(db_input_select(input_table_name[i],"weight_mass_total",primary_key))
  weight_torque_average.append(db_input_select(input_table_name[i],"weight_torque_average",primary_key))
  weight_torque_ripple.append(db_input_select(input_table_name[i],"weight_torque_ripple",primary_key))
  weight_torque_density.append(db_input_select(input_table_name[i],"weight_torque_density",primary_key))
  weight_pm_mass.append(db_input_select(input_table_name[i],"weight_pm_mass",primary_key))
  weight_pmech.append(db_input_select(input_table_name[i],"weight_pmech",primary_key))
  weight_total_losses.append(0)
  weight_J_rms.append(0)


#p_mech_penalty_constant = 1.7
p_mech_penalty_constant = 1.0

if enable_objective_constraints == 0:
  #try to get the primary_key ideal value outputs
  try:
    c.execute("SELECT efficiency_indirect_dq FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[0]),))
    ideal_eff = c.fetchone()[0]
  except:
    ideal_eff = 1
    
  try:  
    c.execute("SELECT total_mass FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[1]),))
    ideal_mass_total = c.fetchone()[0]
  except:
    ideal_mass_total = 1

  try:
    c.execute("SELECT average_torque_dq FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[2]),))
    ideal_torque_average = c.fetchone()[0]
  except:
    ideal_torque_average = 1
    
  try:
    c.execute("SELECT torque_ripple_semfem FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[3]),))
    ideal_torque_ripple = c.fetchone()[0]
  except:
    ideal_torque_ripple = 1
    
  try:
    c.execute("SELECT power_factor FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[4]),))
    ideal_pf = c.fetchone()[0]
  except:
    ideal_pf = 1
    
  try:
    c.execute("SELECT torque_density FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[5]),))
    ideal_torque_density = c.fetchone()[0]
  except:
    ideal_torque_density = 1    

  try:
    c.execute("SELECT magnet_mass FROM best.machine_semfem_output WHERE primary_key = ?",(abs(ideal_value_keys[5]),))
    ideal_pm_mass = c.fetchone()[0]
  except:
    ideal_pm_mass = 1    
else:
  ideal_eff = efficiency_constraint[1]
  ideal_mass_total = total_mass_constraint[1]
  ideal_torque_average = torque_constraint[1]
  ideal_torque_ripple = torque_ripple_constraint[1]
  ideal_pf = power_factor_constraint[1]
  ideal_torque_density = torque_density_constraint[1]
  ideal_pm_mass = magnet_mass_constraint[1]
  ideal_pmech_out = p_mech_constraint[1]
  ideal_J_rms = current_density_rms_constraint[1]
  ideal_total_losses = total_losses_constraint[1]

pretty_terminal_table.add_row(["ideal_eff",ideal_eff,"[%]"])
pretty_terminal_table.add_row(["ideal_mass_total",ideal_mass_total,"[kg]"])
pretty_terminal_table.add_row(["ideal_torque_average",ideal_torque_average,"[N.m]"])
pretty_terminal_table.add_row(["ideal_torque_ripple",ideal_torque_ripple,"[N.m]"])
pretty_terminal_table.add_row(["ideal_pf",ideal_pf,"[%]"])
pretty_terminal_table.add_row(["ideal_torque_density",ideal_torque_density,"[N.m/kg]"])
pretty_terminal_table.add_row(["ideal_pm_mass",ideal_pm_mass,"[N.m/kg]"])
if enable_objective_constraints == 1:
  pretty_terminal_table.add_row(["ideal_pmech_out",ideal_pmech_out,"[W]"])
  pretty_terminal_table.add_row(["ideal_J_rms",ideal_J_rms,"[A/mm2]"])
  pretty_terminal_table.add_row(["ideal_total_losses",ideal_total_losses,"[W]"])

i=0
#read starting values
#ro = db_input_select(input_table_name[i],"outer_radius_in_mm",primary_key)
hc  = db_input_select(input_table_name[i],"coil_height_in_mm",primary_key)
hmi_r = db_input_select(input_table_name[i],"inner_magnet_height_in_mm",primary_key)
hmo_r = db_input_select(input_table_name[i],"outer_magnet_height_in_mm",primary_key)
km = db_input_select(input_table_name[i],"radially_magnetized_PM_width_ratio",primary_key)
kc  = db_input_select(input_table_name[i],"coil_width_ratio",primary_key)
stacklength = db_input_select(input_table_name[i],"stacklength_in_mm",primary_key)

#read minimum values
#ro_min = db_input_select(input_table_name[i],"outer_radius_in_mm",primary_key_lower_limit)
hc_min  = db_input_select(input_table_name[i],"coil_height_in_mm",primary_key_lower_limit)
hmi_r_min = db_input_select(input_table_name[i],"inner_magnet_height_in_mm",primary_key_lower_limit)
hmo_r_min = db_input_select(input_table_name[i],"outer_magnet_height_in_mm",primary_key_lower_limit)
km_min = db_input_select(input_table_name[i],"radially_magnetized_PM_width_ratio",primary_key_lower_limit)
kc_min  = db_input_select(input_table_name[i],"coil_width_ratio",primary_key_lower_limit)
stacklength_min  = db_input_select(input_table_name[i],"stacklength_in_mm",primary_key_lower_limit)

#read maximum values (only those necessary)
#ro_max = db_input_select(input_table_name[i],"outer_radius_in_mm",primary_key_upper_limit)
hc_max  = db_input_select(input_table_name[i],"coil_height_in_mm",primary_key_upper_limit)
hmi_r_max = db_input_select(input_table_name[i],"inner_magnet_height_in_mm",primary_key_upper_limit)
hmo_r_max = db_input_select(input_table_name[i],"outer_magnet_height_in_mm",primary_key_upper_limit)
km_max = db_input_select(input_table_name[i],"radially_magnetized_PM_width_ratio",primary_key_upper_limit)
kc_max  = db_input_select(input_table_name[i],"coil_width_ratio",primary_key_upper_limit)
stacklength_max  = db_input_select(input_table_name[i],"stacklength_in_mm",primary_key_upper_limit)

#ro_pu = (ro-ro_min)/(ro_max-ro_min)
hc_pu = (hc - hc_min)/(hc_max-hc_min)
hmi_r_pu = (hmi_r - hmi_r_min)/(hmi_r_max-hmi_r_min)
hmo_r_pu = (hmo_r - hmo_r_min)/(hmo_r_max-hmo_r_min)
kc_pu = (kc - kc_min)/(kc_max-kc_min)
km_pu = (km - km_min)/(km_max-km_min)
stacklength_pu = (stacklength - stacklength_min)/(stacklength_max-stacklength_min)

print pretty_terminal_table

print("This is the initial/starting solution given to the optimiser")
pretty_variable_table.add_row(["hc",hc_pu,hc,"[mm]"])
pretty_variable_table.add_row(["hmi_r",hmi_r_pu,hmi_r,"[mm]"])
pretty_variable_table.add_row(["hmo_r",hmo_r_pu,hmo_r,"[mm]"])  
pretty_variable_table.add_row(["kc",kc_pu,kc,"[p.u.]"])
pretty_variable_table.add_row(["km",km_pu,km,"[p.u.]"])
pretty_variable_table.add_row(["stacklength",stacklength_pu,stacklength,"[mm]"])
#pretty_variable_table.add_row(["ro",ro_pu,ro,"[mm]"])
print pretty_variable_table
pretty_variable_table.clear_rows()

f_power_factor = np.zeros(len(operating_speeds))
f_average_torque_dq = np.zeros(len(operating_speeds))
f_torque_ripple_semfem = np.zeros(len(operating_speeds))
f_efficiency_indirect_dq = np.zeros(len(operating_speeds))
f_total_mass = np.zeros(len(operating_speeds))
f_torque_density = np.zeros(len(operating_speeds))
f_pm_mass = np.zeros(len(operating_speeds))
f_pmech = np.zeros(len(operating_speeds))
f_current_density_rms = np.zeros(len(operating_speeds))
f_total_losses = np.zeros(len(operating_speeds))

f_power_factor_normalised = np.zeros(len(operating_speeds))
f_average_torque_dq_normalised = np.zeros(len(operating_speeds))
f_torque_ripple_semfem_normalised = np.zeros(len(operating_speeds))
f_efficiency_indirect_dq_normalised = np.zeros(len(operating_speeds))
f_total_mass_normalised = np.zeros(len(operating_speeds))
f_torque_density_normalised = np.zeros(len(operating_speeds))
f_pm_mass_normalised = np.zeros(len(operating_speeds))
f_pmech_normalised = np.zeros(len(operating_speeds))
f_current_density_rms_normalised = np.zeros(len(operating_speeds))
f_total_losses_normalised = np.zeros(len(operating_speeds))

#define objective function to be optimized
def weighted_objective_function(x,grad):
  global c
  global primary_key
  global iteration_number
  global conn
  global operating_point
  #raw_input("Press Enter to continue...")
  print("NEW ITERATION")
  print("iteration number = %d"%(iteration_number))
  #print "optimization.py -- starting weighted_objective_function"
  if single_weight_optimization == 0:
    iteration_number = db_input_select(input_table_name[0],"iteration_number",primary_key)
  else:
    #read last entry in database
    c.execute("SELECT * FROM %s WHERE primary_key = (SELECT MAX(primary_key) FROM %s)"%(input_table_name[0],input_table_name[0]))
    iteration_number = c.fetchone()[0]

  #ro_pu = x[0]
  #hc_pu = x[1]
  #hmi_r_pu = x[2]
  #hmo_r_pu = x[3]
  #kc_pu = x[4]
  #km_pu = x[5]
  #stacklength_pu = x[6]

  hc_pu = x[0]
  hmi_r_pu = x[1]
  hmo_r_pu = x[2]
  kc_pu = x[3]
  km_pu = x[4]
  stacklength_pu = x[5]

  #ro = ro_min + ro_pu*(ro_max-ro_min)
  hc  = hc_min + hc_pu*(hc_max-hc_min)
  hmi_r = hmi_r_min + hmi_r_pu*(hmi_r_max-hmi_r_min)
  hmo_r = hmo_r_min + hmo_r_pu*(hmo_r_max-hmo_r_min)
  km = km_min + km_pu*(km_max-km_min)
  kc  = kc_min + kc_pu*(kc_max-kc_min)
  stacklength = stacklength_min + stacklength_pu*(stacklength_max-stacklength_min)
  
  #if debug == 1:
  pretty_variable_table.add_row(["hc",hc_pu,hc,"[mm]"])
  pretty_variable_table.add_row(["hmi_r",hmi_r_pu,hmi_r,"[mm]"])
  pretty_variable_table.add_row(["hmo_r",hmo_r_pu,hmo_r,"[mm]"])  
  pretty_variable_table.add_row(["kc",kc_pu,kc,"[p.u.]"])
  pretty_variable_table.add_row(["km",km_pu,km,"[p.u.]"])
  pretty_variable_table.add_row(["stacklength",stacklength_pu,stacklength,"[mm]"])
  #pretty_variable_table.add_row(["ro",ro_pu,ro,"[mm]"])
  
  print pretty_variable_table
  
  pretty_variable_table.clear_rows()
  
  #insert previous input parameters into new row
  if single_weight_optimization == 1:
    #c.execute("INSERT INTO best.machine_semfem_output (power_factor,average_torque_dq,efficiency_indirect_dq,torque_ripple_semfem,total_mass,torque_density,magnet_mass,p_out_dq_torque,total_losses,current_density_rms,f_value,primary_key) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
    
    table_name = 'machine_input_parameters'

    pk2 = primary_key
    pk1 = primary_key+1    
    
    c = conn.execute("select * from %s"%(table_name))
    column_names = list(map(lambda x: x[0], c.description))  
  
    for j in range(0,len(operating_speeds)):
      for i in range(1,len(column_names)):
	#get value of each column from the previous input entry 
	c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name+'_rpm_%03d'%(operating_speeds[j]),pk2))
	val = c.fetchone()[0]      
	if i == 1:
	  try:	      
	    #create next entry (next entry is thus created)	  
	    c.execute("INSERT INTO %s (primary_key) VALUES (%d)"%(table_name+'_rpm_%03d'%(operating_speeds[j]),pk1))
	  except:
	    print "Could not create new entry."
	    pass

	#update the new row according to the previous row's entries
	c.execute("UPDATE %s SET %s = ? WHERE primary_key = ?"%(table_name+'_rpm_%03d'%(operating_speeds[j]),column_names[i]),(val,pk1))
  
  
  #write new variable and dimension values into optimise.db
  for j in range(0,len(operating_speeds)): 
    #c.execute("UPDATE %s SET outer_radius_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(ro,primary_key))
    c.execute("UPDATE %s SET coil_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hc,primary_key))
    c.execute("UPDATE %s SET inner_magnet_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hmi_r,primary_key))
    c.execute("UPDATE %s SET outer_magnet_height_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(hmo_r,primary_key))
    c.execute("UPDATE %s SET radially_magnetized_PM_width_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(km,primary_key))
    c.execute("UPDATE %s SET coil_width_ratio = ? WHERE primary_key = ?"%(input_table_name[j]),(kc,primary_key))
    c.execute("UPDATE %s SET stacklength_in_mm = ? WHERE primary_key = ?"%(input_table_name[j]),(stacklength,primary_key))
    
  #c.execute("UPDATE %s SET stacklength_in_mm = ? WHERE primary_key = ?",(l,primary_key))
  #print "optimization.py -- going to update and commit parameters"
  conn.commit()
  
  if (single_weight_optimization == 1 and primary_key>1):
    c.execute("SELECT MAX(primary_key) FROM best."+output_table_name[operating_point])
    primary_key_best = c.fetchone()[0]
    print("primary_key_best = %d"%(primary_key_best))
  else:
    primary_key_best = 1
  
  if enable_multiple_operating_points:
    loops = len(operating_speeds)
  else:
    loops = 1
  
  f_best = 0
  #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
  for i in range(0,loops): #SOLID BARS
    if enable_multiple_operating_points:
      operating_point = i
      
    if (iteration_number > 0 and single_weight_optimization == 0) or (single_weight_optimization == 1 and primary_key>1):
      #if single_weight_optimization == 1:
	#previous_primary_key = primary_key - 1
      #else:
	#previous_primary_key = primary_key
      
      #print(previous_primary_key)
      
      #for s in range(0,len(f_string_names_list)): 
	#c.execute("UPDATE machine_semfem_output SET %s = ? WHERE primary_key = ?"%(f_string_names_list[s]),(f_objectives_list[s],primary_key)) 

      #print("KLIP primary_key = %d"%(primary_key))  
      f_power_factor[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"power_factor",primary_key_best))
      f_average_torque_dq[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"average_torque_dq",primary_key_best))
      f_torque_ripple_semfem[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"torque_ripple_semfem",primary_key_best))
      f_efficiency_indirect_dq[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"efficiency_indirect_dq",primary_key_best))
      f_total_mass[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"total_mass",primary_key_best))
      f_torque_density[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"torque_density",primary_key_best))
      f_pm_mass[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"magnet_mass",primary_key_best))
      if enable_objective_constraints == 1:
	f_pmech[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"p_out_dq_torque",primary_key_best))
	f_current_density_rms[operating_point] = abs(db_input_select("best."+input_table_name[operating_point],"current_density_rms",primary_key_best))
	f_total_losses[operating_point] = abs(db_input_select("best."+output_table_name[operating_point],"total_losses",primary_key_best))
      
      f_power_factor_normalised[operating_point] = abs(f_power_factor[operating_point]/ideal_pf)
      f_average_torque_dq_normalised[operating_point] = abs(f_average_torque_dq[operating_point]/ideal_torque_average)
      f_torque_ripple_semfem_normalised[operating_point] = abs(f_torque_ripple_semfem[operating_point]/ideal_torque_ripple)
      f_efficiency_indirect_dq_normalised[operating_point] = abs(f_efficiency_indirect_dq[operating_point]/ideal_eff)
      f_total_mass_normalised[operating_point] = abs(f_total_mass[operating_point]/ideal_mass_total)   
      f_torque_density_normalised[operating_point] = abs(f_torque_density[operating_point]/ideal_torque_density)
      f_pm_mass_normalised[operating_point] = abs(f_pm_mass[operating_point]/ideal_pm_mass)
      if enable_objective_constraints == 1:
	f_pmech_normalised[operating_point] = abs(f_pmech[operating_point]/ideal_pmech_out)
	f_current_density_rms_normalised[operating_point] = abs(f_current_density_rms[operating_point]/ideal_J_rms)
	f_total_losses_normalised[operating_point] = abs(f_total_losses[operating_point]/ideal_total_losses)
  
      if enable_objective_constraints == 0:
	f_best = -weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] -weight_pf[operating_point]*f_power_factor_normalised[operating_point] -weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] +weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
      
      #print("operating_point = %d"%(operating_point))
      #print("1f_best = %f"%(f_best))
	
      ###efficiency (maximize)
      if f_efficiency_indirect_dq_normalised[operating_point] > efficiency_constraint[1]/ideal_eff:
	f_best = f_best - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]
      else:
	#penalty = (f_efficiency_indirect_dq_normalised - efficiency_constraint[1]/ideal_eff)**2
	penalty = (1+abs(f_efficiency_indirect_dq_normalised[operating_point] - efficiency_constraint[1]/ideal_eff))**2
	f_best = f_best - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] + efficiency_constraint[0][operating_point]*penalty
      #print("2f_best = %f"%(f_best))
      
      ###torque (maximize)
      if f_average_torque_dq_normalised[operating_point] > torque_constraint[1]/ideal_torque_average:
	f_best = f_best - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]
      else:
	#penalty = (f_average_torque_dq_normalised - torque_constraint[1]/ideal_torque_average)**2
	penalty = (1+abs(f_average_torque_dq_normalised[operating_point] - torque_constraint[1]/ideal_torque_average))**2
	f_best = f_best - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + torque_constraint[0][operating_point]*penalty     
      #print("3f_best = %f"%(f_best))
      
      ###total mass (minimize)
      if f_total_mass_normalised[operating_point] < total_mass_constraint[1]/ideal_mass_total:  
	f_best = f_best + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]
      else:
	#penalty = (f_total_mass_normalised - total_mass_constraint[1]/ideal_mass_total)**2
	penalty = (1+abs(f_total_mass_normalised[operating_point] - total_mass_constraint[1]/ideal_mass_total))**2
	f_best = f_best + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + total_mass_constraint[0][operating_point]*penalty
      #print("4f_best = %f"%(f_best))
      
      ###torque ripple (minimize)
      if f_torque_ripple_semfem_normalised[operating_point] < torque_ripple_constraint[1]/ideal_torque_ripple:
	f_best = f_best + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]
      else:
	#penalty = (f_torque_ripple_semfem_normalised - torque_ripple_constraint[1]/ideal_torque_ripple)**2
	penalty = (1+abs(f_torque_ripple_semfem_normalised[operating_point] - torque_ripple_constraint[1]/ideal_torque_ripple))**2
	f_best = f_best + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] + torque_ripple_constraint[0][operating_point]*penalty 
      #print("5f_best = %f"%(f_best))
      
      ###power factor (maximize)
      if f_power_factor_normalised[operating_point] > power_factor_constraint[1]/ideal_pf:
	f_best = f_best - weight_pf[operating_point]*f_power_factor_normalised[operating_point]
      else:
	#penalty = (f_power_factor_normalised-power_factor_constraint[1]/ideal_pf)**2
	penalty = (1+abs(f_power_factor_normalised[operating_point]-power_factor_constraint[1]/ideal_pf))**2
	f_best = f_best - weight_pf[operating_point]*f_power_factor_normalised[operating_point] + power_factor_constraint[0][operating_point]*penalty
      #print("6f_best = %f"%(f_best))
      
      ###torque density (maximize)
      if f_torque_density_normalised[operating_point] > torque_density_constraint[1]/ideal_torque_density:
	f_best = f_best - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]
      else:
	#penalty = (f_torque_density_normalised - torque_density_constraint/ideal_torque_density)**2
	penalty = (1+abs(f_torque_density_normalised[operating_point] - torque_density_constraint[1]/ideal_torque_density))**2
	f_best = f_best - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] + torque_density_constraint[0][operating_point]*penalty
      #print("7f_best = %f"%(f_best))
      
      ###magnet mass (minimize)
      if f_pm_mass_normalised[operating_point] < magnet_mass_constraint[1]/ideal_pm_mass:
	f_best = f_best + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
      else:
	#penalty = (f_pm_mass_normalised - magnet_mass_constraint[1]/ideal_pm_mass)**2
	penalty = (1+abs(f_pm_mass_normalised[operating_point] - magnet_mass_constraint[1]/ideal_pm_mass))**2
	f_best = f_best + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] + magnet_mass_constraint[0][operating_point]*penalty
      #print("8f_best = %f"%(f_best))
      
      ###current density (?)
      if f_current_density_rms_normalised[operating_point] < current_density_rms_constraint[1]/ideal_J_rms:
	f_best = f_best + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point]
      else:
	#penalty = (f_current_density_rms_normalised - current_density_rms_constraint[1]/ideal_J_rms)**2
	penalty = (1+abs(f_current_density_rms_normalised[operating_point] - current_density_rms_constraint[1]/ideal_J_rms))**2
	f_best = f_best + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point] + current_density_rms_constraint[0][operating_point]*penalty
      #print("9f_best = %f"%(f_best))
      
      ###mechanical output power (minimize-since it is usually larger than the desired number)
      if f_pmech_normalised[operating_point] > p_mech_constraint[1]/ideal_pmech_out:
	f_best = f_best + weight_pmech[operating_point]*f_pmech_normalised[operating_point]
      else:
	#penalty = (f_pmech_normalised - p_mech_constraint[1]/ideal_pmech_out)**2
	penalty = (1+abs(f_pmech_normalised[operating_point] - p_mech_constraint[1]/ideal_pmech_out))**2
	f_best = f_best + weight_pmech[operating_point]*f_pmech_normalised[operating_point] + p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant
	#print("penalty_best = %f"%(penalty))
      #print("f_best_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
      #print("p_best_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant = %f"%(p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant))
      #print("10f_best = %f"%(f_best))
      
      ###total losses (minimize)
      if f_total_losses[operating_point] < total_losses_constraint[1]/ideal_total_losses:
	f_best = f_best + weight_total_losses[operating_point]*f_total_losses[operating_point]
      else:
	#penalty = (f_total_losses - total_losses_constraint[1]/ideal_total_losses)**2
	penalty = (1+abs(f_total_losses[operating_point] - total_losses_constraint[1]/ideal_total_losses))**2
	f_best = f_best + weight_total_losses[operating_point]*f_total_losses[operating_point] + total_losses_constraint[0][operating_point]*penalty
      #print("11f_best = %f"%(f_best))

  #print "optimization.py -- going to run semfem script"
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db'"%(primary_key))
  
  
  
  ####################################
  ##ROUND LITZ
  ####################################
  ##obtain inductance L and PM flux linkage
  #table_extension_string = '_rpm_%03d'%(operating_speeds[0])
  #print "Simulating machine at %03d rpm"%(operating_speeds[0])
  ##close connection to the database
  #conn.close()
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  ##copy inductance L and PM flux linkage to other input tables
  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()  
  
  #synchronous_inductance_single_turn = db_input_select(input_table_name[0],"synchronous_inductance_single_turn",primary_key)
  #PM_flux_linkage_single_turn = db_input_select(input_table_name[0],"PM_flux_linkage_single_turn",primary_key)
  
  #for i in range(1,len(operating_speeds)):
    #c.execute("UPDATE %s SET synchronous_inductance_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(synchronous_inductance_single_turn,primary_key))
    #c.execute("UPDATE %s SET PM_flux_linkage_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(PM_flux_linkage_single_turn,primary_key))
  #conn.commit()
  
  ##simulate top speed in order to determine number of turns (and of course the operating point performance in general)
  #table_extension_string = '_rpm_%03d'%(operating_speeds[-1])
  #print "Simulating machine at %03d rpm"%(operating_speeds[-1])
  ##close connection to the database
  #conn.close()  
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()
  ##obtain turns per coil which was determined from top speed
  #turnsPerCoil = db_input_select(output_table_name[-1],"turnsPerCoil",primary_key)
  ##print("Received turnsPerCoil = %d"%(turnsPerCoil))
  #c.execute("UPDATE %s SET force_this_number_of_turns = ? WHERE primary_key = ?"%(input_table_name[1]),(turnsPerCoil,primary_key))
  #conn.commit()
  #conn.close()
  
  ##simulate base speed
  #table_extension_string = '_rpm_%03d'%(operating_speeds[1])
  #print "Simulating machine at %03d rpm"%(operating_speeds[1])
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))  

  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor() 

  #####################################
  ###SOLID BARS
  #####################################
  ##obtain inductance L and PM flux linkage at 100rpm (base speed)
  #table_extension_string = '_rpm_%03d'%(operating_speeds[0])
  #print "Simulating machine at %03d rpm"%(operating_speeds[0])
  ##close connection to the database
  #conn.close()
  #os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  ##copy inductance L and PM flux linkage to other input tables
  ##open connection to the database
  #conn = sqlite3.connect('optimise.db')
  #c = conn.cursor()  
  
  #synchronous_inductance_single_turn = db_input_select(input_table_name[0],"synchronous_inductance_single_turn",primary_key)
  #PM_flux_linkage_single_turn = db_input_select(input_table_name[0],"PM_flux_linkage_single_turn",primary_key)
  
  ##copy to 465rpm (even though we are not using 465rpm yet)
  #for i in range(1,len(operating_speeds)):
    #c.execute("UPDATE %s SET synchronous_inductance_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(synchronous_inductance_single_turn,primary_key))
    #c.execute("UPDATE %s SET PM_flux_linkage_single_turn = ? WHERE primary_key = ?"%(input_table_name[i]),(PM_flux_linkage_single_turn,primary_key))
  #conn.commit()
  
  ###simulate top speed in order to determine if sufficient pcu allowance for flux weakening
  ##table_extension_string = '_rpm_%03d'%(operating_speeds[-1])
  ##print "Simulating machine at %03d rpm"%(operating_speeds[-1])
  ###close connection to the database
  ##conn.close()  
  ##os.system("./SORSPM_semfem.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))  

  ###open connection to the database
  ##conn = sqlite3.connect('optimise.db')
  ##c = conn.cursor()
  #####################################

  ####################################
  ##IDRFPM
  ####################################
  #obtain inductance L and PM flux linkage at 100rpm (base speed)
  table_extension_string = '_rpm_%03d'%(operating_speeds[0])
  print "Simulating machine at %03d rpm"%(operating_speeds[0])
  #close connection to the database
  conn.close()
  os.system("./qhdrfapm.py -pk %d -db 'optimise.db' -tb %s"%(primary_key,table_extension_string))
  
  conn = sqlite3.connect('optimise.db')
  c = conn.cursor()    
  
  ####################################


  ##attach best.db to current db connection
  #c.execute("ATTACH DATABASE 'best.db' as best")
 
  if enable_multiple_operating_points:
    loops = len(operating_speeds)
  else:
    loops = 1
  
  f = 0
  #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
  for i in range(0,loops): #SOLID BARS
    if enable_multiple_operating_points:
      operating_point = i
    
    #read results of semfem simulation
    f_power_factor[operating_point] = abs(db_input_select(output_table_name[operating_point],"power_factor",primary_key))
    f_average_torque_dq[operating_point] = abs(db_input_select(output_table_name[operating_point],"average_torque_dq",primary_key))
    f_torque_ripple_semfem[operating_point] = abs(db_input_select(output_table_name[operating_point],"torque_ripple_semfem",primary_key))
    f_efficiency_indirect_dq[operating_point] = abs(db_input_select(output_table_name[operating_point],"efficiency_indirect_dq",primary_key))
    f_total_mass[operating_point] = abs(db_input_select(output_table_name[operating_point],"total_mass",primary_key))
    f_torque_density[operating_point] = abs(db_input_select(output_table_name[operating_point],"torque_density",primary_key))
    f_pm_mass[operating_point] = abs(db_input_select(output_table_name[operating_point],"magnet_mass",primary_key))
    f_pmech[operating_point] = abs(db_input_select(output_table_name[operating_point],"p_out_dq_torque",primary_key))
    f_current_density_rms[operating_point] = abs(db_input_select(input_table_name[operating_point],"current_density_rms",primary_key))
    f_total_losses[operating_point] = abs(db_input_select(output_table_name[operating_point],"total_losses",primary_key))

    #pretty_output_table.add_row(["iteration_number",iteration_number,"---"])
    ##pretty_output_table.add_row(["poles",poles,"[p.u.]"])
    #pretty_output_table.add_row(["f_power_factor",f_power_factor[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["f_average_torque_dq",f_average_torque_dq[operating_point],"[N.m]"])
    #pretty_output_table.add_row(["f_torque_ripple_semfem",f_torque_ripple_semfem[operating_point],"[%]"])
    #pretty_output_table.add_row(["f_efficiency_indirect_dq",f_efficiency_indirect_dq[operating_point],"[%]"])
    #pretty_output_table.add_row(["f_total_mass",f_total_mass[operating_point],"[kg]"])
    #pretty_output_table.add_row(["f_torque_density",f_torque_density[operating_point],"[N.m/kg]"])
    #pretty_output_table.add_row(["f_pm_mass",f_pm_mass[operating_point],"[kg]"])
    #pretty_output_table.add_row(["f_pmech",f_pmech[operating_point],"[W]"])
    #pretty_output_table.add_row(["f_current_density_rms",f_current_density_rms[operating_point],"[A/mm2]"])
    #pretty_output_table.add_row(["f_total_losses",f_total_losses[operating_point],"[W]"])
  
    #pretty_output_table.add_row(["-------------","------------","------------"])
    #pretty_output_table.add_row(["primary_key",primary_key,""])
    #pretty_output_table.add_row(["weight_eff",weight_eff[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_average",weight_torque_average[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_ripple",weight_torque_ripple[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pf",weight_pf[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_mass_total",weight_mass_total[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_torque_density",weight_torque_density[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pm_mass",weight_pm_mass[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_pmech",weight_pmech[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_J_rms",weight_J_rms[operating_point],"[p.u.]"])
    #pretty_output_table.add_row(["weight_total_losses",weight_total_losses[operating_point],"[p.u.]"])  

    #objective cost function
    f_power_factor_normalised[operating_point] = f_power_factor[operating_point]/ideal_pf
    f_average_torque_dq_normalised[operating_point] = f_average_torque_dq[operating_point]/ideal_torque_average
    f_torque_ripple_semfem_normalised[operating_point] = f_torque_ripple_semfem[operating_point]/ideal_torque_ripple
    f_efficiency_indirect_dq_normalised[operating_point] = f_efficiency_indirect_dq[operating_point]/ideal_eff
    f_total_mass_normalised[operating_point] = f_total_mass[operating_point]/ideal_mass_total
    f_torque_density_normalised[operating_point] = f_torque_density[operating_point]/ideal_torque_density
    f_pm_mass_normalised[operating_point] = f_pm_mass[operating_point]/ideal_pm_mass
    if enable_objective_constraints == 1:
      f_pmech_normalised[operating_point] = abs(f_pmech[operating_point]/ideal_pmech_out)
      f_current_density_rms_normalised[operating_point] = abs(f_current_density_rms[operating_point]/ideal_J_rms)
      f_total_losses_normalised[operating_point] = abs(f_total_losses[operating_point]/ideal_total_losses)  
  
    ##read weights
    #weight_eff = db_input_select(input_table_name[0],"weight_eff",primary_key)
    #weight_pf = db_input_select(input_table_name[0],"weight_pf",primary_key)
    #weight_mass_total = db_input_select(input_table_name[0],"weight_mass_total",primary_key)
    #weight_torque_average = db_input_select(input_table_name[0],"weight_torque_average",primary_key)
    #weight_torque_ripple = db_input_select(input_table_name[0],"weight_torque_ripple",primary_key)

    print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
    print("f_torque_ripple_semfem_normalised[operating_point] = %f"%(f_torque_ripple_semfem_normalised[operating_point]))
    print("f_torque_density_normalised[operating_point] = %f"%(f_torque_density_normalised[operating_point]))
    
    #print pretty_output_table
    #pretty_output_table.clear_rows()
  
    if debug_f == 1:
      print "weights"
      print("weight_eff[operating_point] = %.3f"%(weight_eff[operating_point]))
      print("weight_torque_average[operating_point] = %.3f"%(weight_torque_average[operating_point]))
      print("weight_torque_ripple[operating_point] = %.3f"%(weight_torque_ripple[operating_point]))
      print("weight_mass_total[operating_point] = %.3f"%(weight_mass_total[operating_point]))
      print("weight_pf[operating_point] = %.3f"%(weight_pf[operating_point]))
      print("weight_torque_density[operating_point] = %.3f"%(weight_torque_density[operating_point]))
      print("weight_pm_mass[operating_point] = %.3f"%(weight_pm_mass[operating_point]))
      if enable_objective_constraints == 1:
	print("weight_pmech[operating_point] = %.3f"%(weight_pmech[operating_point]))
	print("weight_J_rms[operating_point] = %.3f"%(weight_J_rms[operating_point]))
	print("weight_total_losses[operating_point] = %.3f"%(weight_total_losses[operating_point]))
      
      print("normalised")
      print("f_efficiency_indirect_dq_normalised[operating_point] = %f"%(f_efficiency_indirect_dq_normalised[operating_point]))
      print("f_average_torque_dq_normalised[operating_point] = %f"%(f_average_torque_dq_normalised[operating_point]))
      print("f_torque_ripple_semfem_normalised[operating_point] = %f"%(f_torque_ripple_semfem_normalised[operating_point]))
      print("f_total_mass_normalised[operating_point] = %f"%(f_total_mass_normalised[operating_point]))
      print("f_power_factor_normalised[operating_point] = %f"%(f_power_factor_normalised[operating_point]))
      print("f_torque_density_normalised[operating_point] = %f"%(f_torque_density_normalised[operating_point]))
      print("f_pm_mass_normalised[operating_point] = %f"%(f_pm_mass_normalised[operating_point]))
      if enable_objective_constraints == 1:
	print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
	print("f_current_density_rms_normalised[operating_point] = %f"%(f_current_density_rms_normalised[operating_point]))
	print("f_total_losses_normalised[operating_point] = %f"%(f_total_losses_normalised[operating_point]))
    
    if enable_objective_constraints == 0:
      f = -weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] -weight_pf[operating_point]*f_power_factor_normalised[operating_point] -weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] +weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]

    #print("operating_point = %d"%(operating_point))
    #print("1f = %f"%(f))
    ###efficiency (maximize)
    if f_efficiency_indirect_dq_normalised[operating_point] > efficiency_constraint[1]/ideal_eff:
      f = f - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]
    else:
      #penalty = (f_efficiency_indirect_dq_normalised - efficiency_constraint[1]/ideal_eff)**2
      penalty = (1+abs(f_efficiency_indirect_dq_normalised[operating_point] - efficiency_constraint[1]/ideal_eff))**2
      f = f - weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] + efficiency_constraint[0][operating_point]*penalty
    #print("2f = %f"%(f))
    ###torque (maximize)
    if f_average_torque_dq_normalised[operating_point] > torque_constraint[1]/ideal_torque_average:
      f = f - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]
    else:
      #penalty = (f_average_torque_dq_normalised - torque_constraint[1]/ideal_torque_average)**2
      penalty = (1+abs(f_average_torque_dq_normalised[operating_point] - torque_constraint[1]/ideal_torque_average))**2
      f = f - weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] + torque_constraint[0][operating_point]*penalty     
    #print("3f = %f"%(f))
    ###total mass (minimize)
    if f_total_mass_normalised[operating_point] < total_mass_constraint[1]/ideal_mass_total:  
      f = f + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]
    else:
      #penalty = (f_total_mass_normalised - total_mass_constraint[1]/ideal_mass_total)**2
      penalty = (1+abs(f_total_mass_normalised[operating_point] - total_mass_constraint[1]/ideal_mass_total))**2
      f = f + weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] + total_mass_constraint[0][operating_point]*penalty
    #print("4f = %f"%(f))
    ###torque ripple (minimize)
    if f_torque_ripple_semfem_normalised[operating_point] < torque_ripple_constraint[1]/ideal_torque_ripple:
      f = f + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]
    else:
      #penalty = (f_torque_ripple_semfem_normalised - torque_ripple_constraint[1]/ideal_torque_ripple)**2
      penalty = (1+abs(f_torque_ripple_semfem_normalised[operating_point] - torque_ripple_constraint[1]/ideal_torque_ripple))**2
      f = f + weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] + torque_ripple_constraint[0][operating_point]*penalty 
    #print("5f = %f"%(f))
    ###power factor (maximize)
    if f_power_factor_normalised[operating_point] > power_factor_constraint[1]/ideal_pf:
      f = f - weight_pf[operating_point]*f_power_factor_normalised[operating_point]
    else:
      #penalty = (f_power_factor_normalised-power_factor_constraint[1]/ideal_pf)**2
      penalty = (1+abs(f_power_factor_normalised[operating_point]-power_factor_constraint[1]/ideal_pf))**2
      f = f - weight_pf[operating_point]*f_power_factor_normalised[operating_point] + power_factor_constraint[0][operating_point]*penalty
    #print("6f = %f"%(f))
    ###torque density (maximize)
    if f_torque_density_normalised[operating_point] > torque_density_constraint[1]/ideal_torque_density:
      f = f - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]
    else:
      #penalty = (f_torque_density_normalised - torque_density_constraint/ideal_torque_density)**2
      penalty = (1+abs(f_torque_density_normalised[operating_point] - torque_density_constraint[1]/ideal_torque_density))**2
      f = f - weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] + torque_density_constraint[0][operating_point]*penalty
    #print("7f = %f"%(f))
    ###magnet mass (minimize)
    if f_pm_mass_normalised[operating_point] < magnet_mass_constraint[1]/ideal_pm_mass:
      f = f + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]
    else:
      #penalty = (f_pm_mass_normalised - magnet_mass_constraint[1]/ideal_pm_mass)**2
      penalty = (1+abs(f_pm_mass_normalised[operating_point] - magnet_mass_constraint[1]/ideal_pm_mass))**2
      f = f + weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] + magnet_mass_constraint[0][operating_point]*penalty
    #print("8f = %f"%(f))
    ###current density (?)
    if f_current_density_rms_normalised[operating_point] < current_density_rms_constraint[1]/ideal_J_rms:
      f = f + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point]
    else:
      #penalty = (f_current_density_rms_normalised - current_density_rms_constraint[1]/ideal_J_rms)**2
      penalty = (1+abs(f_current_density_rms_normalised[operating_point] - current_density_rms_constraint[1]/ideal_J_rms))**2
      f = f + weight_J_rms[operating_point]*f_current_density_rms_normalised[operating_point] + current_density_rms_constraint[0][operating_point]*penalty
    #print("9f = %f"%(f))
    ###mechanical output power (minimize-since it is usually larger than the desired number)
    if f_pmech_normalised[operating_point] > p_mech_constraint[1]/ideal_pmech_out:
      f = f + weight_pmech[operating_point]*f_pmech_normalised[operating_point]
    else:
      #penalty = (f_pmech_normalised - p_mech_constraint[1]/ideal_pmech_out)**2
      penalty = (1+abs(f_pmech_normalised[operating_point] - p_mech_constraint[1]/ideal_pmech_out))**2
      #print("p_mech_penalty = %g"%(penalty))
      #print("p_mech_penalty_term = %g"%(weight_pmech[operating_point]*p_mech_constraint[0][operating_point]*penalty))
      #print("p_mech_score = %g"%(weight_pmech[operating_point]*f_pmech_normalised))
      f = f + weight_pmech[operating_point]*f_pmech_normalised[operating_point] + p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant
      #print("penalty = %f"%(penalty))
    #print("f_pmech_normalised[operating_point] = %f"%(f_pmech_normalised[operating_point]))
    #print("p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant = %f"%(p_mech_constraint[0][operating_point]*penalty*p_mech_penalty_constant))
    #print("10f = %f"%(f))
    ###total losses (minimize)
    if f_total_losses[operating_point] < total_losses_constraint[1]/ideal_total_losses:
      f = f + weight_total_losses[operating_point]*f_total_losses[operating_point]
    else:
      #penalty = (f_total_losses - total_losses_constraint[1]/ideal_total_losses)**2
      penalty = (1+abs(f_total_losses[operating_point] - total_losses_constraint[1]/ideal_total_losses))**2
      f = f + weight_total_losses[operating_point]*f_total_losses[operating_point] + total_losses_constraint[0][operating_point]*penalty
    #print("11f = %f"%(f))
  #f_pmech_normalised_array.append(f_pmech_normalised[operating_point])
  #f_total_mass_normalised_array.append(f_total_mass_normalised[operating_point])
  #f_torque_ripple_semfem_normalised_array.append(f_torque_ripple_semfem_normalised[operating_point])
  #f_efficiency_indirect_dq_normalised_array.append(f_efficiency_indirect_dq_normalised[operating_point])
  #f_torque_density_normalised_array.append(f_torque_density_normalised[operating_point])
  
  #f_array.append(f)
  
  #f_string_names_list = []
  #f_string_names_list.append('f_pmech_normalised')
  #f_string_names_list.append('f_total_mass_normalised')
  #f_string_names_list.append('f_torque_ripple_semfem_normalised')
  #f_string_names_list.append('f_efficiency_indirect_dq_normalised')
  #f_string_names_list.append('f_torque_density_normalised')
  #f_string_names_list.append('f_pm_mass_normalised')
  #f_string_names_list.append('f_power_factor_normalised')
  #f_string_names_list.append('f_average_torque_dq_normalised')
  
  #f_objectives_list = []
  #f_objectives_list.append(f_pmech_normalised[operating_point])
  #f_objectives_list.append(f_total_mass_normalised[operating_point])
  #f_objectives_list.append(f_torque_ripple_semfem_normalised[operating_point])
  #f_objectives_list.append(f_efficiency_indirect_dq_normalised[operating_point])
  #f_objectives_list.append(f_torque_density_normalised[operating_point])
  #f_objectives_list.append(f_pm_mass_normalised[operating_point])
  #f_objectives_list.append(f_power_factor_normalised[operating_point])
  #f_objectives_list.append(f_average_torque_dq_normalised[operating_point])
  
  if (iteration_number == 0 and single_weight_optimization == 0) or (primary_key == 1 and single_weight_optimization == 1):
    ##add f_xxx columns to optimise.db table
    #table = 'machine_semfem_output'
    #for d in range(0,1):
      ##if d == 1:
	##table = 'best.machine_semfem_output'
      #for s in range(0,len(f_string_names_list)):
	#c.execute("ALTER TABLE %s ADD COLUMN %s REAL"%(table,f_string_names_list[s]))

    #create new database file, best.db
    shutil.copy('optimise.db','best.db')
    
    ##attach best.db to current db connection
    #c.execute("ATTACH DATABASE 'best.db' as best")    
    
    #c.execute("DELETE FROM best.machine_input_parameters")
    #c.execute("DELETE FROM best.machine_semfem_output")

  #attach best.db to current db connection
  c.execute("ATTACH DATABASE 'best.db' as best")    
  
  c.execute("DELETE FROM best.machine_input_parameters")
  c.execute("DELETE FROM best.machine_semfem_output")

  #for s in range(0,len(f_string_names_list)): 
    #c.execute("UPDATE machine_semfem_output SET %s = ? WHERE primary_key = ?"%(f_string_names_list[s]),(f_objectives_list[s],primary_key)) 
  
  conn.commit()
  
  #if debug_f == 1:
    #print "result"
    #print("-weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point] = %f"%(-weight_eff[operating_point]*f_efficiency_indirect_dq_normalised[operating_point]))
    #print("-weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point] = %f"%(-weight_torque_average[operating_point]*f_average_torque_dq_normalised[operating_point]))
    #print("weight_mass_total[operating_point]*f_total_mass_normalised[operating_point] = %f"%(weight_mass_total[operating_point]*f_total_mass_normalised[operating_point]))
    #print("weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point] = %f"%(weight_torque_ripple[operating_point]*f_torque_ripple_semfem_normalised[operating_point]))
    #print("-weight_pf[operating_point]*f_power_factor_normalised[operating_point] = %f"%(-weight_pf[operating_point]*f_power_factor_normalised[operating_point]))
    #print("-weight_torque_density[operating_point]*f_torque_density_normalised[operating_point] = %f"%(-weight_torque_density[operating_point]*f_torque_density_normalised[operating_point]))
    #print("weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point] = %f"%(weight_pm_mass[operating_point]*f_pm_mass_normalised[operating_point]))
    #print("f = %f"%(f))

  print("f = %f"%(f))
  f_best_changed = 0
  if (iteration_number == 0 and single_weight_optimization == 0) or (primary_key == 1 and single_weight_optimization == 1):
    f_best = f
    #f_power_factor_best = abs(db_input_select(output_table_name[operating_point],"power_factor",primary_key))
    #f_average_torque_dq_best = abs(db_input_select(output_table_name[operating_point],"average_torque_dq",primary_key))
    #f_torque_ripple_semfem_best = abs(db_input_select(output_table_name[operating_point],"torque_ripple_semfem",primary_key))
    #f_efficiency_indirect_dq_best = abs(db_input_select(output_table_name[operating_point],"efficiency_indirect_dq",primary_key))
    #f_total_mass_best = abs(db_input_select(output_table_name[operating_point],"total_mass",primary_key))
    #f_torque_density_best = abs(db_input_select(output_table_name[operating_point],"torque_density",primary_key))
    #f_pm_mass_best = abs(db_input_select(output_table_name[operating_point],"magnet_mass",primary_key))
    ##if enable_objective_constraints == 1:
    #f_pmech_best = abs(db_input_select(output_table_name[operating_point],"p_out_semfem_torque",primary_key))
    #f_current_density_rms_best = abs(db_input_select(output_table_name[operating_point],"current_density_rms",primary_key))
    #f_total_losses_best = abs(db_input_select(output_table_name[operating_point],"total_losses",primary_key))    
    
    iteration_number_best = iteration_number
    f_best_changed = 1

  elif f<f_best:
    #print("YESSS f<f_best")
    f_best = f
    #f_power_factor_best = f_power_factor[operating_point]
    #f_average_torque_dq_best = f_average_torque_dq[operating_point]
    #f_torque_ripple_semfem_best = f_torque_ripple_semfem[operating_point]
    #f_efficiency_indirect_dq_best = f_efficiency_indirect_dq[operating_point]
    #f_total_mass_best = f_total_mass[operating_point]
    #f_torque_density_best = f_torque_density[operating_point]
    #f_pm_mass_best = f_pm_mass[operating_point]
    ##if enable_objective_constraints == 1:
    #f_pmech_best = f_pmech[operating_point]
    #f_current_density_rms_best = f_current_density_rms[operating_point]
    #f_total_losses_best = f_total_losses[operating_point]
    
    iteration_number_best = iteration_number
    f_best_changed = 1

  if f_best_changed == 1:
    #print("WHOOP going to update best.db with value from primary_key = %d"%(primary_key))
    #copy latest entry from optimise.db to best.db since this is now the new best
    if enable_multiple_operating_points:
      loops = len(operating_speeds)
    else:
      loops = 1
      
    #for i in range(1,loops): #ROUND LITZ #first operating point should not be accounted for 
    for i in range(0,loops): #SOLID BARS
      if enable_multiple_operating_points:
	operating_point = j
      
      table_name = input_table_name[operating_point]
      for s in range(0,2):
	if s == 1: table_name = output_table_name[operating_point]
	pk2 = primary_key
	#pk1 = primary_key_best
	pk1 = primary_key
	
	c = conn.execute("select * from %s"%(table_name))
	column_names = list(map(lambda x: x[0], c.description))  
      
	for i in range(1,len(column_names)):
	  c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name,pk2))
	    
	  val = c.fetchone()[0]
	  if i == 1:
	    try:
	      c.execute("INSERT INTO best.%s (primary_key) VALUES (%d)"%(table_name,pk1))
	    except:
	      #print "Did not insert"
	      pass

	  try:
	    c.execute("UPDATE best.%s SET %s = ? WHERE primary_key = ?"%(table_name,column_names[i]),(val,pk1))      
	  except:
	    print("update did not work")
    
    
    conn.commit()
    #conn.close()
    #sys.exit()
    
    
    
    
    
    
    
    
    
    
    
    #print("YESSS f_best_changed == 1")
    ##print "optimization.py -- going to update best results"
    ##save the specific inputs that were used to achieve (best)
    ##c.execute("UPDATE best.machine_input_parameters SET stator_teeth_to_coil_height_ratio = ? WHERE primary_key = ?",(hs_ratio,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET radially_magnetized_PM_width_ratio = ? WHERE primary_key = ?",(kmr,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET tangentially_magnetized_PM_width_ratio = ? WHERE primary_key = ?",(kmt,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET coil_width_ratio = ? WHERE primary_key = ?",(kc,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_gap_teeth_height_ratio = ? WHERE primary_key = ?",(km_gap_teeth,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_slot_depth_ratio = ? WHERE primary_key = ?",(hmt_slot_depth_ratio,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET tangentially_magnetised_PM_height_ratio_of_slot = ? WHERE primary_key = ?",(hmt_magnet_height_ratio_of_slot,primary_key))
    
    ##c.execute("UPDATE best.machine_input_parameters SET poles = ? WHERE primary_key = ?",(poles,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?",(ryo,primary_key))
    ##c.execute("UPDATE best.machine_input_parameters SET stator_yoke_inner_radius_in_mm = ? WHERE primary_key = ?",(ri,primary_key))
    #c.execute("UPDATE best.machine_conn.close()input_parameters SET rotor_yoke_height_in_mm = ? WHERE primary_key = ?",(hyo,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stator_yoke_height_in_mm = ? WHERE primary_key = ?",(hyi,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET coil_height_in_mm = ? WHERE primary_key = ?",(hc,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET radially_magnetized_PM_height_in_mm = ? WHERE primary_key = ?",(hmr,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stator_rod_distance_from_tooth_tip = ? WHERE primary_key = ?",(hrod,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET stacklength_in_mm = ? WHERE primary_key = ?",(stacklength,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET rotor_yoke_outer_radius_in_mm = ? WHERE primary_key = ?",(ryo,primary_key))
    
    ##c.execute("UPDATE best.machine_input_parameters SET stacklength_in_mm = ? WHERE primary_key = ?",(l,primary_key))
    #c.execute("UPDATE best.machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))

    ##save the most important outputs that were obtained
    ##try:
    #c.execute("SELECT value_int FROM best.info WHERE variable_name = 'have_already_commited_best_solution'")
    #have_already_commited_best_solution = c.fetchone()[0]
    ##print "caught this value"
    ##print("have_already_commited_best_solution = %d"%(have_already_commited_best_solution))
    ##except:
      ##have_already_commited_best_solution = 1
    
    #if have_already_commited_best_solution == 1:
      #print("YESSS going to update   best.machine_semfem_output")
      ##if single_weight_optimization == 1:
	##c.execute("UPDATE best.machine_semfem_output SET primary_key = ?",(primary_key,))
      #if single_weight_optimization == 1:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key_best))
      #else:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
      ##have_already_commited_best_solution = 1
      ##c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = have_already_commited_best_solution",(1,))
      
    #elif have_already_commited_best_solution == 0:
      #print("YESSS first update   best.machine_semfem_output")
      #try:
	#c.execute("INSERT INTO best.machine_semfem_output (power_factor,average_torque_dq,efficiency_indirect_dq,torque_ripple_semfem,total_mass,torque_density,magnet_mass,p_out_dq_torque,total_losses,current_density_rms,f_value,primary_key) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
	##c.execute("INSERT INTO best.info (variable_name,value_int) VALUES (?,?)",('have_already_commited_best_solution',1))
	#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(1,))
      #except:
	#c.execute("UPDATE best.machine_semfem_output SET power_factor = ?, average_torque_dq = ?, efficiency_indirect_dq = ?, torque_ripple_semfem = ?, total_mass = ?, torque_density = ?, magnet_mass = ? , p_out_dq_torque = ?, total_losses = ?, current_density_rms = ?,f_value = ? WHERE primary_key = ?",(f_power_factor_best,f_average_torque_dq_best,f_efficiency_indirect_dq,f_torque_ripple_semfem_best,f_total_mass_best,f_torque_density_best,f_pm_mass_best,f_pmech_best,f_total_losses_best,f_current_density_rms_best,f_best,primary_key))
	#c.execute("UPDATE best.info SET value_int = ? WHERE variable_name = 'have_already_commited_best_solution'",(1,))
    #conn.commit()
    
    #print "optimization.py -- finished with update of best results"

  if display_poly_from_this_iteration == -1:
    pass
  elif iteration_number>=display_poly_from_this_iteration:
    os.system("fpl2 project_sorspm_semfem/drawing.poly")

  iteration_number = iteration_number + 1
  c.execute("UPDATE machine_input_parameters SET iteration_number = ? WHERE primary_key = ?",(iteration_number,primary_key))
  conn.commit()
  
  if single_weight_optimization == 1:
    primary_key = iteration_number
  
  #print "optimization.py -- end of weighted_objective_function"
  
  #print 'hos'
  if single_weight_optimization == 1:
    iterations = range(0,iteration_number-1)
  else:
    iterations = range(0,iteration_number)

 


    
  
  #ax1 = pl.subplot(211)
  #if iteration_number == 1:
    ##fig = pl.figure(1)
    ##ax = fig.add_subplot(2, 1, 1)  # specify (nrows, ncols, axnum)
    #pl.ion()
    #pl.xlabel(r"iteration_number")
    #pl.ylabel(r"f_objectives")
    ##pl.xlim(xmin=0)
    ##pl.title("Pareto Approximation of Efficiency against PM Mass")
    ##pl.scatter(pm_mass,eff, alpha = 0.5)
  
  ##if iteration_number>2:
    ##line3.remove()
    ##scat3.remove()
  
  
  ##total_mass_max_normalised = [x / max(f_total_mass_normalised_array) for x in f_total_mass_normalised_array]
  #total_mass_max_normalised = [x / f_total_mass_normalised_array[0] for x in f_total_mass_normalised_array]
  
  #line1 = pl.plot(iterations,f_array,label='f',c=colors[0])
  #line2 = pl.plot(iterations,f_pmech_normalised_array,label='f_pmech',c=colors[1])
  #line3 = pl.plot(iterations,total_mass_max_normalised,label='f_total_mass',c=colors[2])
  #line4 = pl.plot(iterations,f_torque_ripple_semfem_normalised_array,label='f_torque_ripple',c=colors[3])
  #line5 = pl.plot(iterations,f_efficiency_indirect_dq_normalised_array,label='f_efficiency',c=colors[4])
  #line6 = pl.plot(iterations,f_torque_density_normalised_array,label='f_torque_density',c=colors[5])
  
  #scat1 = pl.scatter(iterations,f_array, alpha = 0.5,c=colors[0])
  #scat2 = pl.scatter(iterations,f_pmech_normalised_array, alpha = 0.5,c=colors[1])
  #scat3 = pl.scatter(iterations,total_mass_max_normalised, alpha = 0.5,c=colors[2])
  #scat4 = pl.scatter(iterations,f_torque_ripple_semfem_normalised_array, alpha = 0.5,c=colors[3])
  #scat5 = pl.scatter(iterations,f_efficiency_indirect_dq_normalised_array, alpha = 0.5,c=colors[4])
  #scat6 = pl.scatter(iterations,f_torque_density_normalised_array, alpha = 0.5,c=colors[5])
  
  #if iteration_number == 1:
    #pl.grid(True)
    #leg = pl.legend(loc='upper right')
    #for legobj in leg.legendHandles:
      #legobj.set_linewidth(2.0)
  ##pl.pause(0.05)  
  
  #ax2 = pl.subplot(212, sharex=ax1)
  #if iteration_number == 1:
    ##fig = pl.figure(2)
    #pl.ion()
    ##pl.xlabel(r"iteration_number")
    #pl.ylabel(r"input variables [p.u.]")
    ##pl.xlim(xmin=0)
  
  #line_input_variables.append(pl.plot(iterations,hyo_pu_array,label='hyo',c=colors[0]))
  #line_input_variables.append(pl.plot(iterations,hyi_pu_array,label='hyi',c=colors[1]))
  #line_input_variables.append(pl.plot(iterations,hc_pu_array,label='hc',c=colors[2]))
  #line_input_variables.append(pl.plot(iterations,hmr_pu_array,label='hmr',c=colors[3]))
  #line_input_variables.append(pl.plot(iterations,kc_pu_array,label='kc',c=colors[4]))
  #line_input_variables.append(pl.plot(iterations,kmr_pu_array,label='kmr',c=colors[5]))
  #line_input_variables.append(pl.plot(iterations,hrod_pu_array,label='hrod',c=colors[6]))
  #line_input_variables.append(pl.plot(iterations,stacklength_pu_array,label='l',c=colors[7]))
  #line_input_variables.append(pl.plot(iterations,ryo_pu_array,label='ryo',c=colors[9]))

  #scat_input_variables.append(pl.scatter(iterations,hyo_pu_array,alpha = 0.5,c=colors[0]))
  #scat_input_variables.append(pl.scatter(iterations,hyi_pu_array,alpha = 0.5,c=colors[1]))
  #scat_input_variables.append(pl.scatter(iterations,hc_pu_array,alpha = 0.5,c=colors[2]))
  #scat_input_variables.append(pl.scatter(iterations,hmr_pu_array,alpha = 0.5,c=colors[3]))
  #scat_input_variables.append(pl.scatter(iterations,kc_pu_array,alpha = 0.5,c=colors[4]))
  #scat_input_variables.append(pl.scatter(iterations,kmr_pu_array,alpha = 0.5,c=colors[5]))
  #scat_input_variables.append(pl.scatter(iterations,hrod_pu_array,alpha = 0.5,c=colors[6]))
  #scat_input_variables.append(pl.scatter(iterations,stacklength_pu_array,alpha = 0.5,c=colors[7]))
  #scat_input_variables.append(pl.scatter(iterations,ryo_pu_array,alpha = 0.5,c=colors[9]))

  ##pl.xlim([0,70])
  ##pl.xlim(xmin=0)

  #if iteration_number == 1:
    #pl.grid(True)
    #leg = pl.legend(loc='upper right')
    #for legobj in leg.legendHandles:
      #legobj.set_linewidth(2.0)
  #pl.pause(0.05)
  
  
  
  
  
  
  
  
  
  
  #fail = 0

  #return f,0,fail
  if use_pyOpt == 1:
    return f,0,0
  if use_nlopt == 1:
    print("end of function")
    #print("primary_key_best = %d"%(primary_key_best))
    #print("f_best = %f"%(f_best))
    #print("primary_key = %d"%(primary_key))
    #print("f = %f"%(f))
    #print("f_best_changed = %d"%(f_best_changed))
    
    #print "return the following value"
    #print(f)
    return f
  #return f,g,fail



def fc(x,grad):
  f_torque_ripple_semfem[operating_point] = abs(db_input_select("machine_semfem_output","torque_ripple_semfem",primary_key))
  print("fc constraint")
  print ("f_torque_ripple_semfem = %g"%(f_torque_ripple_semfem[operating_point]))
  print("return value = %g"%(f_torque_ripple_semfem[operating_point] - torque_ripple_constraint[1]))
  return f_torque_ripple_semfem[operating_point] - torque_ripple_constraint[1]

#main script section
if use_pyOpt == 1:
  opt_prob = pyOpt.Optimization('weighted objective function',weighted_objective_function)
  opt_prob.addObj('f')

  #volgorde maak saak waarin mens die variables hier declare!!
  #opt_prob.addVar('x_hyo','c',lower=0,upper=1,value=x_hyo_init) #outer yoke height
  #opt_prob.addVar('x_hyi','c',lower=0,upper=1,value=x_hyi_init) #inner yoke height
  #opt_prob.addVar('x_hc','c',lower=0,upper=1,value=x_hc_init) #teeth height
  #opt_prob.addVar('x_hmr','c',lower=0,upper=1,value=x_hmr_init) #magnet height
  #opt_prob.addVar('kc','c',lower=kc_min,upper=kc_max,value=kc) #teeth width
  #opt_prob.addVar('kmr','c',lower=kmr_min,upper=kmr_max,value=kmr) #magnet width

  opt_prob.addVar('hyo','c',lower=hyo_min,upper=hyo_max,value=hyo) #outer yoke height
  opt_prob.addVar('hyi','c',lower=hyi_min,upper=hyi_max,value=hyi) #inner yoke height
  opt_prob.addVar('hc','c',lower=hc_min,upper=hc_max,value=hc) #teeth height
  opt_prob.addVar('hmr','c',lower=hmr_min,upper=hmr_max,value=hmr) #magnet height
  opt_prob.addVar('kc','c',lower=kc_min,upper=kc_max,value=kc) #teeth width
  opt_prob.addVar('kmr','c',lower=kmr_min,upper=kmr_max,value=kmr) #magnet width
  opt_prob.addVar('hrod','c',lower=hrod_min,upper=hrod_max,value=hrod) #rod distance from stator tooth tip


  #opt_prob.addVar('pole_pairs','c',lower=int(pole_pairs_min),upper=int(pole_pairs_max),value=pole_pairs)
  #opt_prob.addVar('x_ryo','c',lower=0,upper=1,value=x_ryo_init)
  #opt_prob.addVar('x_ri','c',lower=0,upper=1,value=x_ri_init) 
  #opt_prob.addVar('hs_ratio','c',lower=0,upper=1,value=hs_ratio)
  #opt_prob.addVar('kmt','c',lower=kmt_min,upper=kmt_max,value=kmt)
  #opt_prob.addVar('x_l','c',lower=0,upper=1,value=x_l_init)
  #opt_prob.addVar('km_gap_teeth','c',lower=0,upper=1,value=km_gap_teeth)
  #opt_prob.addVar('hmt_slot_depth_ratio','c',lower=hmt_slot_depth_ratio_min,upper=hmt_slot_depth_ratio_max,value=hmt_slot_depth_ratio)
  #opt_prob.addVar('hmt_magnet_height_ratio_of_slot','c',lower=0,upper=1,value=hmt_magnet_height_ratio_of_slot)



  ##instantiate optimizer
  #conmin = pyOpt.CONMIN()
  ##conmin.setOption('ITMAX',10)
  #conmin.setOption('DELFUN',epsilon1)
  #conmin.setOption('DABFUN',epsilon2)
  #conmin.setOption('IPRINT',2)

  ##use finite differences because the gradient is unknown
  #[fstr, xstr, inform] = conmin(opt_prob,sens_type='FD',store_hst=True,sens_step=1e-3)#sens_step=1e-2

  sdpen = pyOpt.SDPEN()
  sdpen.setOption('ifile','sdpen.out')
  sdpen.setOption('alfa_stop',epsilon1) #default is 1e-6

  #[fstr, xstr, inform] = sdpen(opt_prob,store_hst=True)
  sdpen(opt_prob,store_hst=False)

#def myfunc(x, grad):
    #print "x[0] = %g"%(x[0])
    #print "x[1] = %g"%(x[1])
    #print "x[2] = %g"%(x[2])
    #print "x[3] = %g"%(x[3])
    #print "x[4] = %g"%(x[4])
    #print "x[5] = %g"%(x[5])
  
    #if grad.size > 0:
        #grad[0] = 0.0
        #grad[1] = 0.5 / np.sqrt(x[1])
    #return np.sqrt(x[1])


if use_nlopt == 1:
  
  #lb = np.array([hyo_min,hyi_min,hc_min,hmr_min,kc_min,kmr_min,hrod_min,stacklength_min,ryo_min])
  #ub = np.array([hyo_max,hyi_max,hc_max,hmr_max,kc_max,kmr_max,hrod_max,stacklength_max,ryo_max])
  #x0 = np.array([hyo,hyi,hc,hmr,kc,kmr,hrod,stacklength,ryo])

  #lb = np.array([0,0,0,0,0,0,0,0,0])
  #ub = np.array([1,1,1,1,1,1,1,1,1])
  #x0 = np.array([hyo_pu,hyi_pu,hc_pu,hmr_pu,kc_pu,kmr_pu,hrod_pu,stacklength_pu,ryo_pu])

  lb = np.array([0,0,0,0,0,0])
  ub = np.array([1,1,1,1,1,1])
  x0 = np.array([hc_pu,hmi_r_pu,hmo_r_pu,kc_pu,km_pu,stacklength_pu])
  

  
  #nlopt_algorithm = nlopt.LN_SBPLX
  #nlopt_algorithm = nlopt.GN_DIRECT_L #Global optimisation method - works rather well...
  #nlopt_algorithm = nlopt.GN_DIRECT #Global optimisation method - works rather well... This is my PREFERRED global optimisation method
  #nlopt_algorithm = nlopt.LN_BOBYQA #also works OK (tried on 2017/11/29) with mesh 3.0 and steps 31, did not produce better results
  #nlopt_algorithm = nlopt.LD_SLSQP #converges after only 1 step
  #nlopt_algorithm = nlopt.LD_MMA #converges after only 1 step
  nlopt_algorithm = nlopt.LN_SBPLX #THIS is my preferred LOCAL optimisation "polishing" tool
  #nlopt_algorithm = nlopt.LN_NEWUOA_BOUND
  #nlopt_algorithm = nlopt.LN_COBYLA #works OKish..does not sufficiently change stacklength and outer diameter to fit pmech constraints (which would be an obvious move).  
  #opt = nlopt.opt(nlopt_algorithm, 9)
  opt = nlopt.opt(nlopt_algorithm, 6)
  
  opt.set_lower_bounds(lb)
  opt.set_upper_bounds(ub)  
  
  opt.set_min_objective(weighted_objective_function)
  #opt.set_min_objective(myfunc)
  
  #opt.add_inequality_constraint(fc, tol=1e-3)
  #opt.add_inequality_constraint(fc)
  #opt.add_inequality_constraint(weighted_objective_function)
  
  #opt.set_maxeval(300)
  opt.set_maxeval(2000)
  #opt.set_maxeval(100000) #this number does not "passively" tell the optimiser about the magnitude of iterations you would like to go 
  #opt.set_ftol_rel(1e-4)
  #opt.set_ftol_abs(1e-4)
  #opt.set_xtol_rel(1e-4)

  #opt.set_ftol_rel(1e-5)
  #opt.set_ftol_abs(1e-5)
  #opt.set_xtol_rel(1e-5)

  #opt.set_ftol_rel(1e-6)
  #opt.set_ftol_abs(1e-6)
  #opt.set_xtol_rel(1e-6) 

  opt.set_ftol_rel(1e-7)
  opt.set_ftol_abs(1e-7)
  opt.set_xtol_rel(1e-7) 
 
 
  #opt.set_maxtime(350) #used this exact value for many optimisations (number in seconds)
  opt.set_maxtime(100000)
  
  print(lb)
  print(ub)
  print(x0)
  
  xopt = opt.optimize(x0)
  #xopt = opt.optimize([3.71,3.0,32.69,3.0,0.7635,0.8])

  opt_val = opt.last_optimum_value()
  
  print(opt_val)
  
  result = opt.last_optimize_result()
  
  print(result)

#print "optimization.py -- almost end of script"
#c.execute("UPDATE machine_input_parameters SET completed_optimisation = ? WHERE primary_key = ?",(1,primary_key))

conn.commit()
#print "optimization.py -- finished final commit"

end_time = time.time()

#time.sleep(3)

if use_pyOpt == 1:
  print opt_prob.solution(0)

#iteration_number = db_input_select(input_table_name[0],"iteration_number",primary_key)
#pl.figure(1)
#pl.xlabel(r"iteration_number")
#pl.ylabel(r"f_objectives")
##pl.title("Pareto Approximation of Efficiency against PM Mass")
##pl.scatter(pm_mass,eff, alpha = 0.5)
#iterations = range(0,iteration_number)
#pl.plot(iterations,f_array,label='f')
#pl.plot(iterations,f_pmech_normalised_array,label='f_pmech_normalised')
##pl.plot(pm_mass,eff,'bo')
#pl.grid(True)
#pl.legend(loc='lower left')
#pl.show()

raw_seconds = end_time-start_time
hours = int(raw_seconds/(60*60))
minutes = int(raw_seconds/60) - hours*60
seconds = int(raw_seconds) - hours*60*60 - minutes*60
time_msg = '%d [hours], %d [minutes], %d [seconds]'%(hours,minutes,seconds)
print('time_elapsed = '+time_msg)
now = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

#print("time_elapsed = %d [hours], %d [minutes], %d [seconds]"%(hours,minutes,seconds))
conn.close()
#print "optimization.py -- finished close"

#fromaddr = 'andreasjoss22@gmail.com'
#toaddrs  = 'andreasjoss22@gmail.com'

#optimisation_msg = "Optimisation completed at %s, after %d iterations were done.\n\nThe entire optimisation took %s"%(now,iteration_number,time_msg)

#msg = "\r\n".join([
  #"From: user_me@gmail.com",
  #"To: user_you@gmail.com",
  #"Subject: Optimisation Completed",
  #"",
  #optimisation_msg
  #])


#username = 'andreasjoss22@gmail.com'
#password = ''
#server = smtplib.SMTP('smtp.gmail.com:587')
#server.ehlo()
#server.starttls()
#server.login(username,password)
#server.sendmail(fromaddr, toaddrs, msg)
#server.quit() 