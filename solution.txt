
Optimization Problem -- weighted objective function
================================================================================

        Objective Function: weighted_objective_function

    Objectives:
        Name        Value        Optimum
	     f               0             0

	Variables (c - continuous, i - integer, d - discrete):
        Name    Type       Value       Lower Bound  Upper Bound
	    x_ri      c	      0.808219       0.00e+00     1.00e+00 
	   x_hmo      c	      0.250000       0.00e+00     1.00e+00 
	    x_hc      c	      0.666670       0.00e+00     1.00e+00 
	   x_hmi      c	      1.000000       0.00e+00     1.00e+00 
	     km       c	      0.500000       5.00e-02     9.50e-01 
	     kc       c	      0.717570       5.00e-02     9.50e-01 
	     q        c	      7.000000       3.00e+00     1.40e+01 


SDPEN Solution to weighted objective function
================================================================================

        Objective Function: weighted_objective_function

    Solution: 
--------------------------------------------------------------------------------
    Total Time:                  148.3348
    Total Function Evaluations:       307

    Objectives:
        Name        Value        Optimum
	     f        0.111091             0

	Variables (c - continuous, i - integer, d - discrete):
        Name    Type       Value       Lower Bound  Upper Bound
	    x_ri      c	      1.000000       0.00e+00     1.00e+00 
	   x_hmo      c	      0.250000       0.00e+00     1.00e+00 
	    x_hc      c	      0.666670       0.00e+00     1.00e+00 
	   x_hmi      c	      1.000000       0.00e+00     1.00e+00 
	     km       c	      0.495973       5.00e-02     9.50e-01 
	     kc       c	      0.950000       5.00e-02     9.50e-01 
	     q        c	     14.000000       3.00e+00     1.40e+01 

--------------------------------------------------------------------------------
