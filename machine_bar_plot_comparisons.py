#!/usr/bin/python
from __future__ import division

import sqlite3
import os, sys
import numpy as np
import pylab as pl
import matplotlib.transforms as mtransforms 
import pandas as pd
import itertools

##########################################
###SETTINGS###############################
##########################################
main_dir = 'optimisation/2018-07-13/'
#subfolder = ''
#subfolder = '100/global_'
#subfolder = '100/local_'
subfolder = '465/local_'
#subfolder = '465'
db_file = 'best.db'


#operating point to compare
#this functionality is being phased out, now use 'subfolder' to compare other operating points
#operating_point = 1 #use this setting for 2017-11-29
operating_point = 0 #use this setting for 2017-12-05

db_files = []
db_files.append([main_dir+subfolder+'12/'+db_file,'12 pole','Round Litz'])
db_files.append([main_dir+subfolder+'16/'+db_file,'16 pole','Round Litz'])
db_files.append([main_dir+subfolder+'20/'+db_file,'20 pole','Round Litz'])
db_files.append([main_dir+subfolder+'24/'+db_file,'24 pole','Round Litz'])
db_files.append([main_dir+subfolder+'28/'+db_file,'28 pole','Round Litz'])
db_files.append([main_dir+subfolder+'32/'+db_file,'32 pole','Round Litz'])
db_files.append([main_dir+subfolder+'36/'+db_file,'36 pole','Round Litz'])
db_files.append([main_dir+subfolder+'40/'+db_file,'40 pole','Round Litz'])

#db_files.append(['optimisation/2017-12-05/local_32/'+subfolder+'/best.db','32 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_36/'+subfolder+'/best.db','36 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_40/'+subfolder+'/best.db','40 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_44/'+subfolder+'/best.db','44 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_48/'+subfolder+'/best.db','48 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_52/'+subfolder+'/best.db','52 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_56/'+subfolder+'/best.db','56 pole','Solid Bars'])
#db_files.append(['optimisation/2017-12-05/local_60/'+subfolder+'/best.db','60 pole','Solid Bars'])



#outputs to show (comment out the outputs which must not be shown)
output_names = []
#name in database, graph label, pu scale factor

output_names.append(["torque_density", "Torque Density",7.0,r'Nm$/$kg'])
output_names.append(["current_density_rms", "Current Density",4,r'A rms$/$mm$^2$'])
output_names.append(["torque_ripple_semfem", "Torque Ripple",10.0,r'\%']) #for 100rpm
#output_names.append(["torque_ripple_semfem", "Torque Ripple",50.0,r'\%']) #for 465rpm
output_names.append(["efficiency_indirect_dq", "Efficiency",89,r'\%'])
output_names.append(["power_factor", "Power Factor",1.0,'p.u.'])
#output_names.append(["p_out_dq_torque", "Power Output",2000,'W'])

output_names.append(["total_mass", "Total Mass",100,r'kg'])
output_names.append(["copper_mass", "Copper Mass",10,r'kg'])
output_names.append(["aluminium_mass", "Aluminium Mass",20,r'kg'])
output_names.append(["tufnol_mass", "Tufnol Mass",10,r'kg'])
output_names.append(["magnet_mass", "Magnet Mass",100,r'kg'])

#for 100rpm operating point
output_names.append(["total_losses", "Total Losses",200,r'W'])
output_names.append(["p_conductive_analytical", "Copper Losses",200,r'W'])
output_names.append(["p_iron_losses", "Iron Losses",10,r'W'])
#output_names.append(["p_rotor_core_losses", "Rotor Iron Losses",10,r'W']) #show as zero for IDRFPM machine
output_names.append(["p_eddy", "Eddy Losses",10,r'W']) #show as zero for IDRFPM machine
output_names.append(["p_magnet_losses", "Magnet Losses",10,r'W']) #show as zero for IDRFPM machine

#for 465rpm operating point
#output_names.append(["total_losses", "Total Losses",500,r'W'])
#output_names.append(["p_conductive_analytical", "Copper Losses",200,r'W'])
#output_names.append(["p_stator_core_losses", "Stator Iron Losses",300,r'W'])
#output_names.append(["p_rotor_core_losses", "Rotor Iron Losses",10,r'W']) #neglect rotor losses for for 2017-12-05
###output_names.append(["p_eddy", "Eddy Losses",200,r'W']) #neglect eddy losses for for 2017-11-29
#output_names.append(["p_magnet_losses", "Magnet Losses",100,r'W'])

#input parameters to show
input_names = []

#input_names.append(["rotor_yoke_height_in_mm",'Rotor Yoke Height',5,r'mm',r'$h_{yo}$'])
input_names.append(["active_region_height_in_mm",'Active Region Height',50,r'mm',r'$h_{mo}+h_{c}+h_{mi}+2g$'])
input_names.append(["outer_magnet_height_in_mm",'Outer Magnet Height',30,r'mm',r'$h_{mo}$'])
input_names.append(["inner_magnet_height_in_mm",'Inner Magnet Height',30,r'mm',r'$h_{mi}$'])
input_names.append(["radially_magnetized_PM_width_ratio",'Magnet Pitch Ratio',1,r'p.u.',r'$k_{m}$'])

input_names.append(["stacklength_in_mm",'Stack Length',300,r'mm',r'$l$'])
input_names.append(["coil_height_in_mm",'Coil Height',30,r'mm',r'$h_{c}$'])
input_names.append(["force_this_number_of_turns",'Turns per Coil',10,r'',r'$N$']) #use this setting for 2017-11-29
#input_names.append(["effective_fill_factor",'Fill Factor',1,r'p.u.',r'$p.u.$']) #use this setting for 2017-12-05
input_names.append(["coil_width_ratio",'Coil Pitch Ratio',1,r'p.u.',r'$k_{c}$'])

#input_names.append(["stator_rod_distance_from_tooth_tip",'Stator Hole Height',10,r'mm',r'$h_{h}$'])
#input_names.append(["stator_shoe_width_in_mm",'Shoe Tip Width',5,r'mm',r'$w_{s}$'])
#input_names.append(["stator_shoe_height_in_mm",'Shoe Tip Height',5,r'mm',r'$h_{s}$'])
#input_names.append(["stator_shoe_wedge_degrees",'Shoe Taper Angle',90,r'degrees',r'$\theta_{s}$'])

#save graphs in this folder
save_dir = 'comparison_graphs/'
##########################################
###READ DATABASES#########################
##########################################
def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

#obtain table names
###
#conn = sqlite3.connect('optimise.db')
conn = sqlite3.connect(db_files[0][0])
c = conn.cursor()

#obtain all the working points
operating_speeds = []
res = conn.execute("SELECT name FROM sqlite_master WHERE type='table';")
for name in res:
  if name[0].startswith("machine_input_parameters_"):
    #print name[0]
    operating_speeds.append(int(name[0][-3:]))
    
input_table_name = []
output_table_name = []

for i in range(0,len(operating_speeds)):
  input_table_name.append('machine_input_parameters' + '_rpm_%03d'%(operating_speeds[i]))
  output_table_name.append('machine_semfem_output' + '_rpm_%03d'%(operating_speeds[i]))

#determine best primary keys
###
for i in range(0,len(db_files)):
  
  #setup databse connection
  conn = sqlite3.connect(db_files[i][0])
  c = conn.cursor()
  
  c.execute("SELECT MAX(primary_key) FROM "+output_table_name[operating_point])
  best_pk = c.fetchone()[0]
  db_files[i].append(best_pk)

conn.close()



df_output_column_names = []
for i in range(0,len(output_names)):
  df_output_column_names.append(output_names[i][0])
df_output = pd.DataFrame(columns=df_output_column_names)

graph_output_column_names = []
for i in range(0,len(output_names)):
  #graph_output_column_names.append(output_names[i][1])
  graph_output_column_names.append(output_names[i][1]+'\n[%.0f '%(output_names[i][2])+output_names[i][3] + ']')

df_input_column_names = []
for i in range(0,len(input_names)):
  df_input_column_names.append(input_names[i][0])
df_input = pd.DataFrame(columns=df_input_column_names)

graph_input_column_names = []
for i in range(0,len(input_names)):
  if i < len(input_names)/2:
    if input_names[i][4] == '':
      graph_input_column_names.append(input_names[i][1]+'\n[%.0f '%(input_names[i][2])+input_names[i][3] + ']')
    else:
      graph_input_column_names.append(input_names[i][1]+'\n('+input_names[i][4]+')'+'\n[%.0f '%(input_names[i][2])+input_names[i][3] + ']')
  else:
    graph_input_column_names.append(input_names[i][1]+' ('+input_names[i][4]+')'+'\n[%.0f '%(input_names[i][2])+input_names[i][3] + ']')

graph_category_names = []
for i in range(0,len(db_files)):
  graph_category_names.append(db_files[i][1])


#obtain all data
outputs = []
inputs = []
outputs.append([])
inputs.append([])  
for i in range(0,len(db_files)):
  
  conn = sqlite3.connect(db_files[i][0])
  c = conn.cursor()  
  
  for j in range(0,len(output_names)):
    outputs[i].append(db_input_select(output_table_name[operating_point],output_names[j][0],db_files[i][3])/output_names[j][2]) #obtain output value and immediately scale it to p.u.
  
  for j in range(0,len(input_names)):
    #exception, get this value from output table instead of input table
    if input_names[j][0] == "effective_fill_factor":
      inputs[i].append(db_input_select(output_table_name[operating_point],input_names[j][0],db_files[i][3])/input_names[j][2]) #obtain input value and immediately scale it to p.u.  
    elif input_names[j][0] == "coil_width_ratio":
      inputs[i].append((1-db_input_select(input_table_name[operating_point],input_names[j][0],db_files[i][3]))/input_names[j][2]) #obtain input value and immediately scale it to p.u.      
    else:
      inputs[i].append(db_input_select(input_table_name[operating_point],input_names[j][0],db_files[i][3])/input_names[j][2]) #obtain input value and immediately scale it to p.u.  
  
  df_output.loc[i] = outputs[i]
  df_input.loc[i] = inputs[i]
  
  conn.close()
  
  if i+1<len(db_files):
    outputs.append([])
    inputs.append([])

#graph_output_count_split = []
#if int(len(outputs[0]))%2 == 0:
  #graph_output_count_split.append(int(len(outputs[0])/2))
#else:
  #graph_output_count_split.append(int((len(outputs[0])/2)-1))

#graph_output_count_split.append(int(len(outputs[0])/2))

graph_output_count_split = [5,10,15]
graph_input_count_split = [4,8,12]
#obtain all parameter data



##########################################
###GRAPHS################################# 
##########################################
pl.rc('text', usetex=True)
pl.rc('font', family='serif')
#LINEWIDTH=2
#font_size=14
#axis_label_font_size=22
#tick_label_font_size=22
legend_font_size=10
legend_title_fontsize=12
marker_size = 20
colors = ['brown','b', 'm', 'c', 'y', 'r','khaki','g','y','k']

# Setting the positions and width for the bars
#pos = list(range(len(df_output_column_names)))
width_output = 0.05
width_input = 0.05
begin_offset_output = width_output
begin_offset_input = width_input
group_output_distance = 0.65 #originally 1.0, but then bar groups are a bit far from each other
group_input_distance = 1.1
bar_edge_color = 'none' #'black'
bar_line_width = 1.0
y_axis_value_label_offset = 0.12
y_axis_value_label_offset_input = 0.08
x_axis_value_label_offset = 0.005
value_label_fontsize = 11
value_label_colour = 'blue'

#subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=None)
#The parameter meanings (and suggested defaults) are:

#left  = 0.125  # the left side of the subplots of the figure
#right = 0.9    # the right side of the subplots of the figure
#bottom = 0.1   # the bottom of the subplots of the figure
#top = 0.9      # the top of the subplots of the figure
#wspace = 0.2   # the amount of width reserved for blank space between subplots
#hspace = 0.2   # the amount of height reserved for white space between subplots

def flip(items, ncol):
    return itertools.chain(*[items[i::ncol] for i in range(ncol)])


###PLOT OUTPUTS########################### 
##########################################

# Plotting the bars
#fig, ax = pl.subplots(figsize=(10,5))
#fig1, (ax1,ax2) = pl.subplots(2,1)
fig1, (ax1,ax2,ax3) = pl.subplots(3,1)
old_j = 0
position = np.zeros([len(db_files),len(df_output_column_names)])
for i in range(0,len(db_files)):
  if i == 0:
    for j in range(0,len(df_output_column_names)):
      position[i][j] = group_output_distance*width_output*(len(df_output_column_names)+1)*j+begin_offset_output
  else:
    for j in range(0,len(df_output_column_names)):
      position[i][j] = position[i-1][j] + width_output

#position = np.array(pos)
for i in range(0,len(db_files)): #plot each machine (pole combination)
  ax1.bar(position[i][:graph_output_count_split[0]], 
	  df_output.iloc[[i][0]][:graph_output_count_split[0]],
	  width_output, 
	  alpha=0.5,
	  color=colors[i],
	  label=graph_category_names[i],
	  edgecolor=bar_edge_color,
	  linewidth=bar_line_width)

  #add values of each bar
  for j, v in enumerate(df_output.iloc[[i][0]][:graph_output_count_split[0]]):
    v = v*output_names[j][2]
    if v>1000:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+3*0.04     
    elif v>100:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+2*0.04    
    elif v>10:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+0.04
    else:
      y_axis_value_label_offset_temp = y_axis_value_label_offset
    ax1.text(position[i][:graph_output_count_split[0]][j]+x_axis_value_label_offset, df_output.iloc[[i][0]][:graph_output_count_split[0]][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)

  ax2.bar(position[i][:graph_output_count_split[0]],
	  #position[i][graph_output_count_split[0]:], 
	  df_output.iloc[[i][0]][graph_output_count_split[0]:graph_output_count_split[1]],
	  width_output, 
	  alpha=0.5,
	  color=colors[i],
	  label=graph_category_names[i],
	  edgecolor=bar_edge_color,
	  linewidth=bar_line_width)

  old_j = j+1
  #add values of each bar
  for j, v in enumerate(df_output.iloc[[i][0]][graph_output_count_split[0]:graph_output_count_split[1]]):
    v = v*output_names[j+old_j][2]
    if v>1000:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+3*0.04     
    elif v>100:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+2*0.04
    elif v>10:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+0.04
    else:
      y_axis_value_label_offset_temp = y_axis_value_label_offset
    ax2.text(position[i][:graph_output_count_split[0]][j]+x_axis_value_label_offset, df_output.iloc[[i][0]][graph_output_count_split[0]:graph_output_count_split[1]][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)

  ax3.bar(position[i][:graph_output_count_split[0]],
	  #position[i][graph_output_count_split[0]:], 
	  df_output.iloc[[i][0]][graph_output_count_split[1]:],
	  width_output, 
	  alpha=0.5,
	  color=colors[i],
	  label=graph_category_names[i],
	  edgecolor=bar_edge_color,
	  linewidth=bar_line_width)

  old_j = old_j+j+1
  #add values of each bar
  for j, v in enumerate(df_output.iloc[[i][0]][graph_output_count_split[1]:]):
    v = v*output_names[j+old_j][2]
    if v>1000:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+3*0.04     
    elif v>100:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+2*0.04    
    elif v>10:
      y_axis_value_label_offset_temp = y_axis_value_label_offset+0.04
    else:
      y_axis_value_label_offset_temp = y_axis_value_label_offset
    ax3.text(position[i][:graph_output_count_split[0]][j]+x_axis_value_label_offset, df_output.iloc[[i][0]][graph_output_count_split[1]:][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)


#edgecolor = "none" 
 
 
# Set the y axis label
ax1.set_ylabel('Per Unit')
ax2.set_ylabel('Per Unit')
ax3.set_ylabel('Per Unit')

# Set the chart's title
#ax1.set_title('Performance')

tick_position = []
distance_between_positions = position[0][1]-position[0][0]
position_middle_array = len(position)
if int(position_middle_array%2) == 0:
  position_middle_array = int(position_middle_array/2)
else:
  position_middle_array = int(position_middle_array/2)

if len(position)%2 == 0:
  for i in range(len(position[0])):
    tick_position.append(position[position_middle_array][i])
else:
  for i in range(len(position[0])): 
    tick_position.append(position[position_middle_array][i]+0.5*width_output)
    
# Set the position of the x ticks
ax1.set_xticks(tick_position[:graph_output_count_split[0]])
ax2.set_xticks(tick_position[:graph_output_count_split[0]])
ax3.set_xticks(tick_position[:graph_output_count_split[0]])

ax1.set_xticklabels(graph_output_column_names[:graph_output_count_split[0]])
ax2.set_xticklabels(graph_output_column_names[graph_output_count_split[0]:graph_output_count_split[1]])
ax3.set_xticklabels(graph_output_column_names[graph_output_count_split[1]:])

# Setting the x-axis and y-axis limits
ax1.set_xlim(0, np.amax(position[-1][:graph_output_count_split[0]])+width_output+begin_offset_output)
ax2.set_xlim(0, np.amax(position[-1][:graph_output_count_split[0]])+width_output+begin_offset_output)
ax3.set_xlim(0, np.amax(position[-1][:graph_output_count_split[0]])+width_output+begin_offset_output)

ax1.set_ylim([0, 1.5] ) #increase to acommodate legend
ax2.set_ylim([0, 1.5] )
ax3.set_ylim([0, 1.5] )

legend_text = []
for i in range(0,len(db_files)):
  legend_text.append(db_files[i][1])

# Adding the legend and showing the plot
#ax1.legend(legend_text, loc='upper left')
#ax1.legend(legend_text, loc='upper left',mode="expand", ncol=4)
#ax1.legend(legend_text, loc='upper left', ncol=4)

legend_pos_x = 0.5
leg1 = ax1.legend(legend_text, loc='upper center', ncol=4)
#ax1.legend(legend_text, loc='upper center', ncol=4,  bbox_to_anchor=(legend_pos_x, 1.2))
##ax2.legend(legend_text, loc='upper center', ncol=4)


handles, labels = ax1.get_legend_handles_labels()
#ax1.legend(flip(handles, 2), flip(labels, 2), loc='upper center', ncol=2)
#ax1.legend(flip(handles, 4), flip(labels, 4), loc='upper center', ncol=4)
ax1.legend(flip(handles, 4), flip(labels, 4), loc='upper center', ncol=4, bbox_to_anchor=(legend_pos_x, 1.3))
#ax1.legend(flip(handles, 4), flip(labels, 4), ncol=4)

#ax1.legend(bbox_to_anchor=(0.5, 1.4))

ax1.grid()
ax2.grid()
ax3.grid()

#fig1.set_size_inches(10, 8) #default is (8,6)
#fig1.set_size_inches(10, 12) #default is (8,6)
fig1.set_size_inches(10, 13) #default is (8,6)
fig1.savefig(save_dir+'IDRFPM_performance_comparison.pdf', bbox_inches='tight')
#fig1.savefig(save_dir+'performance_comparison.pdf')

###PLOT INPUTS############################ 
##########################################
x_axis_value_label_offset = 0.009

# Plotting the bars

fig2, (ax5,ax6) = pl.subplots(2,1)
old_j = 0
position = np.zeros([len(db_files),len(df_input_column_names)])
for i in range(0,len(db_files)):
  if i == 0:
    for j in range(0,len(df_input_column_names)):
      position[i][j] = group_input_distance*width_input*(len(df_input_column_names)+1)*j+begin_offset_input
  else:
    for j in range(0,len(df_input_column_names)):
      position[i][j] = position[i-1][j] + width_input

#position = np.array(pos)
for i in range(0,len(db_files)): #plot each machine (pole combination)
  ax5.bar(position[i][:graph_input_count_split[0]], 
	  df_input.iloc[[i][0]][:graph_input_count_split[0]],
	  width_input, 
	  alpha=0.5,
	  color=colors[i],
	  label=graph_category_names[i],
	  edgecolor=bar_edge_color,
	  linewidth=bar_line_width)

  #add values of each bar
  for j, v in enumerate(df_input.iloc[[i][0]][:graph_input_count_split[0]]):
    v = v*input_names[j][2]
    if v>100:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input+2*0.03    
    elif v>10:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input+0.03
    else:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input
    ax5.text(position[i][:graph_input_count_split[0]][j]+x_axis_value_label_offset, df_input.iloc[[i][0]][:graph_input_count_split[0]][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)

  ax6.bar(position[i][:graph_input_count_split[0]],
	  #position[i][graph_input_count_split[0]:], 
	  df_input.iloc[[i][0]][graph_input_count_split[0]:graph_input_count_split[1]],
	  width_input, 
	  alpha=0.5,
	  color=colors[i],
	  label=graph_category_names[i],
	  edgecolor=bar_edge_color,
	  linewidth=bar_line_width)

  old_j = j+1
  #add values of each bar
  for j, v in enumerate(df_input.iloc[[i][0]][graph_input_count_split[0]:graph_input_count_split[1]]):
    v = v*input_names[j+old_j][2]
    if v>100:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input+2*0.03    
    elif v>10:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input+0.03
    else:
      y_axis_value_label_offset_temp = y_axis_value_label_offset_input
    ax6.text(position[i][:graph_input_count_split[0]][j]+x_axis_value_label_offset, df_input.iloc[[i][0]][graph_input_count_split[0]:graph_input_count_split[1]][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)

  #ax7.bar(position[i][:graph_input_count_split[0]],
	  ##position[i][graph_input_count_split[0]:], 
	  #df_input.iloc[[i][0]][graph_input_count_split[1]:],
	  #width_input, 
	  #alpha=0.5,
	  #color=colors[i],
	  #label=graph_category_names[i],
	  #edgecolor=bar_edge_color,
	  #linewidth=bar_line_width)

  #old_j = old_j+j+1
  ##add values of each bar
  #for j, v in enumerate(df_input.iloc[[i][0]][graph_input_count_split[1]:]):
    #v = v*input_names[j+old_j][2]
    #if v>100:
      #y_axis_value_label_offset_temp = y_axis_value_label_offset_input+2*0.03    
    #elif v>10:
      #y_axis_value_label_offset_temp = y_axis_value_label_offset_input+0.03
    #else:
      #y_axis_value_label_offset_temp = y_axis_value_label_offset_input
    #ax7.text(position[i][:graph_input_count_split[0]][j]+x_axis_value_label_offset, df_input.iloc[[i][0]][graph_input_count_split[1]:][j] + y_axis_value_label_offset_temp, str('%.2f'%(v)), color=value_label_colour, fontweight='bold', rotation='vertical', fontsize=value_label_fontsize)


#edgecolor = "none" 
 
 
# Set the y axis label
ax5.set_ylabel('Per Unit')
ax6.set_ylabel('Per Unit')
#ax7.set_ylabel('Per Unit')

# Set the chart's title
#ax5.set_title('Performance')

tick_position = []
distance_between_positions = position[0][1]-position[0][0]
position_middle_array = len(position)
if int(position_middle_array%2) == 0:
  position_middle_array = int(position_middle_array/2)
else:
  position_middle_array = int(position_middle_array/2)

if len(position)%2 == 0:
  for i in range(len(position[0])):
    tick_position.append(position[position_middle_array][i])
else:
  for i in range(len(position[0])): 
    tick_position.append(position[position_middle_array][i]+0.5*width_input)
    
# Set the position of the x ticks
ax5.set_xticks(tick_position[:graph_input_count_split[0]])
ax6.set_xticks(tick_position[:graph_input_count_split[0]])
#ax7.set_xticks(tick_position[:graph_input_count_split[0]])

ax5.set_xticklabels(graph_input_column_names[:graph_input_count_split[0]])
ax6.set_xticklabels(graph_input_column_names[graph_input_count_split[0]:graph_input_count_split[1]])
#ax7.set_xticklabels(graph_input_column_names[graph_input_count_split[1]:])

# Setting the x-axis and y-axis limits
ax5.set_xlim(0, np.amax(position[-1][:graph_input_count_split[0]])+width_input+begin_offset_input)
ax6.set_xlim(0, np.amax(position[-1][:graph_input_count_split[0]])+width_input+begin_offset_input)
#ax7.set_xlim(0, np.amax(position[-1][:graph_input_count_split[0]])+width_input+begin_offset_input)

ax5.set_ylim([0, 1.5] )
ax6.set_ylim([0, 1.5] )
#ax7.set_ylim([0, 1.5] )

legend_text = []
for i in range(0,len(db_files)):
  legend_text.append(db_files[i][1])

# Adding the legend and showing the plot
#ax5.legend(legend_text, loc='upper left')
#ax5.legend(legend_text, loc='upper left',mode="expand", ncol=4)
#ax5.legend(legend_text, loc='upper left', ncol=4)

ax5.legend(legend_text, loc='upper center', ncol=4)
##ax6.legend(legend_text, loc='upper center', ncol=4)

handles, labels = ax5.get_legend_handles_labels()
#ax5.legend(flip(handles, 2), flip(labels, 2), loc='upper center', ncol=2)
#ax5.legend(flip(handles, 4), flip(labels, 4), loc='upper center', ncol=4)
ax5.legend(flip(handles, 4), flip(labels, 4), loc='upper center', ncol=4, bbox_to_anchor=(legend_pos_x, 1.3))

ax5.grid()
ax6.grid()
#ax7.grid()

#pl.subplots_adjust(left=None, bottom=None, right=None, top=None, wspace=None, hspace=0.2)

#fig2.set_size_inches(10, 8) #default is (8,6)
fig2.set_size_inches(10, 12.5) #default is (8,6)
#fig2.set_size_inches(10, 13) #default is (8,6)
fig2.savefig(save_dir+'IDRFPM_input_comparison.pdf', bbox_inches='tight')


#pl.show()
