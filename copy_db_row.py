#!/usr/bin/python

import sqlite3
import os, sys

#copies row information from db2 to db1

db_name_1 = 'IDRFPM.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_44_pole_round_litz/best.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_48_pole_round_litz/best.db'
#db_name_2 = 'optimisation/2017-11-29/local_best_52_pole_round_litz/best.db'

#db_name_2 = 'optimisation/2018-07-13/100/local_12/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_16/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_20/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_24/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_28/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_32/best.db'
#db_name_2 = 'optimisation/2018-07-13/100/local_36/best.db'
db_name_2 = 'optimisation/2018-07-13/100/local_40/best.db'

#db_name_2 = 'best.db'
#db_name_2 = 'SORSPM.db'
#db_name_2 = 'optimise.db'

pk1 = 34
pk2 = 547

#table_name_from = 'machine_input_parameters'
table_name_from = 'machine_input_parameters_rpm_100'

table_name_to = 'machine_input_parameters'

#################################
conn = sqlite3.connect(db_name_1)
c = conn.cursor()

if db_name_1 is not db_name_2:
  c.execute("ATTACH DATABASE '%s' as db2"%(db_name_2))

c = conn.execute("select * from %s"%(table_name_from))
column_names = list(map(lambda x: x[0], c.description))



for i in range(1,len(column_names)):
  if db_name_1 is not db_name_2:
    c.execute("SELECT %s FROM db2.%s WHERE primary_key = %d"%(column_names[i],table_name_from,pk2))
  else:
    c.execute("SELECT %s FROM %s WHERE primary_key = %d"%(column_names[i],table_name_from,pk2))
    
  val = c.fetchone()[0]
  #print(val)
  if i == 1:
    try:
      c.execute("INSERT INTO %s (primary_key) VALUES (%d)"%(table_name_to,pk1))
    except:
      pass

  try:
    c.execute("UPDATE %s SET %s = ? WHERE primary_key = ?"%(table_name_to,column_names[i]),(val,pk1))
  except:
    print "Column is not in both tables: %s"%(column_names[i])

conn.commit()
conn.close()