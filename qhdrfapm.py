#!/usr/bin/python
#-*- coding: utf-8 -*-

#import all other necessary modules
from __future__ import division
from math import pi,degrees,sin,cos,sqrt
#import locale
import numpy as np
from prettytable import PrettyTable
import sys
import csv
import os
import sqlite3
import datetime
import argparse
# from semfem import *
import pylab as pl
# import matplotlib as pl

debug=0
time=True
enable_terminal_output = 0
calculate_AR_effect = 1
optimise_interface = 0
calculate_all_torques = 0
save_flux_distribution = 0

#torque harmonic interaction informative display_drawings
display_informatics=0
print_count = 1
torque_harmonics = []
torque_calc_count=0

enable_shell_mass = 1
rotor_shell_height=10.0/1000
stator_shell_height=10.0/1000

if time:
  import time

if optimise_interface != 1:
  #import matplotlib as mpl
  # import matplotlib.pyplot as mpl
  from matplotlib.patches import Rectangle, Wedge
  from glob import glob

if optimise_interface != 1:
  save_figs=False
  save_csv=False
  show_figs=False
  draw_coils = False
  plot_PM = False
  plot_AR = False
  presentation=False
  linear_repr=False   #Draw the RFAPM machine in a linearised representational format
  plot_contour=False
  plot_current_density=False #dont think these mathematical functions are correct for the quasi-halbach machine
  #plot_km_range=False
  plot_approx=False
  plot_harmonics=False
  plot_total=False     #Plot the analysis for the PM + the windings
  plot_torque=False
  plot_Tm_Lorentz=False
  plot_Tm_Lorentz_simple=False

  #########################
  # Contour Plot settings #
  #########################

  contour_figsize=(6,8)
  colorbar_shrink=0.25
  colorbar_aspect=10

  LINEWIDTH=1

r2d = lambda x: np.rad2deg(x)
to_pd = lambda x: (abs(x), r2d(np.angle(x)))

parser = argparse.ArgumentParser(description="IDRFPM semfem simulation script",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=12, help='Specify the primary_key of the input row of the database that should be used')
parser.add_argument('-db','--database', dest='database', default='IDRFPM.db', help='Specify the database name which should be used')
parser.add_argument('-tb','--table', dest='table', default='', help='Specify the table extension name from which inputs should be read') #for eg. the extension could be '_rpm_050'
parser.add_argument('-pl','--plot', dest='plot', type=int, default=0, help='Reprint and output graphs of previous simulation')

args = parser.parse_args()

Type='II'

pretty_terminal_table = PrettyTable(['SEMFEM Output Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True
has_opened = False
enable_variable_test = False

def addToTable (variable_string_name, value, unit_string, enable_disable, dir_parent):
  if enable_disable == True:
    pretty_terminal_table.add_row([variable_string_name,value,unit_string])
    variable_string_name = variable_string_name.replace(" ", "_")
    with open(dir_parent+'script_results/qhdrfapm/output.csv',"a") as output_dat:
      output_dat.write('%s,%f\n'%(variable_string_name,value))

#def main(*args):
if time:
  start_time = time.time()

VERBOSITY=1		#VERBOSITY setting when this script is executed by itself
#display_drawings=0
dir_parent = ''
number_of_threads = 1

primary_key = args.primary_key
db_name = args.database
input_table_name = "machine_input_parameters"+args.table
output_table_name = "machine_semfem_output"+args.table

store_results_in_database = 1
overwrite_previous_result = 1
database_verbosity = 0

enable_show_inputs = 1
enable_general_calc = 1
enable_l_series_connections = 0

conn = sqlite3.connect(db_name)
c = conn.cursor()

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

def db_output_insert(table,column,primary_key,value):
  global already_commited_something_now
  if already_commited_something_now==0:
    try:
      c.execute("INSERT INTO "+table+"(primary_key,"+column+") "+ "VALUES (?,?)",(primary_key,value))
      already_commited_something_now = 1
      if database_verbosity == 1: print "created new row and added some value to a column"
    except:
      #pass
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
  else:
    try:
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
      if database_verbosity == 1: print "added new value to column of existing row"
    except:
      pass

#flags
result_already_exists = 1
already_commited_something_now = 0
deleted_row_entry = 0

#check if a result is already stored for this primary_key
if store_results_in_database == 1:
  #try:
  if database_verbosity == 1: print "trying to select"
  c.execute("SELECT magnet_mass FROM machine_semfem_output WHERE primary_key = ?", (primary_key,))
  data=c.fetchone()

  if database_verbosity == 1:
    if data is None:
        print('There is no primary_key named %s'%primary_key)
    else:
        print('primary_key %s found with magnet_mass %s'%(primary_key,data[0]))

  if data is None:
    if database_verbosity == 1: print "No entry found"
    result_already_exists = 0
  else:
    if database_verbosity == 1: print "Entry found"
    result_already_exists = 1

#delete entry if it exists and if we are to overwrite any previous results
if overwrite_previous_result == 1 and result_already_exists == 1 and store_results_in_database == 1:
  #conn.commit()
  c.execute("DELETE FROM machine_semfem_output WHERE primary_key=?",(primary_key,))
  #conn.commit()
  deleted_row_entry = 1
  if database_verbosity == 1: print "deleted previous entry"

##################
# Machine Inputs #
##################

poles = db_input_select(input_table_name,"poles",primary_key)
slots = db_input_select(input_table_name,"slots",primary_key)
J = db_input_select(input_table_name,"current_density_rms",primary_key)*np.sqrt(2)
system_voltage = db_input_select(input_table_name,"system_voltage",primary_key)
ro = db_input_select(input_table_name,"outer_radius_in_mm",primary_key)/1000
hc  = db_input_select(input_table_name,"coil_height_in_mm",primary_key)/1000
hmi_r = db_input_select(input_table_name,"inner_magnet_height_in_mm",primary_key)/1000
hmo_r = db_input_select(input_table_name,"outer_magnet_height_in_mm",primary_key)/1000
km = db_input_select(input_table_name,"radially_magnetized_PM_width_ratio",primary_key)
kc  = db_input_select(input_table_name,"coil_width_ratio",primary_key)
kq  = db_input_select(input_table_name,"coils_per_phase_vs_rotor_pole_pair_ratio",primary_key)
l = db_input_select(input_table_name,"stacklength_in_mm",primary_key)/1000
g = db_input_select(input_table_name,"airgap_in_mm",primary_key)/1000
fill_factor = db_input_select(input_table_name,"fill_factor",primary_key)
n_rpm = db_input_select(input_table_name,"n_rpm",primary_key)
Brem= db_input_select(input_table_name,"B_rem",primary_key)						#remnant flux density of NdBFe N48 PMs        [T]
force_this_number_of_turns = db_input_select(input_table_name,"force_this_number_of_turns",primary_key)
coil_temperature = db_input_select(input_table_name,"coil_temperature",primary_key)
initial_electrical_angle = db_input_select(input_table_name,"initial_position_defined_by_electrical_angle_deg",primary_key)
shift_current_electrical_angle = db_input_select(input_table_name,"shift_current_by_electrical_angle_deg",primary_key)
inverter_squarewave0_sinusoidalPWM1 = db_input_select(input_table_name,"inverter_squarewave0_sinusoidalPWM1",primary_key)
copper_losses_allowed = db_input_select(input_table_name,"copper_losses",primary_key)
current_angle_degrees = db_input_select(input_table_name,"current_angle_degrees",primary_key)
input_is_Jrms0_Pcu1_slotcurrent2 = db_input_select(input_table_name,"input_is_Jrms0_Pcu1_slotcurrent2",primary_key)
number_of_parallel_circuits = db_input_select(input_table_name,"number_of_parallel_circuits",primary_key)
three_phase_delta0_wye1 = db_input_select(input_table_name,"three_phase_delta0_wye1",primary_key)
use_parallel_strands = db_input_select(input_table_name,"use_parallel_strands",primary_key)
enable_eddy_losses_calc = db_input_select(input_table_name,"enable_eddy_losses",primary_key)
current_through_slot = db_input_select(input_table_name,"current_through_slot_rms",primary_key)
target_mech_power_kW = db_input_select(input_table_name,"target_mech_power_kW",primary_key)

#enable_target_mech_power determines if the script should try to operate machine with a constant power output. It should be disabled when Ls and flm are to be discovered.
enable_target_mech_power = db_input_select(input_table_name,"enable_target_mech_power",primary_key)

#should only be enabled when the adequate number of turns should be determined at top speed, and thereafter the turns should be kept constant
turns_calculation_for_rpm = db_input_select(input_table_name,"turns_calculation_for_rpm",primary_key)
enable_turns_calculation = db_input_select(input_table_name,"enable_turns_calculation",primary_key)

#should enable for round conductors, and disable for bar conductors
enable_automatic_flux_weakening = db_input_select(input_table_name,"enable_automatic_flux_weakening",primary_key)

enable_voltage_leeway = db_input_select(input_table_name,"enable_voltage_leeway",primary_key)

peak_terminal_line_voltage_limit = system_voltage
RMS_terminal_line_voltage_limit = peak_terminal_line_voltage_limit/np.sqrt(2)

if three_phase_delta0_wye1 == 1:
  RMS_terminal_phase_voltage_limit = RMS_terminal_line_voltage_limit/np.sqrt(3)
  peak_terminal_phase_voltage_limit = peak_terminal_line_voltage_limit/np.sqrt(3)
else:
  RMS_terminal_phase_voltage_limit = RMS_terminal_line_voltage_limit
  peak_terminal_phase_voltage_limit = peak_terminal_line_voltage_limit

######################
# Legacy Conversions #
######################

p = int(poles/2)
kmo = km
kmi = km
muR = 1.05 #recoil permeability of the PMs
active_parts=3
f_e = (n_rpm*(poles))/120
w_e = 2*pi*f_e
#peak_voltage_allowed = (system_voltage*0.95*1.15)/sqrt(3)
c110000_resistivity_20 = 1.72e-8
copper_resistivity = c110000_resistivity_20*(1+0.0039*(coil_temperature-20)) #copper resistivity is given in [Ohm.m]
T=1/f_e
t=0*T/12
gamma=0
a=number_of_parallel_circuits
omega_m=n_rpm*pi/30
omega_m_turns = turns_calculation_for_rpm*np.pi/30
N=1
Ls_k = 2.225

contour_figsize=(6,8)
colorbar_shrink=0.25
colorbar_aspect=10

if optimise_interface != 1:
  if presentation:
    pl.rcParams['font.family']='sans-serif'
    pl.rcParams['font.sans-serif']='Helvetica'
    pl.rcParams['text.latex.preamble']=r'\usepackage{helvet}',r'\usepackage{sansserifmath}',r'\renewcommand{\familydefault}{\sfdefault}'
    pl.rcParams['axes.labelsize']=12
    pl.rcParams['xtick.labelsize']=10
    pl.rcParams['ytick.labelsize']=10

# a small positive current angle will increase the d-axis flux density, thereby aiding the flux produced by the magnets, thus increasing the overall torque produced
if input_is_Jrms0_Pcu1_slotcurrent2 != 2:
  id = J*np.sin((np.pi/180)*current_angle_degrees)
  iq = J*np.cos((np.pi/180)*current_angle_degrees)   #current density[A/mm2] should now be given as PEAK values (J is already converted to peak value)

#####################################
# Calculation of Derived Quantities #
#####################################
km=kmo #TODO: kmi kmo
kDelta=kc #TODO: is dit kc of (1-kc)? Ek is op die regte spoor, die definisie word gegee op Phd p.21

q = int(slots/3)#number of coils per phase
periodicity = q
Q = slots
coils_in_series_per_phase = q/number_of_parallel_circuits
coil_pitch = (2*np.pi)/Q

Delta_max=pi/6
Delta=kDelta*Delta_max


if enable_show_inputs == 1:
  extra_input_info = np.genfromtxt('input_original.csv',skip_header=1,delimiter=',',usecols=(2,3),unpack=True)
  ri_min  =  extra_input_info[1][38]
  hmo_r_min =  extra_input_info[1][36]
  go_min  =  extra_input_info[1][53]
  hc_min  =  extra_input_info[1][7]
  gi_min  =  extra_input_info[1][54]
  hmi_r_min =  extra_input_info[1][37]

  ###DEBUGGING
  if enable_variable_test == True:
    if __name__ != '__main__':	#when this script is invoked by an other script
      print("hallo daar")
      #these variable values are decided by main.py
      g	= input_data[6] 						#each airgap length
      hc	= input_data[7] 						#magnet thickness
      hmo_r	= input_data[36]						#outer magnet width
      hmi_r	= input_data[37]						#inner magnet width
      ri	= input_data[38]						#most inner radius point on machine
    else:				#when this script is executed by itself

      ri  = (ro - ri_min - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
      hmo_r = (ro - ri     - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo_r + hmo_r_min
      go  = (ro - ri     - hmo_r     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
      hc  = (ro - ri     - hmo_r     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
      gi  = (ro - ri     - hmo_r     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
      hmi_r = (ro - ri     - hmo_r     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min

      #print go

  ###

#############################################
# Calculate remaining dimensions and ratios #
#############################################

#x_hmo_rt --> gives ratio between hight of outer radially magnetized PMs compared to outer tangentially magnetized PMs
#hmo_t = x_hmo_rt*hmo_r
#x_hmi_rt --> gives ratio between hight of inner radially magnetized PMs compared to inner tangentially magnetized PMs
#hmi_t = x_hmi_rt*hmi_r


#convert these parameters to my script's parameters
magnetTotal = 4*p

outerMagnetRadiusB = ro
outerMagnetRadiusA = ro-hmo_r
innerMagnetRadiusB = ro-hmo_r-g-hc-g
innerMagnetRadiusA = ro-hmo_r-g-hc-g-hmi_r
ri = ro-hmo_r-g-hc-g-hmi_r
ri_original = ri

CoilRadiusB = ro-hmo_r-g
CoilRadiusA = ro-hmo_r-g-hc

innerBlockRadius = CoilRadiusA				#assuming the coil resin has no thickness
outerBlockRadius = CoilRadiusB				#assuming the coil resin has no thickness
coilBlockWidthRadial = outerBlockRadius - innerBlockRadius
stack_length = l
Br = Brem
mur = muR
coilsTotal = int(kq * 3 * p)


magnetPitch = (2*pi)/magnetTotal
axial_outer_PM_pitch = (1-kmo)*2*magnetPitch
axial_inner_PM_pitch = (1-kmo)*2*magnetPitch
#axial_inner_PM_pitch = (1-kmi)*2*magnetPitch
radial_outer_PM_pitch = kmo*2*magnetPitch
#radial_inner_PM_pitch = kmi*2*magnetPitch
radial_inner_PM_pitch = kmo*2*magnetPitch
angle_inc = 0.5*magnetPitch

innerYoke = 0.9*ri
outerYoke = 1.1*ro

magnetPitch = (2*pi)/magnetTotal
poles = int(magnetTotal/2)					#number of poles in rotor1, and number of poles in rotor2
mechRotation = (4*pi)/poles					#only rotate one electrical full period (amount of radians the machine should mechanically rotate) ##mechRotation = 2*pi * (2/poles)
rotor_symmetry_factor = int(4*magnetTotal/poles)
symmetry_angle = (magnetTotal/(poles/4))*magnetPitch
rotorDAxisOffset = 1.5*magnetPitch				#this angle is used to move rotor d-axis inline with the x-axis (assuming the transformation angle = 0 because we assume the phase-A axis also to be there)
coilBlockPitch = (2*pi)/coilsTotal
meanBlockRadius = innerBlockRadius + 0.5*coilBlockWidthRadial


coil_side_radians = 0.5*kc*coilBlockPitch
blockAngle = (1-kc)*coilBlockPitch
coil_side_area = (CoilRadiusB**2 - CoilRadiusA**2)*(coil_side_radians/2)	#[m^2]
windingRadians = coil_side_radians
end_winding_radius = ((blockAngle+windingRadians)*meanBlockRadius)/2

if input_is_Jrms0_Pcu1_slotcurrent2 == 1:
  #Pcu_alt = Q*copper_resistivity*((J*(1000**2))**2)*coil_side_area*fill_factor*(stack_length+np.pi*end_winding_radius)
  J = np.sqrt(copper_losses_allowed/(Q*copper_resistivity*coil_side_area*fill_factor*(stack_length+np.pi*end_winding_radius)*1000**4))
current_through_slot = J*(1000**2)*coil_side_area*fill_factor			#[A]
area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
N = 1 #turns per coil will be increased until voltage limit is reached
iq = current_through_slot/N
Ip = iq
max_input_phase_current = Ip

if debug == 1:
  print("max_input_phase_current = %g"%(max_input_phase_current))
  print("Using peak current density value of J = %f [A/mm2], which implies peak phase current Ip = %f [A]"%(J,Ip))

N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1
phase = [1,-1,3,-3,2,-2]



#Air-Gaps
innerGap = innerBlockRadius - innerMagnetRadiusB
outerGap = outerMagnetRadiusA - outerBlockRadius

#if enable_show_inputs == 1:
  #pretty_input_table = PrettyTable(['SEMFEM Input Variable','Perturb Count','Minimum Value','Maximum Value','Initial Value','Present Value','Percentage Change','Unit'])
  #pretty_input_table.align = 'l'
  #pretty_input_table.border= True
  #extra_input_info = np.genfromtxt('input_original.csv',skip_header=1,delimiter=',',usecols=(2,3),unpack=True)

  #pretty_input_table.add_row(["ABSOLUTE VALUES","","","","","","",""])
  #pretty_input_table.add_row(["ro",0,extra_input_info[1][39],extra_input_info[0][39],input_data_original[39],ro,"%.3f"%(((ro-input_data_original[39])/input_data_original[39])*100),"[m]"])
  #pretty_input_table.add_row(["ri",int(perturb_count[0]),extra_input_info[1][38],extra_input_info[0][38],input_data_original[38],ri,"%.3f"%(((ri-input_data_original[38])/input_data_original[38])*100),"[m]"])
  #pretty_input_table.add_row(["hmo_r",int(perturb_count[1]),extra_input_info[1][36],extra_input_info[0][36],input_data_original[36],hmo_r,"%.3f"%(((hmo_r-input_data_original[36])/input_data_original[36])*100),"[m]"])
  #pretty_input_table.add_row(["hc",int(perturb_count[2]),extra_input_info[1][7],extra_input_info[0][7],input_data_original[7],hc,"%.3f"%(((hc-input_data_original[7])/input_data_original[7])*100),"[m]"])
  #pretty_input_table.add_row(["hmi_r",int(perturb_count[3]),extra_input_info[1][37],extra_input_info[0][37],input_data_original[37],hmi_r,"%.3f"%(((hmi_r-input_data_original[37])/input_data_original[37])*100),"[m]"])
  #pretty_input_table.add_row(["g",0,extra_input_info[1][6],extra_input_info[0][6],input_data_original[6],g,"%.3f"%(((g-input_data_original[6])/input_data_original[6])*100),"[m]"])
  #pretty_input_table.add_row(["l",0,extra_input_info[1][5],extra_input_info[0][5],input_data_original[5],l,"%.3f"%(((l-input_data_original[5])/input_data_original[5])*100),"[m]"])
  #pretty_input_table.add_row(["kmo",int(perturb_count[4]),extra_input_info[1][8],extra_input_info[0][8],input_data_original[8],kmo,"%.3f"%(((kmo-input_data_original[8])/input_data_original[8])*100),"[p.u.]"])
  #pretty_input_table.add_row(["kc",int(perturb_count[5]),extra_input_info[1][32],extra_input_info[0][32],input_data_original[32],kc,"%.3f"%(((kc-input_data_original[32])/input_data_original[32])*100),"[p.u.]"])
  #pretty_input_table.add_row(["p",int(perturb_count[6]),extra_input_info[1][0],extra_input_info[0][0],input_data_original[0],p,"%.3f"%(((p-input_data_original[0])/input_data_original[0])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["kmi",int(perturb_count[7]),extra_input_info[1][62],extra_input_info[0][62],input_data_original[62],kmi,"%.3f"%(((kmi-input_data_original[62])/input_data_original[62])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["go",int(perturb_count[10]),extra_input_info[1][53],extra_input_info[0][53],input_data_original[53],go,"%.3f"%(((go-input_data_original[53])/input_data_original[53])*100),"[m]"])
  ##pretty_input_table.add_row(["gi",int(perturb_count[11]),extra_input_info[1][54],extra_input_info[0][54],input_data_original[54],gi,"%.3f"%(((gi-input_data_original[54])/input_data_original[54])*100),"[m]"])

  #pretty_input_table.add_row(["RELATIVE VALUES","","","","","","",""])
  #pretty_input_table.add_row(["x_ri" ,int(perturb_count[0]),extra_input_info[1][55],extra_input_info[0][55],input_data_original[55],x_ri,"%.3f"%(((x_ri-input_data_original[55])/input_data_original[55])*100),"[p.u.]"])
  #pretty_input_table.add_row(["x_hmo_r",int(perturb_count[1]),extra_input_info[1][56],extra_input_info[0][56],input_data_original[56],x_hmo_r,"%.3f"%(((x_hmo_r-input_data_original[56])/input_data_original[56])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_go" ,int(perturb_count[10]),extra_input_info[1][57],extra_input_info[0][57],input_data_original[57],x_go,"%.3f"%(((x_go-input_data_original[57])/input_data_original[57])*100),"[p.u.]"])
  #pretty_input_table.add_row(["x_hc" ,int(perturb_count[2]),extra_input_info[1][58],extra_input_info[0][58],input_data_original[58],x_hc,"%.3f"%(((x_hc-input_data_original[58])/input_data_original[58])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_gi" ,int(perturb_count[11]),extra_input_info[1][59],extra_input_info[0][59],input_data_original[59],x_gi,"%.3f"%(((x_gi-input_data_original[59])/input_data_original[59])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_hmi_r" ,int(perturb_count[3]),extra_input_info[1][60],extra_input_info[0][60],input_data_original[60],x_hmi_r,"%.3f"%(((x_hmi_r-input_data_original[60])/input_data_original[60])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_hmo_rt",int(perturb_count[8]),extra_input_info[1][63],extra_input_info[0][63],input_data_original[63],x_hmo_rt,"%.3f"%(x_hmo_rt*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_hmi_rt",int(perturb_count[9]),extra_input_info[1][64],extra_input_info[0][64],input_data_original[64],x_hmi_rt,"%.3f"%(x_hmi_rt*100),"[p.u.]"])

  #save_stdout = sys.stdout
  #sys.stdout = open(dir_parent+"pretty_input_table.txt", "wb")
  #print pretty_input_table
  #sys.stdout = save_stdout

  #if enable_terminal_output == 1:
    #print pretty_input_table


###########
# Regions #
###########

I   = 0
II  = 1
III = 2
IV  = 3
V   = 4

############################################
# Permeability's for the different Regions #
############################################

mu0   =4*pi*1E-7
murI  =1
murII =muR
murIII=1
murIV =muR
murV  =1

##################################################
# Defining Boundaries for the Subdomain Analysis #
##################################################


#all radii values will be relative to ensure that we work with reasonably sized values when computing the A-matrix.
rn  = innerMagnetRadiusB + 0.5*(outerMagnetRadiusA - innerMagnetRadiusB) #NB
rii = ri/rn
riii= innerMagnetRadiusB/rn
riv = outerMagnetRadiusA/rn
rv  = outerMagnetRadiusB/rn
rvi = outerYoke/rn

#from now on, ri will obtain a different definition, namely ri = innerYokeRadiusA/rn
#previously, ri was ri = innerMagnetRadiusA

ri  = innerYoke/rn

#inner & outer magnet centres
rcim=	innerMagnetRadiusA + 0.5*(innerMagnetRadiusB-innerMagnetRadiusA)
rcom= outerMagnetRadiusA + 0.5*(outerMagnetRadiusB-outerMagnetRadiusA)


#all values starting with "h" will stay in ABSOLUTE values
hy_o = outerYoke - outerMagnetRadiusB #outer yoke thickness
hy_i = innerMagnetRadiusA - innerYoke #inner yoke thickness


'''
ri  =(rn-h/2-g-hm-hy)/rn
rii =(rn-h/2-g-hm)/rn
riii=(rn-h/2-g)/rn
riv =(rn+h/2+g)/rn
rv  =(rn+h/2+g+hm)/rn
rvi =(rn+h/2+g+hm+hy)/rn
'''

##############################################
# The number of harmonics for which to solve #
##############################################

#Mmax=11
Mmax=19

###########################
# Other derived quatities #
###########################

beta=(1-km)/2*pi/p                    #beta angle used for Fourier series expansion #NB
q=kq*p                                 #coils per phase

#M=112*2 #from analytical_solution_i.py

#m_PM_range=np.arange(int(M/p))*2+1

#m_AR_range=np.arange(int(M/q))+1
#m_AR_range=np.arange(int(M/q)*2)+1 #original

m_PM_range=np.arange(int((Mmax+2)/2))*2+1 	#the range of harmonics to solve for
m_AR_range=np.arange(int(Mmax))+1 		#the range of harmonics to solve for
#m_AR_range=np.array([1,5,7,11,13])		#these stator space harmonics are mostly interacting with rotor space harmonics
						#but the other in-between harmonics are required to determine the full
						#magnetic field due to AR. This is actually just so that lambda_AR can be
						#determined, and subsequently the Ls, Synchronous Inductance

##################################################
# The range of r & phi values for which to solve #
##################################################

dr=1E-3
r_range=np.arange(ri,rvi+dr,dr) #notice that r_range is RELATIVE values
#print r_range


M = 128 #In order to utilise the FFT optimally later on
phi_range=np.linspace(-pi/q,pi/q,int(M/kq))


############################
# Winding Factor Functions #
############################
def kw_pitch_def(m):
  r'''The Winding Pitch Factor'''
  if Type=='O':
    return np.sin(m*pi/2)
  elif Type=='I':
    return np.sin(m*pi/6)
  elif Type=='II':
    return np.sin(m*(pi/3-Delta))
  else:
    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

def kw_slot_def(m):
  r'''The Winding Slot Factor'''
  return np.sin(m*Delta)/(m*Delta)

def kT_def(m):
  r'''The torque constant of the machine'''
  return 3*q*rn*l*N/a*kw_pitch_def(m)*kw_slot_def(m)*Br1_PM

################################################################################
# A lookup function to determine the index value of a specific floating point  #
# value in a floating point array                                              #
################################################################################

def lookup(ref,array,tol):
  r'''A lookup function to determine the index value of a specific floating point value in a floating point array,
due to the fact that a floating point value of say "0.232" is represented in python as "0.23200000000000001"'''
  for i,val in enumerate(array):
    if abs((val-ref))<tol:
      return i

#################################
# General Function Definitions  #
#################################

# NOT USED #
def am_dM0r_def(m):
  return 4*p*Brem/(pi*mu0)*cos(m*p*beta)

# NOT USED #
def dM0r_def():
  tmp=0.0
  for m in (np.arange(M)*2+1):
    tmp+=am_dM0r_def(m)*np.cos(m*p*phi_range)
  return tmp

def bm_Jz_def(Ip,m): #from analytical_solution_i.py
  r'''The "bm" coefficient for the Fourier series expansion of the Current Density Distribution expressed in terms of the Conductor Density Distribution'''
  return -(3*q*Ip*N)/(rn*hc*pi)*kw_pitch_def(m)*kw_slot_def(m) #TODO: rn or rn/rn

def Jz_def(Ip,t): #from analytical_solution_i.py
  r'''The Current Density Distribution expressed in terms of the Conductor Density Distribution'''
  tmp=0.0
  for m in (3*m_AR_range-2):
  #for m in (3*m_range-2):
    tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t)
  for m in (3*m_AR_range-1):
  #for m in (3*m_range-1):
    tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t)
  return tmp

#########################
# The Forcing Functions #
#########################

def GmII_def(m):
  r'''The Forcing function due to the Permanent Magnets'''
  return 4*Brem*(m*p*cos(m*p*beta)-sin(m*p*beta))/(pi*m*(1-(m*p)**2))

def GmIV_def(m):
  r'''The Forcing function due to the Permanent Magnets'''
  return 4*Brem*(m*p*cos(m*p*beta)+sin(m*p*beta))/(pi*m*(1-(m*p)**2))

def UmII_def(m):
  r'''The Forcing function due to the Permanent Magnets'''
  return 4*Brem*(cos(m*p*beta)-m*p*sin(m*p*beta))/(pi*m*(1-(m*p)**2))

def UmIV_def(m):
  r'''The Forcing function due to the Permanent Magnets'''
  return 4*Brem*(cos(m*p*beta)+m*p*sin(m*p*beta))/(pi*m*(1-(m*p)**2))

def Gmr_AR_def(m,r):
  r'''The Forcing function due to the Armature Windings'''
  if m % 3 == 0:
    return 0
  else:
    return 3*mu0*Ip*N*r**2/(pi*rn*hc*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)

def dGmrdr_AR_def(m,r):
  r'''The derivative with respect to "r" of the Forcing function due to the Armature Windings'''
  if m % 3 == 0:
    return 0
  else:
    return 6*mu0*Ip*N*r/(pi*rn*hc*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)

##################################
# The Vector Potential Functions #
##################################

def am_Az_PM_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Magnetic Vector Potential
due to the Armature Windings'''
  if Region==II:
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+GmII_def(m)*r
  elif Region==IV:
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+GmIV_def(m)*r
  else:
    #print Region
    #print i
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)
  return tmp

def Az_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
  #for i, m in enumerate(m_range):
    #tmp+=am_Az_PM_def(i,m,r,Region)*np.cos(m*p*phi_range)
    tmp+=am_Az_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def bm_Az_AR_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Magnetic Vector Potential
due to the Armature Windings'''
  if Region==III:
    return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r)
  else:
    return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)
  return tmp

def Az_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
  #for i, m in enumerate(m_range):
    if m in (3*m_AR_range-2):
    #if m in (3*m_range-2):
      tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
    elif m in (3*m_AR_range-1):
    #elif m in (3*m_range-1):
      tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
  return tmp

#####################################
# The Radial Flux Density Functions #
#####################################

def bm_Br_PM_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Radial Flux Density
due to the Permanent Magnets'''
  if Region==II:
    return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1)+GmII_def(m))
  elif Region==IV:
    return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1)+GmIV_def(m))
  else:
    return -m*p*(Cm_PM[Region][i]*r**(m*p-1)+Dm_PM[Region][i]*r**(-m*p-1))

#Andreas says, "i" is just a place holder / index number for the Fourier Coefficients array - it just represents the space harmonics "m" in a number counting from 0 upwards. So "i" also actually represents the space harmonic number
def int_bm_Br_III_PM_def(i,m,r):
  r'''The integral of r**2*"bm" (see above) in order to work out the torque with the Lorentz method'''
  return -m*p*(Cm_PM[III][i]/(m*p+2)*r**(m*p+2)+Dm_PM[III][i]/(-m*p+2)*r**(-m*p+2))

def Br_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
  #for i, m in enumerate(m_range):
    #tmp+=bm_Br_PM_def(i,m,r,Region)*np.sin(m*p*phi_range)
    tmp+=bm_Br_PM_def(i,m,r,Region)*np.sin(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def am_Br_AR_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Radial Flux Density
due to the Armature Windings'''
  if Region==III:
      #print "ek bereken3"
      return -m*q*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r))
  else:
      #print "ek bereken net"
      return -m*q*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q))

def Br_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
  #for i, m in enumerate(m_range):
    if m in (3*m_AR_range-2):
    #if m in (3*m_range-2):
      #print "Hos my larney"
      tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t)/r
    elif m in (3*m_AR_range-1):
    #elif m in (3*m_range-1):
      #print "Hos my bradda"
      tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t)/r
  return tmp

########################################
# The Azimuthal Flux Density Functions #
########################################

def am_Bt_PM_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Azimuthal Flux Density
due to the Permanent Magnets'''
  if Region==II:
    return -(m*p*Cm_PM[Region][i]*r**(m*p-1)-m*p*Dm_PM[Region][i]*r**(-m*p-1)+GmII_def(m))
  elif Region==IV:
    return -(m*p*Cm_PM[Region][i]*r**(m*p-1)-m*p*Dm_PM[Region][i]*r**(-m*p-1)+GmIV_def(m))
  else:
    return -m*p*(Cm_PM[Region][i]*r**(m*p-1)-Dm_PM[Region][i]*r**(-m*p-1))

def Bt_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
  #for i, m in enumerate(m_range):
    #tmp+=am_Bt_PM_def(i,m,r,Region)*np.cos(m*p*phi_range)
    tmp+=am_Bt_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def bm_Bt_AR_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Azimuthal Flux Density
due to the Armature Windings'''
  if Region==III:
    return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1)+dGmrdr_AR_def(m,r)/(m*q))
  else:
    return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1))

def Bt_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
  #for i, m in enumerate(m_range):
    if m in (3*m_AR_range-2):
    #if m in (3*m_range-2):
      tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
    elif m in (3*m_AR_range-1):
    #elif m in (3*m_range-1):
      tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
  return tmp

###########################################################################
#The Flux-linkage at a specific value of "r" due to the Armature Windings #
###########################################################################

def lambda_PM_def(r):
  r'''The Flux-linkage for a specific value of "r" due to the Permanent Magnets'''
  tmp=0
  for i, m in enumerate(m_PM_range):
  #for i, m in enumerate(m_range):
    tmp+=(-2*q*N*l/a)*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*am_Az_PM_def(i,m,r,Region=III)
  return tmp

def lambda_AR_def(r):
  r'''The Flux-linkage for a specific value of "r" due to the Armature Windings'''
  tmp=0
  for i, m in enumerate(m_AR_range):
  #for i, m in enumerate(m_range):
    tmp+=(2*q*N*l/a)*kw_pitch_def(m)*kw_slot_def(m)*bm_Az_AR_def(i,m,r,Region=III)
    #FIXME: must be a "(-2*q*N*l/a)..." above
  return tmp

def k_lambda():
  if Type=='O':
    return np.sin(Delta)/(Delta)
  elif Type=='I':
    return np.sin(pi/3)*np.sin(Delta)/(Delta)
  elif Type=='II':
    return np.sin((pi/Q-Delta)*p)*np.sin(Delta)/(Delta)
  else:
    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

if debug == 1:
  print('The analysis will be done for a Type %s Machine' % (Type))


##################################################################################################################
######################################
###ROTOR (PERMANENT MAGNETS)###
######################################
##################################################################################################################

#######################################
# Initialise the PMAG solution arrays #
#######################################

CmI_PM  =np.array([])
DmI_PM  =np.array([])
CmII_PM =np.array([])
DmII_PM =np.array([])
CmIII_PM=np.array([])
DmIII_PM=np.array([])
CmIV_PM =np.array([])
DmIV_PM =np.array([])
CmV_PM  =np.array([])
DmV_PM  =np.array([])

########################################################
# Performing the PMAG Linear Solve for each value of m #
########################################################
#print "m_PM_range"
#print m_PM_range
#for m in m_PM_range:
for m in m_PM_range:
  #print ri**(-m*p)
  A=np.mat([[ri**(m*p)         ,  ri**(-m*p)         ,  0                   ,  0                    ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	    [rii**(m*p-1)      ,  rii**(-m*p-1)      , -rii**(m*p-1)        , -rii**(-m*p-1)        ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	    [murII*rii**(m*p-1), -murII*rii**(-m*p-1), -murI*rii**(m*p-1)   ,  murI*rii**(-m*p-1)   ,  0                  ,  0                   ,  0                  ,  0                   ,  0                ,  0                 ],
	    [0                 ,  0                  ,  riii**(m*p-1)       ,  riii**(-m*p-1)       , -riii**(m*p-1)      , -riii**(-m*p-1)      ,  0                  ,  0                   ,  0                ,  0                 ],
	    [0                 ,  0                  ,  murIII*riii**(m*p-1), -murIII*riii**(-m*p-1), -murII*riii**(m*p-1),  murII*riii**(-m*p-1),  0                  ,  0                   ,  0                ,  0                 ],
	    [0                 ,  0                  ,  0                   ,  0                    ,  riv**(m*p-1)       ,  riv**(-m*p-1)       , -riv**(m*p-1)       , -riv**(-m*p-1)       ,  0                ,  0                 ],
	    [0                 ,  0                  ,  0                   ,  0                    ,  murIV*riv**(m*p-1) , -murIV*riv**(-m*p-1) , -murIII*riv**(m*p-1),  murIII*riv**(-m*p-1),  0                ,  0                 ],
	    [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  rv**(m*p-1)        ,  rv**(-m*p-1)        , -rv**(m*p-1)      , -rv**(-m*p-1)      ],
	    [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  murV*rv**(m*p-1)   , -murV*rv**(-m*p-1)   , -murIV*rv**(m*p-1),  murIV*rv**(-m*p-1)],
	    [0                 ,  0                  ,  0                   ,  0                    ,  0                  ,  0                   ,  0                  ,  0                   ,  rvi**(m*p)       ,  rvi**(-m*p)       ]])

  B=np.mat([[ 0 ],
	    [ GmII_def(m)*rii  ],
	    [ murI*UmII_def(m)  ],
	    [-GmII_def(m)*riii ],
	    [-murIII*UmII_def(m)],
	    [ GmIV_def(m)*riv  ],
	    [ murIII*UmIV_def(m)],
	    [-GmIV_def(m)*rv   ],
	    [-murV*UmIV_def(m)  ],
	    [ 0 ]])

  X=np.linalg.solve(A,B)

  CmI_PM  =np.append(CmI_PM,  X[0,0])
  DmI_PM  =np.append(DmI_PM,  X[1,0])
  CmII_PM =np.append(CmII_PM, X[2,0])
  DmII_PM =np.append(DmII_PM, X[3,0])
  CmIII_PM=np.append(CmIII_PM,X[4,0])
  DmIII_PM=np.append(DmIII_PM,X[5,0])
  CmIV_PM =np.append(CmIV_PM, X[6,0])
  DmIV_PM =np.append(DmIV_PM, X[7,0])
  CmV_PM  =np.append(CmV_PM,  X[8,0])
  DmV_PM  =np.append(DmV_PM,  X[9,0])

  #dM0+=(4*p/pi)*np.cos(m*p*beta)*np.cos(m*p*phi_range)

###########################################################
# Repack the PMAG Solution Coefficient into Matrix format #
###########################################################

Cm_PM=np.array([CmI_PM,CmII_PM,CmIII_PM,CmIV_PM,CmV_PM])
Dm_PM=np.array([DmI_PM,DmII_PM,DmIII_PM,DmIV_PM,DmV_PM])

#print(Cm)
#print(Dm)

###################################
# Initialise PMAG Solution Arrays #
###################################

Az_PM=np.zeros((r_range.size,phi_range.size))
Br_PM=np.zeros((r_range.size,phi_range.size))
Bt_PM=np.zeros((r_range.size,phi_range.size))
Bmag_PM=np.zeros((r_range.size,phi_range.size))

##make PM arrays link to old name convention of analytical_solution_i.py
#Az_PM = Az
#Br_PM = Br
#Bt_PM = Bt
#Bmag_PM = Bmag

#########################################################################
# Calculate the PMAG Solution Arrays for each value of 'r' in 'r_range' #
#########################################################################
if optimise_interface != 1:
  for i,r in enumerate(r_range):
    if r<rii:
      Region=I
    elif r<riii:
      Region=II
    elif r<riv:
      Region=III
    elif r<rv:
      Region=IV
    else:
      Region=V

    #print np.shape(Cm)
    Az_PM[i]=Az_PM_def(r,0,Region)
    Br_PM[i]=Br_PM_def(r,0,Region)
    Bt_PM[i]=Bt_PM_def(r,0,Region)
    Bmag_PM[i]=np.sqrt(Br_PM[i]**2+Bt_PM[i]**2) #magnitude of both radial and tangetial components
    #print("i=%f,  r=%f"%(i,r))

  #print np.shape(Az)

  ###############################################
  # Calculate Br values at specific values of r #
  ###############################################

  #Get index for the following radii
  i_rn=lookup(rn/rn,r_range,dr/2)
  i_rnmh2=lookup((rn-hc/2)/rn,r_range,dr/2)
  i_rnph2=lookup((rn+hc/2)/rn,r_range,dr/2)

  i_rcim=lookup(rcim/rn,r_range,0.1/1000)
  i_rcom=lookup(rcom/rn,r_range,0.1/1000)
  i_rciy=lookup((rn-hc/2-g-hmi_r-hy_i/2)/rn,r_range,0.1/1000)
  i_rcoy=lookup((rn+hc/2+g+hmo_r+hy_o/2)/rn,r_range,0.1/1000)

  Brmax_PM=Br_PM[i_rn].max()
  Brh_PM=np.abs(np.fft.fft(Br_PM[i_rn],M)*2/M)
  Bth_PM=np.abs(np.fft.fft(Bt_PM[i_rn],M)*2/M)

  Br1_PM=Brh_PM[1]
  Bt1_PM=Bth_PM[1]

  Br1_PM_range=np.array([])
  for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
    i=lookup(r,r_range,dr/2)
    Br1_PM_range=np.append(Br1_PM_range,np.abs(np.fft.fft(Br_PM[i],M)*2/M)[1])

  Bt1_PM_range=np.array([])
  for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
    i=lookup(r,r_range,dr/2)
    Bt1_PM_range=np.append(Bt1_PM_range,np.abs(np.fft.fft(Bt_PM[i],M)*2/M)[1])

  Br_PM_THD=np.sqrt(np.sum(Brh_PM[2:int((M)/2)]**2))/Br1_PM
  Bt_THD_PM=np.sqrt(np.sum(Bth_PM[2:int((M)/2)]**2))/Bt1_PM


##################################################################################################################
######################################
###STATOR COILS (ARMATURE REACTION)###
######################################
##################################################################################################################
if optimise_interface != 1:
  ###################################################
  # Redefining Boundaries for the Armature Reaction #
  ###################################################

  riii=(rn-hc/2)/rn #thus riii now starts after the inner airgap (where the inner radius of stator coils actually are)
  riv =(rn+hc/2)/rn #thus riv now starts before the outer airgap (where the outer radius of stator coils actually are)

  ##################################
  # Initialise the solution arrays #
  ##################################

  CmI_AR  =np.array([])
  DmI_AR  =np.array([])
  CmII_AR =np.array([])
  DmII_AR =np.array([])
  CmIII_AR=np.array([])
  DmIII_AR=np.array([])
  CmIV_AR =np.array([])
  DmIV_AR =np.array([])
  CmV_AR  =np.array([])
  DmV_AR  =np.array([])

  #########################################################################
  # Performing the Linear Solve for each value of m due to the Armature Windings Regions #
  #########################################################################

  #print "m_AR_range"
  #print m_AR_range

  #for m in m_AR_range:
    #print m

  #print riii
  #print m
  #print q
  #print m*q
  #print riii**(m*q)

  #for m in m_AR_range:
  for m in m_AR_range:
    A=np.array([[ri**(m*q)          ,  ri**(-m*q)        ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		[rii**(m*q)         ,  rii**(-m*q)       , -rii**(m*q)         , -rii**(-m*q)         ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		[murII*rii**(m*q-1)  , -murII*rii**(-m*q-1), -murI*rii**(m*q-1)   ,  murI*rii**(-m*q-1)   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
		[0                  ,  0                 ,  riii**(m*q)        ,  riii**(-m*q)        , -riii**(m*q)       , -riii**(-m*q)       ,  0                 ,  0                  ,  0               ,  0                ],
		[0                  ,  0                 ,  murIII*riii**(m*q-1), -murIII*riii**(-m*q-1), -murII*riii**(m*q-1),  murII*riii**(-m*q-1),  0                 ,  0                  ,  0               ,  0                ],
		[0                  ,  0                 ,  0                  ,  0                   ,  riv**(m*q)        ,  riv**(-m*q)        , -riv**(m*q)        , -riv**(-m*q)        ,  0               ,  0                ],
		[0                  ,  0                 ,  0                  ,  0                   ,  murIV*riv**(m*q-1) , -murIV*riv**(-m*q-1) , -murIII*riv**(m*q-1),  murIII*riv**(-m*q-1),  0               ,  0                ],
		[0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  rv**(m*q)         ,  rv**(-m*q)         , -rv**(m*q)       , -rv**(-m*q)       ],
		[0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  murV*rv**(m*q-1)   , -murV*rv**(-m*q-1)   , -murIV*rv**(m*q-1),  murIV*rv**(-m*q-1)],
		[0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  rvi**(m*q)      ,  rvi**(-m*q)      ]])

    B=np.array([[ 0 ],
		[ 0 ],
		[ 0 ],
		[ Gmr_AR_def(m,riii)],
		[ murII*dGmrdr_AR_def(m,riii)/(m*q)],
		[-Gmr_AR_def(m,riv)],
		[-murIV*dGmrdr_AR_def(m,riv)/(m*q)],
		[ 0 ],
		[ 0 ],
		[ 0 ]])

    X=np.linalg.solve(A,B)

    CmI_AR  =np.append(CmI_AR,  X[0,0])
    DmI_AR  =np.append(DmI_AR,  X[1,0])
    CmII_AR =np.append(CmII_AR, X[2,0])
    DmII_AR =np.append(DmII_AR, X[3,0])
    CmIII_AR=np.append(CmIII_AR,X[4,0])
    DmIII_AR=np.append(DmIII_AR,X[5,0])
    CmIV_AR =np.append(CmIV_AR, X[6,0])
    DmIV_AR =np.append(DmIV_AR, X[7,0])
    CmV_AR  =np.append(CmV_AR,  X[8,0])
    DmV_AR  =np.append(DmV_AR,  X[9,0])

  #############################################################
  # Repack the Stator Coefficient Solution into Matrix format #
  #############################################################

  Cm_AR=np.array([CmI_AR,CmII_AR,CmIII_AR,CmIV_AR,CmV_AR])
  Dm_AR=np.array([DmI_AR,DmII_AR,DmIII_AR,DmIV_AR,DmV_AR])

  #####################################
  # Initialise Stator Solution Arrays #
  #####################################
  Az_AR=np.zeros((r_range.size,phi_range.size))
  Br_AR=np.zeros((r_range.size,phi_range.size))
  Bt_AR=np.zeros((r_range.size,phi_range.size))
  Bmag_AR=np.zeros((r_range.size,phi_range.size))

########################################################################################
# Calculate a solution for each value of 'r' in 'r_range' due to the Armature Windings #
########################################################################################
def Tm_lorentz_def(t):
  tmp=0
  num=0
  global torque_calc_count
  torque_calc_count+=1
  for i, m in enumerate(m_PM_range):
    global print_count
    global torque_harmonics
    if print_count >= 1 and display_informatics:
        print "i = %d,  m = %d"%(i,m)
    if m in (3*m_AR_range-2): #corresponds with n=3k-2 for Jz harmonics, in my thesis
      num=((3*q*rn**2*N*l*Ip)/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      tmp+=num
      if print_count >= 1 and display_informatics:
	    torque_harmonics.append([m,num])    
    elif m in (3*m_AR_range-1): #corresponds with n=3k-1 for Jz harmonics, in my thesis
      num=((3*q*rn**2*N*l*Ip)/(((rn*hc)/rn)*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,(rn+hc/2)/rn)-int_bm_Br_III_PM_def(i,m,(rn-hc/2)/rn))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      tmp+=num
      if print_count >= 1 and display_informatics:
	    torque_harmonics.append([m,num])        
  if print_count >= 1 and display_informatics:
      print("torque_harmonics = ")
      print(torque_harmonics)

      print("\n(3*m_AR_range-2) =")
      print(3*m_AR_range-2)

      print("\n(3*m_AR_range-1) =")
      print(3*m_AR_range-1)

      #Sm function just printed for illustration purposes
      Sm_N0 = []
      Sm_N1 = []
      for k in range(0,20):
	Sm_N0.append(6*k+1)
      for k in range(1,20):
	Sm_N1.append(6*k-1)
	
      print("\nSm_N0 = ")
      print(Sm_N0)
      print("cos(m-1) = cos(?)")
      print(np.array(Sm_N0)-1)
	
      print("\nSm_N1 = ")
      print(Sm_N1)
      print("sin(m+1) = sin(?)")
      print(np.array(Sm_N1)+1)      

      print_count = print_count - 1

  #print "Torque_Lorentz = %g [Nm]"%(tmp)
  return tmp


if optimise_interface != 1:
  for i,r in enumerate(r_range):
    if r<rii:
      Region=I
    elif r<riii:
      Region=II
    elif r<riv:
      Region=III
    elif r<rv:
      Region=IV
    else:
      Region=V
    Az_AR[i]=Az_AR_def(r,t,Region)
    Br_AR[i]=Br_AR_def(r,t,Region)
    Bt_AR[i]=Bt_AR_def(r,t,Region)
    Bmag_AR[i]=np.sqrt(Br_AR[i]**2+Bt_AR[i]**2) #magnitude of both radial and tangetial components

if optimise_interface != 1:
  Brh_AR=np.abs(np.fft.fft(Br_AR[i_rn],M)*2/M)
  Bth_AR=np.abs(np.fft.fft(Bt_AR[i_rn],M)*2/M)

  Br1_AR=Brh_AR[1]
  Bt1_AR=Bth_AR[1]

  Az_total=Az_PM+Az_AR
  Br_total=Br_PM+Br_AR
  Bt_total=Bt_PM+Bt_AR
  Bmag_total=Bmag_PM+Bmag_AR

  ###
  Brmax_AR=Br_AR[i_rn].max()

  Br1_AR_range=np.array([])
  for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
    i=lookup(r,r_range,dr/2)
    Br1_AR_range=np.append(Br1_AR_range,np.abs(np.fft.fft(Br_AR[i],M)*2/M)[1])

  Bt1_AR_range=np.array([])
  for r in np.linspace((rn-hc/2)/rn,(rn+hc/2)/rn,20):
    i=lookup(r,r_range,dr/2)
    Bt1_AR_range=np.append(Bt1_AR_range,np.abs(np.fft.fft(Bt_AR[i],M)*2/M)[1])

  Br_AR_THD=np.sqrt(np.sum(Brh_AR[2:int((M)/2)]**2))/Br1_AR
  Bt_AR_THD=np.sqrt(np.sum(Bth_AR[2:int((M)/2)]**2))/Bt1_AR

  if debug == 1:
    print('Brmax_AR|rn   = %g T' % Brmax_AR)			#all harmonics included peak value @ rn
    print('Br1_AR|rn     = %g T' % Br1_AR)			#1st harmonic peak value @ rn
    print('Br1_AR|ave    = %g T' % np.average(Br1_AR_range))	#average of 1st harmonic peak values from rn-hc/2 to rn+hc/2
    print('THD(Br_AR|rn) = %g %%' % (Br_AR_THD*100,))
###

t_range=np.linspace(0,T,181)
#print t_range


lambda_PM=lambda_PM_def(rn/rn) #TODO: should it be rn, or rn/rn?
if calculate_AR_effect:
  lambda_AR=lambda_AR_def(rn/rn)
  lambda_AR=lambda_AR*Ls_k
  #lambda_AR=lambda_AR_def(rn)

#scale everything back to [m] values
lambda_PM = lambda_PM*rn
if calculate_AR_effect:
  lambda_AR = lambda_AR*rn
  #lambda_AR = lambda_AR

#m=1 if Type=='O' else 2

if optimise_interface != 1:
  lambda_PM_approx=2*(rn)*l*N*Br1_PM/a*kq*kw_pitch_def(1/kq)*kw_slot_def(1/kq) 
  lambda_AR_approx=(2*q*N*l/a)*kw_pitch_def(1)*kw_slot_def(1)*bm_Az_AR_def(0,1,rn/rn,Region=III)

EMK_PM=lambda_PM*omega_m*p
EMK_PM_turns=lambda_PM*omega_m_turns*p
if calculate_AR_effect:
  EMK_AR=lambda_AR*2*pi*f_e

if optimise_interface != 1 or calculate_AR_effect:
  EMK_PM_approx=lambda_PM_approx*omega_m*p

Tm_lorentz=np.array([])
for t in t_range:
  Tm_lorentz=np.append(Tm_lorentz,Tm_lorentz_def(t))
Tm_lorentz_mean=Tm_lorentz.mean()

#now that we have the induced voltage on the windings, we can determine the maximum number of allowed turns
if enable_turns_calculation == 1:
  #N = int(peak_voltage_allowed/EMK_PM)
  N = int(peak_terminal_phase_voltage_limit/EMK_PM_turns)
  if debug == 1:
    print("Initial turns allocation = %d"%(N))
  #print("Start: N = %d"%(N))
  if enable_target_mech_power == 1:
    #calculate torque using a non-specific current
    Ip_old = Ip
    Ip = 10

    torque_constant = Tm_lorentz_mean/Ip

    #calculate torque required to achieve 2kW mech target
    torque_target_turns = (target_mech_power_kW*1000*30/np.pi)/(turns_calculation_for_rpm)

    #calculate current required to achieve this target torque
    max_input_phase_current_turns = torque_target_turns/torque_constant

    #print("max_input_phase_current_turns = %f [A]"%(max_input_phase_current_turns))
    #print("Ip_old = %f [A]"%(Ip_old))

    Ip = Ip_old
else:
  N = force_this_number_of_turns

#Pcu_alt = Q*copper_resistivity*((J*(1000**2))**2)*coil_side_area*fill_factor*(stack_length+np.pi*end_winding_radius)
#J_alt = np.sqrt(Pcu_alt/(Q*copper_resistivity*coil_side_area*fill_factor*(stack_length+np.pi*end_winding_radius)*1000**4))


max_induced_phase_voltage = N*EMK_PM
max_input_phase_current = max_input_phase_current/N
if calculate_AR_effect:
  Ls = lambda_AR/max_input_phase_current
  if debug == 1:
    print("---------1---------")
    print("lambda_AR = %g"%lambda_AR)
    print("lambda_AR_approx = %g"%lambda_AR_approx)
    print("max_input_phase_current = %g"%max_input_phase_current)
    print("Ls = %g"%Ls)
    print("-------------------")
else:
  Ls=0

#this voltage and current is at another operating point (typically topspeed)
max_induced_phase_voltage_turns = N*EMK_PM_turns

if optimise_interface != 1:
  if debug == 1:
    print("\n(0) Wire_resistivity = %f [Ohm.m]  @ T = %d [Celsius Deg]"%(copper_resistivity,coil_temperature))
    #print("(0) Maximum allowed peak voltage for this design V_a(ln) = %f [V]"%peak_voltage_allowed)
    print("(0) Peak voltage induced in 1 turn V_a(ln) = %f [V/t]"%EMK_PM)
    print("\n(1) Maximum number of turns allowed N = %d turns"%N)
    print("(1) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
    print("(1) Peak input phase current Ia = %f [A]"%max_input_phase_current)

#CALCULATE I^2R CONDUCTION LOSSES (ANALYTICAL)

if enable_l_series_connections:
  coil_center_radius = 0.5*(CoilRadiusA+CoilRadiusB)
  l_series_connections = coils_in_series_per_phase*(3*coil_pitch*(coil_center_radius)) #technically should be (q-1) but over compensate for the lengths
else:
  l_series_connections = 0

l_coil = 2*(l+(pi*end_winding_radius))*N
l_total = ((coilsTotal/3)*l_coil)+l_series_connections

area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
Ra_analytical = (copper_resistivity*l_total)/(area_available_per_turn/1e6)
max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical + max_input_phase_current*w_e*Ls*1j

if enable_target_mech_power == 1 and enable_turns_calculation:
  max_input_phase_voltage_turns = max_induced_phase_voltage_turns + max_input_phase_current_turns*Ra_analytical
else:
  max_input_phase_voltage_turns = max_induced_phase_voltage_turns + max_input_phase_current*Ra_analytical

#print("2: max_input_phase_current = %f [A]"%(max_input_phase_current))
#if enable_target_mech_power == 1 and enable_turns_calculation:
  #print("2: max_input_phase_current_turns = %f [A]"%(max_input_phase_current_turns))
  #print("2: max_input_phase_voltage_turns = %f [V]"%(max_input_phase_voltage_turns))

if debug == 1:
  print("(1) Ra_analytical = %f [Ohm]"%Ra_analytical)
  print("(1) Peak system input phase voltage V_a(ln) = %f [V]"%abs(max_input_phase_voltage))

#this turns calculation could be for any other operating point
if enable_turns_calculation == 1:
  #now we check that this max_input_phase_voltage also falls below the maximum allowed system voltage
  #while max_input_phase_voltage > peak_terminal_phase_voltage_limit:

  #print("BEFORE: max_input_phase_voltage_turns = %f [V]"%(max_input_phase_voltage_turns))

  while abs(max_input_phase_voltage_turns) > peak_terminal_phase_voltage_limit and N>2:
    N = N - 1

    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
    max_induced_phase_voltage = N*EMK_PM
    max_induced_phase_voltage_turns = N*EMK_PM_turns

    max_input_phase_current = Ip/N
    if calculate_AR_effect:
      #Ls = lambda_AR/max_input_phase_current
      if debug == 1:
	print("---------2---------")
	print("lambda_AR = %g"%lambda_AR)
	print("lambda_AR_approx = %g"%lambda_AR_approx)
	print("max_input_phase_current = %g"%max_input_phase_current)
	print("Ls = %g"%Ls)
	print("-------------------")

    l_coil = 2*(l+(pi*end_winding_radius))*N
    l_total = ((Q/3)*l_coil)+l_series_connections

    Ra_analytical = (copper_resistivity*l_total)/(area_available_per_turn/1e6)
    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical + max_input_phase_current*w_e*Ls*1j
    if enable_target_mech_power == 1 and enable_turns_calculation:
      max_input_phase_voltage_turns = max_induced_phase_voltage_turns + max_input_phase_current_turns*Ra_analytical
    else:
      max_input_phase_voltage_turns = max_induced_phase_voltage_turns + max_input_phase_current*Ra_analytical

  #print("AFTER: max_input_phase_voltage_turns = %f [V]"%(max_input_phase_voltage_turns))

  if debug == 1:
    print("\n(2) Maximum number of turns allowed N = %d turns"%N)
    print("(2) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
    print("(2) Peak input phase current Ia = %f [A]"%max_input_phase_current)
    print("(2) Ra_analytical = %f [Ohm]"%Ra_analytical)
    print("(2) Peak system input phase voltage V_a(ln) = %f [V]"%abs(max_input_phase_voltage))

  #introduce contingency for 1 less turn during construction, thereby reducing the voltage.
  if enable_voltage_leeway == 1 and N>2:
    N = N - 1
    turnsPerCoil = N

    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/N
    max_induced_phase_voltage = N*EMK_PM
    max_input_phase_current = Ip/N
    if calculate_AR_effect:
      #Ls = lambda_AR/max_input_phase_current
      if debug == 1:
	print("---------3---------")
	print("lambda_AR = %g"%lambda_AR)
	print("lambda_AR_approx = %g"%lambda_AR_approx)
	print("max_input_phase_current = %g"%max_input_phase_current)
	print("Ls = %g"%Ls)
	print("-------------------")

    l_coil = 2*(l+(pi*end_winding_radius))*N
    l_total = ((coilsTotal/3)*l_coil)+l_series_connections

    Ra_analytical = (copper_resistivity*l_total)/(area_available_per_turn/1e6)
    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical + max_input_phase_current*w_e*Ls*1j

    if debug == 1:
      print("\n(3) Maximum number of turns allowed N = %d turns"%N)
      print("(3) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
      print("(3) Peak input phase current Ia = %f [A]"%max_input_phase_current)
      print("(3) Ra_analytical = %f [Ohm]"%Ra_analytical)
      print("(3) Peak system input phase voltage V_a(ln) = %f [V]"%abs(max_input_phase_voltage))


if enable_target_mech_power == 1:
  Ip = max_input_phase_current
  if calculate_AR_effect:
    #Ls = lambda_AR/max_input_phase_current
    if debug == 1:
      print("---------4---------")
      print("lambda_AR = %g"%lambda_AR)
      print("lambda_AR_approx = %g"%lambda_AR_approx)
      print("max_input_phase_current = %g"%max_input_phase_current)
      print("Ls = %g"%Ls)
      print("-------------------")

  torque_constant = Tm_lorentz_mean/Ip

  #calculate torque required to achieve 2kW mech target
  torque_target = (target_mech_power_kW*1000*30/np.pi)/(n_rpm)

  if Tm_lorentz_mean > torque_target:
    #calculate current required to achieve this target torque
    old_max_input_phase_current = max_input_phase_current
    max_input_phase_current = torque_target/torque_constant
    factor_decrease = max_input_phase_current/old_max_input_phase_current
    Ip = Ip*factor_decrease
    iq = iq*factor_decrease
    J = J*factor_decrease
    current_through_slot = current_through_slot*factor_decrease
    Tm_lorentz_mean=Tm_lorentz_mean*factor_decrease
    if calculate_AR_effect:
      #Ls = lambda_AR/max_input_phase_current
      if debug == 1:
	print("---------5---------")
	print("lambda_AR = %g"%lambda_AR)
	print("lambda_AR_approx = %g"%lambda_AR_approx)
	print("max_input_phase_current = %g"%max_input_phase_current)
	print("Ls = %g"%Ls)
	print("-------------------")
    if debug == 1:
      print("factor_decrease due to torque abundance= %g"%factor_decrease)
      print("New quantities due to factor_decrease")
      print("max_input_phase_current = %g"%max_input_phase_current)
      print("Ip = %g"%Ip)
      print("iq = %g"%iq)
      print("J = %g"%J)
      print("current_through_slot = %g"%current_through_slot)
      print("Tm_lorentz_mean = %g"%Tm_lorentz_mean)
      print("Now that the amount of current injected is decreased, we actually again need to determine if the turns per coil is optimal. It could be that we can have more turns now.")
	

P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical
if debug == 1:
  print("P_conductive_analytical = %g"%(P_conductive_analytical))
  print("copper_losses_allowed = %g"%(copper_losses_allowed))

if P_conductive_analytical > 1.0001*copper_losses_allowed and input_is_Jrms0_Pcu1_slotcurrent2 == 1:
  print("For some reason the copper losses were greater than was initially allowed")
  max_input_phase_current = np.sqrt((2*copper_losses_allowed)/(3*Ra_analytical))
  if calculate_AR_effect:
    #Ls = lambda_AR/max_input_phase_current
    if debug == 1:
      print("---------6---------")
      print("lambda_AR = %g"%lambda_AR)
      print("lambda_AR_approx = %g"%lambda_AR_approx)
      print("max_input_phase_current = %g"%max_input_phase_current)
      print("Ls = %g"%Ls)
      print("-------------------")
  max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical + max_input_phase_current*w_e*Ls*1j
  P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical

Ip = max_input_phase_current #this is important because Ip is sent to Tm_lorentz calculation after this
if debug == 1:
  print("(3) Total analytical conductive (copper) losses P_cu = %f [W]\n"%(P_conductive_analytical))

#determine line quantities
if three_phase_delta0_wye1 == 1:
  RMS_input_line_current = max_input_phase_current/np.sqrt(2)
  RMS_terminal_line_voltage = np.sqrt(3)*(abs(max_input_phase_voltage)/np.sqrt(2))
  peak_terminal_line_voltage = np.sqrt(3)*abs(max_input_phase_voltage)
else:
  RMS_input_line_current = np.sqrt(3)*(max_input_phase_current/np.sqrt(2))
  RMS_terminal_line_voltage = abs(max_input_phase_voltage)/np.sqrt(2)
  peak_terminal_line_voltage = abs(max_input_phase_voltage)

if optimise_interface != 1:
  Tm_lorentz_simple=np.array([])

def Tm_lorentz_simple_def(t):
  tmp=0
  for i, m in enumerate(m_PM_range):
  #for i, m in enumerate(m_range):
    if m in (3*m_AR_range-2):
    #if m in (3*m_range-2):
      #tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      #TODO: rn or rn/rn
      tmp+=(3*q*(rn)*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,(rn/rn),III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
    elif m in (3*m_AR_range-1):
    #elif m in (3*m_range-1):
      #tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      #TODO: rn or rn/rn
      tmp+=(3*q*(rn)*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,(rn/rn),III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
  return tmp

for t in t_range:
  if optimise_interface != 1:
    #Analytical Flux-linkage calculations for each phase as function of time due to the Permanent Magnets
    lambda_a_PM=lambda_PM*np.sin(omega_m*p*t_range)
    lambda_b_PM=lambda_PM*np.sin(omega_m*p*t_range-2*pi/3)
    lambda_c_PM=lambda_PM*np.sin(omega_m*p*t_range-4*pi/3)

    lambda_a_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range)
    lambda_b_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-2*pi/3)
    lambda_c_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-4*pi/3)

    #Analytical Flux-linkage calculations for each phase as function of time due to the Armature Reaction
    if calculate_AR_effect:
      lambda_a_AR=lambda_AR*np.cos(2*pi*f_e*t_range)
      lambda_b_AR=lambda_AR*np.cos(2*pi*f_e*t_range-2*pi/3)
      lambda_c_AR=lambda_AR*np.cos(2*pi*f_e*t_range-4*pi/3)

    e_a_PM=EMK_PM*np.cos(omega_m*p*t_range)
    e_b_PM=EMK_PM*np.cos(omega_m*p*t_range-2*pi/3)
    e_c_PM=EMK_PM*np.cos(omega_m*p*t_range-4*pi/3)

    e_a_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range)
    e_b_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-2*pi/3)
    e_c_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-4*pi/3)

    if calculate_AR_effect:
      e_a_AR=-EMK_AR*np.sin(omega_m*p*t_range)
      e_b_AR=-EMK_AR*np.sin(omega_m*p*t_range-2*pi/3)
      e_c_AR=-EMK_AR*np.sin(omega_m*p*t_range-4*pi/3)

    dphi=phi_range[1]-phi_range[0]

####################
# Determine torque #
####################

  #Tm_maxwell=np.append(Tm_maxwell,dphi*rn*l*hc/mu0*(Br_PM_def(rn,t,III)*Bt_AR_def(rn,t,III)-Br_AR_def(rn,t,III)*Bt_PM_def(rn,t,III)).sum())
  #FIXME: must be "+Br_AR_def(..."

  if optimise_interface != 1:
    Tm_lorentz_simple=np.append(Tm_lorentz_simple,Tm_lorentz_simple_def(t))

if optimise_interface != 1:
  #Calculate the Harmonic content of Tm_lorentz
  M=np.size(Tm_lorentz)-1
  #print(Tm_lorentz)
  Tmh_lorentz=np.abs(np.fft.fft(Tm_lorentz,M)/M)
  Tm0_lorentz=Tmh_lorentz[0]
  #print M
  #print(int(M/2))
  #print(Tmh_lorentz[2:int(M/2)])

  Tm_lorentz_THD=np.sqrt(np.sum(Tmh_lorentz[2:int((M)/2)]**2))/Tm0_lorentz

  #Calculate the Harmonic content of Tm_lorentz_simple

  M=np.size(Tm_lorentz_simple)-1
  Tmh_lorentz_simple=np.abs(np.fft.fft(Tm_lorentz_simple,M)/M)
  Tm0_lorentz_simple=Tmh_lorentz_simple[0]
  Tm_lorentz_simple_THD=np.sqrt(np.sum(Tmh_lorentz_simple[2:int((M)/2)]**2))/Tm0_lorentz_simple

  #Convert 's' into 'ms'
  t_range=t_range*1000
  T=T*1000

  #Average Torque calculations - Take I
  Tm=3/2*EMK_PM*Ip/omega_m

  #Average Torque calculations - Take II
  kT=kT_def(1/kq)
  Tm_kT=kT*Ip

  #print "Tm_lorentz_simple"
  #print Tm_lorentz_simple
  #print "Tm_lorentz"
  #print Tm_lorentz

  Tm_lorentz_simple_mean=Tm_lorentz_simple.mean()

  if save_csv:
    np.savetxt('sam_output.csv', np.transpose([t_range,Tm_lorentz]),delimiter = ',',newline='\n',header='time_stamp,Tm_lorentz',fmt='%8f')
  

torque_ripple = (max(Tm_lorentz)-min(Tm_lorentz))/Tm_lorentz_mean*100

#print "Tm_lorentz_simple"
#print Tm_lorentz_simple
#print "Tm_lorentz_simple_mean"
#print Tm_lorentz_simple_mean

if optimise_interface != 1:
  Tm_lorentz_simple_ripple=max(Tm_lorentz_simple-Tm_lorentz_simple_mean)-min(Tm_lorentz_simple-Tm_lorentz_simple_mean)
  Tm_lorentz_ripple=max(Tm_lorentz-Tm_lorentz_mean)-min(Tm_lorentz-Tm_lorentz_mean)

if debug == 1:
  if optimise_interface != 1:
    print('Tm=%g [Nm]' % (Tm))
    print('Tm|Lorentz_simple.mean=%g or %g [Nm]' % (Tm_lorentz_simple_mean, Tm0_lorentz_simple))
    print('Tm|Lorentz_simple.ripple=%g [Nm] or %g%%' % (Tm_lorentz_simple_ripple,Tm_lorentz_simple_ripple/Tm_lorentz_simple_mean*100))
    print('Tm|Lorentz_simple.THD=%g %%' % (Tm_lorentz_simple_THD*100))

    print('Tm|Lorentz.mean=%g or %g [Nm]' % (Tm_lorentz_mean,Tm0_lorentz))
    print('Tm|Lorentz.ripple=%g [Nm] or %g%%' % (Tm_lorentz_ripple,Tm_lorentz_ripple/Tm_lorentz_mean*100))
    print('Tm|Lorentz.THD=%g %%' % (Tm_lorentz_THD*100))


if save_flux_distribution == 1:
  i_rn=lookup(rn/rn,r_range,dr/2)
  i_rnmh2=lookup((rn-hc/2)/rn,r_range,dr/2)
  i_rnph2=lookup((rn+hc/2)/rn,r_range,dr/2)
  np.savetxt(dir_parent+'script_results/qhdrfapm/br.csv', np.transpose([Br_PM[i_rnmh2],Br_PM[i_rn],Br_PM[i_rnph2]]), delimiter = ',',newline='\n',header='Br @(rn-h/2), Br @(rn), Br @(rn+h/2)',fmt='%f')
  if calculate_AR_effect:
    np.savetxt(dir_parent+'script_results/qhdrfapm/br_AR_SAM.csv', np.transpose([Br_AR[i_rnmh2],Br_AR[i_rn],Br_AR[i_rnph2]]), delimiter = ',',newline='\n',header='Br @(rn-h/2), Br @(rn), Br @(rn+h/2)',fmt='%f')


if optimise_interface != 1 and show_figs:
  #pl.close('all')
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  #################
  # Contour Plots #
  #################

  #change these values back to their original values before AR solutions were found
  #riii=rn-hc/2
  #riv =rn+hc/2
  riii= innerMagnetRadiusB/rn
  riv = outerMagnetRadiusA/rn

  # Initialise x & y array
  x=np.zeros(np.shape(Az_PM))
  y=np.zeros(np.shape(Az_PM))

  # Convert r & phi values -> x & y for the polar plot representation
  for i, r in enumerate(r_range):
    x[i]=r*rn*np.cos(phi_range)
    y[i]=r*rn*np.sin(phi_range)

  #Function to draw the RFAPM machine in polar coordinates
  def draw_polar_machine(ax,draw_coils):
    inner_yoke=Wedge((0,0), rii*rn, -180/q, 180/q, width=hy_i, facecolor="grey", alpha=0.5)
    outer_yoke=Wedge((0,0), rvi*rn, -180/q, 180/q, width=hy_o, facecolor="grey", alpha=0.5)
    #Radial magnets
    inner_S_magnet_0=Wedge((0,0), riii*rn, degrees(beta),       -degrees(beta)+180/p, width=hmi_r, facecolor="red",  alpha=0.25)
    outer_S_magnet_0=Wedge((0,0), rv*rn,   degrees(beta),       -degrees(beta)+180/p, width=hmo_r, facecolor="red", alpha=0.25)
    inner_N_magnet_0=Wedge((0,0), riii*rn, degrees(beta)-180/p, -degrees(beta),       width=hmi_r, facecolor="blue", alpha=0.25)
    outer_N_magnet_0=Wedge((0,0), rv*rn,   degrees(beta)-180/p, -degrees(beta),       width=hmo_r, facecolor="blue",  alpha=0.25)
    #Azimuthal magnets
    inner_CW_magnet_0=Wedge((0,0), riii*rn,-degrees(beta),        degrees(beta),       width=hmi_r, facecolor="red",  alpha=0.25)
    outer_CCW_magnet_0=Wedge((0,0), rv*rn,  -degrees(beta),        degrees(beta),       width=hmo_r, facecolor="blue",  alpha=0.25)
    inner_CCW_magnet_0=Wedge((0,0), riii*rn,-degrees(beta)-180/p,        degrees(beta)-180/p,       width=hmi_r, facecolor="blue",  alpha=0.25)
    outer_CW_magnet_0=Wedge((0,0), rv*rn,-degrees(beta)-180/p,        degrees(beta)-180/p,       width=hmo_r, facecolor="red",  alpha=0.25)
    inner_CCW_magnet_1=Wedge((0,0), riii*rn,-degrees(beta)+180/p,        degrees(beta)+180/p,       width=hmi_r, facecolor="blue",  alpha=0.25)
    outer_CW_magnet_1=Wedge((0,0), rv*rn,-degrees(beta)+180/p,        degrees(beta)+180/p,       width=hmo_r, facecolor="red",  alpha=0.25)
    if p!=q:
      #Radial magnets
      inner_S_magnet_1=Wedge((0,0), riii*rn, degrees(beta)-360/p, -degrees(beta)-180/p, width=hmi_r, facecolor="red",  alpha=0.25)
      outer_S_magnet_1=Wedge((0,0), rv*rn,   degrees(beta)-360/p, -degrees(beta)-180/p, width=hmo_r, facecolor="red", alpha=0.25)
      inner_N_magnet_1=Wedge((0,0), riii*rn, degrees(beta)+180/p, -degrees(beta)+360/p, width=hmi_r, facecolor="blue", alpha=0.25)
      outer_N_magnet_1=Wedge((0,0), rv*rn,   degrees(beta)+180/p, -degrees(beta)+360/p, width=hmo_r, facecolor="blue",  alpha=0.25)
      #Azimuthal magnets
      inner_CW_magnet_1=Wedge((0,0), riii*rn,-degrees(beta)+360/p,min(180/q,degrees(beta)+360/p),       width=hmi_r, facecolor="red",  alpha=0.25)
      outer_CCW_magnet_1=Wedge((0,0), rv*rn,  -degrees(beta)+360/p,min(180/q,degrees(beta)+360/p),       width=hmo_r, facecolor="blue",  alpha=0.25)
      inner_CW_magnet_2=Wedge((0,0), riii*rn,max(-180/q,-degrees(beta)-360/p),degrees(beta)-360/p,       width=hmi_r, facecolor="red",  alpha=0.25)
      outer_CCW_magnet_2=Wedge((0,0), rv*rn,  max(-180/q,-degrees(beta)-360/p),degrees(beta)-360/p,       width=hmo_r, facecolor="blue",  alpha=0.25)
    if draw_coils:
      if Type=='O':
	coil_phase_A_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=hc, facecolor="red",    alpha=0.5)
	coil_phase_A_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=hc, facecolor="red",    alpha=0.5)
	coil_phase_B_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_B_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_C_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=hc, facecolor="blue",   alpha=0.5)
	coil_phase_C_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=hc, facecolor="blue",   alpha=0.5)
      elif Type=='I':
	coil_phase_A_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=hc, facecolor="red",    alpha=0.5)
	coil_phase_A_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=hc, facecolor="red",    alpha=0.5)
	coil_phase_B_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_B_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_C_pos=Wedge((0,0), riv*rn, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=hc, facecolor="blue",   alpha=0.5)
	coil_phase_C_neg=Wedge((0,0), riv*rn, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=hc, facecolor="blue",   alpha=0.5)
      elif Type=='II':
	coil_phase_A_pos=Wedge((0,0), (riv*rn)-g, -60/q,                     -60/q+2*degrees(Delta/q),  width=hc, facecolor="red",    alpha=0.5)
	coil_phase_A_neg=Wedge((0,0), (riv*rn)-g,  60/q-2*degrees(Delta/q),   60/q,                     width=hc, facecolor="red",    alpha=0.5)
	coil_phase_B_pos=Wedge((0,0), (riv*rn)-g,  60/q,                      60/q+2*degrees(Delta/q),  width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_B_neg=Wedge((0,0), (riv*rn)-g,  180/q-2*degrees(Delta/q),  180/q,                    width=hc, facecolor="yellow", alpha=0.5)
	coil_phase_C_pos=Wedge((0,0), (riv*rn)-g, -180/q,                    -180/q+2*degrees(Delta/q), width=hc, facecolor="blue",   alpha=0.5)
	coil_phase_C_neg=Wedge((0,0), (riv*rn)-g, -60/q-2*degrees(Delta/q),  -60/q,                     width=hc, facecolor="blue",   alpha=0.5)
      else:
	raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
    ax.add_patch(inner_yoke)
    ax.add_patch(outer_yoke)
    ax.add_patch(inner_N_magnet_0)
    ax.add_patch(outer_S_magnet_0)
    ax.add_patch(inner_S_magnet_0)
    ax.add_patch(outer_N_magnet_0)
    ax.add_patch(inner_CW_magnet_0)
    ax.add_patch(outer_CCW_magnet_0)
    ax.add_patch(inner_CCW_magnet_0)
    ax.add_patch(outer_CW_magnet_0)
    ax.add_patch(inner_CW_magnet_1)
    ax.add_patch(outer_CCW_magnet_1)
    ax.add_patch(inner_CCW_magnet_1)
    ax.add_patch(outer_CW_magnet_1)
    ax.add_patch(inner_CW_magnet_2)
    ax.add_patch(outer_CCW_magnet_2)
    if p!=q:
      ax.add_patch(inner_N_magnet_1)
      ax.add_patch(outer_S_magnet_1)
      ax.add_patch(inner_S_magnet_1)
      ax.add_patch(outer_N_magnet_1)
    if draw_coils:
      ax.add_patch(coil_phase_A_pos)
      ax.add_patch(coil_phase_A_neg)
      ax.add_patch(coil_phase_B_pos)
      ax.add_patch(coil_phase_B_neg)
      ax.add_patch(coil_phase_C_pos)
      ax.add_patch(coil_phase_C_neg)
    ax.set_aspect('equal')


  if show_figs or save_figs == True:
    ###############################################################
    # Plot Magnetic Vector Potential contour plot in polar format #
    ###############################################################
    if plot_PM:
      pl.figure(figsize=contour_figsize)
      CS=pl.contour(x,y,Az_PM*1000,40)
      draw_polar_machine(pl.gca(),draw_coils)
      pl.clabel(CS, inline=True, fontsize=5, fmt='%g')
      cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
      cb.set_label(r'Magnetic vector potential, $A_{z}$ [mWb/m]')
      #pl.axis(xmin=0.05,xmax=0.2,ymin=-0.08,ymax=0.08)
      pl.xlabel(r'$x$ [m]')
      pl.ylabel(r'$y$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the PM's Magnetic Vector Potential")
      else:
	pl.savefig('Az_xy.pdf', transparent=True, bbox_inches='tight')

    if plot_AR:
      pl.figure(figsize=contour_figsize)
      CS=pl.contour(x,y,Az_AR*1000,40)
      draw_polar_machine(pl.gca(),draw_coils=True)
      pl.clabel(CS, inline=True, fontsize=5, fmt='%g')
      cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
      cb.set_label(r'Magnetic Vector potential, $A_{z|AR}$ [mWb/m]')
      pl.xlabel(r'$x$ [m]')
      pl.ylabel(r'$y$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the Windings' Magnetic Vector Potential")
      else:
	pl.savefig('Az_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')



    ###########################################################
    # Plot Magnetic Flux Density contour plot in polar format #
    ###########################################################
    if plot_PM:
      pl.figure(figsize=contour_figsize)
      CS=pl.contour(x,y,Bmag_PM,40)
      CS=pl.contourf(x,y,Bmag_PM,40)
      draw_polar_machine(pl.gca(),draw_coils)
      cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.2f')
      cb.set_label(r'Flux density, $B_{mag}$ [T]')
      pl.xlabel(r'$x$ [m]')
      pl.ylabel(r'$y$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the PM's Magnetic Flux Density Magnitude")
      else:
	pl.savefig('Bmag_xy.pdf',transparent=True, bbox_inches='tight')

    if plot_AR:
      pl.figure(figsize=contour_figsize)
      CS=pl.contour(x,y,Bmag_AR*1000,40)
      CS=pl.contourf(x,y,Bmag_AR*1000,40)
      draw_polar_machine(pl.gca(),draw_coils=True)
      cb=pl.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.1f')
      cb.set_label(r'Flux density, $B_{|AR}$ [mT]')
      pl.xlabel(r'$x$ [m]')
      pl.ylabel(r'$y$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the Windings' Magnetic Flux Density Magnitude")
      else:
	pl.savefig('Bmag_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if linear_repr:
      ##############################################################################
      # Function to draw the RFAPM machine in a linearised representational format #
      ##############################################################################
      def draw_linearised_machine(ax):
	inner_yoke=Rectangle((-180/q, ri*rn), 360/q, hy_i, facecolor="grey", alpha=0.5)
	outer_yoke=Rectangle((-180/q, rv*rn), 360/q, hy_o, facecolor="grey", alpha=0.5)
	inner_S_magnet_0=Rectangle((degrees(beta)-180/p, rii*rn),    km*180/p, hmi_r, facecolor="red", alpha=0.25)
	outer_S_magnet_0=Rectangle((degrees(beta)-180/p, riv*rn+g), km*180/p, hmo_r, facecolor="red",  alpha=0.25)
	inner_N_magnet_0=Rectangle((degrees(beta),       rii*rn),    km*180/p, hmi_r, facecolor="blue",  alpha=0.25)
	outer_N_magnet_0=Rectangle((degrees(beta),       riv*rn+g), km*180/p, hmo_r, facecolor="blue", alpha=0.25)
	if p!=q:
	  inner_S_magnet_1=Rectangle((degrees(beta)-360/p, rii*rn),    km*180/p, hmi_r, facecolor="red",  alpha=0.25)
	  outer_S_magnet_1=Rectangle((degrees(beta)-360/p, riv*rn+g), km*180/p, hmo_r, facecolor="red", alpha=0.25)
	  inner_N_magnet_1=Rectangle((degrees(beta)+180/p, rii*rn),    km*180/p, hmi_r, facecolor="blue", alpha=0.25)
	  outer_N_magnet_1=Rectangle((degrees(beta)+180/p, riv*rn+g), km*180/p, hmo_r, facecolor="blue",  alpha=0.25)
	if draw_coils:
	  if Type=='O':
	    coil_phase_A_pos=Rectangle(( -90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_A_neg=Rectangle((  90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_B_pos=Rectangle((-150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_B_neg=Rectangle((  30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_C_pos=Rectangle(( -30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    coil_phase_C_neg=Rectangle(( 150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	  elif Type=='I':
	    coil_phase_A_pos=Rectangle(( -30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_A_neg=Rectangle((  30/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_B_pos=Rectangle((  90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_B_neg=Rectangle(( 150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_C_pos=Rectangle((-150/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    coil_phase_C_neg=Rectangle(( -90/q-degrees(Delta/q),   riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	  elif Type=='II':
	    coil_phase_A_pos=Rectangle(( -60/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_A_neg=Rectangle((  60/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="red",    alpha=0.5)
	    coil_phase_B_pos=Rectangle((  60/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_B_neg=Rectangle(( 180/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="yellow", alpha=0.5)
	    coil_phase_C_pos=Rectangle((-180/q,                    riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	    coil_phase_C_neg=Rectangle(( -60/q-2*degrees(Delta/q), riii*rn),   2*degrees(Delta/q), hc,  facecolor="blue",   alpha=0.5)
	  else:
	    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
	ax.add_patch(inner_yoke)
	ax.add_patch(outer_yoke)
	ax.add_patch(inner_N_magnet_0)
	ax.add_patch(outer_S_magnet_0)
	ax.add_patch(inner_S_magnet_0)
	ax.add_patch(outer_N_magnet_0)
	if p!=q:
	  ax.add_patch(inner_N_magnet_1)
	  ax.add_patch(outer_S_magnet_1)
	  ax.add_patch(inner_S_magnet_1)
	  ax.add_patch(outer_N_magnet_1)
	if draw_coils:
	  ax.add_patch(coil_phase_A_pos)
	  ax.add_patch(coil_phase_A_neg)
	  ax.add_patch(coil_phase_B_pos)
	  ax.add_patch(coil_phase_B_neg)
	  ax.add_patch(coil_phase_C_pos)
	  ax.add_patch(coil_phase_C_neg)
	ax.set_aspect('equal')

      #Meshgrid for linearised contour plot
      phi,r=np.meshgrid(np.degrees(phi_range),r_range)

    if plot_PM and linear_repr:
      #################################################################
      # Plot Magnetic Vector Potential contour plot in rectangular format #
      #################################################################
      pl.figure()
      CS=pl.contour(phi,r,Az_PM,50)
      ax=pl.gca()
      draw_linearised_machine(ax)
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
      pl.axis([-180/q,180/q,ri*rn,rvi*rn])
      pl.clabel(CS, inline=True, fontsize=5, fmt='%g')
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'$r$ [m]')
      if not save_figs:
	  pl.title(r"Contour plot of the PM's Magnetic Vector Potential")
      else:
	pl.savefig('/Az_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_AR and linear_repr:
      #################################################################
      # Plot Magnetic Vector Potential contour plot in rectangular format #
      #################################################################
      pl.figure()
      CS=pl.contour(phi,r,Az_AR,50)
      ax=pl.gca()
      draw_linearised_machine(ax)
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
      pl.axis([-180/q,180/q,ri*rn,rvi*rn])
      pl.clabel(CS, inline=True, fontsize=5, fmt='%g')
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'$r$ [m]')
      if not save_figs:
	  pl.title(r"Contour plot of the Windings' Magnetic Vector Potential")
      else:
	pl.savefig('/Az_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_PM and linear_repr:
      #################################################################
      # Plot Magnetic Flux Density contour plot in rectangular format #
      #################################################################
      pl.figure()
      CS=pl.contour(phi,r,Bmag_PM,40)
      CS=pl.contourf(phi,r,Bmag_PM,50)
      ax=pl.gca()
      draw_linearised_machine(ax)
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
      pl.axis([-180/q,180/q,ri*rn,rvi*rn])
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'$r$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the PM's Magnetic Flux Density Magnitude")
      else:
	pl.savefig('Bmag_rt_PM.pdf')

    if plot_AR and linear_repr:
      #################################################################
      # Plot Magnetic Flux Density contour plot in rectangular format #
      #################################################################
      pl.figure()
      CS=pl.contour(phi,r,Bmag_AR,40)
      CS=pl.contourf(phi,r,Bmag_AR,50)
      ax=pl.gca()
      draw_linearised_machine(ax)
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      ax.set_yticks(np.linspace(ri*rn,rvi*rn,7))
      pl.axis([-180/q,180/q,ri*rn,rvi*rn])
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'$r$ [m]')
      if not save_figs:
	pl.title(r"Contour plot of the Windings' Magnetic Flux Density Magnitude")
      else:
	pl.savefig('Bmag_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_current_density:
      #####################################
      # Plot Current Density Distribution #
      #####################################
      pl.figure()
      pl.plot(np.degrees(phi_range),Jz_def(Ip,t))
      ax=pl.gca()
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'$J_z$ [A$/$m$^2$]')
      pl.axis(xmin=-180/q,xmax=180/q)
      pl.grid(True)
      if not save_figs:
	pl.title('Current Density Distribution')
      else:
	pl.savefig('Jz-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    ##########################
    # Plot the Flux-linkages #
    ##########################
    #Print important flux-linkage and induced voltage info
    if debug == 1:
      print('lambda_PM=%g [Wbt]' % (abs(lambda_PM)))
      print('lambda_PM_approx=%g [Wbt]' % (lambda_PM_approx))

    pl.figure()
    pl.plot(t_range,lambda_a_PM,label=r'$\lambda_{a|PM}(t)$')
    pl.plot(t_range,lambda_b_PM,label=r'$\lambda_{b|PM}(t)$')
    pl.plot(t_range,lambda_c_PM,label=r'$\lambda_{c|PM}(t)$')

    ax=pl.gca()
    ax.set_xticks(np.linspace(0,T,11))
    pl.xlabel(r'Time, $t$ [ms]')
    pl.ylabel(r'Flux linkage, $\lambda_{|PM}$ [Wbt]')
    pl.legend()
    #pl.axis(xmin=0,xmax=T,ymin=-2,ymax=2)
    pl.grid(True)
    if not save_figs:
      pl.title('Flux-linkage due to the Permanent Magnets')
    else:
      pl.savefig('lambda_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_approx:
      #Add the flux-linkage approximations
      pl.figure()
      pl.plot(t_range,lambda_a_PM_approx,label=r'$\lambda_{a|PM}(t)$ (Approx.)')
      pl.plot(t_range,lambda_b_PM_approx,label=r'$\lambda_{b|PM}(t)$ (Approx.)')
      pl.plot(t_range,lambda_c_PM_approx,label=r'$\lambda_{c|PM}(t)$ (Approx.)')

      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.xlabel(r'Time, $t$ [ms]')
      pl.ylabel(r'Flux linkage, $\lambda_{|PM}$ [Wbt]')
      pl.legend()
      #pl.axis(xmin=0,xmax=T,ymin=-2,ymax=2)
      pl.grid(True)
      if not save_figs:
	pl.title('Approximate Flux-linkage due to the Permanent Magnets')
      else:
	pl.savefig('lambda_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    #########################
    # Plot Induced Voltages #
    #########################
    pl.figure()
    pl.plot(t_range,e_a_PM,linewidth=1,label=r'$e_{a|PM}(t)$')
    pl.plot(t_range,e_b_PM,linewidth=1,label=r'$e_{b|PM}(t)$')
    pl.plot(t_range,e_c_PM,linewidth=1,label=r'$e_{c|PM}(t)$')

    ax=pl.gca()
    ax.set_xticks(np.linspace(0,T,11))
    pl.xlabel(r'Time, $t$ [ms]')
    pl.ylabel(r'Back EMF, $e_{|PM}$ [V]')
    pl.legend()
    #pl.axis(xmin=0,xmax=T,ymin=-1000,ymax=1000)
    pl.axis(xmin=0,xmax=T)
    pl.grid(True)
    if not save_figs:
      pl.title('Induced Voltages due to the Permanent Magnets')
    else:
      pl.savefig('e_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_approx:
      #Add the induced voltage approximations
      pl.figure()
      pl.plot(t_range,e_a_PM_approx,linewidth=1,label=r'$e_{a|PM}(t)$ (Approx.)')
      pl.plot(t_range,e_b_PM_approx,linewidth=1,label=r'$e_{b|PM}(t)$ (Approx.)')
      pl.plot(t_range,e_c_PM_approx,linewidth=1,label=r'$e_{c|PM}(t)$ (Approx.)')

      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.xlabel(r'Time, $t$ [ms]')
      pl.ylabel(r'Back EMF, $e_{|PM}$ [V]')
      pl.legend()
      #pl.axis(xmin=0,xmax=T,ymin=-1000,ymax=1000)
      pl.axis(xmin=0,xmax=T)
      pl.grid(True)
      if not save_figs:
	pl.title('Induced Voltages due to the Permanent Magnets')
      else:
	pl.savefig('e_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_AR and calculate_AR_effect:
      ############################################################################
      # Plot the Flux-linkages and Induced Voltages due to the Armature Reaction #
      ############################################################################
      #Plot the Flux-linkages
      pl.figure()
      pl.plot(t_range,lambda_a_AR,label=r'$\lambda_{a|AR}(t)$')
      pl.plot(t_range,lambda_b_AR,label=r'$\lambda_{b|AR}(t)$')
      pl.plot(t_range,lambda_c_AR,label=r'$\lambda_{c|AR}(t)$')
      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.xlabel(r'Time, $t$ [ms]')
      pl.ylabel(r'Flux-linkage, $\lambda$ [Wbt]')
      pl.legend()
      #pl.axis(xmin=0,xmax=T,ymax=0.06,ymin=-0.06)
      pl.axis(xmin=0,xmax=T)
      pl.grid(True)
      if not save_figs:
	pl.title('Flux-linkage due to the Armature Windings')
      else:
	pl.savefig('lambda_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      #Plot the Induced Voltages
      pl.figure()
      pl.plot(t_range,e_a_AR,label=r'$e_{a|AR}(t)$')
      pl.plot(t_range,e_b_AR,label=r'$e_{b|AR}(t)$')
      pl.plot(t_range,e_c_AR,label=r'$e_{c|AR}(t)$')
      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.xlabel(r'Time, $t$ [ms]')
      pl.ylabel(r'$e(t)$~[V]')
      pl.legend()
      pl.axis(xmin=0,xmax=T)
      pl.grid(True)
      if not save_figs:
	pl.title('Induced Voltages due to the Armature Windings')
      else:
	pl.savefig('e_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')




    if plot_PM:
      #####################################################################################
      # Plot the Radial Flux Density Variation in the Stator due to the Permanent Magnets #
      #####################################################################################
      pl.figure()
      #Br_semfem=np.loadtxt("../semfem/br.csv",skiprows=1,delimiter=",")
      #phi_range_semfem=np.linspace(-pi/q,pi/q,np.size(Br_semfem[:,0]))

      #print "i_rnmh2"
      #print i_rnmh2
      #print r_range[i_rnmh2]
      #print "i_rn"
      #print i_rn
      #print r_range[i_rn]
      #print "i_rnph2"
      #print i_rnph2
      #print r_range[i_rnph2]

      pl.plot(np.degrees(phi_range),Br_PM[i_rnmh2],'b',linewidth=LINEWIDTH*2,label=r'$B_{r|r_n-h/2}$')
      #pl.plot(np.degrees(phi_range_semfem),Br_semfem[:,0],'b',linewidth=LINEWIDTH,label=r'$B_{r|r_n-h/2}$ \small (SEMFEM)')
      pl.plot(np.degrees(phi_range),Br_PM[i_rn],'g',linewidth=LINEWIDTH*2,label=r'$B_{r|r_{n}}$')
      #pl.plot(np.degrees(phi_range_semfem),Br_semfem[:,1],'g',linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ \small  (SEMFEM)')
      pl.plot(np.degrees(phi_range),Br_PM[i_rnph2],'r',linewidth=LINEWIDTH*2,label=r'$B_{r|r_n+h/2}$')
      #pl.plot(np.degrees(phi_range_semfem),Br_semfem[:,2],'r',linewidth=LINEWIDTH,label=r'$B_{r|r_n+h/2}$ \small  (SEMFEM)')

      ax=pl.gca()
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'Radial flux density, $B_{r}$ [T]')
      pl.legend(loc='upper left')
      pl.axis(xmin=-180/q,xmax=180/q)
      pl.grid(True)
      if not save_figs:
	pl.title(r'$B_r(\phi)$ in stator due to the Permanent Magnets')
      else:
	pl.savefig('Br_phi.pdf',transparent=True, bbox_inches='tight')

    if plot_AR:
      ########################################################################################################
      # Plot the Radial Flux Density Variation in the Stator and Magnet Centers due to the Armature Windings #
      ########################################################################################################
      pl.figure()

      #print(np.shape(phi_range))
      #print(np.shape(Br_AR[i_rcim][0][0]))

      pl.plot(np.degrees(phi_range),Br_AR[i_rcim][0][0],label=r'$B_{r|r_{cim},AR}$') #FIXME: [0][0] was not orignally here
      pl.plot(np.degrees(phi_range),Br_AR[i_rn],label=r'$B_{r|r_{n},AR}$')
      pl.plot(np.degrees(phi_range),Br_AR[i_rcom][0][0],label=r'$B_{r|r_{com},AR}$') #FIXME: [0][0] was not orignally here

      ax=pl.gca()
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'Radial flux density, $B_{r|AR}$ [T]')
      pl.legend(loc='upper left')
      pl.axis(xmin=-180/q,xmax=180/q)
      pl.grid(True)
      if not save_figs:
	pl.title(r'$B_r(\phi)$ in stator and magnet centers due to the Armature windings')
      else:
	pl.savefig('Br_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_PM:
      ################################################################
      # Plot the Azimuthal Flux Density due to the Permanent Magnets #
      ################################################################
      #Plot the Azimuthal Flux Density due to the Permanent Magnets
      pl.figure()
      pl.plot(np.degrees(phi_range),Bt_PM[i_rcim][0][0],label=r'$B_{\phi|r_{cim},PM}$')
      pl.plot(np.degrees(phi_range),Bt_PM[i_rn],label=r'$B_{\phi|r_{n},PM}$')
      pl.plot(np.degrees(phi_range),Bt_PM[i_rcom][0][0],label=r'$B_{\phi|r_{com},PM}$')

      ax=pl.gca()
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'Azimuthal flux density, $B_{{\phi}|PM}$ [T]')
      pl.legend()
      pl.axis(xmin=-180/q,xmax=180/q)
      pl.grid(True)
      if not save_figs:
	pl.title(r'$B_t(\phi)$ in stator and magnet centers due to the Permanent Magnets')
      else:
	pl.savefig('Bt_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      #TOTAL FLUX DENSITY MAGNITUDE IN YOKES
      #Plot the Magnitude of the Flux Density Variation in the Yokes due to the Permanent Magnets
      pl.figure()
      pl.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rciy][0][0]**2+Bt_PM[i_rciy][0][0]**2),label=r'$B_{mag|PM,r_{ciy}}$')
      pl.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rcoy][0][0]**2+Bt_PM[i_rcoy][0][0]**2),label=r'$B_{mag|PM,r_{coy}}$')

      ax=pl.gca()
      ax.set_xticks(np.linspace(-180/q,180/q,9))
      pl.xlabel(r'Angle, $\phi$ [$^\circ$]')
      pl.ylabel(r'Magntitude of Flux density, $B_{mag|PM}$ [T]')
      pl.legend(loc='upper left')
      pl.axis(xmin=-180/q,xmax=180/q)
      pl.grid(True)
      if not save_figs:
	pl.title(r'Magnitude of flux density $B_{mag}(\phi)$  in yokes due to the Permanent Magnets')
      else:
	pl.savefig('Bmag_yokes_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

    if plot_torque:
      #if Type=='O':
	#Tmin=290
	#Tmax=340
      #elif Type=='I':
	#Tmin=175
	#Tmax=200
      #else:
	#Tmin=200
	#Tmax=225

      #Plot the Torque calculated using the simplified Lorentz's Method vs. Ansoft Maxwell 2D
      pl.figure()
      pl.plot(t_range,np.abs(Tm_lorentz_simple),'-',linewidth=1,label=r'Using the Simplified Lorentz Method')
      #pl.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
      if not save_figs: pl.title(r'Torque -- Type %s' % Type)
      pl.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
      pl.xlabel(r'Time, $t$ [ms]')
      pl.ylabel(r'$T_{mech}$ [Nm]')
      pl.legend(loc='best')
      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.grid(True)
      if not save_figs:
	pl.title(r"Using the simplified Lorentz's method")
      else:
	pl.savefig('Tm_lorentz_simple-%s.pdf' % Type, transparent=True, bbox_inches='tight')

      #Plot the Torque calculated using the Lorentz's Method vs. Ansoft Maxwell 2D
      pl.figure()
      #print "np.shape"
      #print np.shape(t_range)
      #print np.shape(Tm_lorentz)
      pl.plot(t_range,np.abs(Tm_lorentz),'-',linewidth=1,label=r'Using the Lorentz Method')
      #pl.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
      if not save_figs: pl.title(r'Torque -- Type %s' % Type)
      pl.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
      pl.xlabel(r'Time, $t$ (ms)')
      pl.ylabel(r'Mechanical Torque, $T_{mech}$ (Nm)')
      pl.legend(loc='best')
      ax=pl.gca()
      ax.set_xticks(np.linspace(0,T,11))
      pl.grid(True)
      if not save_figs:
	pl.title(r"Using the Lorentz's Method")
      else:
	pl.savefig('Tm_lorentz-%s.pdf' % Type, transparent=True, bbox_inches='tight')


      #Plot the harmonics
      pl.figure()
      if Type=='II':
	bar_width=0.25
      else:
	bar_width=0.33
      h_range=np.arange(1,20)
      pl.bar(h_range,100*Tmh_lorentz[h_range]/abs(Tm_lorentz_mean),bar_width,color='b',label='Analytical')
      ax=pl.gca()
      ax.set_xticks(h_range)
      pl.grid(True)
      pl.xlabel(r'Harmonic Number')
      pl.ylabel(r'$\frac{T_{mech|h}}{T_{mech|ave.}}\times 100$~\%')
      pl.legend(loc='best')
      if not save_figs:
	pl.title(r'$T_{mh}$')
      else:
	pl.savefig('Tmh-%s.pdf' % Type, transparent=True, bbox_inches='tight')


  if show_figs: pl.show()


if optimise_interface != 1:
  K1=kq*sin(kDelta*pi/(6*kq))*kw_pitch_def(int(1/kq))

  if debug == 1:
    print('K1=%g' % K1)

  if Type=='O':
    le=2*pi*rn/q+2*h
  elif Type=='I':
    le=pi/Q*rn*pi/2
  elif Type=='II':
    le=(2*pi/Q-2*Delta/q)*rn*pi/2
  else:
    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

  ke=1+le/l

  K2=ke*kDelta

  if debug == 1:
    print('K2=%g' % K2)

  #############################
  # Print results to terminal #
  #############################
  if debug == 1:
    print('Brmax_PM|rn   = %g T' % Brmax_PM)			#all harmonics included peak value @ rn
    print('Br1_PM|rn     = %g T' % Br1_PM)			#1st harmonic peak value @ rn
    print('Br1_PM|ave    = %g T' % np.average(Br1_PM_range))	#average of 1st harmonic peak values from rn-hc/2 to rn+hc/2
    print('THD(Br_PM|rn) = %g %%' % (Br_PM_THD*100,))
    print('Done!')

  #debugging
  #print('Br_avg = %g T' % np.average(Br))

##################
# Calculate mass #
##################

#Assuming NdFeB N48 grade magnets
magnet_density=7500 #[kg/m^3]
copper_density=8950 #[kg/m^3] #some sources say 8940 and some say 8960
aluminium_density=2700 #[kg/m^3]
tufnol_density=1360 #[kg/m^3]

inner_magnets_mass = magnet_density*((np.pi*innerMagnetRadiusB**2)-(np.pi*innerMagnetRadiusA**2))*l
outer_magnets_mass = magnet_density*((np.pi*outerMagnetRadiusB**2)-(np.pi*outerMagnetRadiusA**2))*l
magnet_mass = inner_magnets_mass + outer_magnets_mass

single_coil_mass = copper_density*fill_factor*2*coil_side_area*l #coil_side_area is only one side of a single coil
copper_mass = Q*single_coil_mass

aluminium_mass = 0

if enable_shell_mass == 1:
  rotor_shell_mass = np.pi*((rotor_shell_height+ro)**2 - ro**2)*l*aluminium_density
  stator_shell_mass = np.pi*(ri_original**2 - (ri_original-stator_shell_height)**2)*l*aluminium_density
  aluminium_mass = aluminium_mass + rotor_shell_mass + stator_shell_mass

stator_area = (CoilRadiusB**2 - CoilRadiusA**2)*np.pi
tufnol_mass = (stator_area - 2*coil_side_area*Q)*l*tufnol_density

total_mass = magnet_mass+copper_mass+aluminium_mass+tufnol_mass

if debug == 1:
  print("inner_magnets_mass = %f [kg]"%inner_magnets_mass)
  print("outer_magnets_mass = %f [kg]"%outer_magnets_mass)

##################################
# Calculate Power and Efficiency #
##################################
P_core_loss = 0
P_eddy = 0
P_wf = 0
P_losses = P_conductive_analytical + P_core_loss +  + P_wf

P_out = (n_rpm*(np.pi/30))*Tm_lorentz_mean
P_in = P_out + P_losses
efficiency = (P_out/P_in)*100

S_in = (3/2)*max_input_phase_voltage*max_input_phase_current #rms apparant power injected into terminals
power_factor_method_1 = np.cos(to_pd(max_input_phase_voltage)[1]*(np.pi/180))
power_factor_method_2 = P_in/abs(S_in)

if debug:
  print("power_factor_method_1 = %g"%power_factor_method_1)
  print("power_factor_method_2 = %g"%power_factor_method_2)

active_mass_torque_density = Tm_lorentz_mean/(magnet_mass+copper_mass)
total_mass_torque_density = Tm_lorentz_mean/total_mass

#################
# Print results #
#################
if three_phase_delta0_wye1 == 0:
  addToTable("DELTA connection",three_phase_delta0_wye1, "", True,dir_parent) 	#phase A max induced voltage
else:
  addToTable("WYE connection",three_phase_delta0_wye1, "", True,dir_parent) 	#phase A max induced voltage

addToTable("peak_terminal_phase_voltage_limit",peak_terminal_phase_voltage_limit, "[V]", True,dir_parent) 	#phase A max induced voltage
addToTable("RMS input phase current", max_input_phase_current/np.sqrt(2), "[A]", True,dir_parent)
addToTable("Max input phase current", max_input_phase_current, "[A]", True,dir_parent)
addToTable("Max induced phase voltage",max_induced_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
addToTable("Max input phase voltage (abs)",to_pd(max_input_phase_voltage)[0], "[V]", True,dir_parent) 	#phase A max induced voltage
addToTable("Max input phase voltage (deg)",to_pd(max_input_phase_voltage)[1], "[deg]", True,dir_parent) 	#phase A max induced voltage
addToTable("Max flux linkage (PM) (for original input current)",lambda_PM*1000, "[mWb]", True,dir_parent)
if optimise_interface != 1:
  addToTable("Max flux linkage (PM) Approx (for original input current)",lambda_PM_approx*1000, "[mWb]", True,dir_parent)
if calculate_AR_effect:
  addToTable("Max flux linkage (AR) (for original input current)",lambda_AR*1000, "[mWb]", True,dir_parent)
  addToTable("Max flux linkage (AR) Approx (for original input current)",lambda_AR_approx*1000, "[mWb]", True,dir_parent)
addToTable("******Lorentz average torque produced******",Tm_lorentz_mean,"[Nm]", True,dir_parent)
addToTable("Torque Constant (Tm_lorentz_mean/max_input_phase_current)",Tm_lorentz_mean/max_input_phase_current,"[Nm/A]", True,dir_parent)
addToTable("Torque Constant (Tm_lorentz_mean/current_density_rms)",Tm_lorentz_mean/(J/np.sqrt(2)),"[Nm/(A/mm^2)]", True,dir_parent)
if optimise_interface != 1:
  addToTable("Simplified Lorentz average torque produced",Tm_lorentz_simple_mean,"[Nm]", True ,dir_parent)
if calculate_AR_effect:  
  addToTable("Synchronous Inductance (Ls)",Ls*1e6,"[uH]", True ,dir_parent)
addToTable("Copper mass",copper_mass,"[kg]", True,dir_parent)
addToTable("*****************Magnet mass***************",magnet_mass,"[kg]", True,dir_parent)
addToTable("Tufnol mass",tufnol_mass,"[kg]", True,dir_parent)
if enable_shell_mass == 1:#IDRFPM shell/drum doubles-up as the yoke as well.
  pretty_terminal_table.add_row(["aluminium_mass",aluminium_mass,"[kg]"])
  pretty_terminal_table.add_row(["---rotor_shell/yoke_mass",rotor_shell_mass,"[kg]"])
  pretty_terminal_table.add_row(["---stator_shell/yoke_mass",stator_shell_mass,"[kg]"])
addToTable("Total mass",total_mass,"[kg]", True,dir_parent)
addToTable("Phase resistance",Ra_analytical,"[Ohm]", True ,dir_parent)
addToTable("Phase resistance (analytical)",Ra_analytical,"[Ohm]", True,dir_parent)
addToTable("Electrical frequency",f_e,"[Hz]", True,dir_parent)
addToTable("Machine speed",n_rpm,"[rpm]", True,dir_parent)
addToTable("****************Efficiency****************",efficiency,"[%]", True,dir_parent)
addToTable("Output Torque/Mass Ratio",(Tm_lorentz_mean/(magnet_mass+copper_mass)),"[Nm/kg]", True,dir_parent)
addToTable("Output Torque/PM Mass Ratio",(Tm_lorentz_mean/magnet_mass),"[Nm/kg]", True,dir_parent)
addToTable("Input Apparant Power (S_in)",abs(S_in),"[VA]", True,dir_parent)
addToTable("Input Real Power (P_in)",P_in,"[W]", True,dir_parent)
addToTable("Output Mechanical Power",P_out,"[W]", True,dir_parent)
addToTable("Conductive loss (analytical with end-windings)",P_conductive_analytical,"[W]", True,dir_parent)
addToTable('Eddy current losses',P_eddy,'[W]', enable_eddy_losses_calc == 1,dir_parent)
addToTable("Wind & Friction losses",P_wf,"[W]", True,dir_parent)
addToTable("Core loss",P_core_loss,"[W]", True,dir_parent)					#core loss will be zero because there is no B-H hysteresis effect for an ironless machine
addToTable("Total losses",P_losses,"[W]", True,dir_parent)
addToTable("Power factor",power_factor_method_1,"[p.u.]", True,dir_parent)
addToTable("Fill factor",fill_factor,"[p.u.]",True,dir_parent)
addToTable("Coil side area",coil_side_area*1000**2,"[mm^2]",True,dir_parent)
#addToTable("Area available per Turn (including fill factor)",area_available_per_turn,"[mm^2]",True,dir_parent)
#addToTable("Area required per Turn (Litz with parallel strands)",single_turn_area*1000**2,"[mm^2]",True,dir_parent)
addToTable("Turns per coil (N)",N,"[Turns]",True,dir_parent)
addToTable("Current through single slot (peak)",current_through_slot,"[A]",True,dir_parent)
addToTable("Current density (peak)",J,"[A/mm^2]",True,dir_parent)
addToTable("Current density (RMS)",J/np.sqrt(2),"[A/mm^2]",True,dir_parent)
addToTable("Coil temperature",coil_temperature,"[Deg. C]",True,dir_parent)
addToTable("Coil winding length",l_coil,"[m]",True,dir_parent)
addToTable("Phase winding length",l_total,"[m]",True,dir_parent)
addToTable("Copper resistivity",copper_resistivity,"[Ohm.m]",True,dir_parent)

#addToTable("Active slot area",active_slot_area*1000**2,"[mm^2]",True,dir_parent)
#addToTable("Achieved fill factor",active_slot_area/coil_side_area,"[p.u.]",True,dir_parent)


#if enable_flux_density_harmonic_calc == 1:
#addToTable('Brmax|rn',max_Br_1,'[T]', True,dir_parent)
#addToTable('Br1|rn',Br1,'[T]', True,dir_parent)
#addToTable('THD(Br|rn)',(Br_THD*100),'[%]', True,dir_parent)

###############################
###write outputs to database###
###############################
active_mass_torque_density = Tm_lorentz_mean/(magnet_mass+copper_mass)
total_mass_torque_density = Tm_lorentz_mean/total_mass

db_output_insert(output_table_name,"magnet_mass",primary_key,magnet_mass)
db_output_insert(output_table_name,"inner_magnets_mass",primary_key,inner_magnets_mass)
db_output_insert(output_table_name,"outer_magnets_mass",primary_key,outer_magnets_mass)
db_output_insert(output_table_name,"copper_mass",primary_key,copper_mass)
db_output_insert(output_table_name,"aluminium_mass",primary_key,aluminium_mass)
db_output_insert(output_table_name,"tufnol_mass",primary_key,tufnol_mass)
db_output_insert(output_table_name,"total_mass",primary_key,total_mass)
db_output_insert(input_table_name,"active_region_height_in_mm",primary_key,(hmi_r+hmo_r+hc+2*g)*1000)
db_output_insert(output_table_name,"turnsPerCoil",primary_key,N)
db_output_insert(output_table_name,"average_torque_dq",primary_key,Tm_lorentz_mean)
db_output_insert(output_table_name,"torque_ripple_semfem",primary_key,torque_ripple)
db_output_insert(output_table_name,"terminal_LL_peak_voltage",primary_key,peak_terminal_line_voltage)
db_output_insert(output_table_name,"phase_current_peak",primary_key,max_input_phase_current)
db_output_insert(output_table_name,"p_in_electrical",primary_key,P_in)
db_output_insert(output_table_name,"p_out_dq_torque",primary_key,P_out)
db_output_insert(output_table_name,"efficiency_dq",primary_key,efficiency)
db_output_insert(output_table_name,"efficiency_semfem",primary_key,efficiency)
db_output_insert(output_table_name,"efficiency_indirect",primary_key,efficiency)
db_output_insert(output_table_name,"efficiency_indirect_dq",primary_key,efficiency)
db_output_insert(output_table_name,"power_factor",primary_key,power_factor_method_1)
db_output_insert(output_table_name,"p_conductive_analytical",primary_key,P_conductive_analytical)
db_output_insert(output_table_name,"p_eddy",primary_key,P_eddy)
db_output_insert(output_table_name,"p_iron_losses",primary_key,0)
db_output_insert(output_table_name,"p_magnet_losses",primary_key,0)
db_output_insert(output_table_name,"total_losses",primary_key,P_losses)
db_output_insert(output_table_name,"Ra_analytical_DC",primary_key,Ra_analytical)
db_output_insert(output_table_name,"p_in_indirect",primary_key,P_in)
db_output_insert(output_table_name,"p_in_indirect_dq",primary_key,P_in)
db_output_insert(output_table_name,"torque_density",primary_key,total_mass_torque_density)
db_output_insert(output_table_name,"torque_density_active_mass",primary_key,active_mass_torque_density)
db_output_insert(output_table_name,"id",primary_key,id)
db_output_insert(output_table_name,"iq",primary_key,iq)
db_output_insert(output_table_name,"phase_voltage_rms",primary_key,abs(max_input_phase_voltage)/np.sqrt(2))
db_output_insert(output_table_name,"line_current_rms",primary_key,RMS_input_line_current)
db_output_insert(output_table_name,"phase_voltage_peak",primary_key,abs(max_input_phase_voltage))
#db_output_insert(output_table_name,"l_series_connections",primary_key,l_series_connections)
db_output_insert(output_table_name,"l_coil",primary_key,l_coil)
db_output_insert(output_table_name,"l_total",primary_key,l_total)
if calculate_AR_effect:
  db_output_insert(output_table_name,"synchronous_inductance_single_turn",primary_key,Ls)

if enable_turns_calculation:
  db_output_insert(input_table_name,"force_this_number_of_turns",primary_key,N)
  db_output_insert(output_table_name,"turnsPerCoil",primary_key,N)

if input_is_Jrms0_Pcu1_slotcurrent2 == 1 or enable_target_mech_power:
  db_output_insert(output_table_name,"current_density_rms",primary_key,J/np.sqrt(2))
  db_output_insert(input_table_name,"current_density_rms",primary_key,J/np.sqrt(2))

now = datetime.datetime.now()
db_output_insert(output_table_name,"date_of_result",primary_key,now)
db_output_insert(input_table_name,"date_of_result",primary_key,now)


#if enable_terminal_output == 1:
save_stdout = sys.stdout
sys.stdout = open(dir_parent+"pretty_terminal_table.txt", "wb")
print pretty_terminal_table
sys.stdout = save_stdout

if VERBOSITY == 1:
  print pretty_terminal_table
  if enable_eddy_losses_calc == 1:
    print pretty_eddy
    print pretty_layer
    pretty_eddy.clear_rows()
    pretty_layer.clear_rows()

pretty_terminal_table.clear_rows()

dir = dir_parent+'script_results/main/'
if not os.path.isdir(dir): os.makedirs(dir)

#with open(dir_parent+'script_results/main/running_flags.csv', 'wb') as running_flags_file:
  #running_flags_file.write("iteration_number,%d"%(iteration_number+1))

if store_results_in_database == 1 and overwrite_previous_result == 1:
  conn.commit()
conn.close()

#print "Tm_Lorentz (min) = %g"%(min(Tm_lorentz))
#print "Tm_Lorentz (max) = %g"%(max(Tm_lorentz))
#print "torque_calc_count = %d"%(torque_calc_count)

if time:
  end_time = time.time()
  if enable_terminal_output == 1:
    print("Elapsed time is: %f"%(end_time-start_time))

#if __name__ == '__main__': #when executed by itself
  #main('input_original.csv')