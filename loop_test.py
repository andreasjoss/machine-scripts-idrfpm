#!/usr/bin/python

from __future__ import division
import threading
from prettytable import PrettyTable
import os
import pylab as pl

w_eff = 0
w_tor = 0
w_pm = 0

w_pm_init = 0
w_tor_init = 0
w_eff_init = 1

itr = 0.025
processes = 4

PRINT_READY = [0,0,0,0]

pretty_multi_proc_table = PrettyTable(['Process number','Progress count'])
pretty_multi_proc_table.align = 'l'
pretty_multi_proc_table.border = True  

array_w_eff = []
array_w_tor = []
array_w_pm = []

#determine entire loop_array structure
div_o = int(1/itr)
div_i = div_o
loop_array = []
i = 0
j = 0
while i<div_o+1:
  
  j = i
  while j<div_i+1:
    w_tor = w_tor_init + j*itr - w_pm
    array_w_tor.append(w_tor)
    w_eff = abs(1 - w_tor - w_pm)  
    loop_array.append([w_eff,w_tor,w_pm])
    array_w_eff.append(w_eff)
    #print("w_eff = %.3f, w_tor = %.3f, w_pm = %.3f"%(w_eff,w_tor,w_pm))
    
    j = j + 1

  i = i + 1
  w_pm = w_pm_init + i*itr
  array_w_pm.append(w_pm)


fig, axarr = pl.subplots(3, sharex=True)

axarr[0].plot(range(0,len(array_w_eff)),array_w_eff,label='w_eff')
axarr[1].plot(range(0,len(array_w_tor)),array_w_tor,label='w_tor')
axarr[2].plot(range(0,len(array_w_pm)),array_w_pm,label='w_pm')
pl.show()


##determine start and stop index for each process
#start_end_index = []
#end_num = -1
#for p in range (1,processes+1):
  ##start_num = int(((p-1)/processes) *len(loop_array))
  #start_num = end_num+1
  #end_num = int((p/processes) *len(loop_array))
  
  #start_end_index.append([start_num,end_num])

#print start_end_index



###this class is a subclass of threading.Thread class (or put otherwise, inherites from that class)
##class MyThread(threading.Thread):
    ##def __init__(self, *args, **kwargs):
        ##self.parent = threading.current_thread()
        ##Thread.__init__(self, *args, **kwargs)

##thread function prototype
##def optimizeThread(p,loop_array,start_end_index,pretty_multi_proc_table):
#class MyThread(threading.Thread):
  #def __init__(self, *args, **kwargs):
      #self.parent = threading.current_thread()
      #threading.Thread.__init__(self, *args, **kwargs)
      ##self.Thread.__init__(self, *args, **kwargs)
      
#def optimizeThread(p,loop_array,start_end_index,pretty_multi_proc_table):
  #print("thread %d initiated, thread active count %d"%(p,threading.active_count()))

  #start_num = start_end_index[p-1][0]
  #end_num = start_end_index[p-1][1]
  #count = 0

  #print("thread %d:: start at %d and end at %d"%(p,start_num,end_num))
  
  #os.system("./post_script.py 21")
  #os.system("./post_script.py 22")
  
  #for i in range(start_num, end_num+1):
    ##print("w_eff = %.3f, w_tor = %.3f, w_pm = %.3f"%(loop_array[i][0],loop_array[i][1],loop_array[i][2]))
    ##count = count + 1
    #for j in range(0,1000):
      #for k in range(0,300):
	#pass
    ##pass
    #progress_string = "%d/%d"%(count,end_num-start_num)
    #count = count + 1
    #pretty_multi_proc_table.add_row([p,progress_string])
    #PRINT_READY[p-1] = 1
    
    #all_ready = 0
    #for k in range (0, len(start_end_index)):
      #if PRINT_READY[k] == 1:
	#all_ready = all_ready + 1
    
    #if all_ready == len(start_end_index):
      #for k in range (0, len(start_end_index)):
	#PRINT_READY[k] = 0
      #print pretty_multi_proc_table
      #pretty_multi_proc_table.clear_rows()
      

    
  #print("thread %d finished, thread active count %d"%(p,threading.active_count()))
  


##initiate each thread
#thread_names = []
#p = 1
#while p <= processes:
  #t = threading.Thread(target=optimizeThread, args=(p,loop_array,start_end_index,pretty_multi_proc_table,))
  #t.start()
  ##thread_names.append(t.current_thread())

  ##t = MyThread(target=optimizeThread, args=(p,loop_array,start_end_index,pretty_multi_proc_table,))
  ##t.start()

  #p = p + 1
  
  #print("thread active count %d"%threading.active_count())


#print thread_names


















##print(count)