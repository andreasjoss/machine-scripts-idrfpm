#!/usr/bin/python
from __future__ import division
import argparse
import os,sys
import pylab as pl
import numpy as np
from prettytable import PrettyTable
#import cairosvg
#from scitools.std import movie
from datetime import datetime
import shutil
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from scipy.interpolate import griddata
#import idrfpm_semfem
import threading
#from loop_test import MyThread
#import matplotlib

#spline libraries
from scipy.interpolate import Rbf
from matplotlib import cm 
from scipy.stats import gaussian_kde
from scipy import stats
from scipy.interpolate import interp1d
import matplotlib.mlab as mlab
from itertools import chain

#reload(idrfpm_semfem)

pl.rc('text', usetex=True)
pl.rc('font', family='serif')

function_evaluation_script = 'idrfpm'
#function_evaluation_script = 'qhdrfapm'

print "Current matplotlib backend:"
print(pl.get_backend())

markersize=12

#matplotlib.markers.set_fillstyle('full')

###PARSER
##########################################################################################
parser = argparse.ArgumentParser(description="Draw graphs",formatter_class=argparse.RawTextHelpFormatter)

parser.add_argument("option", help="Select which graph to produce:\n"+
		    "1 - main.py          -- Draw flux density graphs from perturb results\n"+
		    "2 - main.py          -- Draw flux density graphs from proximity effect results\n"+
		    "3 - main.py          -- Draw comparison between two machines\n"+
		    "4 - idrfpm_semfem.py -- Draw flux density graphs\n"+
		    "5 - idrfpm_semfem.py -- Draw torque graphs\n"+
		    "6 - idrfpm_semfem.py -- Draw other miscallenous graphs\n"+
		    "7 - main.py          -- Create pretty optimization output table file\n"+
		    "8 - main.py          -- Draw f(x) optimization results\n"+
		    "9 - main.py          -- Draw Pareto curve\n"+
		    "10- main.py          -- Convert svg files to png files\n"+
		    "11- main.py          -- Create gif animation from png files\n"+
		    "12- main.py          -- Create mp4 animation from png files\n"+
		    "13- main.py          -- Backup most important optimization results into a date-named directory\n"+
		    "14- main.py          -- Extract optimal solution from optimization output file and write in an easier format\n"+
		    "15- main.py          -- Plot effeciency vs mass given the directories of the optimization database\n"+
		    "16- main.py          -- Plot torque vs mass given the directories of the optimization database\n"+
		    "17- main.py          -- Plot efficiency vs mass torque the directories of the optimization database\n"+
		    "18- main.py          -- Plot 3D pareto surface of effeciency, torque and PM mass.\n"+
		    "19- main.py          -- Get elite solution designs\n"+
		    "20- main.py          -- Plot colour-contour grahp of eff,tor and PM mass output\n"+
		    "21- main.py          -- Test MyThread subclass\n"+
		    "22- main.py          -- Test MyThread subclass\n"+
		    "23- main.py          -- Plot more than one dataset for 3D pareto plot\n"+
		    "24- main.py          -- Plot KDE graphs\n"+
		    "25- main.py          -- Filter 3D scatter plot to plot 2D manual contour\n"+
		    "26- main.py          -- Use contour matplotlib function\n"+
		    "27- main.py          -- Plot deviation between two contour plots from option 25\n"+
		    "28- main.py          -- Determine scalar dimensions by using p.u. design variables\n"
		    ,type=int, choices=[1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28])

parser.add_argument('-t','--thread', dest='thread', type=int, default=0,
                   help='Specify this thread number')


args = parser.parse_args()									#store the argument parsed into the variable "args"

##########################################################################################

#print("This is thread %d"%(args.thread))

#temp fix
dir_parent = './mp/p%d/'%args.thread

#if args.thread > 1:
  #dir_parent = './mp/p%d/'%args.thread
#else:
  #dir_parent = ''

LINEWIDTH=1
enable_read_maxwell=0
enable_smc_cored_coils=0

file_input_name = 'input_edited.csv'
#file_prototype_performance = './optimization_database/Gert_prototype/19turns/pretty_terminal_table.txt'
file_prototype_performance = './optimization_database/Gert_prototype/subdomain/pretty_terminal_table.txt'


input_data = np.genfromtxt('input_original.csv',skip_header=1,delimiter=',',usecols=(1),unpack=True)
steps = int(input_data[22])

P  = 0,"p","[pairs]"
KQ = 1,"kq","[ratio]"
L  = 5,"l","[m]"
G  = 6,"g","[m]"
HC  = 7,"hc","[m]"
KMO = 8,"kmo","[ratio]"
KC = 32,"kc","[unitless]"
HMO = 36,"hmo","[m]"
hmi_r = 37,"hmi_r","[m]"
RI = 38,"ri","[m]"
RO = 39,"ro","[m]"
GO = 53,"go","[m]"
GI = 54,"gi","[m]"
KMI = 62,"kmi","[ratio]"

def isclose(a, b, rel_tol, abs_tol):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)

def replaceLine(key,newValue):
  if type(newValue) == int:
    # now change the 2nd line, note that you have to add a newline
    data[key[0]+1] = '%s,%d,%d,%d\n'%(key[1],newValue,float(data[key[0]+1].split(",")[2]),float(data[key[0]+1].split(",")[3]))
  elif type(newValue) == float:
    # now change the 2nd line, note that you have to add a newline
    #print key[0]+1
    #print data[key[0]+1]
    data[key[0]+1] = '%s,%s,%s,%s\n'%(key[1],newValue,float(data[key[0]+1].split(",")[2]),float(data[key[0]+1].split(",")[3])) #should use string because otherwise value of small decimal numbers can be lost

def replaceFile(replace_this):    
  # and write everything back
  with open(replace_this, 'w') as file:
      file.writelines( data )    

if steps != 1:
  per_unit_time = np.ndarray(steps,float)
  temp = 0
  for i in range(steps):
    per_unit_time[i] = temp
    temp = temp + 1.0/(steps-1)

#if args.option > 3:
if enable_read_maxwell==1:
  flA_maxwell,flB_maxwell,flC_maxwell,eA_maxwell,eB_maxwell,eC_maxwell,iA_maxwell,iB_maxwell,iC_maxwell=np.loadtxt('maxwellData/DIRFPM-10_1_l=40.00_r=120.00_Ip=20_N=22_hm=6.0_p=28_hc=9.0_n=290_w=9.96335_hy=5.0-lambda.txt',skip_header=7,usecols=(1,2,3,4,5,6,7,8,9),unpack=True)
  time_maxwell,torque_maxwell=np.loadtxt('maxwellData/DIRFPM-10_1_l=40.00_r=120.00_Ip=20_N=22_hm=6.0_p=28_hc=9.0_n=290_w=9.96335_hy=5.0-Tm.txt',skip_header=7,usecols=(0,1),unpack=True)
  steps_mw = torque_maxwell.size
  per_unit_time_mw = np.ndarray(steps_mw,float)
  temp = 0
  for i in range(steps_mw):
    per_unit_time_mw[i] = temp
    temp = temp + 1.0/(steps_mw-1)

  #shift Maxwell values by pi/2 to right
  #FUTURE WORK

  print "\nAnsys Maxwell results:"
  print("Max phase current: %f [A]")%max(iA_maxwell)
  print("Max phase voltage: %f [V]")%max(eA_maxwell) 			#phase A max induced voltage
  print("Max flux linkage: %f [Wb]")%max(flA_maxwell)
  print("Average torque: %f [Nm]")%np.average(torque_maxwell)

  #python arrays (lists) are constructed which will hold the values of the columns selected
  #obtain simulation results from SEMFEM
  rotor_inner,rotor_outer,t1,t2,t3,g1,g2,fl1,fl2,fl3,i1,i2,i3=np.loadtxt('project_idrfpm_semfem/results/post.res',skip_header=2,usecols=(0,2,3,4,5,6,7,8,9,10,11,12,13),unpack=True)  
  voltage = np.loadtxt('./script_results/idrfpm/voltage.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
  torque_dq = np.loadtxt('./script_results/idrfpm/torque_dq.csv',skip_header=1,usecols=(0,),delimiter=",",unpack=True)
  

  #print steps
  #print np.size(per_unit_time)
  #print np.size(t1)

#############################################################################################################

# main.py -- display graphs for perturb results
if args.option == 1:
  print "hallo"
  
  
  
# main.py -- display graphs for proximity effect results
elif args.option == 2:
  Br_sim1 = []
  Br_sim2 = []
  Br_sim3 = []     
  
  M = 1024
  q = input_data[1]*input_data[0]
  phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/input_data[1]))
  iq = np.rint(input_data[13])

  #load text files from completed simulation
  Br_sim1 = np.genfromtxt('./AR_effect_study/br_PM_J=0.0.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True) #PM
  Br_sim2 = np.genfromtxt('./AR_effect_study/br_AR_J=4.0.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True) #AR
  Br_sim3 = np.genfromtxt('./AR_effect_study/br_ALL_J=4.0.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True) #ALL
  
  #Br_sim2 = np.genfromtxt('./AR_effect_study/br_AR_J=20.0.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True) #AR
  #Br_sim3 = np.genfromtxt('./AR_effect_study/br_ALL_J=20.0.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True) #ALL  

  #save results at radius "rn - h/2"
  fig = pl.figure(1)
  #pl.title(r'$B_{r|r_n - h/2}(\phi)$ showing Proximity Effect')
  pl.plot(np.degrees(phi_range),Br_sim1[0],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} - h/2}$ PMs only')
  pl.plot(np.degrees(phi_range),Br_sim2[0],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} - h/2}$ Windings only')
  pl.plot(np.degrees(phi_range),Br_sim3[0],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} - h/2}$ All')
  
  ax=pl.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=10)
  pl.ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=10)
  #pl.legend(loc='upper right',fontsize=8)
  pl.axis(xmin=-180/q,xmax=180/q)
  pl.grid(True)
  
  ax.set_ylim(-0.8,0.8)
  fig.set_size_inches(8, 4) #default is (8,6)
  leg = ax.legend(loc='upper right')
  for line in leg.get_lines():
    line.set_linewidth(2)  
  
  #check if directory exists, if not, create the directory
  #dir = dir_parent+'./script_results/main/proximityEffect/current_%dA'%iq
  #if not os.path.isdir(dir): os.makedirs(dir)
  pl.savefig('./AR_effect_study/Br_rn-h_div_2.pdf'%iq,transparent=True, bbox_inches='tight')
  
  
  #save results at radius "rn"
  fig = pl.figure(2)
  #pl.title(r'$B_{r|r_n}(\phi)$ showing Proximity Effect')
  pl.plot(np.degrees(phi_range),Br_sim1[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ PMs only')
  pl.plot(np.degrees(phi_range),Br_sim2[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ Windings only')
  pl.plot(np.degrees(phi_range),Br_sim3[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ All')
  
  ax=pl.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=10)
  pl.ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=10)
  #pl.legend(loc='upper right',fontsize=8)
  pl.axis(xmin=-180/q,xmax=180/q)
  pl.grid(True)

  ax.set_ylim(-0.8,0.8)
  fig.set_size_inches(8, 4) #default is (8,6)
  leg = ax.legend(loc='upper right')
  for line in leg.get_lines():
    line.set_linewidth(2) 

  #check if directory exists, if not, create the directory
  #dir = dir_parent+'./script_results/main/proximityEffect/current_%dA'%iq
  #if not os.path.isdir(dir): os.makedirs(dir)  
  pl.savefig('./AR_effect_study/Br_rn.pdf'%iq,transparent=True, bbox_inches='tight')
  
  
  #save results at radius "rn + h/2"
  fig = pl.figure(3)
  #pl.title(r'$B_{r|r_n + h/2}(\phi)$ showing Proximity Effect')
  pl.plot(np.degrees(phi_range),Br_sim1[2],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} + h/2}$ PMs only')
  pl.plot(np.degrees(phi_range),Br_sim2[2],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} + h/2}$ Windings only')
  pl.plot(np.degrees(phi_range),Br_sim3[2],linewidth=LINEWIDTH,label=r'$B_{r|r_{n} + h/2}$ All')
  
  ax=pl.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=10)
  pl.ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=10)
  #pl.legend(loc='upper right',fontsize=8)
  pl.axis(xmin=-180/q,xmax=180/q)
  pl.grid(True)

  ax.set_ylim(-0.8,0.8)
  fig.set_size_inches(8, 4) #default is (8,6)
  leg = ax.legend(loc='upper right')
  for line in leg.get_lines():
    line.set_linewidth(2) 

  #check if directory exists, if not, create the directory
  #dir = dir_parent+'./script_results/main/proximityEffect/current_%dA'%iq
  #if not os.path.isdir(dir): os.makedirs(dir)
  pl.savefig('./AR_effect_study/Br_rn+h_div_2.pdf'%iq,transparent=True, bbox_inches='tight')   


#3 - main.py	  -- Draw comparison between two machines
elif args.option == 3:
  print "hallo daar"


# idrfpm_semfem.py -- display radial flux graphs
elif args.option == 4:
  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  font_size=14
  
  M = 1024
  q = input_data[1]*input_data[0]
  phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/input_data[1]))  

  #dir1 = './script_results/idrfpm/br.csv'
  
  dir1='/home/andreas/Documents/Meesters/ICEM/radial_flux_density_comparison/semfem/br.csv'
  dir2='/home/andreas/Documents/Meesters/ICEM/radial_flux_density_comparison/subdomain/br.csv'

  Br_dir1 = np.genfromtxt(dir1,skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
  Br_dir2 = np.genfromtxt(dir2,skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)


  pl.figure(4)
  #pl.title(r'$B_r(\phi)$')
  pl.plot(np.degrees(phi_range),Br_dir1[0],linewidth=LINEWIDTH,label=r'$B_{r|r_n-h_c/2}$ (FEM)')
  pl.plot(np.degrees(phi_range),Br_dir1[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ (FEM)')
  pl.plot(np.degrees(phi_range),Br_dir1[2],linewidth=LINEWIDTH,label=r'$B_{r|r_n+h_c/2}$ (FEM)')
  
  M = 128 #In order to utilise the FFT optimally later on
  kq=0.5
  phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/kq))  
  pl.plot(np.degrees(phi_range),Br_dir2[0],linewidth=LINEWIDTH,label=r'$B_{r|r_n-h_c/2}$ (Analytical)')
  pl.plot(np.degrees(phi_range),Br_dir2[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ (Analytical)')
  pl.plot(np.degrees(phi_range),Br_dir2[2],linewidth=LINEWIDTH,label=r'$B_{r|r_n+h_c/2}$ (Analytical)')    
  
  ax=pl.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
  pl.ylabel(r'Radial Flux Density, $B_{r}$ [T]')
  pl.legend(loc='upper right')
  pl.axis(xmin=-180/q,xmax=180/q)
  pl.grid(True)  
  
  #pl.figure(5)
  #M = 128 #In order to utilise the FFT optimally later on
  #kq=0.5
  #phi_range=np.linspace(-np.pi/q,np.pi/q,int(M/kq))  
  #pl.plot(np.degrees(phi_range),Br_dir2[0],linewidth=LINEWIDTH,label=r'$B_{r|r_n-h_c/2}$ (Analytical)')
  #pl.plot(np.degrees(phi_range),Br_dir2[1],linewidth=LINEWIDTH,label=r'$B_{r|r_{n}}$ (Analytical)')
  #pl.plot(np.degrees(phi_range),Br_dir2[2],linewidth=LINEWIDTH,label=r'$B_{r|r_n+h_c/2}$ (Analytical)')  
  
  #ax=pl.gca()
  #ax.set_xticks(np.linspace(-180/q,180/q,9))
  #pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
  #pl.ylabel(r'Radial Flux Density, $B_{r}$ [T]')
  #pl.legend(loc='upper right')
  #pl.axis(xmin=-180/q,xmax=180/q)
  #pl.grid(True)

  pl.savefig('/home/andreas/Documents/Meesters/ICEM/radial_flux_density_comparison/br.pdf', transparent=True, bbox_inches='tight')


# idrfpm_semfem.py -- display torque graphs
elif args.option == 5:
  pl.figure(5)
  pl.xlabel("Per unit time (electrical)")
  pl.ylabel("Torque [N.m]")
  pl.title("Component Torque vs. p.u. Time")
  
  #pl.plot(per_unit_time,t1,label="SEMFEM Torque 1")
  pl.plot(per_unit_time,t2,label="SEMFEM Torque 2")
  #pl.plot(per_unit_time,t3,label="SEMFEM Torque 3")
  
  #for i in range(steps):
    #torque_dq[i] = -torque_dq[i]
    #print(torque_dq[i])
  
  pl.plot(per_unit_time,torque_dq,label="SEMFEM Torque_dq")

  #pl.plot(per_unit_time,sf.semfem_torque_vec[:,0],label="SEMFEM Torque 1")
  #pl.plot(per_unit_time,sf.semfem_torque_vec[:,1],label="SEMFEM Torque 2")
  #pl.plot(per_unit_time,sf.semfem_torque_vec[:,2],label="SEMFEM Torque 3")

  average_torque = np.average(t2)

  pl.plot([0,1],[average_torque,average_torque],label="SEMFEM Average Torque")
  if enable_read_maxwell == True:
    pl.plot(per_unit_time_mw,-torque_maxwell,label="Ansys Torque")
  pl.legend()


# idrfpm_semfem.py -- diplay miscallenous graphs
elif args.option == 6:
    pl.figure(6)
    pl.xlabel("Per unit time (electrical)")
    pl.ylabel("Gap Torque [N.m]")
    pl.title("Gap Torque vs. p.u. Time")
    pl.plot(per_unit_time,g1,label="SEMFEM Gap Torque 1")
    pl.plot(per_unit_time,g2,label="SEMFEM Gap Torque 2")
    pl.legend()
    
    
    fig = pl.figure(7)
    pl.xlabel("Per unit time (electrical)")
    pl.title("Phase Flux Linkage vs. p.u. Time")
    pl.xlim(0,1)

    ax = fig.add_subplot(111)
    ax1 = pl.subplot(211)
    pl.plot(per_unit_time,fl1,label="Flux Linkage 1")
    pl.plot(per_unit_time,fl2,label="Flux Linkage 2")
    pl.plot(per_unit_time,fl3,label="Flux Linkage 3")
    pl.legend()
    ax2 = pl.subplot(212)
    if enable_read_maxwell == 1:  
      pl.plot(per_unit_time_mw,flA_maxwell,label="Flux Linkage A")
      pl.plot(per_unit_time_mw,flB_maxwell,label="Flux Linkage B")
      pl.plot(per_unit_time_mw,flC_maxwell,label="Flux Linkage C")
      ax2.set_title('Ansys Flux Linkages')

    ax.set_xlabel('Per unit time (electrical)')

    ax1.set_ylabel("Flux Linkage [Wb]")
    ax2.set_ylabel("Flux Linkage [Wb]")

    ax1.set_title('SEMFEM Flux Linkages')

    pl.legend()
    pl.show()

    fig = pl.figure(8)
    pl.xlabel("Per unit time (electrical)")
    pl.title("Phase Current vs. p.u. Time")
    pl.xlim(0,1)
    ax = fig.add_subplot(111)
    ax1 = pl.subplot(211)
    pl.plot(per_unit_time,i1,label="Current 1")
    pl.plot(per_unit_time,i2,label="Current 2")
    pl.plot(per_unit_time,i3,label="Current 3")
    pl.legend()
    ax2 = pl.subplot(212)
    if enable_read_maxwell == 1: 
      pl.plot(per_unit_time_mw,iA_maxwell,label="Current A")
      pl.plot(per_unit_time_mw,iB_maxwell,label="Current B")
      pl.plot(per_unit_time_mw,iC_maxwell,label="Current C")
      ax2.set_title('Ansys Phase Currents')
    pl.legend()

    ax.set_xlabel('Per unit time (electrical)')

    ax1.set_ylabel("Phase Current [A]")
    ax2.set_ylabel("Phase Current [A]")

    ax1.set_title('SEMFEM Phase Currents')
    pl.show()
    
    pl.figure(9)
    pl.xlabel("Per unit time (electrical)")
    pl.title("Induced Voltages vs. p.u. Time")
    ax = fig.add_subplot(111)
    ax1 = pl.subplot(211)

    for i in range(3):
      pl.plot(per_unit_time, voltage[i],label = "Induced Voltage Phase %d"%(i+1))
    pl.xlim(min(per_unit_time), max(per_unit_time))
    pl.ylim(min(0, 1.2*min(voltage[0])), max(0, 1.2*max(voltage[0])))  
    pl.legend()

    ax2 = pl.subplot(212)
    if enable_read_maxwell == 1:
      #The Maxwell data in the text file for voltage records a zero at time 0 which looks funny
      eA_maxwell[0] = eA_maxwell[np.size(eA_maxwell)-1]
      eB_maxwell[0] = eB_maxwell[np.size(eB_maxwell)-1]
      eC_maxwell[0] = eC_maxwell[np.size(eC_maxwell)-1]
    
      pl.plot(per_unit_time_mw,eA_maxwell,label="Induced Voltage Winding A")
      pl.plot(per_unit_time_mw,eB_maxwell,label="Induced Voltage Winding B")
      pl.plot(per_unit_time_mw,eC_maxwell,label="Induced Voltage Winding C")
      ax2.set_title('Ansys Induced Voltage')
      
    pl.xlim(0,1)
    pl.legend()

    ax.set_xlabel('Per unit time (electrical)')

    ax1.set_ylabel("Induced Voltage [V]")
    ax2.set_ylabel("Induced Voltage [V]")

    ax1.set_title('SEMFEM Induced Voltage')
    
elif args.option == 7:
  from prettytable import from_csv
  fp = open("./script_results/idrfpm/optimization_output.csv", "r")
  pt = from_csv(fp)
  fp.close()
  
  pt.border= True
  
  with open('./script_results/idrfpm/pretty_optimization_output.txt','wb') as pretty_output:
    pretty_string = pt.get_string(border = True)
    pretty_output.write(pretty_string)

elif args.option == 8:
  data = np.genfromtxt('./script_results/idrfpm/optimization_output.csv',skip_header=1,delimiter=',',usecols=(34,35),unpack=True)
  w1 = 0.5
  w2 = 0.5
  efficiency = data[0]
  torque = data[1]
  
  #print(torque)
  #print(efficiency)
  
  f1 = w1*efficiency
  f2 = w2*torque
  f = f1 + f2
  
  pl.figure()
  pl.xlabel("Iteration number")
  pl.ylabel(r"$f(\mathbf{x}), \eta(\mathbf{x}), \tau(\mathbf{x})$")
  pl.title(r"Objective function $f(\mathbf{x}), \eta(\mathbf{x}), \tau(\mathbf{x})$")
  pl.plot(range(0,np.size(f1)),f1,label=r"$\eta$")
  pl.plot(range(0,np.size(f2)),f2,label=r"$\tau$")
  pl.plot(range(0,np.size(f)),f,label=r"$f(\mathbf{x}) = w_{1}\eta(\mathbf{x}) + w_{2}\tau(\mathbf{x})$")
  pl.legend(loc='lower left')

  pl.savefig('./script_results/main/fx.pdf',transparent=True)#, bbox_inches='tight')


elif args.option == 10:
  dir = dir_parent+'./script_results/main/optimization_animation/png/'
  if not os.path.isdir(dir): os.makedirs(dir)
  
  run_flags = np.genfromtxt('./script_results/main/running_flags.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
  iteration_number = int(run_flags)
  
  for i in range(1,iteration_number+1):
    svg_file = './script_results/main/optimization_animation/svg/%d.svg'%(i)
    fout = open('./script_results/main/optimization_animation/png/%d.png'%(i),'w')
    cairosvg.svg2png(url=svg_file,write_to=fout)
    fout.close()

elif args.option == 11:
  movie('./script_results/main/optimization_animation/png/*.png',fps=2,output_file='./script_results/main/optimization_animation/optimization.gif')

elif args.option == 12:
  #framerate = 2
  #os.system("ffmpeg -f image2 -r %d -i ./script_results/main/optimization_animation/png/*.png -vcodec mpeg4 -y ./script_results/main/optimization_animation/movie.mp4"%(framerate))
  #os.system("ffmpeg -framerate 5 -i ./script_results/main/optimization_results_backup/optimization_animation/png/%03d.png -c:v libx264 -vf fps=5 -pix_fmt yuv420p ./script_results/main/optimization_animation/movie.mp4")
  #os.system("ffmpeg -framerate 5 -i ./script_results/main/optimization_results_backup/optimization_animation/png/%03d.png -c:v libx264 -vf "fps=5,format=yuv420p" ./script_results/main/optimization_animation/movie.mp4")
  
  os.system("ffmpeg -framerate 5 -pattern_type glob -i './script_results/main/optimization_animation/png/*.png' -c:v libx264 -vf scale=320:240 -r 5 -pix_fmt yuv420p output.mp4")
  #os.system("ffmpeg -framerate 1 -i ./script_results/main/optimization_results_backup/optimization_animation/png/%03d.png -c:v libx264 -vf scale=320:240 -r 1 -pix_fmt yuv420p output.mp4")
  #os.system("ffmpeg -r 1 -i ./script_results/main/optimization_results_backup/optimization_animation/png/%3d.png -c:v libx264 -vf scale=320:240 -pix_fmt yuv420p output.mp4")
  
  #os.system("avconv -f image2 -i ./script_results/main/optimization_animation/png/*.png -r 76 -s 800x600 ./script_results/main/optimization_animation/movie.avi")

elif args.option == 13:
  folder_name = datetime.strftime(datetime.now(), '%Y-%m-%d_%H:%M:%S')
  
  dir = dir_parent+'./optimization_database/%s'%(folder_name)
  if not os.path.isdir(dir): os.makedirs(dir)
  
  shutil.copy('input_original.csv',dir+'/input_original.csv')
  shutil.copy(dir_parent+'input_edited.csv',dir+'/input_edited.csv')
  shutil.copy(dir_parent+'./script_results/main/running_flags.csv',dir+'/running_flags.csv')
  shutil.copy(dir_parent+'./script_results/main/fx.pdf',dir+'/fx.pdf')
  shutil.copy(dir_parent+'./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')

elif args.option == 14:
  data = []
  f=open(dir_parent+"solution.txt","rb")
  for line in f:
      repr(line) #reading as STRINGS!
      columns = line.split()
      data=data+([columns])
  f.close()

  #Removing Variable Indication
  for i in range(len(data)/2):
    del data[0]

  #Removing initial jargen
  for i in range(14):
    del data[0]

  variable_name=[]
  variable=[]
  for i in range(len(data)-2):
    variable_name.append(str(data[i][0]))
    variable.append(float(data[i][2]))

  with open(dir_parent+'optimization_results_summary.csv',"wb") as output_file:
    for i in range(len(data)-2):
      output_file.write('%s,%f\n'%(variable_name[i],variable[i]))

#plot effeciency vs mass given the directories of the optimization database
elif args.option == 15:
  #dir = './optimization_database/run5(eff vs mass)'
  #dir = './optimization_database/run10(eff vs mass)'
  dir = dir_parent+'./optimization_database/run11(eff vs mass)'
  
  w_eff = 0
  w_tor = 0
  w_pm  = 1
  itr = 0.05
  stay_in_loop = True
  eff = []
  pm_mass = []
  tor = []
  
  while stay_in_loop == True:
    dir_temp = dir+'/w1=%.2f,w2=%.2f,w3=%.2f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    
    f_best = np.genfromtxt(dir_temp+'/f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    eff.append(f_best[1])
    pm_mass.append(f_best[3])
    tor.append(f_best[2])
    
    #get png file of the optimal design in this subdirectory
    optimal_design_number = int(f_best[4])-1
    #create new directory for optimal solutions to be saved in
    dir_temp2 = dir + '/png_optimal_collection/'
    if not os.path.isdir(dir_temp2): os.makedirs(dir_temp2)
    
    shutil.copy(dir_temp+'/png/%d.png'%(optimal_design_number),dir_temp2+'/w_eff=%.2f,w_tor=%.2f,w_pm=%.2f.png'%(abs(w_eff),abs(w_tor),abs(w_pm)))

    w_eff = w_eff+itr
    w_pm  = 1-w_eff
    
    if w_pm < 0-(0.5*itr):
      stay_in_loop = False

  ideal_objective_data = np.genfromtxt(dir+'/w1=0.10,w2=0.00,w3=0.90'+'/ideal_solutions.csv',skip_header=1,delimiter=',',unpack=False)
  f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
  f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
  f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number

  #print(f_eff_ideal)
  #print(f_pm_ideal)

  #print(eff)
  #print(pm_mass)

  for i in range(0, len(eff)):
    #if i == 0: eff[i] = eff[i]/100
    #print(eff[i])
    #print(pm_mass[i])    
    eff[i] = eff[i]*f_eff_ideal
    pm_mass[i] = pm_mass[i]*f_pm_ideal
    tor[i] = tor[i]*f_tor_ideal
    #print(eff[i])


  #print(eff)
  #print(pm_mass)


  pl.figure(1)
  pl.xlabel(r"PM Mass ($m$) [kg]")
  pl.ylabel(r"Efficiency ($\eta$) [%]")
  pl.title("Pareto Approximation of Efficiency against PM Mass")
  #pl.scatter(pm_mass,eff, alpha = 0.5)
  pl.plot(pm_mass,eff)
  pl.plot(pm_mass,eff,'bo')
  pl.grid(True)
  #pl.legend(loc='lower left')
  
  pl.savefig(dir+'/eff_pm_pareto(eff).pdf',transparent=True)#, bbox_inches='tight')   
  
  pl.figure(2)
  pl.xlabel(r"PM Mass ($m$) [kg]")
  pl.ylabel(r"Torque ($\tau$) [Nm]")
  pl.title("Torque against PM Mass due to Efficiency vs PM Mass Pareto Curve")
  #pl.scatter(pm_mass,tor, alpha = 0.5)
  pl.plot(pm_mass,tor)
  pl.plot(pm_mass,tor,'bo')
  pl.grid(True)
  #pl.legend(loc='lower left')

  pl.savefig(dir+'/tor_pm_pareto(eff).pdf',transparent=True)#, bbox_inches='tight')   

#plot torque vs mass given the directories of the optimization database
elif args.option == 16:
  #dir = './optimization_database/run8(tor vs mass)'
  dir = dir_parent+'./optimization_database/run12(tor vs mass)'
  
  w_eff = 0
  w_tor = 0
  w_pm  = 1
  itr = 0.05
  stay_in_loop = True
  tor = []
  pm_mass = []
  eff = []
  
  while stay_in_loop == True:
    dir_temp = dir+'/w1=%.2f,w2=%.2f,w3=%.2f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    
    f_best = np.genfromtxt(dir_temp+'/f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    tor.append(f_best[2])
    pm_mass.append(f_best[3])
    eff.append(f_best[1])
    
    #get png file of the optimal design in this subdirectory
    optimal_design_number = int(f_best[4])-1
    #create new directory for optimal solutions to be saved in
    dir_temp2 = dir + '/png_optimal_collection/'
    if not os.path.isdir(dir_temp2): os.makedirs(dir_temp2)
    
    shutil.copy(dir_temp+'/png/%d.png'%(optimal_design_number),dir_temp2+'/w_eff=%.2f,w_tor=%.2f,w_pm=%.2f.png'%(abs(w_eff),abs(w_tor),abs(w_pm)))

    w_tor = w_tor+itr
    w_pm  = 1-w_tor
    
    if w_pm < 0-(0.5*itr):
      stay_in_loop = False

  ideal_objective_data = np.genfromtxt(dir_temp+'/ideal_solutions.csv',skip_header=1,delimiter=',',unpack=False)
  f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
  f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
  f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number

  #print(f_tor_ideal)
  #print(f_pm_ideal)

  for i in range(0, len(tor)):
    tor[i] = tor[i]*f_tor_ideal
    pm_mass[i] = pm_mass[i]*f_pm_ideal
    eff[i] = eff[i]*f_eff_ideal

  #print(tor)
  #print(pm_mass)

  #print(len(tor))
  #print(len(pm_mass))

  pl.figure(1)
  pl.xlabel(r"PM Mass ($m$) [kg]")
  pl.ylabel(r"Torque ($\tau$) [Nm]")
  pl.title("Pareto Approximation of Torque against PM Mass")
  #pl.scatter(pm_mass,tor, alpha = 0.5)
  pl.plot(pm_mass,tor)
  pl.plot(pm_mass,tor,'bo')
  pl.grid(True)
  #pl.legend(loc='lower left')

  pl.savefig(dir+'/tor_pm_pareto(tor).pdf',transparent=True)#, bbox_inches='tight')   
  
  pl.figure(2)
  pl.xlabel(r"PM Mass ($m$) [kg]")
  pl.ylabel(r"Efficiency ($\eta$) [%]")
  pl.title("Efficiency against PM Mass due to Torque vs PM Mass Pareto Curve")
  #pl.scatter(pm_mass,eff, alpha = 0.5)
  pl.plot(pm_mass,eff)
  pl.plot(pm_mass,eff,'bo')
  pl.grid(True)
  #pl.legend(loc='lower left')
  
  pl.savefig(dir+'/eff_pm_pareto(tor).pdf',transparent=True)#, bbox_inches='tight')   

#plot eff vs torque given the directories of the optimization database
elif args.option == 17:
  #dir = './optimization_database/run8(tor vs mass)'
  dir = dir_parent+'./optimization_database/run13(eff vs tor)'
  
  w_eff = 0
  w_tor = 1
  w_pm  = 0
  itr = 0.05
  stay_in_loop = True
  tor = []
  pm_mass = []
  eff = []
  
  while stay_in_loop == True:
    dir_temp = dir+'/w1=%.2f,w2=%.2f,w3=%.2f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    
    f_best = np.genfromtxt(dir_temp+'/f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    tor.append(f_best[2])
    pm_mass.append(f_best[3])
    eff.append(f_best[1])
    
    #get png file of the optimal design in this subdirectory
    optimal_design_number = int(f_best[4])-1
    #create new directory for optimal solutions to be saved in
    dir_temp2 = dir + '/png_optimal_collection/'
    if not os.path.isdir(dir_temp2): os.makedirs(dir_temp2)
    
    shutil.copy(dir_temp+'/png/%d.png'%(optimal_design_number),dir_temp2+'/w_eff=%.2f,w_tor=%.2f,w_pm=%.2f.png'%(abs(w_eff),abs(w_tor),abs(w_pm)))

    w_eff = w_eff+itr
    w_tor  = 1-w_eff
    
    if w_tor < 0-(0.5*itr):
      stay_in_loop = False

  ideal_objective_data = np.genfromtxt(dir_temp+'/ideal_solutions.csv',skip_header=1,delimiter=',',unpack=False)
  f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
  f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
  f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number

  #print(f_tor_ideal)
  #print(f_pm_ideal)

  for i in range(0, len(tor)):
    tor[i] = tor[i]*f_tor_ideal
    pm_mass[i] = pm_mass[i]*f_pm_ideal
    eff[i] = eff[i]*f_eff_ideal

  #print(tor)
  #print(eff)

  #print(len(tor))
  #print(len(eff))



  pl.figure(1)
  pl.xlabel(r"Efficiency ($\eta$) [%]")
  pl.ylabel(r"Torque ($\tau$) [Nm]")
  pl.title("Pareto Approximation of Efficiency against Torque")
  #pl.scatter(eff,tor, alpha = 0.5)
  pl.plot(eff,tor)
  pl.plot(eff,tor,'bo')
  pl.grid(True)
  #pl.legend(loc='lower left')

  pl.savefig(dir+'/eff_tor_pareto.pdf',transparent=True)#, bbox_inches='tight')   
  
  
  fig, ax1 = pl.subplots()
  ax2 = ax1.twinx()
  
  pl.figure(2)
  ax1.set_xlabel(r"PM Mass ($m$) [kg]")
  ax1.set_ylabel(r"Efficiency ($\eta$) [%]")
  ax2.set_ylabel(r"Torque ($\tau$) [Nm]")
  
  pl.title("Efficiency, Torque against PM Mass due to Efficiency vs Torque Pareto Curve")
  #pl.scatter(pm_mass,eff, alpha = 0.5)
  ax1.plot(pm_mass,eff,'b')
  ax1.plot(pm_mass,eff,'bo')
  ax2.plot(pm_mass,tor,'r')
  ax2.plot(pm_mass,tor,'ro')  
  #fig.grid(True)
  #pl.legend(loc='lower left')
  
  pl.savefig(dir+'/eff_tor_pm_pareto.pdf',transparent=True)#, bbox_inches='tight')   



#3D plot of pareto surface of efficiency, torque and PM mass
elif args.option == 18 or args.option == 20:
  #dir = './optimization_database/run15(loop correct)/'
  #dir = './optimization_database/run14(itr=0.05)/'
  dir = './optimization_database/run16{itr=0.025}/'
  #dir = dir_parent+'./optimization_database/run17{itr=0.025, kmo & kmi}/optimization_database/'
  #dir = dir_parent+'./optimization_database/run18{hmo_rt & hmi_rt}/optimization_database/'
  #dir = './optimization_database/run19{go & gi}/optimization_database/'
  #dir = './optimization_database/'
  
  print("Looking for optimization results in %s"%(dir))
  
  elite = []
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  folders = [ name for name in os.listdir(dir) if os.path.isdir(os.path.join(dir, name)) ]

  initial_number_of_dirs = len(folders)
  dirs = []
  get_ideal = True
  tor = []
  pm_mass = []
  eff = []
  weights = []
  weights_temp = []
  
  for i in range(0,initial_number_of_dirs):
    bool = folders[i][0]=='w'
    if bool == True:
      dirs.append(dir+folders[i])
      weights_temp.append(folders[i].split(','))
  
  print("Number of folders detected: %d"%(len(dirs)))
  if get_ideal == True: 
    ideal_dir = 'ideal_solutions.csv'
    #ideal_dir = dirs[0]+'/ideal_solutions.csv'
    #print dirs[1]
    print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
    ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
    
    f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
    f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
    f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

  print("f_eff_ideal = %f"%f_eff_ideal)
  print("f_tor_ideal = %f"%f_tor_ideal)
  print("f_pm_ideal  = %f"%f_pm_ideal)

  not_elite_x = []
  not_elite_y = []
  not_elite_z = []

  total_optimisation_time = 0

  for i in range(0,len(dirs)-1):
    f_best_dir = dirs[i]+'/f_data.csv'
    if os.path.isfile(f_best_dir):
      #print f_best_dir
      f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
      if f_best[1]*f_eff_ideal < 120:
	tor.append(f_best[2]*f_tor_ideal)
	pm_mass.append(f_best[3]*f_pm_ideal)
	eff.append(f_best[1]*f_eff_ideal)
	
	#if f_best[2]>min_tor and f_best[1]>min_eff and f_best[3]<max_pm:
	if f_best[2]*f_tor_ideal>min_tor:
	  tor_qualifiers = tor_qualifiers + 1
	if f_best[1]*f_eff_ideal>min_eff:
	  eff_qualifiers = eff_qualifiers + 1
	if f_best[3]*f_pm_ideal<max_pm:
	  pm_mass_qualifiers = pm_mass_qualifiers + 1	
	
	if f_best[2]*f_tor_ideal>min_tor and f_best[1]*f_eff_ideal>min_eff and f_best[3]*f_pm_ideal<max_pm: 
	  elite.append([f_best[1]*f_eff_ideal,f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	  weights.append(weights_temp[i])
	else:
	  not_elite_x.append(f_best[3]*f_pm_ideal)
	  not_elite_y.append(f_best[2]*f_tor_ideal)
	  not_elite_z.append(f_best[1]*f_eff_ideal)
      
      if len(f_best)>5:
	#calculate total time it took to obtain these solution points
	total_optimisation_time = total_optimisation_time + f_best[5]
	hours = int(total_optimisation_time/(60*60))
	minutes = int((total_optimisation_time - hours*60*60)/60)
	seconds = int(total_optimisation_time - hours*60*60 - minutes*60)

  if len(f_best)>5:
    print("Total optimisation time was: %d hours, %d minutes, %d seconds"%(hours,minutes,seconds))
  

  print("tor_qualifiers = %d"%tor_qualifiers)
  print("eff_qualifiers = %d"%eff_qualifiers)
  print("pm__mass_qualifiers = %d"%pm_mass_qualifiers)
  
  x_elite = []
  y_elite = []
  z_elite = []
  
  pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
  pretty_elite_table.align = 'l'
  pretty_elite_table.border= True  
  
  pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
  
  for i in range(0,len(elite)):
    x_elite.append(elite[i][2])
    y_elite.append(elite[i][1])
    z_elite.append(elite[i][0])
    
    pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights[i][0][3:], weights[i][1][3:], weights[i][2][3:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
    
  

  
  print pretty_elite_table
  
  dir_elite = dir + "elite/"
  if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
  
  save_stdout = sys.stdout
  
  sys.stdout = open(dir_elite+"elite_list.txt", "wb")
  print pretty_elite_table
  
  sys.stdout = save_stdout
  
  #with open (dir_elite+"elite_list.txt", "wb") as file:
    #file.write(print(pretty_elite_table))
  
  #x_elite_dummy = []
  #y_elite_dummy = []
  #z_elite_dummy = []
  
  #for i in range(0,len(elite)):
    #x_elite_dummy.append(elite[i][2]*1.1)
    #y_elite_dummy.append(elite[i][1]*0.9)
    #z_elite_dummy.append(elite[i][0]*0.95)
  
  #pl.figure(1)
  #pl.xlabel(r"Efficiency ($\eta$) [%]")
  #pl.ylabel(r"Torque ($\tau$) [Nm]")
  #pl.title("Pareto Approximation of Efficiency against Torque")
  ##pl.scatter(eff,tor, alpha = 0.5)
  #pl.plot(eff,tor)
  #pl.plot(eff,tor,'bo')
  #pl.grid(True)

  #print(eff)

  #wired graph
  
  
  fig = pl.figure()
  

  
  if args.option == 18:
    #use latex font
    pl.rc('text', usetex=True)
    pl.rc('font', family='serif')
    
    font_size=18
    
    #ax = fig.add_subplot(111)
    ax = fig.add_subplot(111, projection='3d')
    #ax = fig.gca(projection='3d')
    #X, Y, Z = axes3d.get_test_data(0.05)
    X, Y, Z = pm_mass, tor, eff
    ax.set_xlabel(r"PM Mass ($m$) [kg]",fontsize=font_size)
    ax.set_ylabel(r"Torque ($\tau$) [Nm]",fontsize=font_size)
    ax.set_zlabel(r"Efficiency ($\eta$) [\%]",fontsize=font_size)
    
    ax.tick_params(axis='x', labelsize=font_size)
    ax.tick_params(axis='y', labelsize=font_size)
    ax.tick_params(axis='z', labelsize=font_size)  
    
    #ax.plot_wireframe(X, Y, Z,rstride=10, cstride=10)
    #ax.scatter(X, Y, Z, s=10, color='b', marker='s', label='all')
    ax.scatter(x_elite, y_elite, z_elite, s=10, color='r', marker='o', label='Elite solutions')
    #ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
    ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=10, color='b', marker='o', label='Remaining solutions')
    ax.scatter(prototype[2], prototype[1], prototype[0], s=50, color='m', marker='o', label='Prototype')
    #pl.legend(loc='upper right', fontsize = font_size)
    pl.legend(loc='upper center', fontsize = font_size)
    
    pl.savefig('3d_pareto.pdf',transparent=True, bbox_inches='tight')
    
    #ax.plot_surface(X, Y, Z, color='b')
    #ax.plot_trisurf(X, Y, Z, cmap=cm.jet, linewidth=0.2) #glad nie bad nie!
    #ax.plot_trisurf(X, Y, Z,cmap=cm.prism, linewidth=0.2)
    #ax.plot_trisurf(X, Y, Z,cmap=cm.gray, linewidth=0.2) #baie goed
    #ax.plot_trisurf(not_elite_x, not_elite_y, not_elite_z,cmap=cm.gray, linewidth=0.2) #baie goed
    #ax.plot_trisurf(x_elite, y_elite, z_elite,cmap=cm.jet, linewidth=0.2) #baie goed
    #ax.plot_trisurf(X, Y, Z,cmap=cm.RdGy, linewidth=0.2)
    
    #X = np.array(X)
    #Y = np.array(Y)
    #Z = np.array(Z)
    
    #zi = griddata((X, Y), Z, (X[None, :], Y[:, None]), method='cubic')    # create a uniform spaced grid
    #xig,yig = np.meshgrid(X, Y)
    #surf = ax.plot_wireframe(X, Y, Z=zi, rstride=5, cstride=3, linewidth=1) 
    #ax.plot_surface(xig, yig, zi, cmap = cm.jet)
    
    #ax.plot_surface(X, Y, Z, cmap = cm.jet)
    
    #x= np.arange(1, 100, 1)
    #y= np.arange(1, 100, 1)
    #z= np.arange(1, 100, 1)
    #fig = pl.figure()
    #ax = fig.gca(projection='3d')
    #xi = np.linspace(x.min(), x.max(), 50)
    #yi = np.linspace(y.min(), y.max(), 50)
    #zi = griddata((x, y), z, (xi[None, :], yi[:, None]), method='nearest')    # create a uniform spaced grid
    #xig, yig = np.meshgrid(xi, yi)
    #surf = ax.plot_wireframe(X=xig, Y=yig, Z=zi, rstride=5, cstride=3, linewidth=1)  
  elif args.option == 20:
    #ax = fig.add_subplot(111, projection='3d')
    X, Y, Z = pm_mass, tor, eff
    
    X = np.array(X)
    Y = np.array(Y)
    Z = np.array(Z)

    #MESHGRID
    #meshgrid is very useful to evaluate functions on a grid.
    #1-D arrays X,Y representing coordinates of a grid
    #xv,yv = np.meshgrid(X, Y)
    
    no_of_points = len(Z)
    x_lin = np.linspace(min(X),max(X),no_of_points)
    y_lin = np.linspace(min(X),max(X),no_of_points)
    xv,yv = np.meshgrid(x_lin, y_lin)
    
    #GRIDDATA
    '''
    original_points : ndarray (n-dimensional)(numpy array) of floats, shape (n, D)
    Data point coordinates. Can either be an array of shape (n, D), or a tuple of ndim arrays.
    
    values : ndarray of float or complex, shape (n,) (thus 1-D array)
    Data values.
    
    desired_points : ndarray of float, shape (M, D)
      Desired points at which to interpolate data (which will be shown on graph)
    '''
    
    original_points = (X,Y)
    #original_points = zip(*[X,Y]) #transposes the coordinates
    values = Z
    #desired_points = (X[None, :], Y[:, None])
    desired_points = (xv,yv)
    
    #scipy.interpolate.griddata(original_points, values, desired_points, method='linear', fill_value=nan, rescale=False)[source]
    zi = griddata(original_points, values, desired_points, method='cubic')    # create a uniform spaced grid
    #zi = griddata((X, Y), values, (X[None, :], Y[:, None]), method='cubic')    # create a uniform spaced grid    
    #zi = griddata(
    
    v = np.linspace(min(Z),max(Z),20)
    #v = [40,50,55,60,65,70,75,80,82,84,86,88,90,92,94,98]
    cs = pl.contour(X,Y,zi, v, linewidths =0.5, colours='k')
    CSf = pl.contourf(X, Y, zi)
    #CS = pl.contour(xv, yv, zi)
    #CS = pl.contour(X, Y, zi)
    #pl.clabel(CS, inline=1, fontsize=10)
    pl.title('Simplest default with labels')

#get elite solution designs as were decided upon in elif args.option == 18
elif args.option == 19:  
  #reset iteration number to zero
  with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%0)  
  
  #dir = './optimization_database/run16{itr=0.025}/'
  dir = dir_parent+'./optimization_database/'
  dir_elite = dir + "elite/"

  

  with open(dir_elite+"elite_list.txt", 'r') as file:
      # read a list of lines into data
      data_elite = file.readlines()
    
  old_folders = []  
  new_folders = []    
  
  constant_input_data = np.genfromtxt(dir_elite+'input_original.csv',skip_header=1,delimiter=',',usecols=(1,2,3),unpack=False)
  kq		= constant_input_data[1]						#coils per pole pair per phase                      [m]                             [m]
  l		= constant_input_data[5] 						#axial rotor/stator length                    [m]
  g		= constant_input_data[6] 						#each airgap length
  hc		= constant_input_data[7] 						#coil/winding thickness     
  hmo		= constant_input_data[36]						#outer magnet width
  hmi_r		= constant_input_data[37]						#inner magnet width
  ri		= constant_input_data[38]						#most inner radius point on machine
  ro		= constant_input_data[39]						#most outer radius point on machine  
  go		= constant_input_data[53]						
  gi		= constant_input_data[54]
  
  ri_min  =  ri[2]
  hmo_min =  hmo[2]
  go_min  =  go[2]
  hc_min  =  hc[2]
  gi_min  =  gi[2]
  hmi_r_min =  hmi_r[2]
  
  for i in range(4,len(data_elite)-1):
    entries = data_elite[i].split('|')    
    entries = entries[4:][:3]
  
    old_folders.append(dir + "w1=%.3f,w2=%.3f,w3=%.3f/"%(float(entries[0]),float(entries[1]),float(entries[2])))
    new_folders.append(dir_elite + "w_eff=%.3f,w_tor=%.3f,w_pm_mass=%.3f/"%(float(entries[0]),float(entries[1]),float(entries[2])))
  
  #get final design variables from original directories
  for i in range (0, len(old_folders)):
    if not os.path.isdir(new_folders[i]): os.makedirs(new_folders[i]) 

    #print(old_folders[i]+'input_original.csv')
    #with open(old_folders[i]+'input_original.csv', 'rb') as file:
      #data2 = file.readlines()
    #print data2

    shutil.copy(old_folders[i]+'input_original.csv',new_folders[i]+'input_edited.csv')
    shutil.copy(old_folders[i]+'optimization_results_summary.csv',new_folders[i]+'optimization_results_summary.csv')
    shutil.copy(old_folders[i]+'ideal_solutions.csv',new_folders[i]+'ideal_solutions.csv')
    variable_input_data = np.genfromtxt(old_folders[i] + 'optimization_results_summary.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
    
    #this file will be replaced with the command replaceLine
    #print(new_folders[i]+'input_edited.csv')
    with open(new_folders[i]+'input_edited.csv', 'rb') as file:
      data = file.readlines()
    #print data
    
    #edit input_edited.csv file
    p		= int(variable_input_data[6])*2						#pole pairs
    kmo		= variable_input_data[4] 						#radial PM width / pole pitch
    kc		= variable_input_data[5]						#coil winding ratio range(0-1)
    x_ri	= variable_input_data[0]						#design variable
    x_hmo	= variable_input_data[1]						#design variable
    x_go	= 0									#design variable
    x_hc	= variable_input_data[2]						#design variable
    x_gi	= 0									#design variable
    x_hmi_r	= variable_input_data[3]						#design variable
    kmi		= variable_input_data[7] 						#radial PM width / pole pitch

    ri  = (ro - ri_min - hmo_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
    hmo = (ro - ri     - hmo_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo + hmo_min
    go  = (ro - ri     - hmo     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
    hc  = (ro - ri     - hmo     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
    gi  = (ro - ri     - hmo     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
    hmi_r = (ro - ri     - hmo     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min

    #insert absolute values into input_edited.csv
    replaceLine(RI, float(ri[0]))
    replaceLine(HMO,float(hmo[0]))
    #replaceLine(GO,float(go[0]))
    replaceLine(HC, float(hc[0]))
    #replaceLine(GI,float(gi[0]))
    replaceLine(hmi_r,float(hmi_r[0]))
    replaceLine(KMO,float(kmo))
    replaceLine(KMI,float(kmi))
    replaceLine(KC,float(kc))
    replaceLine(P,int(p))
      
    replaceFile(new_folders[i]+file_input_name)
  
    #simulate these design variables (with finer mesh) (to verify previous results and this time save pictures too)
    #run idrfpm_semfem
    idrfpm_semfem.main(new_folders[i]+file_input_name)
  
    #place results in these directories (now with pdf and png pictures too)
    shutil.copy(dir_parent+'project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',new_folders[i]+'/band_machine_field001.fpl')
    shutil.copy(dir_parent+'project_idrfpm_semfem/results/post.res',new_folders[i]+'/post.res')
    shutil.copy(dir_parent+'pretty_input_table.txt',new_folders[i]+'/pretty_input_table.txt')
    shutil.copy(dir_parent+'pretty_terminal_table.txt',new_folders[i]+'/pretty_terminal_table.txt')
  
    #convert fpl file to pdf and png
    os.system('fpl2svg project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg')
    os.system('inkscape -A %sband_machine_field001.pdf ./project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg'%(new_folders[i]))

    svg_file = 'project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg'
    fout = open('%sband_machine_field001.png'%(new_folders[i]),'w')
    cairosvg.svg2png(url=svg_file,write_to=fout)
    fout.close()

    os.system('fpl2svg -ld -n 25 -w 0.15 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg')
    os.system('inkscape -A %sband_machine_field001_flux.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg'%(new_folders[i]))
  
    svg_file = 'project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg'
    fout = open('%sband_machine_field001_flux.png'%(new_folders[i]),'w')
    cairosvg.svg2png(url=svg_file,write_to=fout)
    fout.close()

  #reset iteration number to zero
  with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%0)  
    
  #generate more accurate elite list info  
  x_elite = []
  y_elite = []
  z_elite = []
  
  pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
  pretty_elite_table.align = 'l'
  pretty_elite_table.border= True  
  
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
  
  
  
  folders = [ name for name in os.listdir(dir_elite) if os.path.isdir(os.path.join(dir_elite, name)) ]

  initial_number_of_dirs = len(folders)
  dirs = []
  get_ideal = True
  tor = []
  pm_mass = []
  eff = []
  weights = []
  weights_temp = []
  elite = []
  
  for i in range(0,initial_number_of_dirs):
    bool = folders[i][0]=='w'
    if bool == True:
      dirs.append(dir_elite+folders[i])
      weights_temp.append(folders[i].split(','))
  
  print("Number of folders detected: %d"%(len(dirs)))
  if get_ideal == True: 
    ideal_objective_data = np.genfromtxt(dir_elite+folders[1]+'/ideal_solutions.csv',skip_header=1,delimiter=',',unpack=False)
    f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
    f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
    f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  


  for i in range(0,len(dirs)):
      #f_best = np.genfromtxt(dirs[i]+'/f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)

    with open(dir_elite+folders[i]+"/pretty_terminal_table.txt", 'r') as file:
	# read a list of lines into data
	data = file.readlines()

    output = []
    i = 3
    output.append(data[i+11].split('|')[2:][:1]) #efficiency
    output.append(data[i+4].split('|')[2:][:1]) #torque
    output.append(data[i+5].split('|')[2:][:1]) #pm_mass
    
    output[0] = float(output[0][0])
    output[1] = float(output[1][0])
    output[2] = float(output[2][0])
	
    elite.append([output[0],output[1],output[2]])
    #weights.append(weights_temp[i])	
	
    #if output[1]>min_tor and output[0]>min_eff and output[2]<max_pm: 
      #elite.append([output[0],output[1],output[2]])
      #weights.append(weights_temp[i])

  
  for i in range(0,len(elite)):
    x_elite.append(elite[i][2])
    y_elite.append(elite[i][1])
    z_elite.append(elite[i][0])
    
    pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights_temp[i][0][-5:], weights_temp[i][1][-5:], weights_temp[i][2][-5:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
    
  
  print pretty_elite_table
  
  #dir_elite = dir + "elite/"
  if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
  
  save_stdout = sys.stdout
  
  sys.stdout = open(dir_elite+"new_elite_list.txt", "wb")
  print pretty_elite_table
  
  sys.stdout = save_stdout

elif args.option == 21: 
  print("This is post_script.py 21 and this script is being run by thread %s"%(threading.current_thread()))
  #print("This is post_script.py and this script is being run by thread %s"%(MyThread.threading.current_thread()))

elif args.option == 22: 
  print("This is post_script.py 22 and this script is being run by thread %s"%(threading.current_thread()))  

#def fpl2pdf(enable_smc_cored_coils):
  #os.system("fpl2svg --help")
  #fpl2svg -- convert SEMFEM fpl files to svg

    #-d, --fluxdensity          Show flux density colormap
    #-l, --fluxlines            Show flux lines
    #-m, --mesh                 Show the mesh
    #-n, --nlines=<value>       Specify the number of flux lines
    #-s, --strokewidth=<value>  Specify the line width [mm] used for drawing triangles
    #-w, --fluxwidth=<value>    Specify the line width [mm] used for drawing flux lines
    #-?, --help                 Give this help list
    #    --usage                Give a short usage message
  if enable_smc_cored_coils == 0:
    os.system("fpl2svg project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_aircored_fpl.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")

    os.system("fpl2svg 0 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    #os.system("fpl2svg -m -s 0 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_aircored_mesh.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")

    os.system("fpl2svg -ld -n 25 -w 0.15 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_aircored_flux.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")

  else:
    os.system("fpl2svg project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_smccored_fpl.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")

    os.system("fpl2svg -m -s 0 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_smccored_mesh.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    
    os.system("fpl2svg -ld -n 25 -w 0.15 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")
    os.system("inkscape -A project_idrfpm_semfem/saved_pdf/band_machine_field001_smccored_flux.pdf project_idrfpm_semfem/results/fieldplots/band_machine_field001.svg")


#if steps != 1:
  ##print semfem results
  #print "\n\nSEMFEM results:"
  #print("Max phase current: %f [A]"%max(i1))
  #print("Max phase voltage: %f [V]"%max(voltage[0])) 	#phase A max induced voltage
  #print("Max flux linkage: %f [Wb]"%max(fl1))
  #print("Average torque: %f [Nm]"%np.average(t2))

elif args.option == 23:
  #dir = './optimization_database/run15(loop correct)/'
  #dir = './optimization_database/run14(itr=0.05)/'
  #dir = './optimization_database/run16{itr=0.025}/'
  #dir = dir_parent+'./optimization_database/run17{itr=0.025, kmo & kmi}/optimization_database/'
  #dir = dir_parent+'./optimization_database/run18{hmo_rt & hmi_rt}/optimization_database/'
  #dir = './optimization_database/run19{go & gi}/optimization_database/'
  #dir1 = './optimization_database/'
  same_colour = False
  dir = []
  dir.append(['./optimization_database/run16{itr=0.025}/','./optimization_database/run16{itr=0.025}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv']) 		#SEMFEM 7 variables
  #dir.append(['./optimization_database/run18{hmo_rt & hmi_rt}/optimization_database/','./optimization_database/run18{hmo_rt & hmi_rt}/ideal_solutions.csv']) 		#SEMFEM 7 variables
  #dir.append(['./optimization_database/run19{go & gi}/optimization_database/','./optimization_database/run19{go & gi}/ideal_solutions.csv']) 		#SEMFEM 7 variables
  dir.append(['./optimization_database/run20{analytical}/','./optimization_database/run20{analytical}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv'])		#Analytical 7 variables
  
  #print("Looking for optimization results in:")
  #print(dir)
  
  elite = []
  elite.append([])
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #PLOT THE RESULTS
  fig = pl.figure()

  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  
  font_size=18
  
  #ax = fig.add_subplot(111)
  ax = fig.add_subplot(111, projection='3d')
  #ax = fig.gca(projection='3d')
  #X, Y, Z = axes3d.get_test_data(0.05)
  #X, Y, Z = pm_mass, tor, eff
  ax.set_xlabel(r"PM Mass ($m$) [kg]",fontsize=font_size)
  ax.set_ylabel(r"Torque ($\tau$) [Nm]",fontsize=font_size)
  ax.set_zlabel(r"Efficiency ($\eta$) [\%]",fontsize=font_size)
  
  ax.tick_params(axis='x', labelsize=font_size)
  ax.tick_params(axis='y', labelsize=font_size)
  ax.tick_params(axis='z', labelsize=font_size)    
  
  for k in range (0,len(dir[:])):
    print("\nObtaining results from dir [%d]"%k)
    folders = [ name for name in os.listdir(dir[k][0]) if os.path.isdir(os.path.join(dir[k][0], name)) ]

    initial_number_of_dirs = len(folders)
    dirs = []
    get_ideal = True
    tor = []
    pm_mass = []
    eff = []
    weights = []
    weights_temp = []
    elite = []
    
    tor_qualifiers = 0
    eff_qualifiers = 0
    pm_mass_qualifiers = 0
    
    #print("weights initialized")
    #print weights
    #print ""
    
    for i in range(0,initial_number_of_dirs):
      bool = folders[k][0]=='w'
      if bool == True:
	dirs.append(dir[k][0]+folders[i])
	weights_temp.append(folders[i].split(','))
    
    print("Number of folders detected in %s: %d"%(dir[k][0],len(dirs)))
    if get_ideal == True: 
      ideal_dir = dir[k][1]
      #ideal_dir = dirs[0]+'/ideal_solutions.csv'
      #print dirs[1]
      print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
      ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
      
      f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
      f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
      f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

    print("f_eff_ideal = %f"%f_eff_ideal)
    print("f_tor_ideal = %f"%f_tor_ideal)
    print("f_pm_ideal  = %f"%f_pm_ideal)

    not_elite_x = []
    not_elite_y = []
    not_elite_z = []

    total_optimisation_time = 0

    for i in range(0,len(dirs)-1):
      f_best_dir = dirs[i]+'/f_data.csv'
      #print "here comes dirs"
      #print dirs
      if os.path.isfile(f_best_dir):
	#print f_best_dir
	f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
	if f_best[1]*f_eff_ideal < 120:
	  tor.append(f_best[2]*f_tor_ideal)
	  pm_mass.append(f_best[3]*f_pm_ideal)
	  eff.append(f_best[1]*f_eff_ideal)
	  
	  #if f_best[2]>min_tor and f_best[1]>min_eff and f_best[3]<max_pm:
	  if f_best[2]*f_tor_ideal>min_tor:
	    tor_qualifiers = tor_qualifiers + 1
	  if f_best[1]*f_eff_ideal>min_eff:
	    eff_qualifiers = eff_qualifiers + 1
	  if f_best[3]*f_pm_ideal<max_pm:
	    pm_mass_qualifiers = pm_mass_qualifiers + 1	
	  
	  if isclose(f_best[1]*f_eff_ideal, prototype[0], rel_tol=1e-09, abs_tol=2e-2):
	    print "[1] true"
	    if isclose(f_best[2]*f_tor_ideal, prototype[1], rel_tol=1e-09, abs_tol=2e-2):
	      print "[2] true"
	      if isclose(f_best[3]*f_pm_ideal, prototype[2], rel_tol=1e-09, abs_tol=2e-2):
		print "[3] true"
	  
	  if f_best[2]*f_tor_ideal>min_tor and f_best[1]*f_eff_ideal>min_eff and f_best[3]*f_pm_ideal<max_pm: 
	    elite.append([f_best[1]*f_eff_ideal,f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	    weights.append(weights_temp[i])
	    #print("dir[%d] has delivered this elite result"%k)
	  else:
	    not_elite_x.append(f_best[3]*f_pm_ideal)
	    not_elite_y.append(f_best[2]*f_tor_ideal)
	    not_elite_z.append(f_best[1]*f_eff_ideal)
	
	if len(f_best)>5:
	  #calculate total time it took to obtain these solution points
	  total_optimisation_time = total_optimisation_time + f_best[5]
	  days =  int(total_optimisation_time/(60*60*24))
	  hours = int(total_optimisation_time/(60*60))
	  minutes = int((total_optimisation_time - hours*60*60)/60)
	  seconds = int(total_optimisation_time - hours*60*60 - minutes*60)

    #print "looking for duration here"
    #print(dir[0][0]+'duration.txt')

    if len(f_best)>5:
      print("Total optimisation time was: %d days, %d hours, %d minutes, %d seconds"%(days,hours,minutes,seconds))
    elif os.path.isfile(dir[0][0]+'duration.txt'):
      with open(dir[0][0]+'duration.txt', 'r') as file:
      # read a list of lines into data
	total_optimisation_time = file.readlines()
      #total_optimisation_time = np.genfromtxt(dir[0][1]+'duration.txt',skip_header=0,delimiter=',',usecols=(2),unpack=True)
      print("Total optimisation time was: "+total_optimisation_time[1])
      
    print("tor_qualifiers = %d"%tor_qualifiers)
    print("eff_qualifiers = %d"%eff_qualifiers)
    print("pm_mass_qualifiers = %d"%pm_mass_qualifiers)
    
    x_elite = []
    y_elite = []
    z_elite = []
    
    pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
    pretty_elite_table.align = 'l'
    pretty_elite_table.border= True  
    
    pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
    
    #print weights
    
    
    if not elite[0]:
      del elite[0]
      #del weights[0]
    
    #print weights
   
    for i in range(0,len(elite)):
      #else:
      x_elite.append(elite[i][2])
      y_elite.append(elite[i][1])
      z_elite.append(elite[i][0])
      
      #print elite[i][0]
      #print elite[i][1]
      #print elite[i][2]
      #print weights
      #print weights[i][0][3:]
      #print weights[i][1][3:]
      #print weights[i][2][3:]
      pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights[i][0][3:], weights[i][1][3:], weights[i][2][3:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
      
    

    
    print pretty_elite_table
    
    dir_elite = dir[k][0] + "elite/"
    if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
    
    save_stdout = sys.stdout
    
    sys.stdout = open(dir_elite+"elite_list.txt", "wb")
    print pretty_elite_table
    
    sys.stdout = save_stdout
   
    pretty_elite_table.clear_rows()
   
    #ax.set_xlim3d(0,70)
    ax.set_ylim3d(0,70)
    #ax.set_ylim3d(sorted_sequence[len(sorted_sequence)-2][4],1.1*lim[1])
    #ax.set_zlim3d(sorted_sequence[len(sorted_sequence)-1][4],1.1*lim[2])
   
    if k == 0:
      ax.scatter(prototype[2], prototype[1], prototype[0], s=80, depthshade = False, c='chartreuse', marker='o',edgecolors='black', label='Existing prototype')
   
   #DONT USE "COLOR", RATHER USE "C" BECAUSE OF FILLSTYLE BUG
    if same_colour:
      ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
      #ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, c='b', marker='o', label='elite')
      ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False,marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)
    else:
      if k == 0:
	#ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, c='r', marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
	ax.scatter(x_elite, y_elite, z_elite, s=80, depthshade = False, c='r', marker='o',edgecolors='black', label='Solutions dominating prototype')
	#ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
	#ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False,c='b', marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)
	ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False,c='b', marker='o',edgecolors='none', label='Remaining solutions (FEM)')
      else:
	#ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, c='m', marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
	#ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
	#ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False, c='darkgreen', marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)   
	#ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False, c='orange', marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)
	ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False, c='orange', marker='o',edgecolors='none', label='Remaining solutions (SAM)')


    
  #ax.scatter(prototype[2], prototype[1], prototype[0], s=50, color='y', marker='o', label='Prototype')
  #ax.scatter(prototype[2], prototype[1], prototype[0], s=200, color='y', marker='o', label='Prototype')
  #ax.scatter(prototype[2], prototype[1], prototype[0], s=200, edgecolor='y' ,facecolors='y', color='y', marker='o', label='Prototype')
  #ax.scatter(prototype[2], prototype[1], prototype[0], s=50, depthshade = False, c='chartreuse', marker='o',edgecolors='black', label='Existing prototype')
  #pl.legend(loc='upper right', fontsize = font_size)
  pl.legend(loc='upper center', fontsize = font_size)
  
  pl.savefig('3d_pareto.pdf',transparent=True, bbox_inches='tight')  

if args.option == 24:
  dir = []
  dir.append(['./optimization_database/run16{itr=0.025}/','./optimization_database/run16{itr=0.025}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv']) 		#SEMFEM 7 variables
  dir.append(['./optimization_database/run20{analytical}/','./optimization_database/run20{analytical}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv'])		#Analytical 7 variables
  
  #print("Looking for optimization results in:")
  #print(dir)
  
  elite = []
  elite.append([])
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #PLOT THE RESULTS
  #fig = pl.figure()

  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  
  font_size=18
  

  
  #ax = fig.add_subplot(111)
  #ax = fig.add_subplot(111, projection='3d')
  #ax = fig.gca(projection='3d')
  #X, Y, Z = axes3d.get_test_data(0.05)
  #X, Y, Z = pm_mass, tor, eff
  #ax.set_xlabel(r"PM Mass ($m$) [kg]",fontsize=font_size)
  #ax.set_ylabel(r"Torque ($\tau$) [Nm]",fontsize=font_size)
  #ax.set_zlabel(r"Efficiency ($\eta$) [\%]",fontsize=font_size)
  
  #ax.tick_params(axis='x', labelsize=font_size)
  #ax.tick_params(axis='y', labelsize=font_size)
  #ax.tick_params(axis='z', labelsize=font_size)    
  
  #for k in range (0,len(dir[:])):
  k=0
  print("\nObtaining results from dir [%d]"%k)
  folders = [ name for name in os.listdir(dir[k][0]) if os.path.isdir(os.path.join(dir[k][0], name)) ]

  initial_number_of_dirs = len(folders)
  dirs = []
  get_ideal = True
  tor = []
  pm_mass = []
  eff = []
  weights = []
  weights_temp = []
  elite = []
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #print("weights initialized")
  #print weights
  #print ""
  
  for i in range(0,initial_number_of_dirs):
    bool = folders[k][0]=='w'
    if bool == True:
      dirs.append(dir[k][0]+folders[i])
      weights_temp.append(folders[i].split(','))
  
  print("Number of folders detected in %s: %d"%(dir[k][0],len(dirs)))
  if get_ideal == True: 
    ideal_dir = dir[k][1]
    #ideal_dir = dirs[0]+'/ideal_solutions.csv'
    #print dirs[1]
    print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
    ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
    
    f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
    f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
    f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

  print("f_eff_ideal = %f"%f_eff_ideal)
  print("f_tor_ideal = %f"%f_tor_ideal)
  print("f_pm_ideal  = %f"%f_pm_ideal)

  not_elite_x = []
  not_elite_y = []
  not_elite_z = []

  total_optimisation_time = 0

  for i in range(0,len(dirs)-1):
    f_best_dir = dirs[i]+'/f_data.csv'
    #print "here comes dirs"
    #print dirs
    if os.path.isfile(f_best_dir):
      #print f_best_dir
      f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
      if f_best[1]*f_eff_ideal < 120:
	tor.append(f_best[2]*f_tor_ideal)
	pm_mass.append(f_best[3]*f_pm_ideal)
	eff.append(f_best[1]*f_eff_ideal)
	
	#if f_best[2]>min_tor and f_best[1]>min_eff and f_best[3]<max_pm:
	if f_best[2]*f_tor_ideal>min_tor:
	  tor_qualifiers = tor_qualifiers + 1
	if f_best[1]*f_eff_ideal>min_eff:
	  eff_qualifiers = eff_qualifiers + 1
	if f_best[3]*f_pm_ideal<max_pm:
	  pm_mass_qualifiers = pm_mass_qualifiers + 1	
	
	if isclose(f_best[1]*f_eff_ideal, prototype[0], rel_tol=1e-09, abs_tol=2e-2):
	  print "[1] true"
	  if isclose(f_best[2]*f_tor_ideal, prototype[1], rel_tol=1e-09, abs_tol=2e-2):
	    print "[2] true"
	    if isclose(f_best[3]*f_pm_ideal, prototype[2], rel_tol=1e-09, abs_tol=2e-2):
	      print "[3] true"
	
	if f_best[2]*f_tor_ideal>min_tor and f_best[1]*f_eff_ideal>min_eff and f_best[3]*f_pm_ideal<max_pm: 
	  elite.append([f_best[1]*f_eff_ideal,f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	  weights.append(weights_temp[i])
	  #print("dir[%d] has delivered this elite result"%k)
	else:
	  not_elite_x.append(f_best[3]*f_pm_ideal)
	  not_elite_y.append(f_best[2]*f_tor_ideal)
	  not_elite_z.append(f_best[1]*f_eff_ideal)
      
      if len(f_best)>5:
	#calculate total time it took to obtain these solution points
	total_optimisation_time = total_optimisation_time + f_best[5]
	days =  int(total_optimisation_time/(60*60*24))
	hours = int(total_optimisation_time/(60*60))
	minutes = int((total_optimisation_time - hours*60*60)/60)
	seconds = int(total_optimisation_time - hours*60*60 - minutes*60)

  #print "looking for duration here"
  #print(dir[0][0]+'duration.txt')

  if len(f_best)>5:
    print("Total optimisation time was: %d days, %d hours, %d minutes, %d seconds"%(days,hours,minutes,seconds))
  elif os.path.isfile(dir[0][0]+'duration.txt'):
    with open(dir[0][0]+'duration.txt', 'r') as file:
    # read a list of lines into data
      total_optimisation_time = file.readlines()
    #total_optimisation_time = np.genfromtxt(dir[0][1]+'duration.txt',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    print("Total optimisation time was: "+total_optimisation_time[1])
    
  print("tor_qualifiers = %d"%tor_qualifiers)
  print("eff_qualifiers = %d"%eff_qualifiers)
  print("pm_mass_qualifiers = %d"%pm_mass_qualifiers)
  
  x_elite = []
  y_elite = []
  z_elite = []
  
  pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
  pretty_elite_table.align = 'l'
  pretty_elite_table.border= True  
  
  pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
  
  #print weights
  
  
  if not elite[0]:
    del elite[0]
    #del weights[0]
  
  #print weights
  
  for i in range(0,len(elite)):
    #else:
    x_elite.append(elite[i][2])
    y_elite.append(elite[i][1])
    z_elite.append(elite[i][0])
    
    #print elite[i][0]
    #print elite[i][1]
    #print elite[i][2]
    #print weights
    #print weights[i][0][3:]
    #print weights[i][1][3:]
    #print weights[i][2][3:]
    pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights[i][0][3:], weights[i][1][3:], weights[i][2][3:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
    
  

  
  print pretty_elite_table
  
  dir_elite = dir[k][0] + "elite/"
  if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
  
  save_stdout = sys.stdout
  
  sys.stdout = open(dir_elite+"elite_list.txt", "wb")
  print pretty_elite_table
  
  sys.stdout = save_stdout
  
  pretty_elite_table.clear_rows()
  
  x_opt = np.array(not_elite_x)
  y_opt = np.array(not_elite_y)
  z_opt = np.array(not_elite_z)
  
  xmin = min(x_opt)
  ymin = min(y_opt)
  xmax = max(x_opt)
  ymax = max(y_opt)
  
  ###############################################
  # Perform a kernel density estimate on the data:
  
  X, Y = np.mgrid[xmin:xmax:100j, ymin:ymax:100j]
  positions = np.vstack([X.ravel(), Y.ravel()])
  #positions = np.hstack([X.ravel(), Y.ravel()])
  values = np.vstack([x_opt, y_opt])
  kernel = stats.gaussian_kde(values)
  Z = np.reshape(kernel(positions).T, X.shape)
  #Z = np.reshape(kernel(positions), Y.shape)

  # Plot the results:
  fig, ax = pl.subplots()
  img = ax.imshow(np.rot90(Z), cmap=cm.jet_r,#plt.cm.gist_earth_r,
	    extent=[xmin, xmax, ymin, ymax])
  ax.plot(x_opt, y_opt, 'k.', markersize=4)
  ax.set_xlim([xmin, xmax/5])
  ax.set_ylim([ymin, ymax/8]) 
  #pl.colorbar()
  #fig.colorbar(img) #works but gives weird numbers on scale, because Z has weird numbers
  ###############################################
  
  ###############################################
  # Perform Rbf
  #ti_x = np.linspace(min(x_opt),max(x_opt),np.size(x_opt))
  #ti_y = np.linspace(min(y_opt),max(y_opt),np.size(y_opt))
  #XI, YI = np.meshgrid(ti_x, ti_y)
  
  ##x--PM mass
  ##y--Torque
  ##z--Efficiency
  
  #rbf = Rbf(x_opt,y_opt,z_opt)#, epsilon = 2)
  #ZI = rbf(XI, YI)
  
  ## plot the result
  #pl.subplot(1, 1, 1)
  #pl.pcolor(XI, YI, ZI, cmap=cm.jet)
  #pl.scatter(x_opt, y_opt, 100, z_opt, cmap=cm.jet)
  #pl.title('RBF interpolation - multiquadrics')
  #pl.xlim(min(x_opt),max(x_opt))
  #pl.ylim(min(y_opt),max(y_opt))
  #pl.colorbar()   
  ###############################################
  
  #DONT USE "COLOR", RATHER USE "C" BECAUSE OF FILLSTYLE BUG
  #if same_colour:
    #ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
    ##ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, c='b', marker='o', label='elite')
    #ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False,marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)
  #else:
    #if k == 0:
      #ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, c='r', marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
      ##ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
      #ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False,c='b', marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)
    #else:
      #ax.scatter(x_elite, y_elite, z_elite, s=markersize, depthshade = False, c='m', marker='o',edgecolors='none', label='Elite solutions[%d]'%k)
      ##ax.scatter(x_elite_dummy, y_elite_dummy, z_elite_dummy, s=10, color='b', marker='o', label='elite')
      #ax.scatter(not_elite_x, not_elite_y, not_elite_z, s=markersize, depthshade = False, c='darkgreen', marker='o',edgecolors='none', label='Remaining solutions[%d]'%k)   


  
#ax.scatter(prototype[2], prototype[1], prototype[0], s=50, depthshade = False, c='chartreuse', marker='o',edgecolors='black', label='Prototype')
#pl.legend(loc='upper center', fontsize = font_size)

#pl.savefig('3d_pareto.pdf',transparent=True, bbox_inches='tight')  

if args.option == 25:
  dir = []
  dir.append(['./optimization_database/run16{itr=0.025}/','./optimization_database/run16{itr=0.025}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv']) 		#SEMFEM 7 variables
  #dir.append(['./optimization_database/run20{analytical}/','./optimization_database/run20{analytical}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv'])		#Analytical 7 variables
  
  filter_eff = []
  filter_eff.append([88,90,[]])
  filter_eff.append([84,86,[]])
  filter_eff.append([80,82,[]])
  filter_eff.append([76,78,[]])
  filter_eff.append([72,74,[]])
  #filter_eff.append([68,70,[]])
  #filter_eff.append([64,66,[]])
  
  
  #print("Looking for optimization results in:")
  #print(dir)
  
  elite = []
  elite.append([])
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #PLOT THE RESULTS
  #fig = pl.figure()

  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  font_size=18
   
  
  #for k in range (0,len(dir[:])):
  k=0
  print("\nObtaining results from dir [%d]"%k)
  folders = [ name for name in os.listdir(dir[k][0]) if os.path.isdir(os.path.join(dir[k][0], name)) ]

  initial_number_of_dirs = len(folders)
  dirs = []
  get_ideal = True
  tor = []
  pm_mass = []
  eff = []
  weights = []
  weights_temp = []
  elite = []
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #print("weights initialized")
  #print weights
  #print ""
  
  for i in range(0,initial_number_of_dirs):
    bool = folders[k][0]=='w'
    if bool == True:
      dirs.append(dir[k][0]+folders[i])
      weights_temp.append(folders[i].split(','))
  
  print("Number of folders detected in %s: %d"%(dir[k][0],len(dirs)))
  if get_ideal == True: 
    ideal_dir = dir[k][1]
    #ideal_dir = dirs[0]+'/ideal_solutions.csv'
    #print dirs[1]
    print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
    ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
    
    f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
    f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
    f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

  print("f_eff_ideal = %f"%f_eff_ideal)
  print("f_tor_ideal = %f"%f_tor_ideal)
  print("f_pm_ideal  = %f"%f_pm_ideal)

  not_elite_x = []
  not_elite_y = []
  not_elite_z = []

  total_optimisation_time = 0

  for i in range(0,len(dirs)-1):
    f_best_dir = dirs[i]+'/f_data.csv'
    #print "here comes dirs"
    #print dirs
    if os.path.isfile(f_best_dir):
      #print f_best_dir
      f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
      if f_best[1]*f_eff_ideal < 120:
	tor.append(f_best[2]*f_tor_ideal)
	pm_mass.append(f_best[3]*f_pm_ideal)
	eff.append(f_best[1]*f_eff_ideal)
	
	#if f_best[2]>min_tor and f_best[1]>min_eff and f_best[3]<max_pm:
	if f_best[2]*f_tor_ideal>min_tor:
	  tor_qualifiers = tor_qualifiers + 1
	if f_best[1]*f_eff_ideal>min_eff:
	  eff_qualifiers = eff_qualifiers + 1
	if f_best[3]*f_pm_ideal<max_pm:
	  pm_mass_qualifiers = pm_mass_qualifiers + 1	
	
	if isclose(f_best[1]*f_eff_ideal, prototype[0], rel_tol=1e-09, abs_tol=2e-2):
	  print "[1] true"
	  if isclose(f_best[2]*f_tor_ideal, prototype[1], rel_tol=1e-09, abs_tol=2e-2):
	    print "[2] true"
	    if isclose(f_best[3]*f_pm_ideal, prototype[2], rel_tol=1e-09, abs_tol=2e-2):
	      print "[3] true"
	
	for n in range(0,len(filter_eff)):
	  if f_best[1]*f_eff_ideal>filter_eff[n][0] and f_best[1]*f_eff_ideal<filter_eff[n][1]:
	    filter_eff[n][2].append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	##if eff is between 78 and 80
	#if f_best[1]*f_eff_ideal<80 and f_best[1]*f_eff_ideal>78:
	  #filter_eff_78_80.append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal]) #append torque and pm mass 
	#if f_best[1]*f_eff_ideal<74 and f_best[1]*f_eff_ideal>72:  
	  #filter_eff_76_78.append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal]) #append torque and pm mass 
	
	if f_best[2]*f_tor_ideal>min_tor and f_best[1]*f_eff_ideal>min_eff and f_best[3]*f_pm_ideal<max_pm: 
	  elite.append([f_best[1]*f_eff_ideal,f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	  weights.append(weights_temp[i])
	  #print("dir[%d] has delivered this elite result"%k)
	else:
	  not_elite_x.append(f_best[3]*f_pm_ideal)
	  not_elite_y.append(f_best[2]*f_tor_ideal)
	  not_elite_z.append(f_best[1]*f_eff_ideal)
      
      if len(f_best)>5:
	#calculate total time it took to obtain these solution points
	total_optimisation_time = total_optimisation_time + f_best[5]
	days =  int(total_optimisation_time/(60*60*24))
	hours = int(total_optimisation_time/(60*60))
	minutes = int((total_optimisation_time - hours*60*60)/60)
	seconds = int(total_optimisation_time - hours*60*60 - minutes*60)

  #print "looking for duration here"
  #print(dir[0][0]+'duration.txt')

  if len(f_best)>5:
    print("Total optimisation time was: %d days, %d hours, %d minutes, %d seconds"%(days,hours,minutes,seconds))
  elif os.path.isfile(dir[0][0]+'duration.txt'):
    with open(dir[0][0]+'duration.txt', 'r') as file:
    # read a list of lines into data
      total_optimisation_time = file.readlines()
    #total_optimisation_time = np.genfromtxt(dir[0][1]+'duration.txt',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    print("Total optimisation time was: "+total_optimisation_time[1])
    
  print("tor_qualifiers = %d"%tor_qualifiers)
  print("eff_qualifiers = %d"%eff_qualifiers)
  print("pm_mass_qualifiers = %d"%pm_mass_qualifiers)
  
  x_elite = []
  y_elite = []
  z_elite = []
  
  pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
  pretty_elite_table.align = 'l'
  pretty_elite_table.border= True  
  
  pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
  
  #print weights
  
  
  if not elite[0]:
    del elite[0]
  
  for i in range(0,len(elite)):
    #else:
    x_elite.append(elite[i][2])
    y_elite.append(elite[i][1])
    z_elite.append(elite[i][0])
    pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights[i][0][3:], weights[i][1][3:], weights[i][2][3:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
    
  

  
  print pretty_elite_table
  
  dir_elite = dir[k][0] + "elite/"
  if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
  
  save_stdout = sys.stdout
  
  sys.stdout = open(dir_elite+"elite_list.txt", "wb")
  print pretty_elite_table
  
  sys.stdout = save_stdout
  
  pretty_elite_table.clear_rows()
  

  
  fig = pl.figure(1)
  colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']
  scatter = []
  text = []
  handles_list = []
  markersize=150
  
  for n in range (0,len(filter_eff)):
    x = np.zeros(len(filter_eff[n][2]))
    y = np.zeros(len(filter_eff[n][2]))
    text.append(r'$%d<\eta<%d$'%(filter_eff[n][0],filter_eff[n][1]))
    for p in range (0,len(filter_eff[n][2])):
      x[p] = filter_eff[n][2][p][0]
      y[p] = filter_eff[n][2][p][1]
      
    #f = interp1d(x, y, kind='cubic')  
    f = interp1d(x, y, kind='linear',assume_sorted=False) 
    xnew = np.linspace(min(x), max(x), num=len(x), endpoint=True)
    handles_list.append(pl.plot(xnew,f(xnew),c=colors[n]))
    scatter.append(pl.scatter(x,y,c=colors[n]))#,legend=r'$%d<\eta<%d$'%(filter_eff[n][0],filter_eff[n][1])))
  
  #try to plot elite solutions too
  scatter.append(pl.scatter(y_elite,x_elite,c='orange',s=markersize,marker='^'))
  text.append(r'Dominating solution')
  
  #try to add prototype design too
  scatter.append(pl.scatter(min_tor,max_pm,c='darkgreen',s=markersize,marker='p'))
  text.append(r'Existing prototype (P)')
  
  
  handles_list = list(chain.from_iterable(handles_list))
  
  tuple_text = tuple(text)
  tuple_scatter = tuple(scatter)
  #pl.legend(handles=[f_interpolate],tuple_scatter,tuple_text,scatterpoints=1,loc='best',title=r'Efficiency ($\eta$) Range [$\%$]')
  #pl.legend(handles=handles_list)
  legend = pl.legend(tuple_scatter,tuple_text,scatterpoints=3,markerscale=1.5 ,loc='best',title=r'Efficiency ($\eta$) Range [$\%$]')#,fontsize=12)
  #pl.setp(legend.get_title(),fontsize='xx-small')
  pl.setp(legend.get_title(),fontsize='x-large')
  pl.xlim([0,70])
  pl.ylabel(r"PM Mass ($m$) [kg]")
  pl.xlabel(r"Torque ($\tau$) [N.m]")
  
  
  #try to plot elite solutions too
  #markersize=500
  #pl.scatter(y_elite,x_elite,c='orange',s=markersize,marker='*')
  
  #markersize=200
  #pl.scatter(y_elite,x_elite,c='orange',s=markersize,marker='^')
  
  pl.grid(True)
  savefig = True
  
  if savefig == True:
    pl.savefig('contour_tor_pm.pdf',transparent=True)#, bbox_inches='tight')     
  else:
    pl.title(r"Pareto Approximation of PM Mass against Torque (given a certain efficiency)") # (with $78<\eta<80$)



##########################################################
if args.option == 26:
  dir = []
  dir.append(['./optimization_database/run16{itr=0.025}/','./optimization_database/run16{itr=0.025}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv']) 		#SEMFEM 7 variables
  dir.append(['./optimization_database/run20{analytical}/','./optimization_database/run20{analytical}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv'])		#Analytical 7 variables
  
  filter_eff_78_80 = []
  filter_eff_76_78 = []
  #filter_eff.append()
  
  #print("Looking for optimization results in:")
  #print(dir)
  
  elite = []
  elite.append([])
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #PLOT THE RESULTS
  #fig = pl.figure()

  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  
  font_size=18
  

  
  #ax = fig.add_subplot(111)
  #ax = fig.add_subplot(111, projection='3d')
  #ax = fig.gca(projection='3d')
  #X, Y, Z = axes3d.get_test_data(0.05)
  #X, Y, Z = pm_mass, tor, eff
  #ax.set_xlabel(r"PM Mass ($m$) [kg]",fontsize=font_size)
  #ax.set_ylabel(r"Torque ($\tau$) [Nm]",fontsize=font_size)
  #ax.set_zlabel(r"Efficiency ($\eta$) [\%]",fontsize=font_size)
  
  #ax.tick_params(axis='x', labelsize=font_size)
  #ax.tick_params(axis='y', labelsize=font_size)
  #ax.tick_params(axis='z', labelsize=font_size)    
  
  #for k in range (0,len(dir[:])):
  k=0
  print("\nObtaining results from dir [%d]"%k)
  folders = [ name for name in os.listdir(dir[k][0]) if os.path.isdir(os.path.join(dir[k][0], name)) ]

  initial_number_of_dirs = len(folders)
  dirs = []
  get_ideal = True
  tor = []
  pm_mass = []
  eff = []
  weights = []
  weights_temp = []
  elite = []
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #print("weights initialized")
  #print weights
  #print ""
  
  for i in range(0,initial_number_of_dirs):
    bool = folders[k][0]=='w'
    if bool == True:
      dirs.append(dir[k][0]+folders[i])
      weights_temp.append(folders[i].split(','))
  
  print("Number of folders detected in %s: %d"%(dir[k][0],len(dirs)))
  if get_ideal == True: 
    ideal_dir = dir[k][1]
    #ideal_dir = dirs[0]+'/ideal_solutions.csv'
    #print dirs[1]
    print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
    ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
    
    f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
    f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
    f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

  print("f_eff_ideal = %f"%f_eff_ideal)
  print("f_tor_ideal = %f"%f_tor_ideal)
  print("f_pm_ideal  = %f"%f_pm_ideal)

  not_elite_x = []
  not_elite_y = []
  not_elite_z = []

  total_optimisation_time = 0

  for i in range(0,len(dirs)-1):
    f_best_dir = dirs[i]+'/f_data.csv'
    #print "here comes dirs"
    #print dirs
    if os.path.isfile(f_best_dir):
      #print f_best_dir
      f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
      if f_best[1]*f_eff_ideal < 120:
	tor.append(f_best[2]*f_tor_ideal)
	pm_mass.append(f_best[3]*f_pm_ideal)
	eff.append(f_best[1]*f_eff_ideal)
	
	#if f_best[2]>min_tor and f_best[1]>min_eff and f_best[3]<max_pm:
	if f_best[2]*f_tor_ideal>min_tor:
	  tor_qualifiers = tor_qualifiers + 1
	if f_best[1]*f_eff_ideal>min_eff:
	  eff_qualifiers = eff_qualifiers + 1
	if f_best[3]*f_pm_ideal<max_pm:
	  pm_mass_qualifiers = pm_mass_qualifiers + 1	
	
	if isclose(f_best[1]*f_eff_ideal, prototype[0], rel_tol=1e-09, abs_tol=2e-2):
	  print "[1] true"
	  if isclose(f_best[2]*f_tor_ideal, prototype[1], rel_tol=1e-09, abs_tol=2e-2):
	    print "[2] true"
	    if isclose(f_best[3]*f_pm_ideal, prototype[2], rel_tol=1e-09, abs_tol=2e-2):
	      print "[3] true"
	
	##if eff is between 78 and 80
	#if f_best[1]*f_eff_ideal<80 and f_best[1]*f_eff_ideal>78:
	  #filter_eff_78_80.append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal]) #append torque and pm mass 
	#if f_best[1]*f_eff_ideal<74 and f_best[1]*f_eff_ideal>72:  
	  #filter_eff_76_78.append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal]) #append torque and pm mass 
	
	if f_best[2]*f_tor_ideal>min_tor and f_best[1]*f_eff_ideal>min_eff and f_best[3]*f_pm_ideal<max_pm: 
	  elite.append([f_best[1]*f_eff_ideal,f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
	  weights.append(weights_temp[i])
	  #print("dir[%d] has delivered this elite result"%k)
	else:
	  not_elite_x.append(f_best[3]*f_pm_ideal)
	  not_elite_y.append(f_best[2]*f_tor_ideal)
	  not_elite_z.append(f_best[1]*f_eff_ideal)
      
      if len(f_best)>5:
	#calculate total time it took to obtain these solution points
	total_optimisation_time = total_optimisation_time + f_best[5]
	days =  int(total_optimisation_time/(60*60*24))
	hours = int(total_optimisation_time/(60*60))
	minutes = int((total_optimisation_time - hours*60*60)/60)
	seconds = int(total_optimisation_time - hours*60*60 - minutes*60)

  #print "looking for duration here"
  #print(dir[0][0]+'duration.txt')

  if len(f_best)>5:
    print("Total optimisation time was: %d days, %d hours, %d minutes, %d seconds"%(days,hours,minutes,seconds))
  elif os.path.isfile(dir[0][0]+'duration.txt'):
    with open(dir[0][0]+'duration.txt', 'r') as file:
    # read a list of lines into data
      total_optimisation_time = file.readlines()
    #total_optimisation_time = np.genfromtxt(dir[0][1]+'duration.txt',skip_header=0,delimiter=',',usecols=(2),unpack=True)
    print("Total optimisation time was: "+total_optimisation_time[1])
    
  print("tor_qualifiers = %d"%tor_qualifiers)
  print("eff_qualifiers = %d"%eff_qualifiers)
  print("pm_mass_qualifiers = %d"%pm_mass_qualifiers)
  
  x_elite = []
  y_elite = []
  z_elite = []
  
  pretty_elite_table = PrettyTable(['Efficiency [%]','Torque [Nm]','PM Mass [kg]','w_eff','w_tor','w_pm','[%] Eff increase','[%] Torque increase','[%] PM Mass increase'])
  pretty_elite_table.align = 'l'
  pretty_elite_table.border= True  
  
  pretty_elite_table.add_row([prototype[0],prototype[1],prototype[2], "Present", "prototype", "",0,0,0])    
  
  #print weights
  
  
  if not elite[0]:
    del elite[0]
    #del weights[0]
  
  #print weights
  
  for i in range(0,len(elite)):
    #else:
    x_elite.append(elite[i][2])
    y_elite.append(elite[i][1])
    z_elite.append(elite[i][0])
    
    #print elite[i][0]
    #print elite[i][1]
    #print elite[i][2]
    #print weights
    #print weights[i][0][3:]
    #print weights[i][1][3:]
    #print weights[i][2][3:]
    pretty_elite_table.add_row([elite[i][0], elite[i][1], elite[i][2], weights[i][0][3:], weights[i][1][3:], weights[i][2][3:], ((elite[i][0]-prototype[0])/prototype[0])*100, ((elite[i][1]-prototype[1])/prototype[1])*100, ((elite[i][2]-prototype[2])/prototype[2])*100])
    
  

  
  print pretty_elite_table
  
  dir_elite = dir[k][0] + "elite/"
  if not os.path.isdir(dir_elite): os.makedirs(dir_elite)
  
  save_stdout = sys.stdout
  
  sys.stdout = open(dir_elite+"elite_list.txt", "wb")
  print pretty_elite_table
  
  sys.stdout = save_stdout
  
  pretty_elite_table.clear_rows()
  
  ##x--PM mass
  ##y--Torque
  ##z--Efficiency
  
  fig = pl.figure(1)
  
  X, Y = np.meshgrid(not_elite_x[:20], not_elite_y[:20])
  Z1 = mlab.bivariate_normal(X, Y, 1.0, 1.0, 0.0, 0.0)
  Z2 = mlab.bivariate_normal(X, Y, 1.5, 0.5, 1, 1)
  # difference of Gaussians
  Z = 10.0 * (Z2 - Z1)
  CS = pl.contour(X, Y, Z)
  pl.clabel(CS, inline=1, fontsize=10)
  pl.title('Simplest default with labels')  
  pl.scatter(not_elite_x[:20],not_elite_y[:20])
  
  
  
  
  
  
  
  ##eff 78 to 80
  #x = np.zeros(len(filter_eff_78_80))
  #y = np.zeros(len(filter_eff_78_80))  

  #for i in range (0, len(filter_eff_78_80)):
    #x[i]=filter_eff_78_80[i][0]
    #y[i]=filter_eff_78_80[i][1]
 
  #f = interp1d(x, y)
  #xnew = np.linspace(min(x), max(x), num=41, endpoint=True) 
  
  #pl.plot(xnew,f(xnew))
 
  ##plot 2d scatter plot
  #pl.scatter(x,y,c='r')
  
  
  ##eff 76 to 78
  #x = np.zeros(len(filter_eff_76_78))
  #y = np.zeros(len(filter_eff_76_78))  
  #for i in range (0, len(filter_eff_76_78)):
    #x[i]=filter_eff_76_78[i][0]
    #y[i]=filter_eff_76_78[i][1]
  
  ##plot 2d scatter plot
  #pl.scatter(x,y,c='b')
  
  
  
  
  #pl.ylabel(r"PM Mass ($m$) [kg]")
  #pl.xlabel(r"Torque ($\tau$) [N.m]")
  #pl.title(r"Pareto Approximation of PM Mass against Torque") # (with $78<\eta<80$)
  ##pl.scatter(pm_mass,eff, alpha = 0.5)
  #pl.grid(True)
  ##pl.legend(loc='lower left')
  ##pl.savefig(dir+'/eff_pm_pareto(eff).pdf',transparent=True)#, bbox_inches='tight')    

if args.option == 27:
  dir = []
  dir.append(['./optimization_database/run16{itr=0.025}/','./optimization_database/run16{itr=0.025}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv']) 		#SEMFEM 7 variables
  dir.append(['./optimization_database/run20{analytical}/','./optimization_database/run20{analytical}/w1=0.000,w2=1.000,w3=0.000/ideal_solutions.csv'])		#Analytical 7 variables
  
  filter_eff = []
  filter_eff.append([88,90,[]])
  filter_eff.append([84,86,[]])
  filter_eff.append([80,82,[]])
  filter_eff.append([76,78,[]])
  filter_eff.append([72,74,[]])
  #filter_eff.append([68,70,[]])
  #filter_eff.append([64,66,[]])
  
  
  #print("Looking for optimization results in:")
  #print(dir)
  
  elite = []
  elite.append([])
 
  with open(file_prototype_performance, 'r') as file:
    # read a list of lines into data
    data = file.readlines()
  
  prototype = []
  prototype.append(float(data[14].split('|')[2])) #efficiency
  prototype.append(float(data[7].split('|')[2]))  #torque
  prototype.append(float(data[8].split('|')[2]))  #pm_mass
  
  min_eff = prototype[0]
  min_tor = prototype[1]
  max_pm = prototype[2]
  
  tor_qualifiers = 0
  eff_qualifiers = 0
  pm_mass_qualifiers = 0
  
  #PLOT THE RESULTS
  fig = pl.figure()

  #use latex font
  pl.rc('text', usetex=True)
  pl.rc('font', family='serif')
  font_size=18
  colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']
  deviation = []
  f = []
   
  
  for k in range (0,len(dir[:])):
    f.append([])
    print("\nObtaining results from dir [%d]"%k)
    folders = [ name for name in os.listdir(dir[k][0]) if os.path.isdir(os.path.join(dir[k][0], name)) ]

    initial_number_of_dirs = len(folders)
    dirs = []
    get_ideal = True

    
    for i in range(0,initial_number_of_dirs):
      bool = folders[k][0]=='w'
      if bool == True:
	dirs.append(dir[k][0]+folders[i])
	#weights_temp.append(folders[i].split(','))
    
    #print("Number of folders detected in %s: %d"%(dir[k][0],len(dirs)))
    if get_ideal == True: 
      ideal_dir = dir[k][1]
      #ideal_dir = dirs[0]+'/ideal_solutions.csv'
      #print dirs[1]
      print("Using the following directory to obtain ideal_solutions: %s"%ideal_dir)
      ideal_objective_data = np.genfromtxt(ideal_dir,skip_header=1,delimiter=',',unpack=False)
      
      f_eff_ideal = abs(ideal_objective_data[0]) #f_eff_ideal is already a negative number
      f_tor_ideal = abs(ideal_objective_data[1]) #f_tor_ideal is already a negative number
      f_pm_ideal  = abs(ideal_objective_data[2]) #f_pm_ideal stays a positive number	  

    print("f_eff_ideal = %f"%f_eff_ideal)
    print("f_tor_ideal = %f"%f_tor_ideal)
    print("f_pm_ideal  = %f"%f_pm_ideal)

    for i in range(0,len(dirs)-1):
      f_best_dir = dirs[i]+'/f_data.csv'
      #print "here comes dirs"
      #print dirs
      if os.path.isfile(f_best_dir):
	#print f_best_dir
	f_best = np.genfromtxt(f_best_dir,skip_header=0,delimiter=',',usecols=(2),unpack=True)
	for n in range(0,len(filter_eff)):
	  if f_best[1]*f_eff_ideal>filter_eff[n][0] and f_best[1]*f_eff_ideal<filter_eff[n][1]:
	    filter_eff[n][2].append([f_best[2]*f_tor_ideal,f_best[3]*f_pm_ideal])
    
    
    scatter = []
    text = []
    handles_list = []
    
    for n in range (0,len(filter_eff)):
      x = np.zeros(len(filter_eff[n][2]))
      y = np.zeros(len(filter_eff[n][2]))
      text.append(r'$%d<\eta<%d$'%(filter_eff[n][0],filter_eff[n][1]))
      for p in range (0,len(filter_eff[n][2])):
	x[p] = filter_eff[n][2][p][0]
	y[p] = filter_eff[n][2][p][1]
	
      #f = interp1d(x, y, kind='cubic')  
      f[k] = interp1d(x, y, kind='linear',assume_sorted=False) 
      xnew = np.linspace(min(x), max(x), num=len(x), endpoint=True)
      #handles_list.append(pl.plot(xnew,f(xnew),c=colors[n]))
      scatter.append(pl.scatter(x,y,c=colors[n]))#,legend=r'$%d<\eta<%d$'%(filter_eff[n][0],filter_eff[n][1])))
    
    #handles_list = list(chain.from_iterable(handles_list))
    
    tuple_text = tuple(text)
    tuple_scatter = tuple(scatter)
    #pl.legend(handles=[f_interpolate],tuple_scatter,tuple_text,scatterpoints=1,loc='best',title=r'Efficiency ($\eta$) Range [$\%$]')
    #pl.legend(handles=handles_list)
  
  #f_stdev = []
  #for n in range (0,len(filter_eff)):
    #for p in range (0,len(filter_eff[n][2])):
      
      #f_stdev.append(stdev(f[
  
  pl.legend(tuple_scatter,tuple_text,scatterpoints=3,markerscale=1.5 ,loc='best',title=r'Efficiency ($\eta$) Range [$\%$]')
  
  pl.xlim([0,70])
  pl.ylabel(r"PM Mass ($m$) [kg]")
  pl.xlabel(r"Torque ($\tau$) [N.m]")
  
  pl.grid(True)
  savefig = True
  
  if savefig == True:
    pl.savefig('contour_tor_pm_deviation.pdf',transparent=True)#, bbox_inches='tight')     
  else:
    pl.title(r"Pareto Deviation of PM Mass against Torque (given a certain efficiency)") # (with $78<\eta<80$)


elif args.option == 28:
    #dir_pimp = '/home/andreas/Documents/Meesters/SEMFEM/machine_scripts/optimization_database/run20{analytical}/w1=0.425,w2=0.250,w3=0.325/'
    dir_pimp = '/home/andreas/Documents/Meesters/SEMFEM/machine_scripts/optimization_database/run16{itr=0.025}/w1=0.450,w2=0.250,w3=0.300/'
    #dir_pimp = '/home/andreas/Documents/Meesters/SEMFEM/machine_scripts/optimization_database/run16{itr=0.025}/w1=0.425,w2=0.250,w3=0.325/'
    #dir_pimp = '/home/andreas/Documents/Meesters/SEMFEM/machine_scripts/optimization_database/run16{itr=0.025}/w1=0.400,w2=0.275,w3=0.325/'
    
    variable_input_data = np.genfromtxt(dir_pimp + 'optimization_results_summary.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
    
    print("Comparing prototype design variables with optimization_results_summary.csv obtained from")
    print(dir_pimp)
    
    
    #this file will be replaced with the command replaceLine
    #print(new_folders[i]+'input_edited.csv')
    #with open(new_folders[i]+'input_edited.csv', 'rb') as file:
      #data = file.readlines()
    #print data
    
    old_ri =0.109
    old_hmo_r=0.0055
    old_go=0.001
    old_hc=0.009
    old_gi=0.001
    old_hmi_r = 0.0055
    old_kc = 0.71757
    old_kmo = 0.5
    old_kmi = old_kmo
    old_p=14
    
    ri_min=0.05
    hmo_r_min=0.002
    go_min=0.001
    hc_min=0.002
    gi_min=0.001
    hmi_r_min=0.002
    
    #edit input_edited.csv file
    ro=0.131
    p		= int(variable_input_data[6])*2						#pole pairs
    kmo		= variable_input_data[4] 						#radial PM width / pole pitch
    kmi = kmo
    kc		= variable_input_data[5]						#coil winding ratio range(0-1)
    x_ri	= variable_input_data[0]						#design variable
    x_hmo_r	= variable_input_data[1]						#design variable
    x_go	= 0									#design variable
    x_hc	= variable_input_data[2]						#design variable
    x_gi	= 0									#design variable
    x_hmi_r	= variable_input_data[3]						#design variable
    #kmi		= variable_input_data[7] 						#radial PM width / pole pitch

    #ri  = (ro - ri_min - hmo_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
    #hmo = (ro - ri     - hmo_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo + hmo_min
    #go  = (ro - ri     - hmo     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
    #hc  = (ro - ri     - hmo     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
    #gi  = (ro - ri     - hmo     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
    #hmi_r = (ro - ri     - hmo     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min
  
  
    ri  = (ro - ri_min - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
    hmo_r = (ro - ri     - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo_r + hmo_r_min
    go  = (ro - ri     - hmo_r     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
    hc  = (ro - ri     - hmo_r     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
    gi  = (ro - ri     - hmo_r     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
    hmi_r = (ro - ri     - hmo_r     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min

    pretty_variables = PrettyTable(['variable names (p.u.)','variables NEW (p.u.)','variable names (SI)','variables OLD (SI)','variables NEW (SI)','[%] increase (SI)'])
    pretty_variables.align = 'l'
    pretty_variables.border= True  
    
    pretty_variables.add_row(['x_ri',x_ri,'ri',old_ri,ri,(-(old_ri-ri)/old_ri)*100])    
    pretty_variables.add_row(['x_hmo_r',x_hmo_r,'hmo_r',old_hmo_r,hmo_r,(-(old_hmo_r-hmo_r)/old_hmo_r)*100])
    pretty_variables.add_row(['x_go',x_go,'go',old_go,go,(-(old_go-go)/old_go)*100])
    pretty_variables.add_row(['x_hmi_r',x_hmi_r,'hmi_r',old_hmi_r,hmi_r,(-(old_hmi_r-hmi_r)/old_hmi_r)*100])
    pretty_variables.add_row(['x_hc',x_hc,'hc',old_hc,hc,(-(old_hc-hc)/old_hc)*100])
    pretty_variables.add_row(['x_gi',x_gi,'gi',old_gi,gi,(-(old_gi-gi)/old_gi)*100])
    pretty_variables.add_row(['kc',kc,'kc',old_kc,kc,(-(old_kc-kc)/old_kc)*100])
    pretty_variables.add_row(['kmo',kmo,'kmo',old_kmo,kmo,(-(old_kmo-kmo)/old_kmo)*100])
    pretty_variables.add_row(['kmi',kmi,'kmi',old_kmi,kmi,(-(old_kmi-kmi)/old_kmi)*100])
    pretty_variables.add_row(['p',p,'p',old_p,p,(-(old_p-p)/old_p)*100])
    
    print pretty_variables
    

pl.show()