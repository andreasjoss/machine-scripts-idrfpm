#!/usr/bin/python
# -*- coding: utf-8 -*-

import re

def loadmxwltxt(filename):
  lines=file(filename,'r').readlines()

  #Strip of header lines
  for i in range(7):
    lines.pop(0)

  #Determine the number of columns
  columns=0
  line=lines[0].strip('\r\n').split(' ')
  for val in line:
    if val!='': columns+=1

  #Build the re search pattern
  re_float_nan=r'(-?\d+\.\d+|nan)'
  re_pattern=r'\s*'
  for i in range(columns):
    re_pattern+=re_float_nan
    re_pattern+=r'\s*'

  #Convert the strings in the list to floats
  for i, line in enumerate(lines):
    match=re.search(re_pattern,lines.pop(i))
    line=list(match.groups())
    for j, val in enumerate(line):
      if val=='nan':
        line[j]=0
      else:
        line[j]=float(val)
    lines.insert(i,line)

  return lines
