#!/usr/bin/python
from __future__ import division

from math import pi


# ############ #
# Machine Data #
# ############ #

rfapm_type= 2									# type machine, 0, 1 (I) or 2 (II)
ONESIDED = False              # magnets on the inner rotor True/False

p   			= 32 								# the total number of poles (i.e. magnets) on the inner/outer rotor yoke

k_q 			= 1/4   						# coils-per-phase to number-of-poles ratio

Q   			= int(p*k_q*3)  		# total number of coils

k_m 			= 0.7   						# the magnet angle to pole-pitch angle ratio

r_n 			= 232.  						# nominal stator radius                  [mm]
l         =  76.              # active stack/copper lenth              [mm]

w         = 22.4729           # coil side width                        [mm]
Delta     = w/(2*r_n)         # coil side width angle                  [rad]
Delta_max = pi/(2*Q)					# maximum coil side-width angle          [rad]
k_Delta   = Delta/Delta_max   # coil side-width factor

h   			=  10.   						# height/thickness of the stator coils   [mm]
h_m 			=   8.2 					  # magnet height/thickness                [mm]
h_iy 			=   8.  	 	  		  # yoke height/thickness                  [mm]
h_oy 			=   8. 						  # yoke height/thickness                  [mm]
l_g 			=   1.  						# air gap length                         [mm]
h_b 			=  10.  						# boundary height/thickness                     [mm]

N         =  96               # number of turns per coil               [turns]
a         =   8               # number of parallel branches

n         = 300               # [rated] speed                          [rpm]
f         = n*p/120           # [rated] frequency                      [Hz]

mag_name  ="NdBFe N48"        # the name of permanent magnets used
H_c       = 1050000           # the maximum coercivity force           [A/m]
B_r       =  1.4              # the average remanent flux density      [T]

I_p       = 58.65             # [rated] peak current value per phase   [A]
