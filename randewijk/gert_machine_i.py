#!/usr/bin/python
from __future__ import division

from math import pi


# ############ #
# Machine Data #
# ############ #

rfapm_type= 2									# type machine, 0, 1 (I) or 2 (II)
ONESIDED = False              # magnets on the inner rotor True/False

p   			= 28 								# the total number of poles (i.e. magnets) on the inner/outer rotor yoke

k_q 			= 1/4   						# coils-per-phase to number-of-poles ratio

Q   			= int(p*k_q*3)  		# total number of coils

k_m 			= 0.5   						# the magnet angle to pole-pitch angle ratio

r_n 			= 120.  						# nominal stator radius                  [mm]
l         =  40.              # active stack/copper lenth              [mm]

w         = 22.4729*((r_n)/232)*(18/Q)           # coil side width                        [mm]
Delta     = w/(2*r_n)         # coil side width angle                  [rad]
Delta_max = pi/(2*Q)					# maximum coil side-width angle          [rad]
#k_Delta   = Delta/Delta_max   # coil side-width factor
k_Delta   = 0.71757

h   			=  9.0   						# height/thickness of the stator coils   [mm]
h_m 			=   5.5 					  # magnet height/thickness                [mm]
h_iy 			=   9.0  	 	  		  # yoke height/thickness                  [mm]
h_oy 			=   9.0 						  # yoke height/thickness                  [mm]
l_g 			=   1.  						# air gap length                         [mm]
h_b 			=  15.0*(80/232)  						# boundary height/thickness                     [mm]

N         =  19               # number of turns per coil               [turns]
a         =   1               # number of parallel branches

n         = 290               # [rated] speed                          [rpm]
f         = n*p/120           # [rated] frequency                      [Hz]

mag_name  ="NdBFe N48"        # the name of permanent magnets used
H_c       = 1050000           # the maximum coercivity force           [A/m]
B_r       =  1.4              # the average remanent flux density      [T]

I_p       = 20             # [rated] peak current value per phase   [A]
