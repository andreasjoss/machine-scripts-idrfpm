#!/usr/bin/python
from __future__ import division

from math import pi,degrees,sin,sqrt

import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

from matplotlib.patches import Rectangle, Wedge

from glob import glob

from decimal import Decimal
from sympy.matrices import *

from gert_machine_i import *
#from test_machine_i import *
#from rfapm_david import *
from mxwltxt import loadmxwltxt

#Type='O'
Type='II'
presentation=True
linear_repr=True   #Draw the RFAPM machine in a linearised representational format
plot_contour=True
plot_km_range=False
plot_PM=True       #Plot the analysis for the PM
plot_AR=True      #Plot the analysis for the windings
plot_approx=True
plot_harmonics=True
plot_total=True     #Plot the analysis for the PM + the windings
plot_maxwell=False
plot_torque=True
plot_Tm_Lorentz=True
plot_Tm_Lorentz_simple=True
save_figs=False
show_figs=False
draw_coils = True

T=1/f
t=0*T/12
gamma=0

if presentation:
  mpl.rcParams['font.family']='sans-serif'
  mpl.rcParams['font.sans-serif']='Helvetica'
  mpl.rcParams['text.latex.preamble']=r'\usepackage{helvet}',r'\usepackage{sansserifmath}',r'\renewcommand{\familydefault}{\sfdefault}'
  mpl.rcParams['axes.labelsize']=12
  mpl.rcParams['xtick.labelsize']=10
  mpl.rcParams['ytick.labelsize']=10
#else:
  #mpl.rcParams['font.family']='serif'
  #mpl.rcParams['font.serif']='Palatino'
  #mpl.rcParams['text.latex.preamble']=r'\usepackage[slantedGreek]{mathpazo}'
  
#########################
# Contour Plot settings #
#########################

if Type=='O':
  contour_figsize=(6,4)
  colorbar_shrink=0.5
  colorbar_aspect=10
else:
  contour_figsize=(6,8)
  colorbar_shrink=0.25
  colorbar_aspect=10

#######################
# Convert Abri's Data #
#######################

#poles -> pole pairs
p=p/2

#coils in term of pole pairs
kq=k_q*2

#mm -> m
#print "r_n = %f"%r_n
rn=r_n/1000
#print "rn = %f"%rn
hy=h_iy/1000
hm=h_m/1000
l=l/1000
lg=l_g/1000
h=h/1000

#new naming convention
Ip=I_p/8      #all the coils are in series...
              #FIXME: - xy -> r\theta, looking from the back (i.e. -z direction)
km=k_m
kDelta=k_Delta
Brem=B_r

#override kq & kDelta for the Overlap case
if Type=='O':
  kq=1
  kDelta=1
  N=65

#parallel circuits
a=1

#####################################
# Calculation of Derived Quantities #
#####################################

#mechanical speed
omega_m=n*pi/30

#coil info
q=kq*p
Q=3*q
Delta_max=pi/6
Delta=kDelta*Delta_max

############################################
# Permeability's for the different Regions #
############################################

mu0  =4*pi*1E-7
muI  =1000
muII =1
muIII=1
muIV =1
muV  =1000

##############################################
# The number of harmonics for which to solve #
##############################################

M=112*2

m_PM_range=np.arange(int(M/p)-7)*2+1
#m_PM_range=np.arange(int(M/p)*2)+1
m_AR_range=np.arange(int(M/q)*2-27)+1
#m_AR_range=np.arange(int(M/q)*2)+1

################################################################################
# A lookup function to determine the index value of a specific floating point  #
# value in a floating point array                                              #
################################################################################

def lookup(ref,array,tol):
  r'''A lookup function to determine the index value of a specific floating point value in a floating point array,
due to the fact that a floating point value of say "0.232" is represented in python as "0.23200000000000001"'''
  for i,val in enumerate(array):
    if abs((val-ref))<tol:
      return i


############################
# Winding Factor Functions #
############################
def kw_pitch_def(m):
  r'''The Winding Pitch Factor'''
  if Type=='O':
    return np.sin(m*pi/2)
  elif Type=='I':
    return np.sin(m*pi/6)
  elif Type=='II':
    return np.sin(m*(pi/3-Delta))
  else:
    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

def kw_slot_def(m):
  r'''The Winding Slot Factor'''
  return np.sin(m*Delta)/(m*Delta)

#################################
# General Function Definitions  #
#################################

# NOT USED #
def bm_M0_def(m):
  #return 4*p*Brem/(mu0*pi)*np.sin(m*pi/2)*np.cos(m*p*beta)
  return 4*Brem/(m*mu0*pi)*np.cos(m*p*beta)

# NOT USED #
def M0_def():
  tmp=0.0
  for m in (np.arange(200)*2+1):
    tmp+=bm_M0_def(m)*np.sin(m*p*phi_range)
  return tmp

# NOT USED #
def bm_n_def(m,Phases):
  r'''The "bm" coefficient for the Fourier series expansion of the Conductor Density Distribution'''
  return -(2*q*N)/pi*kw_pitch_def(m)*kw_slot_def(m)

# NOT USED #
def n_def():
  r'''The Fourier series expansion of the Conductor Density Distribution'''
  tmp=0.0
  for m in m_AR_range:
    tmp+=bm_n_def(m,1)*np.sin(m*p*phi_range)
  return tmp

# NOT USED #
def bm_Jz_def(Ip,m):
  r'''The "bm" coefficient for the Fourier series expansion of the Current Density Distribution expressed in terms of the Conductor Density Distribution'''
  return -(3*q*Ip*N)/(rn*h*pi)*kw_pitch_def(m)*kw_slot_def(m)

# NOT USED #
def Jz_def(Ip,t):
  r'''The Current Density Distribution expressed in terms of the Conductor Density Distribution'''
  tmp=0.0
  for m in (3*m_AR_range-2):
    tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t)
  for m in (3*m_AR_range-1):
    tmp+=bm_Jz_def(Ip,m)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Jz_def(Ip,m)*np.sin(m*q*phi_range-p*omega_m*t)
  return tmp

def kT_def(m):
  r'''The torque constant of the machine'''
  return 3*q*rn*l*N/a*kw_pitch_def(m)*kw_slot_def(m)*Br1_PM


#########################
# The Forcing Functions #
#########################

def Gmr_PM_def(m,r):
  r'''The Forcing function due to the Permanent Magnets'''
  #return 4*p*r*Brem*np.cos(beta*m*p)/((1-(m*p)**2)*pi) #FIXME: 
  return -4*p*r*Brem*np.cos(beta*m*p)/((m*p)**2*pi)

def Gmr_AR_def(m,r):
  r'''The Forcing function due to the Armature Windings'''
  if m % 3 == 0:
    return 0
  else:
    return 3*mu0*Ip*N*r**2/(pi*rn*h*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)

def dGmrdr_AR_def(m,r):
  r'''The derivative with respect to "r" of the Forcing function due to the Armature Windings'''
  if m % 3 == 0:
    return 0
  else:
    return 6*mu0*Ip*N*r/(pi*rn*h*q*m**2)*kw_pitch_def(m)*kw_slot_def(m)


##################################
# The Vector Potential Functions #
##################################

def am_Az_PM_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Magnetic Vector Potential
due to the Armature Windings'''
  if Region==II:
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+Gmr_PM_def(m,rcim)
  elif Region==IV:
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+Gmr_PM_def(m,rcom)
  else:
    return Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)
  return tmp

def Az_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
    tmp+=am_Az_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def bm_Az_AR_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Magnetic Vector Potential
due to the Armature Windings'''
  if Region==III:
    return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r)
  else:
    return Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)
  return tmp

def Az_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Magnetic Vector Potential for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
    if m in (3*m_AR_range-2):
      tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
    elif m in (3*m_AR_range-1):
      tmp+=bm_Az_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
  return tmp


#####################################
# The Radial Flux Density Functions #
#####################################

def bm_Br_PM_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Radial Flux Density
due to the Permanent Magnets'''
  if Region==II:
    return -m*p*(Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+Gmr_PM_def(m,rcim))/r
  elif Region==IV:
    return -m*p*(Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p)+Gmr_PM_def(m,rcom))/r
  else:
    return -m*p*(Cm_PM[Region][i]*r**(m*p)+Dm_PM[Region][i]*r**(-m*p))/r


def int_bm_Br_III_PM_def(i,m,r):
  r'''The integral of r**2*"bm" (see above) in order to work out the torque with the Lorentz method'''
  return -m*p*(Cm_PM[III][i]/(m*p+2)*r**(m*p+2)+Dm_PM[III][i]/(-m*p+2)*r**(-m*p+2))

def Br_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
    tmp+=bm_Br_PM_def(i,m,r,Region)*np.sin(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def am_Br_AR_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Radial Flux Density
due to the Armature Windings'''
  if Region==III:
      return -m*q/r*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q)+Gmr_AR_def(m,r))
  else:
      return -m*q/r*(Cm_AR[Region][i]*r**(m*q)+Dm_AR[Region][i]*r**(-m*q))

def Br_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Radial Flux Density for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
    if m in (3*m_AR_range-2):
      tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t)
    elif m in (3*m_AR_range-1):
      tmp+=am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range+p*omega_m*t) if Type=='O' else am_Br_AR_def(i,m,r,Region)*np.cos(m*q*phi_range-p*omega_m*t)
  return tmp


########################################
# The Azimuthal Flux Density Functions #
########################################

def am_Bt_PM_def(i,m,r,Region):
  r'''The "am" coefficient for the Fourier series expansion of the Azimuthal Flux Density
due to the Permanent Magnets'''
  return m*p*(Cm_PM[Region][i]*r**(m*p-1)-Dm_PM[Region][i]*r**(-m*p-1))
  #FIXME: must be a "-m*p*(Cm*r**(m*p-1)-Dm*r**(-m*p-1))" above

def Bt_PM_def(r,t,Region):
  r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
due to the Permanent Magnets'''
  tmp=0.0
  for i, m in enumerate(m_PM_range):
    tmp+=am_Bt_PM_def(i,m,r,Region)*np.cos(m*p*phi_range-m*p*omega_m*t-m*gamma)
  return tmp

def bm_Bt_AR_def(i,m,r,Region):
  r'''The "bm" coefficient for the Fourier series expansion of the Azimuthal Flux Density
due to the Armature Windings'''
  if Region==III:
    return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1)+dGmrdr_AR_def(m,r)/(m*q))
  else:
    return -m*q*(Cm_AR[Region][i]*r**(m*q-1)-Dm_AR[Region][i]*r**(-m*q-1))

def Bt_AR_def(r,t,Region):
  r'''The Fourier series expansion of the Azimuthal Flux Density for a specific value of "r"
due to the Armature Windings'''
  tmp=0.0
  for i, m in enumerate(m_AR_range):
    if m in (3*m_AR_range-2):
      tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t)
    elif m in (3*m_AR_range-1):
      tmp+=bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range+p*omega_m*t) if Type=='O' else bm_Bt_AR_def(i,m,r,Region)*np.sin(m*q*phi_range-p*omega_m*t)
  return tmp


###########################################################################
#The Flux-linkage at a specific value of "r" due to the Armature Windings #
###########################################################################

def lambda_PM_def(r):
  r'''The Flux-linkage for a specific value of "r" due to the Permanent Magnets'''
  tmp=0
  for i, m in enumerate(m_PM_range):
    tmp+=(-2*q*N*l/a)*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*am_Az_PM_def(i,m,r,Region=III)
  return tmp

def lambda_AR_def(r):
  r'''The Flux-linkage for a specific value of "r" due to the Armature Windings'''
  tmp=0
  for i, m in enumerate(m_AR_range):
    tmp+=(2*q*N*l/a)*kw_pitch_def(m)*kw_slot_def(m)*bm_Az_AR_def(i,m,r,Region=III)
    #FIXME: must be a "(-2*q*N*l/a)..." above
  return tmp

def k_lambda():
  if Type=='O':
    return np.sin(Delta)/(Delta)
  elif Type=='I':
    return np.sin(pi/3)*np.sin(Delta)/(Delta)
  elif Type=='II':
    return np.sin((pi/Q-Delta)*p)*np.sin(Delta)/(Delta)
  else:
    raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

print('The analysis will be done for a Type %s Machine' % (Type))

###########
# Regions #
###########

I   = 0
II  = 1
III = 2
IV  = 3
V   = 4

#####################################
# Defining Boundaries for the Rotor #
#####################################

ri  =rn-h/2-lg-hm-hy
rii =rn-h/2-lg-hm
riii=rn-h/2-lg
riv =rn+h/2+lg
rv  =rn+h/2+lg+hm
rvi =rn+h/2+lg+hm+hy

#inner & outer magnet centres
rcim=rn-h/2-lg-hm/2
rcom=rn+h/2+lg+hm/2

##################################################
# The range of r & phi values for which to solve #
##################################################

#In order to utilise the FFT optimally later on
M = 256

r_range=np.arange(ri,rvi+0.2/1000,0.2/1000)
phi_range=np.linspace(-pi/q,pi/q,int(M/kq))

#Get index for the following radii
i_rcim=lookup(rcim,r_range,0.1/1000)
i_rnmh2=lookup(rn-h/2,r_range,0.1/1000)
i_rn=lookup(rn,r_range,0.1/1000)
i_rnph2=lookup(rn+h/2,r_range,0.1/1000)
i_rcom=lookup(rcom,r_range,0.1/1000)
i_rciy=lookup(rn-h/2-lg-hm-hy/2,r_range,0.1/1000)
i_rcoy=lookup(rn+h/2+lg+hm+hy/2,r_range,0.1/1000)

if plot_km_range:
  km_range=np.arange(0.5,1.0,0.0125)
else:
  km_range=np.array([])

km_range=np.append(km_range,km)

Brmax_range_PM=np.array([])

Br1_range_PM=np.array([])
Bt1_range_PM=np.array([])

dM0_THD_range=np.array([])

Br_THD_range_PM=np.array([])
Bt_THD_range_PM=np.array([])

for km in km_range:
  #magnet info
  beta  = pi*(1-km)/(2*p)

  #######################################
  # Initialise the PMAG solution arrays #
  #######################################

  CmI_PM  =np.array([])
  DmI_PM  =np.array([])
  CmII_PM =np.array([])
  DmII_PM =np.array([])
  CmIII_PM=np.array([])
  DmIII_PM=np.array([])
  CmIV_PM =np.array([])
  DmIV_PM =np.array([])
  CmV_PM  =np.array([])
  DmV_PM  =np.array([])

  dM0=0

  ########################################################
  # Performing the PMAG Linear Solve for each value of m #
  ########################################################

  for m in m_PM_range:
    #A=np.mat([[ri**(m*p)          ,  (Decimal(ri**(-m)))**int(p)                      ,  0                   ,  0                                                          ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
    A=Matrix([[ri**(m*p)          ,  (Decimal(ri**(-m)))**int(p)                      ,  0                   ,  0                                                          ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [rii**(m*p)         ,  (Decimal(rii**(-m)))**int(p)                     , -rii**(m*p)          , -(Decimal(rii**(-m)))**int(p)                               ,  0                  ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [muII*rii**(m*p-1)  , -muII*((Decimal(rii**(-m)))**int(p))/Decimal(rii) , -muI*rii**(m*p-1)    ,  muI*((Decimal(rii**(-m)))**int(p))/Decimal(rii)            ,  0                  ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                                                ,  riii**(m*p)         ,  (Decimal(riii**(-m)))**int(p)                              , -riii**(m*p)        , -(Decimal(riii**(-m)))**int(p)       ,  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                                                ,  muIII*riii**(m*p-1) , -muIII*((Decimal(riii**(-m)))**int(p))/Decimal(riii)        , -muII*riii**(m*p-1),  muII*((Decimal(riii**(-m)))**int(p))/Decimal(riii),  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                                                ,  0                   ,  0                                                          ,  riv**(m*p)        ,  (Decimal(riv**(-m)))**int(p)        , -riv**(m*p)        , -(Decimal(riv**(-m)))**int(p)        ,  0               ,  0                ],
              [0                  ,  0                                                ,  0                   ,  0                                                          ,  muIV*riv**(m*p-1) , -muIV*((Decimal(riv**(-m)))**int(p))/Decimal(riv) , -muIII*riv**(m*p-1),  muIII*((Decimal(riv**(-m)))**int(p))/Decimal(riv),  0               ,  0                ],
              [0                  ,  0                                                ,  0                   ,  0                                                          ,  0                 ,  0                  ,  rv**(m*p)         ,  (Decimal(rv**(-m)))**int(p)          , -rv**(m*p)       , -(Decimal(rv**(-m)))**int(p)        ],
              [0                  ,  0                                                ,  0                   ,  0                                                          ,  0                 ,  0                  ,  muV*rv**(m*p-1)   , -muV*((Decimal(rv**(-m)))**int(p))/Decimal(rv)   , -muIV*rv**(m*p-1),  muIV*((Decimal(rv**(-m)))**int(p))/Decimal(rv)],
              [0                  ,  0                                                ,  0                   ,  0                                                          ,  0                 ,  0                  ,  0                 ,  0                  ,  rvi**(m*p)      ,  (Decimal(rvi**(-m)))**int(p)      ]])

    #B=np.mat([[ 0 ],
    B=Matrix([[ 0 ],
              [ Gmr_PM_def(m,rcim)],
              [ 0 ],
              [-Gmr_PM_def(m,rcim)],
              [ 0 ],
              [ Gmr_PM_def(m,rcom)],
              [ 0 ],
              [-Gmr_PM_def(m,rcom)],
              [ 0 ],
              [ 0 ]])

    #X=np.linalg.solve(A,B)
    X = A.LUsolve(B)
    print("m = %d"%(m))
    print(X)
    print("\n")

    CmI_PM  =np.append(CmI_PM,  X[0,0])
    DmI_PM  =np.append(DmI_PM,  X[1,0])
    CmII_PM =np.append(CmII_PM, X[2,0])
    DmII_PM =np.append(DmII_PM, X[3,0])
    CmIII_PM=np.append(CmIII_PM,X[4,0])
    DmIII_PM=np.append(DmIII_PM,X[5,0])
    CmIV_PM =np.append(CmIV_PM, X[6,0])
    DmIV_PM =np.append(DmIV_PM, X[7,0])
    CmV_PM  =np.append(CmV_PM,  X[8,0])
    DmV_PM  =np.append(DmV_PM,  X[9,0])

    dM0+=(4*p/pi)*np.cos(m*p*beta)*np.cos(m*p*phi_range)

  ###########################################################
  # Repack the PMAG Solution Coefficient into Matrix format #
  ###########################################################

  Cm_PM=np.array([CmI_PM,CmII_PM,CmIII_PM,CmIV_PM,CmV_PM])
  Dm_PM=np.array([DmI_PM,DmII_PM,DmIII_PM,DmIV_PM,DmV_PM])

  ###################################
  # Initialise PMAG Solution Arrays #
  ###################################

  Az_PM=np.zeros((r_range.size,phi_range.size))
  Br_PM=np.zeros((r_range.size,phi_range.size))
  Bt_PM=np.zeros((r_range.size,phi_range.size))
  Bmag_PM=np.zeros((r_range.size,phi_range.size))

  #########################################################################
  # Calculate the PMAG Solution Arrays for each value of 'r' in 'r_range' #
  #########################################################################

  for i,r in enumerate(r_range):
    if r<rii:
      Region=I
    elif r<riii:
      Region=II
    elif r<riv:
      Region=III
    elif r<rv:
      Region=IV
    else:
      Region=V
    #print t
    Az_PM[i]=Az_PM_def(r,t,Region)
    Br_PM[i]=Br_PM_def(r,t,Region)
    Bt_PM[i]=Bt_PM_def(r,t,Region)
    Bmag_PM[i]=np.sqrt(Br_PM[i]**2+Bt_PM[i]**2)

  dM0h=np.abs(np.fft.fft(dM0,M)*2/M)

  Brmax_PM=Br_PM[i_rn].max()
  Brmax_range_PM=np.append(Brmax_range_PM,Brmax_PM)

  Brh_PM=np.abs(np.fft.fft(Br_PM[i_rn],M)*2/M)
  Bth_PM=np.abs(np.fft.fft(Bt_PM[i_rn],M)*2/M)

  Br1_PM=Brh_PM[1]
  Bt1_PM=Bth_PM[1]

  Br1_range_PM=np.append(Br1_range_PM,Br1_PM)
  Bt1_range_PM=np.append(Bt1_range_PM,Bt1_PM)

  #print int((M-1)/2)
  print (M)/2
  #print dM0h


  dM0_THD=np.sqrt(np.sum(dM0h[2:int((M)/2)]**2))/dM0h[1]

  Br_THD_PM=np.sqrt(np.sum(Brh_PM[2:int((M)/2)]**2))/Br1_PM
  Bt_THD_PM=np.sqrt(np.sum(Bth_PM[2:int((M)/2)]**2))/Bt1_PM

  dM0_THD_range=np.append(dM0_THD_range,dM0_THD)

  Br_THD_range_PM=np.append(Br_THD_range_PM,Br_THD_PM)
  Bt_THD_range_PM=np.append(Bt_THD_range_PM,Bt_THD_PM)

if plot_km_range:
  ################################################################
  # Remove the true (i.e. last) value of km from km_range, etc. #
  ################################################################
  km_range=km_range[0:-1]

  dM0_THD_range=dM0_THD_range[0:-1]

  Brmax_range_PM=Brmax_range_PM[0:-1]

  Br1_range_PM=Br1_range_PM[0:-1]
  Bt1_range_PM=Bt1_range_PM[0:-1]

  Br_THD_range_PM=Br_THD_range_PM[0:-1]
  Bt_THD_range_PM=Bt_THD_range_PM[0:-1]

###################################################
# Redefining Boundaries for the Armature Reaction #
###################################################

riii=rn-h/2
riv =rn+h/2

##################################
# Initialise the solution arrays #
##################################

CmI_AR  =np.array([])
DmI_AR  =np.array([])
CmII_AR =np.array([])
DmII_AR =np.array([])
CmIII_AR=np.array([])
DmIII_AR=np.array([])
CmIV_AR =np.array([])
DmIV_AR =np.array([])
CmV_AR  =np.array([])
DmV_AR  =np.array([])

#########################################################################
# Performing the Linear Solve for each value of m due to the Armature Windings Regions #
#########################################################################

for m in m_AR_range:
  A=np.array([[ri**(m*q)          ,  ri**(-m*q)        ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [rii**(m*q)         ,  rii**(-m*q)       , -rii**(m*q)         , -rii**(-m*q)         ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [muII*rii**(m*q-1)  , -muII*rii**(-m*q-1), -muI*rii**(m*q-1)   ,  muI*rii**(-m*q-1)   ,  0                 ,  0                  ,  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                 ,  riii**(m*q)        ,  riii**(-m*q)        , -riii**(m*q)       , -riii**(-m*q)       ,  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                 ,  muIII*riii**(m*q-1), -muIII*riii**(-m*q-1), -muII*riii**(m*q-1),  muII*riii**(-m*q-1),  0                 ,  0                  ,  0               ,  0                ],
              [0                  ,  0                 ,  0                  ,  0                   ,  riv**(m*q)        ,  riv**(-m*q)        , -riv**(m*q)        , -riv**(-m*q)        ,  0               ,  0                ],
              [0                  ,  0                 ,  0                  ,  0                   ,  muIV*riv**(m*q-1) , -muIV*riv**(-m*q-1) , -muIII*riv**(m*q-1),  muIII*riv**(-m*q-1),  0               ,  0                ],
              [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  rv**(m*q)         ,  rv**(-m*q)         , -rv**(m*q)       , -rv**(-m*q)       ],
              [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  muV*rv**(m*q-1)   , -muV*rv**(-m*q-1)   , -muIV*rv**(m*q-1),  muIV*rv**(-m*q-1)],
              [0                  ,  0                 ,  0                  ,  0                   ,  0                 ,  0                  ,  0                 ,  0                  ,  rvi**(m*q)      ,  rvi**(-m*q)      ]])

  B=np.array([[ 0 ],
              [ 0 ],
              [ 0 ],
              [ Gmr_AR_def(m,riii)],
              [ muII*dGmrdr_AR_def(m,riii)/(m*q)],
              [-Gmr_AR_def(m,riv)],
              [-muIV*dGmrdr_AR_def(m,riv)/(m*q)],
              [ 0 ],
              [ 0 ],
              [ 0 ]])

  X=np.linalg.solve(A,B)

  CmI_AR  =np.append(CmI_AR,  X[0,0])
  DmI_AR  =np.append(DmI_AR,  X[1,0])
  CmII_AR =np.append(CmII_AR, X[2,0])
  DmII_AR =np.append(DmII_AR, X[3,0])
  CmIII_AR=np.append(CmIII_AR,X[4,0])
  DmIII_AR=np.append(DmIII_AR,X[5,0])
  CmIV_AR =np.append(CmIV_AR, X[6,0])
  DmIV_AR =np.append(DmIV_AR, X[7,0])
  CmV_AR  =np.append(CmV_AR,  X[8,0])
  DmV_AR  =np.append(DmV_AR,  X[9,0])

#############################################################
# Repack the Stator Coefficient Solution into Matrix format #
#############################################################

Cm_AR=np.array([CmI_AR,CmII_AR,CmIII_AR,CmIV_AR,CmV_AR])
Dm_AR=np.array([DmI_AR,DmII_AR,DmIII_AR,DmIV_AR,DmV_AR])

#####################################
# Initialise Stator Solution Arrays #
#####################################
Az_AR=np.zeros((r_range.size,phi_range.size))
Br_AR=np.zeros((r_range.size,phi_range.size))
Bt_AR=np.zeros((r_range.size,phi_range.size))
Bmag_AR=np.zeros((r_range.size,phi_range.size))

########################################################################################
# Calculate a solution for each value of 'r' in 'r_range' due to the Armature Windings #
########################################################################################

for i,r in enumerate(r_range):
  if r<rii:
    Region=I
  elif r<riii:
    Region=II
  elif r<riv:
    Region=III
  elif r<rv:
    Region=IV
  else:
    Region=V
  Az_AR[i]=Az_AR_def(r,t,Region)
  Br_AR[i]=Br_AR_def(r,t,Region)
  Bt_AR[i]=Bt_AR_def(r,t,Region)
  Bmag_AR[i]=np.sqrt(Br_AR[i]**2+Bt_AR[i]**2)

Brh_AR=np.abs(np.fft.fft(Br_AR[i_rn],M)*2/M)
Bth_AR=np.abs(np.fft.fft(Bt_AR[i_rn],M)*2/M)

Br1_AR=Brh_AR[1]
Bt1_AR=Bth_AR[1]

Az_total=Az_PM+Az_AR
Br_total=Br_PM+Br_AR
Bt_total=Bt_PM+Bt_AR
Bmag_total=Bmag_PM+Bmag_AR

t_range=np.linspace(0,T,181)

lambda_PM=lambda_PM_def(rn)
lambda_AR=lambda_AR_def(rn)

#m=1 if Type=='O' else 2

lambda_PM_approx=2*rn*l*N*Br1_PM/a*kq*kw_pitch_def(1/kq)*kw_slot_def(1/kq)
lambda_AR_approx=(2*q*N*l/a)*kw_pitch_def(1)*kw_slot_def(1)*bm_Az_AR_def(0,1,rn,Region=III)

EMK_PM=lambda_PM*omega_m*p
EMK_AR=lambda_AR*2*pi*f

EMK_PM_approx=lambda_PM_approx*omega_m*p

Tm_maxwell=np.array([])
Tm_lorentz=np.array([])
Tm_lorentz_simple=np.array([])

def Tm_lorentz_simple_def(t):
  tmp=0
  for i, m in enumerate(m_PM_range):
    if m in (3*m_AR_range-2):
      tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
    elif m in (3*m_AR_range-1):
      tmp+=(3*q*rn*N*l*Ip/(a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*bm_Br_PM_def(i,m,rn,III)*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
  return tmp

def Tm_lorentz_def(t):
  tmp=0
  for i, m in enumerate(m_PM_range):
    if m in (3*m_AR_range-2):
      #tmp+=(3*q*N*l*Ip/(rn*h*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,rn+h/2)-int_bm_Br_III_PM_def(i,m,rn-h/2))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
      tmp+=((3*q*N*l*Ip)/(rn*h*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,rn+h/2)-int_bm_Br_III_PM_def(i,m,rn-h/2))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)+np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
    elif m in (3*m_AR_range-1):
      tmp+=((3*q*N*l*Ip)/(rn*h*a))*kw_pitch_def(m/kq)*kw_slot_def(m/kq)*(int_bm_Br_III_PM_def(i,m,rn+h/2)-int_bm_Br_III_PM_def(i,m,rn-h/2))*(np.cos(p*omega_m*t)*np.cos(m*p*omega_m*t)-np.sin(p*omega_m*t)*np.sin(m*p*omega_m*t))
  return tmp

for t in t_range:
  #Analytical Flux-linkage calculations for each phase as function of time due to the Permanent Magnets
  lambda_a_PM=lambda_PM*np.sin(omega_m*p*t_range)
  lambda_b_PM=lambda_PM*np.sin(omega_m*p*t_range-2*pi/3)
  lambda_c_PM=lambda_PM*np.sin(omega_m*p*t_range-4*pi/3)

  lambda_a_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range)
  lambda_b_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-2*pi/3)
  lambda_c_PM_approx=lambda_PM_approx*np.sin(omega_m*p*t_range-4*pi/3)

  #Analytical Flux-linkage calculations for each phase as function of time due to the Armature Reaction
  lambda_a_AR=lambda_AR*np.cos(2*pi*f*t_range)
  lambda_b_AR=lambda_AR*np.cos(2*pi*f*t_range-2*pi/3)
  lambda_c_AR=lambda_AR*np.cos(2*pi*f*t_range-4*pi/3)

  e_a_PM=EMK_PM*np.cos(omega_m*p*t_range)
  e_b_PM=EMK_PM*np.cos(omega_m*p*t_range-2*pi/3)
  e_c_PM=EMK_PM*np.cos(omega_m*p*t_range-4*pi/3)

  e_a_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range)
  e_b_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-2*pi/3)
  e_c_PM_approx=EMK_PM_approx*np.cos(omega_m*p*t_range-4*pi/3)

  e_a_AR=-EMK_AR*np.sin(omega_m*p*t_range)
  e_b_AR=-EMK_AR*np.sin(omega_m*p*t_range-2*pi/3)
  e_c_AR=-EMK_AR*np.sin(omega_m*p*t_range-4*pi/3)

  dphi=phi_range[1]-phi_range[0]

  Tm_maxwell=np.append(Tm_maxwell,dphi*rn*l*h/mu0*(Br_PM_def(rn,t,III)*Bt_AR_def(rn,t,III)-Br_AR_def(rn,t,III)*Bt_PM_def(rn,t,III)).sum())
  #FIXME: must be "+Br_AR_def(..."

  #Tm_lorentz=np.append(Tm_lorentz,(dphi*q*rn**2*h*l*Jz_def(Ip,t)*Br_PM_def(rn,t,III)).sum())
  Tm_lorentz=np.append(Tm_lorentz,Tm_lorentz_def(t))
  Tm_lorentz_simple=np.append(Tm_lorentz_simple,Tm_lorentz_simple_def(t))

#Calculate the Harmonic content of Tm_lorentz
M=np.size(Tm_lorentz)-1
Tmh_lorentz=np.abs(np.fft.fft(Tm_lorentz,M)/M)
Tm0_lorentz=Tmh_lorentz[0]
Tm_lorentz_THD=np.sqrt(np.sum(Tmh_lorentz[2:int((M)/2)]**2))/Tm0_lorentz

#Calculate the Harmonic content of Tm_lorentz_simple
M=np.size(Tm_lorentz_simple)-1
Tmh_lorentz_simple=np.abs(np.fft.fft(Tm_lorentz_simple,M)/M)
Tm0_lorentz_simple=Tmh_lorentz_simple[0]
Tm_lorentz_simple_THD=np.sqrt(np.sum(Tmh_lorentz_simple[2:int((M)/2)]**2))/Tm0_lorentz_simple

#Convert 's' into 'ms'
t_range=t_range*1000
T=T*1000

#if kDelta==1.0:
#  kDelta=0.99

plt.close('all')
if plot_contour:
  #################
  # Contour Plots #
  #################

  # Initialise x & y array
  x=np.zeros(np.shape(Az_AR))
  y=np.zeros(np.shape(Az_AR))

  # Convert r & phi values -> x & y for the polar plot representation
  for i, r in enumerate(r_range):
    x[i]=r*np.cos(phi_range)
    y[i]=r*np.sin(phi_range)

  #Function to draw the RFAPM machine in polar coordinates
  def draw_polar_machine(ax,draw_coils=False):
    inner_yoke=Wedge((0,0), rii, -180/q, 180/q, width=hy, facecolor="grey", alpha=0.5)
    outer_yoke=Wedge((0,0), rvi, -180/q, 180/q, width=hy, facecolor="grey", alpha=0.5)
    inner_S_magnet_0=Wedge((0,0), riii-lg, degrees(beta),       -degrees(beta)+180/p, width=hm, facecolor="red",  alpha=0.25)
    outer_S_magnet_0=Wedge((0,0), rv,      degrees(beta),       -degrees(beta)+180/p, width=hm, facecolor="red", alpha=0.25)
    inner_N_magnet_0=Wedge((0,0), riii-lg, degrees(beta)-180/p, -degrees(beta),       width=hm, facecolor="blue", alpha=0.25)
    outer_N_magnet_0=Wedge((0,0), rv,      degrees(beta)-180/p, -degrees(beta),       width=hm, facecolor="blue",  alpha=0.25)
    if p!=q:
      inner_S_magnet_1=Wedge((0,0), riii-lg, degrees(beta)-360/p, -degrees(beta)-180/p, width=hm, facecolor="red",  alpha=0.25)
      outer_S_magnet_1=Wedge((0,0), rv,      degrees(beta)-360/p, -degrees(beta)-180/p, width=hm, facecolor="red", alpha=0.25)
      inner_N_magnet_1=Wedge((0,0), riii-lg, degrees(beta)+180/p, -degrees(beta)+360/p, width=hm, facecolor="blue", alpha=0.25)
      outer_N_magnet_1=Wedge((0,0), rv,      degrees(beta)+180/p, -degrees(beta)+360/p, width=hm, facecolor="blue",  alpha=0.25)
    if draw_coils:
      if Type=='O':
        coil_phase_A_pos=Wedge((0,0), riv, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=h, facecolor="red",    alpha=0.5)
        coil_phase_A_neg=Wedge((0,0), riv, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=h, facecolor="red",    alpha=0.5)
        coil_phase_B_pos=Wedge((0,0), riv, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=h, facecolor="yellow", alpha=0.5)
        coil_phase_B_neg=Wedge((0,0), riv, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=h, facecolor="yellow", alpha=0.5)
        coil_phase_C_pos=Wedge((0,0), riv, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=h, facecolor="blue",   alpha=0.5)
        coil_phase_C_neg=Wedge((0,0), riv, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=h, facecolor="blue",   alpha=0.5)
      elif Type=='I':
        coil_phase_A_pos=Wedge((0,0), riv, -degrees(Delta/q)-30/q,     degrees(Delta/q)-30/q,    width=h, facecolor="red",    alpha=0.5)
        coil_phase_A_neg=Wedge((0,0), riv, -degrees(Delta/q)+30/q,     degrees(Delta/q)+30/q,    width=h, facecolor="red",    alpha=0.5)
        coil_phase_B_pos=Wedge((0,0), riv, -degrees(Delta/q)+90/q,     degrees(Delta/q)+90/q,    width=h, facecolor="yellow", alpha=0.5)
        coil_phase_B_neg=Wedge((0,0), riv, -degrees(Delta/q)+150/q,    degrees(Delta/q)+150/q,   width=h, facecolor="yellow", alpha=0.5)
        coil_phase_C_pos=Wedge((0,0), riv, -degrees(Delta/q)-150/q,    degrees(Delta/q)-150/q,   width=h, facecolor="blue",   alpha=0.5)
        coil_phase_C_neg=Wedge((0,0), riv, -degrees(Delta/q)-90/q,     degrees(Delta/q)-90/q,    width=h, facecolor="blue",   alpha=0.5)
      elif Type=='II':
        coil_phase_A_pos=Wedge((0,0), riv, -60/q,                     -60/q+2*degrees(Delta/q),  width=h, facecolor="red",    alpha=0.5)
        coil_phase_A_neg=Wedge((0,0), riv,  60/q-2*degrees(Delta/q),   60/q,                     width=h, facecolor="red",    alpha=0.5)
        coil_phase_B_pos=Wedge((0,0), riv,  60/q,                      60/q+2*degrees(Delta/q),  width=h, facecolor="yellow", alpha=0.5)
        coil_phase_B_neg=Wedge((0,0), riv,  180/q-2*degrees(Delta/q),  180/q,                    width=h, facecolor="yellow", alpha=0.5)
        coil_phase_C_pos=Wedge((0,0), riv, -180/q,                    -180/q+2*degrees(Delta/q), width=h, facecolor="blue",   alpha=0.5)
        coil_phase_C_neg=Wedge((0,0), riv, -60/q-2*degrees(Delta/q),  -60/q,                     width=h, facecolor="blue",   alpha=0.5)
      else:
        raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
    ax.add_patch(inner_yoke)
    ax.add_patch(outer_yoke)
    #ax.add_patch(inner_N_magnet_0)
    #ax.add_patch(outer_S_magnet_0)
    #ax.add_patch(inner_S_magnet_0)
    #ax.add_patch(outer_N_magnet_0)
    #if p!=q:
      #ax.add_patch(inner_N_magnet_1)
      #ax.add_patch(outer_S_magnet_1)
      #ax.add_patch(inner_S_magnet_1)
      #ax.add_patch(outer_N_magnet_1)
    if draw_coils:
      ax.add_patch(coil_phase_A_pos)
      ax.add_patch(coil_phase_A_neg)
      ax.add_patch(coil_phase_B_pos)
      ax.add_patch(coil_phase_B_neg)
      ax.add_patch(coil_phase_C_pos)
      ax.add_patch(coil_phase_C_neg)
    ax.set_aspect('equal')

  if plot_PM:
    ###############################################################
    # Plot Magnetic Vector Potential contour plot in polar format #
    ###############################################################
    plt.figure(figsize=contour_figsize)
    CS=plt.contour(x,y,Az_PM*1000,40)
    draw_polar_machine(plt.gca(),draw_coils=False)
    plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
    cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.1f')
    cb.set_label(r'Vector potential, $A_{z|PM}$ [mWb/m]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the PM's Magnetic Vector Potential")
    else:
      plt.savefig('./pdf/Az_xy_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if plot_AR:
    ###############################################################
    # Plot Magnetic Vector Potential contour plot in polar format #
    ###############################################################
    plt.figure(figsize=contour_figsize)
    CS=plt.contour(x,y,Az_AR*1000,40)
    draw_polar_machine(plt.gca(),draw_coils=True)
    plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
    cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%+5.2f')
    cb.set_label(r'Vector potential, $A_{z|AR}$ [mWb/m]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the Windings' Magnetic Vector Potential -- Type %s" % Type)
    else:
      plt.savefig('./pdf/Az_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if plot_PM:
    ###########################################################
    # Plot Magnetic Flux Density contour plot in polar format #
    ###########################################################
    plt.figure(figsize=contour_figsize)
    CS=plt.contour(x,y,Bmag_PM,40)
    CS=plt.contourf(x,y,Bmag_PM,40)
    draw_polar_machine(plt.gca())
    cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.2f')
    cb.set_label(r'Flux density, $B_{|PM}$ (T)')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the PM's Magnetic Flux Density")
    else:
      plt.savefig('./pdf/Bmag_xy_PM-%s.pdf' % Type,transparent=True, bbox_inches='tight')

  if plot_AR:
    ###########################################################
    # Plot Magnetic Flux Density contour plot in polar format #
    ###########################################################
    plt.figure(figsize=contour_figsize)
    CS=plt.contour(x,y,Bmag_AR*1000,40)
    CS=plt.contourf(x,y,Bmag_AR*1000,40)
    draw_polar_machine(plt.gca())
    cb=plt.colorbar(shrink=colorbar_shrink,aspect=colorbar_aspect,format='%5.1f')
    cb.set_label(r'Flux density, $B_{|AR}$ [mT]')
    plt.xlabel(r'$x$ [m]')
    plt.ylabel(r'$y$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the Windings' Magnetic Flux Density -- Type %s" % Type)
    else:
      plt.savefig('./pdf/Bmag_xy_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if linear_repr:
    ##############################################################################
    # Function to draw the RFAPM machine in a linearised representational format #
    ##############################################################################
    def draw_linearised_machine(ax):
      inner_yoke=Rectangle((-180/q, ri), 360/q, hy, facecolor="grey", alpha=0.5)
      outer_yoke=Rectangle((-180/q, rv), 360/q, hy, facecolor="grey", alpha=0.5)
      inner_S_magnet_0=Rectangle((degrees(beta)-180/p, rii),    km*180/p, hm, facecolor="red", alpha=0.25)
      outer_S_magnet_0=Rectangle((degrees(beta)-180/p, riv+lg), km*180/p, hm, facecolor="red",  alpha=0.25)
      inner_N_magnet_0=Rectangle((degrees(beta),       rii),    km*180/p, hm, facecolor="blue",  alpha=0.25)
      outer_N_magnet_0=Rectangle((degrees(beta),       riv+lg), km*180/p, hm, facecolor="blue", alpha=0.25)
      if p!=q:
        inner_S_magnet_1=Rectangle((degrees(beta)-360/p, rii),    km*180/p, hm, facecolor="red",  alpha=0.25)
        outer_S_magnet_1=Rectangle((degrees(beta)-360/p, riv+lg), km*180/p, hm, facecolor="red", alpha=0.25)
        inner_N_magnet_1=Rectangle((degrees(beta)+180/p, rii),    km*180/p, hm, facecolor="blue", alpha=0.25)
        outer_N_magnet_1=Rectangle((degrees(beta)+180/p, riv+lg), km*180/p, hm, facecolor="blue",  alpha=0.25)
      if draw_coils:
        if Type=='O':
          coil_phase_A_pos=Rectangle(( -90/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_A_neg=Rectangle((  90/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_B_pos=Rectangle((-150/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_B_neg=Rectangle((  30/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_C_pos=Rectangle(( -30/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
          coil_phase_C_neg=Rectangle(( 150/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
        elif Type=='I':
          coil_phase_A_pos=Rectangle(( -30/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_A_neg=Rectangle((  30/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_B_pos=Rectangle((  90/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_B_neg=Rectangle(( 150/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_C_pos=Rectangle((-150/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
          coil_phase_C_neg=Rectangle(( -90/q-degrees(Delta/q),   riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
        elif Type=='II':
          coil_phase_A_pos=Rectangle(( -60/q,                    riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_A_neg=Rectangle((  60/q-2*degrees(Delta/q), riii),   2*degrees(Delta/q), h,  facecolor="red",    alpha=0.5)
          coil_phase_B_pos=Rectangle((  60/q,                    riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_B_neg=Rectangle(( 180/q-2*degrees(Delta/q), riii),   2*degrees(Delta/q), h,  facecolor="yellow", alpha=0.5)
          coil_phase_C_pos=Rectangle((-180/q,                    riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
          coil_phase_C_neg=Rectangle(( -60/q-2*degrees(Delta/q), riii),   2*degrees(Delta/q), h,  facecolor="blue",   alpha=0.5)
        else:
          raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."
      ax.add_patch(inner_yoke)
      ax.add_patch(outer_yoke)
      ax.add_patch(inner_N_magnet_0)
      ax.add_patch(outer_S_magnet_0)
      ax.add_patch(inner_S_magnet_0)
      ax.add_patch(outer_N_magnet_0)
      if p!=q:
        ax.add_patch(inner_N_magnet_1)
        ax.add_patch(outer_S_magnet_1)
        ax.add_patch(inner_S_magnet_1)
        ax.add_patch(outer_N_magnet_1)
      if draw_coils:
        ax.add_patch(coil_phase_A_pos)
        ax.add_patch(coil_phase_A_neg)
        ax.add_patch(coil_phase_B_pos)
        ax.add_patch(coil_phase_B_neg)
        ax.add_patch(coil_phase_C_pos)
        ax.add_patch(coil_phase_C_neg)
      ax.set_aspect('equal')

    #Meshgrid for linearised contour plot
    phi,r=np.meshgrid(np.degrees(phi_range),r_range)

  if plot_PM and linear_repr:
    #################################################################
    # Plot Magnetic Flux Density contour plot in rectangular format #
    #################################################################
    plt.figure()
    CS=plt.contour(phi,r,Az_PM,50)
    ax=plt.gca()
    draw_linearised_machine(ax)
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    ax.set_yticks(np.linspace(ri,rvi,7))
    plt.axis([-180/q,180/q,ri,rvi])
    plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
    plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
    plt.ylabel(r'$r$ [m]')
    if not save_figs:
        plt.title(r"Contour plot of the PM's Magnetic Vector Potential")
    else:
      plt.savefig('./pdf/Az_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if plot_AR and linear_repr:
    #################################################################
    # Plot Magnetic Flux Density contour plot in rectangular format #
    #################################################################
    plt.figure()
    CS=plt.contour(phi,r,Az_AR,50)
    ax=plt.gca()
    draw_linearised_machine(ax)
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    ax.set_yticks(np.linspace(ri,rvi,7))
    plt.axis([-180/q,180/q,ri,rvi])
    plt.clabel(CS, inline=True, fontsize=5, fmt='%g')
    plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
    plt.ylabel(r'$r$ [m]')
    if not save_figs:
        plt.title(r"Contour plot of the Windings' Magnetic Vector Potential")
    else:
      plt.savefig('./pdf/Az_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if plot_PM and linear_repr:
    #################################################################
    # Plot Magnetic Flux Density contour plot in rectangular format #
    #################################################################
    plt.figure()
    CS=plt.contour(phi,r,Bmag_PM,40)
    CS=plt.contourf(phi,r,Bmag_PM,50)
    ax=plt.gca()
    draw_linearised_machine(ax)
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    ax.set_yticks(np.linspace(ri,rvi,7))
    plt.axis([-180/q,180/q,ri,rvi])
    plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
    plt.ylabel(r'$r$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the PM's Magnetic Flux Density")
    else:
      plt.savefig('./pdf/Bmag_rt_PM.pdf')

  if plot_AR and linear_repr:
    #################################################################
    # Plot Magnetic Flux Density contour plot in rectangular format #
    #################################################################
    plt.figure()
    CS=plt.contour(phi,r,Bmag_AR,40)
    CS=plt.contourf(phi,r,Bmag_AR,50)
    ax=plt.gca()
    draw_linearised_machine(ax)
    ax.set_xticks(np.linspace(-180/q,180/q,9))
    ax.set_yticks(np.linspace(ri,rvi,7))
    plt.axis([-180/q,180/q,ri,rvi])
    plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
    plt.ylabel(r'$r$ [m]')
    if not save_figs:
      plt.title(r"Contour plot of the Windings' Magnetic Flux Density -- Type %s" % Type)
    else:
      plt.savefig('./pdf/Bmag_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_AR:
  #####################################
  # Plot Current Density Distribution #
  #####################################
  plt.figure()
  plt.plot(np.degrees(phi_range),Jz_def(Ip,t))
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'$J_z$ [A$/$m$^2$]')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title('Current Density Distribution -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Jz-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_PM:
  ############################################################################
  # Plot the Flux-linkages and Induced Voltages due to the Permanent Magnets #
  ############################################################################
  if plot_maxwell:
    #Read the Winding Quick Report (WQR) from Maxwell 2D
    WQR_PM_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_PM-lambda.txt' % (Type,kDelta))[0]),
      names='time,lambda_a,lambda_b,lambda_c,e_a,e_b,e_c,i_a,i_b,i_c')

    if plot_harmonics:
      #Calculate the Harmonic content of the 'lambda_a'
      M=np.size(WQR_PM_Maxwell2D['lambda_a'])-1
      lambda_ah_PM_Maxwell2D=np.abs(np.fft.fft(WQR_PM_Maxwell2D['lambda_a'],M)*2/M)
      lambda_a1_PM_Maxwell2D=lambda_ah_PM_Maxwell2D[1]
      lambda_a_PM_Maxwell2D_THD=np.sqrt(np.sum(lambda_ah_PM_Maxwell2D[2:(M-1)/2]**2))/lambda_a1_PM_Maxwell2D

      #Calculate the Harmonic content of the 'e_a'
      M=np.size(WQR_PM_Maxwell2D['e_a'])-1
      e_ah_PM_Maxwell2D=np.abs(np.fft.fft(WQR_PM_Maxwell2D['e_a'],M)*2/M)
      e_a1_PM_Maxwell2D=e_ah_PM_Maxwell2D[1]
      e_a_PM_Maxwell2D_THD=np.sqrt(np.sum(e_ah_PM_Maxwell2D[2:(M-1)/2]**2))/e_a1_PM_Maxwell2D

      #Plot the Harmonics
      plt.figure()
      h_range=np.arange(20)
      plt.bar(h_range,e_ah_PM_Maxwell2D[h_range])
      plt.grid(True)
      plt.xlabel(r'Harmonic Number')
      plt.ylabel(r'Back EMF, $E_{a,h|PM}$ [V]')
      if not save_figs:
	plt.title(r'$E_{a|PM}$')
      else:
	plt.savefig('./pdf/e_ah_PM_Maxwell2D-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Print important flux-linkage and induced voltage info
  print('lambda_AR=%g [Wbt]' % (abs(lambda_AR)))
  print('lambda_AR_approx=%g [Wbt]' % (abs(lambda_AR_approx)))
  print('lambda_PM=%g [Wbt]' % (abs(lambda_PM)))
  print('lambda_PM_approx=%g [Wbt]' % (lambda_PM_approx))
  if plot_maxwell:
    print('lambda_PM_Maxwell=%g [Wbt]' % (np.max(WQR_PM_Maxwell2D['lambda_a'])))
    if plot_harmonics:
      print('%%THD|lambda_a=%g%%' % (lambda_a_PM_Maxwell2D_THD*100))
      print('%%THD|e_a=%g%%' % (e_a_PM_Maxwell2D_THD*100))

  ##########################
  # Plot the Flux-linkages #
  ##########################
  plt.figure()
  plt.plot(t_range,lambda_a_PM,label=r'$\lambda_{a|PM}(t)$')
  plt.plot(t_range,lambda_b_PM,label=r'$\lambda_{b|PM}(t)$')
  plt.plot(t_range,lambda_c_PM,label=r'$\lambda_{c|PM}(t)$')

  if plot_approx:
    #Add the flux-linkage approximations
    plt.plot(t_range,lambda_a_PM_approx,label=r'$\lambda_{a|PM}(t)$ (Approx.)')
    plt.plot(t_range,lambda_b_PM_approx,label=r'$\lambda_{b|PM}(t)$ (Approx.)')
    plt.plot(t_range,lambda_c_PM_approx,label=r'$\lambda_{c|PM}(t)$ (Approx.)')

  if plot_maxwell:
    #Add the flux-linkages as calculated by Maxwell 2D
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['lambda_a'],label=r'$\lambda_{a|PM}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['lambda_b'],label=r'$\lambda_{b|PM}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['lambda_c'],label=r'$\lambda_{c|PM}(t)$ (Maxwell\textregistered~2D)')

  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.xlabel(r'Time, $t$ [ms]')
  plt.ylabel(r'Flux linkage, $\lambda_{|PM}$ [Wbt]')
  plt.legend()
  plt.axis(xmin=0,xmax=T,ymin=-2,ymax=2)
  plt.grid(True)
  if not save_figs:
    plt.title('Flux-linkage due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/lambda_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #########################
  # Plot Induced Voltages #
  #########################
  plt.figure()
  plt.plot(t_range,e_a_PM,linewidth=1,label=r'$e_{a|PM}(t)$')
  plt.plot(t_range,e_b_PM,linewidth=1,label=r'$e_{b|PM}(t)$')
  plt.plot(t_range,e_c_PM,linewidth=1,label=r'$e_{c|PM}(t)$')

  if plot_approx:
    #Add the induced voltage approximations
    plt.plot(t_range,e_a_PM_approx,linewidth=1,label=r'$e_{a|PM}(t)$ (Approx.)')
    plt.plot(t_range,e_b_PM_approx,linewidth=1,label=r'$e_{b|PM}(t)$ (Approx.)')
    plt.plot(t_range,e_c_PM_approx,linewidth=1,label=r'$e_{c|PM}(t)$ (Approx.)')

  if plot_maxwell:
    #Add the induced voltages as calculated by Maxwell 2D
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['e_a'],linewidth=1,label=r'$e_{a|PM}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['e_b'],linewidth=1,label=r'$e_{b|PM}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_PM_Maxwell2D['time'],WQR_PM_Maxwell2D['e_c'],linewidth=1,label=r'$e_{c|PM}(t)$ (Maxwell\textregistered~2D)')

  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.xlabel(r'Time, $t$ [ms]')
  plt.ylabel(r'Back EMF, $e_{|PM}$ [V]')
  plt.legend()
  plt.axis(xmin=0,xmax=T,ymin=-1000,ymax=1000)
  plt.grid(True)
  if not save_figs:
    plt.title('Induced Voltages due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/e_abc_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_AR:
  ############################################################################
  # Plot the Flux-linkages and Induced Voltages due to the Armature Reaction #
  ############################################################################
  if plot_maxwell:
    #Read the Winding Quick Report (WQR) from Maxwell 2D
    WQR_AR_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_AR-lambda.txt' % (Type,kDelta))[0]),
      names='time,lambda_a,lambda_b,lambda_c,e_a,e_b,e_c,i_a,i_b,i_c')

  #Plot the Flux-linkages
  plt.figure()
  plt.plot(t_range,lambda_a_AR,label=r'$\lambda_{a|AR}(t)$')
  plt.plot(t_range,lambda_b_AR,label=r'$\lambda_{b|AR}(t)$')
  plt.plot(t_range,lambda_c_AR,label=r'$\lambda_{c|AR}(t)$')
  if plot_maxwell:
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['lambda_a'],label=r'$\lambda_{a|AR}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['lambda_b'],label=r'$\lambda_{b|AR}(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['lambda_c'],label=r'$\lambda_{c|AR}(t)$ (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.xlabel(r'Time, $t$ [ms]')
  plt.ylabel(r'Flux-linkage, $\lambda$ [Wbt]')
  plt.legend()
  plt.axis(xmin=0,xmax=T,ymax=0.06,ymin=-0.06)
  plt.grid(True)
  if not save_figs:
    plt.title('Flux-linkage due to the Armature Windings -- Type %s' % Type)
  else:
    plt.savefig('./pdf/lambda_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the Induced Voltages
  plt.figure()
  plt.plot(t_range,e_a_AR,label=r'$e_{a|AR}(t)$')
  plt.plot(t_range,e_b_AR,label=r'$e_{b|AR}(t)$')
  plt.plot(t_range,e_c_AR,label=r'$e_{c|AR}(t)$')
  if plot_maxwell:
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['e_a'],label=r'$e_a(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['e_b'],label=r'$e_b(t)$ (Maxwell\textregistered~2D)')
    plt.plot(WQR_AR_Maxwell2D['time'],WQR_AR_Maxwell2D['e_c'],label=r'$e_c(t)$ (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.xlabel(r'Time, $t$ [ms]')
  plt.ylabel(r'$e(t)$~[V]')
  plt.legend()
  plt.axis(xmin=0,xmax=T)
  plt.grid(True)
  if not save_figs:
    plt.title('Induced Voltages due to the Armature Windings -- Type %s' % Type)
  else:
    plt.savefig('./pdf/e_abc_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_PM:
  #############################################################
  # Plot the Radial Flux Density due to the Permanent Magnets #
  #############################################################
  if plot_maxwell:
    #Read Br @ r_n, r_cim & r_com due to the Permanent Magnets from Maxwell 2D
    Br_PM_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_NO_WIND-Br.txt' % (Type,kDelta))[0]),
      names='NormDistance1, r_n, NormDistance2, r_cim,NormDistance3, r_com, NormDistance4, r_ciy, NormDistance5, r_coy, NormDistance6, r_nmh2, NormDistance7,r_nph2')
    Maxwell2D_angle_range=(Br_PM_Maxwell2D['NormDistance1']*22.5-11.25)/kq

    #Calculate the Harmonic content of Br
    M=int(kq*(np.size(Br_PM_Maxwell2D['r_n'])-1))
    Brh_PM_Maxwell2D=np.abs(np.fft.fft(Br_PM_Maxwell2D['r_n'],M)*2/M)
    Br1_PM_Maxwell2D=Brh_PM_Maxwell2D[1]
    Br_PM_Maxwell2D_THD=np.sqrt(np.sum(Brh_PM_Maxwell2D[2:(M-1)/2]**2))/Br1_PM_Maxwell2D

    if plot_harmonics:
      #Plot the harmonics
      plt.figure()
      h_range=np.arange(20)
      plt.bar(h_range,Brh_PM[h_range],0.5,color='r',label='Analytical')
      plt.bar(h_range+0.5,Brh_PM_Maxwell2D[h_range],0.5,label=r'Maxwell\textregistered~2D')
      plt.grid(True)
      plt.xlabel(r'Harmonic Number')
      plt.ylabel(r'Radial flux density, $B_{r,h|PM}$ [T]')
      plt.legend(loc='best')
      if not save_figs:
	plt.title(r'$B_{r|PM}$')
      else:
	plt.savefig('./pdf/Brh_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the Radial Flux Density due to the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),Br_PM[i_rcim],label=r'$B_{r|r_{cim},PM}$')
  plt.plot(np.degrees(phi_range),Br_PM[i_rn],label=r'$B_{r|r_{n},PM}$')
  plt.plot(np.degrees(phi_range),Br_PM[i_rcom],label=r'$B_{r|r_{com},PM}$')

  if plot_maxwell:
    #Add the Maxwell 2D results
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_cim'],label=r'$B_{r|r_{cim},PM}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_n'],label=r'$B_{r|r_{n},PM}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_com'],label=r'$B_{r|r_{com},PM}$ (Maxwell\textregistered~2D)')

  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Radial flux density, $B_{r|PM}$ [T]')
  plt.legend(loc='upper left')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_r(\phi)$ due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Br_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the Radial Flux Density Variation in the Stator due to the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),Br_PM[i_rnmh2],linewidth=1,label=r'$B_{r|r_n-h/2|PM}$')
  plt.plot(np.degrees(phi_range),Br_PM[i_rn],linewidth=1,label=r'$B_{r|r_{n}|PM}$')
  plt.plot(np.degrees(phi_range),Br_PM[i_rnph2],linewidth=1,label=r'$B_{r|r_n+h/2|PM}$')

  if plot_maxwell:
    #Add the Maxwell 2D results
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_nmh2'],linewidth=1,label=r'$B_{r|r_n-h/2|PM}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_n'],linewidth=1,label=r'$B_{r|r_{n}|PM}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_PM_Maxwell2D['r_nph2'],linewidth=1,label=r'$B_{r|r_n+h/2|PM}$ (Maxwell\textregistered~2D)')

  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Radial flux density, $B_{r}$ [T]')
  plt.legend(loc='upper left')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_r(\phi)$ due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Br_stator_PM-%s.pdf' % Type,transparent=True, bbox_inches='tight')

if plot_AR:
  if plot_maxwell:
    #Get the Radial Flux Density values in the airgap and in the centre of the magnets due to
    #the Armature windings, from Maxwell 2D
    Br_AR_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_AR-Br.txt' % (Type,kDelta))[0]),
      names='NormDistance1, r_n, NormDistance2, r_cim,NormDistance3, r_com')
    Maxwell2D_angle_range=(Br_AR_Maxwell2D['NormDistance1']*22.5-11.25)/kq

  #Plot the Radial Flux Density due to the Armature windings
  plt.figure()
  plt.plot(np.degrees(phi_range),Br_AR[i_rcim],label=r'$B_{r|r_{cim},AR}$')
  plt.plot(np.degrees(phi_range),Br_AR[i_rn],label=r'$B_{r|r_{n},AR}$')
  plt.plot(np.degrees(phi_range),Br_AR[i_rcom],label=r'$B_{r|r_{com},AR}$')
  if plot_maxwell:
    plt.plot(Maxwell2D_angle_range,Br_AR_Maxwell2D['r_cim'],label=r'$B_{r|r_{cim},AR}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_AR_Maxwell2D['r_n'],label=r'$B_{r|r_{n},AR}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_AR_Maxwell2D['r_com'],label=r'$B_{r|r_{com},AR}$ (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Radial flux density, $B_{r|AR}$ [T]')
  plt.legend(loc='upper left')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_r(\phi)$ due to the Armature windings -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Br_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_total:
  if plot_maxwell:
    #Get the Radial Flux Density values in the airgap and in the centre of the magnets due to
    #the Armature windings *AND* the Permanent Magnets, from Maxwell 2D
    Br_total_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f-Br.txt' % (Type,kDelta))[0]),
      names='NormDistance1,r_n,NormDistance2,r_cim,NormDistance3,r_com')
    Maxwell2D_angle_range=(Br_total_Maxwell2D['NormDistance1']*22.5-11.25)/kq

  #Plot the Radial Flux Density due to the Armature windings *AND* the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),Br_total[i_rcim],label=r'$B_{r|r_{cim}}$')
  plt.plot(np.degrees(phi_range),Br_total[i_rn],label=r'$B_{r|r_{n}}$')
  plt.plot(np.degrees(phi_range),Br_total[i_rcom],label=r'$B_{r|r_{com}}$')
  if plot_maxwell:
    plt.plot(Maxwell2D_angle_range,Br_total_Maxwell2D['r_cim'],label=r'$B_{r|r_{cim}}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_total_Maxwell2D['r_n'],label=r'$B_{r|r_{n}}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Br_total_Maxwell2D['r_com'],label=r'$B_{r|r_{com}}$ (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'$B_r$ [T]')
  plt.legend(loc='upper left')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_r(\phi)$ Total -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Br_rt-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_PM:
  ################################################################
  # Plot the Azimuthal Flux Density due to the Permanent Magnets #
  ################################################################
  if plot_maxwell:
    #Read Br @ r_n, r_cim & r_com due to the Permanent Magnets from Maxwell 2D
   Bt_PM_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_NO_WIND-Bt.txt' % (Type,kDelta))[0]),
      names='NormDistance1, r_n, NormDistance2, r_cim,NormDistance3, r_com, NormDistance4, r_ciy, NormDistance5, r_coy')
   Maxwell2D_angle_range=(Bt_PM_Maxwell2D['NormDistance1']*22.5-11.25)/kq

  #Plot the Azimuthal Flux Density due to the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),Bt_PM[i_rcim],label=r'$B_{\phi|r_{cim},PM}$')
  plt.plot(np.degrees(phi_range),Bt_PM[i_rn],label=r'$B_{\phi|r_{n},PM}$')
  plt.plot(np.degrees(phi_range),Bt_PM[i_rcom],label=r'$B_{\phi|r_{com},PM}$')
  if plot_maxwell:
    plt.plot(Maxwell2D_angle_range,Bt_PM_Maxwell2D['r_cim'],label=r'$B_{\phi|r_{cim},PM}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_PM_Maxwell2D['r_n'],label=r'$B_{\phi|r_{n},PM}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_PM_Maxwell2D['r_com'],label=r'$B_{\phi|r_{com},PM}$  (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Azimuthal flux density, $B_{{\phi}|PM}$ [T]')
  plt.legend()
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_t(\phi)$ due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Bt_rt_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the Magnitude of the Flux Density Variation in the Yokes due to the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rciy]**2+Bt_PM[i_rciy]**2),label=r'$B_{mag|PM,r_{ciy}}$')
  plt.plot(np.degrees(phi_range),np.sqrt(Br_PM[i_rcoy]**2+Bt_PM[i_rcoy]**2),label=r'$B_{mag|PM,r_{coy}}$')

  if plot_maxwell:
    #Add the Maxwell 2D results
    plt.plot(Maxwell2D_angle_range,np.sqrt(Br_PM_Maxwell2D['r_ciy']**2+Bt_PM_Maxwell2D['r_ciy']**2),label=r'$B_{mag|PM,r_{ciy}}$ (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,np.sqrt(Br_PM_Maxwell2D['r_coy']**2+Bt_PM_Maxwell2D['r_coy']**2),label=r'$B_{mag|PM,r_{coy}}$ (Maxwell\textregistered~2D)')

  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Flux density, $B_{mag|PM}$ [T]')
  plt.legend(loc='upper left')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_mag(\phi)$ due to the Permanent Magnets -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Bmag_yokes_PM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_AR:
  if plot_maxwell:
    #Get the Azimuthal Flux Density values in the airgap and in the magnets due to
    #the Armature windings, from Maxwell 2D
    Bt_ARMaxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_AR-Bt.txt' % (Type,kDelta))[0]),
      names='NormDistance1,r_n,NormDistance2,r_mci,NormDistance3,r_mco')
    Maxwell2D_angle_range=(Bt_ARMaxwell2D['NormDistance1']*22.5-11.25)/kq

  #Plot the Azimuthal Flux Density due to the Armature windings
  plt.figure()
  plt.plot(np.degrees(phi_range),Bt_AR[i_rcim],label=r'$B_{\phi|r_{cim},AR}$')
  plt.plot(np.degrees(phi_range),Bt_AR[i_rn],label=r'$B_{\phi|r_{n},AR}$')
  plt.plot(np.degrees(phi_range),Bt_AR[i_rcom],label=r'$B_{\phi|r_{com},AR}$')
  if plot_maxwell:
    plt.plot(Maxwell2D_angle_range,Bt_ARMaxwell2D['r_mci'],label=r'$B_{\phi|r_{cim},AR}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_ARMaxwell2D['r_n'],label=r'$B_{\phi|r_{n},AR}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_ARMaxwell2D['r_mco'],label=r'$B_{\phi|r_{com},AR}$  (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'Azimuthal flux density, $B_{{\phi}|AR}$ [T]')
  plt.legend()
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_t(\phi)$ due to the Armature windings -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Bt_rt_AR-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_total:
  if plot_maxwell:
    #Get the Azimuthal Flux Density values in the airgap and in the centre of the magnets due to
    #the Armature windings *AND* the Permanent Magnets, from Maxwell 2D
    Bt_total_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f-Bt.txt' % (Type,kDelta))[0]),
      names='NormDistance1,r_n,NormDistance2,r_mci,NormDistance3,r_mco')
    Maxwell2D_angle_range=(Bt_total_Maxwell2D['NormDistance1']*22.5-11.25)/kq

  #Plot the Azimuthal Flux Density due to the Armature windings *AND* the Permanent Magnets
  plt.figure()
  plt.plot(np.degrees(phi_range),Bt_total[i_rcim],label=r'$B_{\phi|r_{cim}}$')
  plt.plot(np.degrees(phi_range),Bt_total[i_rn],label=r'$B_{\phi|r_{n}}$')
  plt.plot(np.degrees(phi_range),Bt_total[i_rcom],label=r'$B_{\phi|r_{com}}$')
  if plot_maxwell:
    plt.plot(Maxwell2D_angle_range,Bt_total_Maxwell2D['r_mci'],label=r'$B_{\phi|r_{cim}}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_total_Maxwell2D['r_n'],label=r'$B_{\phi|r_{n}}$  (Maxwell\textregistered~2D)')
    plt.plot(Maxwell2D_angle_range,Bt_total_Maxwell2D['r_mco'],label=r'$B_{\phi|r_{com}}$  (Maxwell\textregistered~2D)')
  ax=plt.gca()
  ax.set_xticks(np.linspace(-180/q,180/q,9))
  plt.xlabel(r'Angle, $\phi$ [$^\circ$]')
  plt.ylabel(r'$B_{\phi}$ [T]')
  plt.axis(xmin=-180/q,xmax=180/q)
  plt.legend()
  plt.grid(True)
  if not save_figs:
    plt.title(r'$B_t(\phi)$ Total -- Type %s' % Type)
  else:
    plt.savefig('./pdf/Bt_rt-%s.pdf' % Type, transparent=True, bbox_inches='tight')

if plot_km_range:
  fig=plt.figure()
  ax1=fig.add_subplot(111)
  l1=ax1.plot(km_range,Brmax_range_PM,'b-',linewidth=1,label=r'${B}_{r|PM}$')
  l2=ax1.plot(km_range,Br1_range_PM,'g-',linewidth=1,label=r'$B_{r,1|PM}$')
  ax1.axis(ymin=0,ymax=1)
  ax1.set_xlabel(r'Pole embracing factor, $k_m$')
  ax1.grid(True)
  ax1.set_ylabel(r'Radial flux density, $B_{r|PM}$ [T]')
  
  ax2=ax1.twinx()
  l3=ax2.plot(km_range,Br_THD_range_PM*100, 'r-',linewidth=1)
  ax2.axis(ymin=0)
  ax2.set_ylabel(r'$\%THD(B_{r|PM})$')
  ax2.set_yticks(np.linspace(0,20,6))

  #trans=ax1.get_xaxis_transform()
  #rect=Rectangle((0.6875,0), width=0.0125, height=1, color='gray', alpha=0.5, hatch='/')
  #ax1.add_patch(rect)
  
  ax1.legend((l1,l2,l3),(r'${B}_{r|PM}$',r'$B_{r,1|PM}$',r'$\%THD(B_{r|PM})$'),'lower right')

  if not save_figs:
    plt.title(r'Fundamental Component of $B_{r|PM}$ vs. $k_m$')
  else:
    plt.savefig('./pdf/Br1_PM_vs_km-%s.pdf' % Type, transparent=True, bbox_inches='tight')

plot_Tm_Lorentz=True
plot_Tm_Lorentz_simple=True
plot_Tm_SEMFEM=False if Type=="II" else False

if plot_torque:
  #Average Torque calculations - Take I
  Tm=3/2*EMK_PM*Ip/omega_m

  #Average Torque calculations - Take II
  kT=kT_def(1/kq)
  Tm_kT=kT*Ip

  Tm_lorentz_simple_mean=Tm_lorentz_simple.mean()
  Tm_lorentz_mean=Tm_lorentz.mean()

  Tm_lorentz_simple_ripple=max(Tm_lorentz_simple-Tm_lorentz_simple_mean)-min(Tm_lorentz_simple-Tm_lorentz_simple_mean)
  Tm_lorentz_ripple=max(Tm_lorentz-Tm_lorentz_mean)-min(Tm_lorentz-Tm_lorentz_mean)

  print('Tm=%g [Nm]' % (Tm))
  print('Tm|Lorentz_simple.mean=%g or %g [Nm]' % (Tm_lorentz_simple_mean, Tm0_lorentz_simple))
  print('Tm|Lorentz_simple.ripple=%g [Nm] or %g%%' % (Tm_lorentz_simple_ripple,Tm_lorentz_simple_ripple/Tm_lorentz_simple_mean*100))
  print('Tm|Lorentz_simple.THD=%g %%' % (Tm_lorentz_simple_THD*100))

  print('Tm|Lorentz.mean=%g or %g [Nm]' % (Tm_lorentz_mean,Tm0_lorentz))
  print('Tm|Lorentz.ripple=%g [Nm] or %g%%' % (Tm_lorentz_ripple,Tm_lorentz_ripple/Tm_lorentz_mean*100))
  print('Tm|Lorentz.THD=%g %%' % (Tm_lorentz_THD*100))

#print('Tm|Maxwell.mean=%g [Nm]' % (Tm_maxwell.mean()))

  if plot_maxwell:
    #Read in Ansoft Maxwell 2D data
    Tm_Maxwell2D=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f-Tm.txt' % (Type,kDelta))[0]),
    names='Time,Tm')

    #Calculate the Harmonic content of Tm
    M=np.size(Tm_Maxwell2D['Tm'])-1
    Tmh_Maxwell2D=np.abs(np.fft.fft(Tm_Maxwell2D['Tm'],M)/M)
    Tm0_Maxwell2D=Tmh_Maxwell2D[0]
    Tm_Maxwell2D_THD=np.sqrt(np.sum(Tmh_Maxwell2D[2:(M-1)/2]**2))/Tm0_Maxwell2D

    Tm_Maxwell2D_mean=Tm_Maxwell2D['Tm'].mean()
    Tm_Maxwell2D_ripple=max(Tm_Maxwell2D['Tm']-Tm_Maxwell2D_mean)-min(Tm_Maxwell2D['Tm']-Tm_Maxwell2D_mean)

    print('Tm|Maxwell2D.mean=%g or %g [Nm]' % (Tm_Maxwell2D_mean, Tm0_Maxwell2D))
    print('Tm|Maxwell2D.ripple=%g [Nm] or %g%%' % (Tm_Maxwell2D_ripple,abs(Tm_Maxwell2D_ripple/Tm_Maxwell2D_mean*100)))
    print('Tm|Maxwell2D.THD=%g %%' % (Tm_Maxwell2D_THD*100))

    if Type=='II':
      Tm_Maxwell2D_CLIP=np.rec.array(loadmxwltxt(glob(r'../maxwell/txt/RFAPM-%s_km=0.70_kD=%.2f_CLIP-Tm.txt' % (Type,kDelta))[0]),
      names='Time,Tm')

      #Calculate the Harmonic content of Tm
      M=np.size(Tm_Maxwell2D_CLIP['Tm'])-1
      Tmh_Maxwell2D_CLIP=np.abs(np.fft.fft(Tm_Maxwell2D_CLIP['Tm'],M)/M)
      Tm0_Maxwell2D_CLIP=Tmh_Maxwell2D_CLIP[0]
      Tm_Maxwell2D_CLIP_THD=np.sqrt(np.sum(Tmh_Maxwell2D_CLIP[2:(M-1)/2]**2))/Tm0_Maxwell2D_CLIP

      Tm_Maxwell2D_CLIP_mean=Tm_Maxwell2D_CLIP['Tm'].mean()
      Tm_Maxwell2D_CLIP_ripple=max(Tm_Maxwell2D_CLIP['Tm']-Tm_Maxwell2D_CLIP_mean)-min(Tm_Maxwell2D_CLIP['Tm']-Tm_Maxwell2D_CLIP_mean)

      print('Tm|Maxwell2D_CLIP.mean=%g or %g [Nm]' % (Tm_Maxwell2D_CLIP_mean, Tm0_Maxwell2D_CLIP))
      print('Tm|Maxwell2D_CLIP.ripple=%g [Nm] or %g%%' % (Tm_Maxwell2D_CLIP_ripple,abs(Tm_Maxwell2D_CLIP_ripple/Tm_Maxwell2D_CLIP_mean*100)))
      print('Tm|Maxwell2D_CLIP.THD=%g %%' % (Tm_Maxwell2D_CLIP_THD*100))
      
  if plot_Tm_SEMFEM:
    #Read in SEMFEM data
    t_SEMFEM,Tm_SEMFEM=np.loadtxt(r'../SEMFEM/SEMFEM.txt',skiprows=1,usecols=(0,4),unpack=True)
    t_SEMFEM*=1000

    #Calculate the Harmonic content of Tm
    M=np.size(Tm_SEMFEM)-1
    Tmh_SEMFEM=np.abs(np.fft.fft(Tm_SEMFEM,M)/M)
    Tm0_SEMFEM=Tmh_SEMFEM[0]
    Tm_SEMFEM_THD=np.sqrt(np.sum(Tmh_SEMFEM[2:(M-1)/2]**2))/Tm0_SEMFEM

    Tm_SEMFEM_mean=Tm_SEMFEM.mean()
    Tm_SEMFEM_ripple=max(Tm_SEMFEM-Tm_SEMFEM_mean)-min(Tm_SEMFEM-Tm_SEMFEM_mean)

    print('Tm|SEMFEM.mean=%g or %g [Nm]' % (Tm_SEMFEM_mean, Tm0_SEMFEM))
    print('Tm|SEMFEM.ripple=%g [Nm] or %g%%' % (Tm_SEMFEM_ripple,abs(Tm_SEMFEM_ripple/Tm_SEMFEM_mean*100)))
    print('Tm|SEMFEM.THD=%g %%' % (Tm_SEMFEM_THD*100))

  if Type=='O':
    Tmin=290
    Tmax=340
  elif Type=='I':
    Tmin=175
    Tmax=200
  else:
    Tmin=200
    Tmax=225

  #Plot the Torque calculated using the simplified Lorentz's Method vs. Ansoft Maxwell 2D
  plt.figure()
  plt.plot(t_range,np.abs(Tm_lorentz_simple),'-',linewidth=1,label=r'Using the Simplified Lorentz Method')
  #plt.plot(t_range,np.abs(Tm_maxwell),'-s',label=r'$T_{m|Maxwell}$')
  if plot_maxwell:
    plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-',linewidth=1,label=r'Using Maxwell\textregistered~2D')
  plt.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
  if not save_figs: plt.title(r'Torque -- Type %s' % Type)
  plt.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
  plt.xlabel(r'Time, $t$ [ms]')
  plt.ylabel(r'$T_{mech}$ [Nm]')
  plt.legend(loc='best')
  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.grid(True)
  if not save_figs:
    plt.title(r"Using the simplified Lorentz's method.}}$")
  else:
    plt.savefig('./pdf/Tm_lorentz_simple-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the Torque calculated using the Lorentz's Method vs. Ansoft Maxwell 2D
  plt.figure()
  plt.plot(t_range,np.abs(Tm_lorentz),'-',linewidth=1,label=r'Using the Lorentz Method')
  if plot_maxwell:
    #plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-',linewidth=1,label=r'Using Maxwell\textregistered~2D')
    plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-',linewidth=1,label=r'Using FEM')
  plt.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
  if not save_figs: plt.title(r'Torque -- Type %s' % Type)
  plt.plot(t_range,np.ones(np.size(t_range))*Tm_kT,'-',linewidth=1,label=r'Simplified Average Torque Calculation')
  plt.xlabel(r'Time, $t$ (ms)')
  plt.ylabel(r'Mechanical Torque, $T_{mech}$ (Nm)')
  plt.legend(loc='best')
  ax=plt.gca()
  ax.set_xticks(np.linspace(0,T,11))
  plt.grid(True)
  if not save_figs:
    plt.title(r"Using the Lorentz's Method)}}$")
  else:
    plt.savefig('./pdf/Tm_lorentz-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  if plot_Tm_SEMFEM:
    #Plot the Torque using the Lorentz's Method vs SEMFEM
    plt.figure()
    if plot_maxwell:
      #plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-',linewidth=1,label=r'Using Maxwell\textregistered~2D')
      plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-',linewidth=1,label=r'Using FEM')
    plt.plot(t_range,np.abs(Tm_lorentz),'-^',label=r'Lorentz')
    #plt.plot(Tm_Maxwell2D['Time'],np.abs(Tm_Maxwell2D['Tm']),'-v',label=r'FEM')
    plt.plot(t_SEMFEM,np.abs(Tm_SEMFEM),'-*',label=r'SEMFEM')
    plt.axis(xmin=0,xmax=T,ymin=Tmin,ymax=Tmax)
    if not save_figs: plt.title(r'Torque -- Type %s' % Type)
    plt.xlabel(r'Time, $t$ [ms]')
    plt.ylabel(r'$T_{mech}$ [Nm]')
    plt.legend(loc='best')
    ax=plt.gca()
    ax.set_xticks(np.linspace(0,T,11))
    plt.grid(True)
    if not save_figs:
      plt.title(r"Using Lorentz's Law compared to SEMFEM}}$")
    else:
      plt.savefig('./pdf/Tm_SEMFEM-%s.pdf' % Type, transparent=True, bbox_inches='tight')

  #Plot the harmonics
  plt.figure()
  if not plot_maxwell:
    bar_width=0.5 if plot_Tm_SEMFEM else 1.0
  else:
    if Type=='II':
      bar_width=0.25 if plot_Tm_SEMFEM else 0.5
    else:
      bar_width=0.33 if plot_Tm_SEMFEM else 0.5
  h_range=np.arange(1,20)
  plt.bar(h_range,100*Tmh_lorentz[h_range]/abs(Tm_lorentz_mean),bar_width,color='b',label='Analytical')
  if plot_maxwell:
    #plt.bar(h_range+bar_width,100*Tmh_Maxwell2D[h_range]/abs(Tm_Maxwell2D_mean),bar_width,color='g',label=r'Maxwell\textregistered~2D')
    plt.bar(h_range+bar_width,100*Tmh_Maxwell2D[h_range]/abs(Tm_Maxwell2D_mean),bar_width,color='g',label=r'FEM')
    if Type=='II':
      #plt.bar(h_range+bar_width*2,100*Tmh_Maxwell2D_CLIP[h_range]/abs(Tm_Maxwell2D_CLIP_mean),bar_width,color='y',label=r'Maxwell\textregistered~2D (Tweaked)')
      plt.bar(h_range+bar_width*2,100*Tmh_Maxwell2D_CLIP[h_range]/abs(Tm_Maxwell2D_CLIP_mean),bar_width,color='y',label=r'FEM (Tweaked)')
  if plot_Tm_SEMFEM:
    if not plot_maxwell:
      plt.bar(h_range+bar_width,100*Tmh_SEMFEM[h_range]/abs(Tm_SEMFEM.mean()),bar_width,color='r',label='SEMFEM')
    else:
      if Type=='II':
        plt.bar(h_range+bar_width*3,100*Tmh_SEMFEM[h_range]/abs(Tm_SEMFEM.mean()),bar_width,color='r',label='SEMFEM')
      else:
        plt.bar(h_range+bar_width*2,100*Tmh_SEMFEM[h_range]/abs(Tm_SEMFEM.mean()),bar_width,color='r',label='SEMFEM')
  ax=plt.gca()
  ax.set_xticks(h_range)
  plt.grid(True)
  plt.xlabel(r'Harmonic Number')
  plt.ylabel(r'$\frac{T_{mech|h}}{T_{mech|ave.}}\times 100$~\%')
  plt.legend(loc='best')
  if not save_figs:
    plt.title(r'$T_{mh}$')
  else:
    plt.savefig('./pdf/Tmh-%s.pdf' % Type, transparent=True, bbox_inches='tight')

K1=kq*sin(kDelta*pi/(6*kq))*kw_pitch_def(int(1/kq))

print('K1=%g' % K1)

if Type=='O':
  le=2*pi*rn/q+2*h
elif Type=='I':
  le=pi/Q*rn*pi/2
elif Type=='II':
  le=(2*pi/Q-2*Delta/q)*rn*pi/2
else:
  raise ValueError, "The value of 'Type' must either be equal to 'O', 'I' or 'II'."

ke=1+le/l

K2=ke*kDelta

print('K2=%g' % K2)

if show_figs: plt.show()
print('Done!')
