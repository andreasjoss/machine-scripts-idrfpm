#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: 2D FEM simulation of a single-outrunner-rotor radial flux permanent magnet synchronous motor
#Author : Andreas Joss

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)

import pylab as pl
import numpy as np
#from SEMFEM_plotter import *
from prettytable import PrettyTable
import os, sys
import sqlite3
import shutil

single_weight_optimisation = 1

#control variables
source_db = 'IDRFPM.db'
#source_db = 'best.db'
primary_key = 23 #primary_key of the source database
pk_upper_limit = 10
pk_lower_limit = 11

string_order_of_ideal_optimization = []
enable_state_machine_operating_points = 1

##ROUND LITZ
#string_order_of_ideal_optimization.append(["weight_torque_density",[0,0.3,0]]) #constant weights added for different operating_speeds
#string_order_of_ideal_optimization.append(["weight_pm_mass",[0,0,0]])
#string_order_of_ideal_optimization.append(["weight_torque_ripple",[0,0.1,0]]) #was [0,0.1,0.1] and works very well with [0,0.1,0]
#string_order_of_ideal_optimization.append(["weight_eff",[0,0.1,0]])
#string_order_of_ideal_optimization.append(["weight_torque_average",[0,0,0]])
#string_order_of_ideal_optimization.append(["weight_mass_total",[0,0,0]])
#string_order_of_ideal_optimization.append(["weight_pf",[0,0,0]])
#string_order_of_ideal_optimization.append(["weight_pmech",[0,0.6,0]]) #was previously [0,0.6,0] or [0,0.4,0]

#operating_speeds = [50,100,465] #first operating point is to determine L and lambda_PM
#enable_target_mech_power = [0,1,1]
#enable_turns_calculation = [0,0,1]
#enable_automatic_flux_weakening = [0,1,1]
#mesh_densities = [3,2,3] #was previously [2,2,3]
#steps = [31,127,7] #was previously [127,127,7]
#target_mech_power_kW = [2,3,2]

#SOLID BARS
#string_order_of_ideal_optimization.append(["weight_torque_density",[0.3,0]]) #constant weights added for different operating_speeds
#string_order_of_ideal_optimization.append(["weight_pm_mass",[0,0]])
#string_order_of_ideal_optimization.append(["weight_torque_ripple",[0.1,0]])
#string_order_of_ideal_optimization.append(["weight_eff",[0.1,0]])
#string_order_of_ideal_optimization.append(["weight_torque_average",[0,0]])
#string_order_of_ideal_optimization.append(["weight_mass_total",[0,0]])
#string_order_of_ideal_optimization.append(["weight_pf",[0,0]])
#string_order_of_ideal_optimization.append(["weight_pmech",[0.6,0]])

#operating_speeds = [100,465] #first operating point is to determine L and lambda_PM
#enable_target_mech_power = [0,1]
#enable_turns_calculation = [0,0]
#enable_automatic_flux_weakening = [0,1]
#mesh_densities = [3,3] #increase mesh density when local optimisation
#steps = [31,31] #increase steps when local optimisation
#target_mech_power_kW = [3,2] #3 ensures entire 200W is spent at 100rpm operating point

#string_order_of_ideal_optimization.append(["weight_torque_density",[0.3]]) #constant weights added for different operating_speeds
#string_order_of_ideal_optimization.append(["weight_pm_mass",[0]])
#string_order_of_ideal_optimization.append(["weight_torque_ripple",[0.1]])
#string_order_of_ideal_optimization.append(["weight_eff",[0.1]])
#string_order_of_ideal_optimization.append(["weight_torque_average",[0]])
#string_order_of_ideal_optimization.append(["weight_mass_total",[0]])
#string_order_of_ideal_optimization.append(["weight_pf",[0]])
#string_order_of_ideal_optimization.append(["weight_pmech",[0.6]])

#IDRFPM
string_order_of_ideal_optimization.append(["weight_torque_density",[0.5]]) #constant weights added for different operating_speeds
string_order_of_ideal_optimization.append(["weight_pm_mass",[0]])
string_order_of_ideal_optimization.append(["weight_torque_ripple",[0]])
string_order_of_ideal_optimization.append(["weight_eff",[0]])
string_order_of_ideal_optimization.append(["weight_torque_average",[0]])
string_order_of_ideal_optimization.append(["weight_mass_total",[0]])
string_order_of_ideal_optimization.append(["weight_pf",[0]])
string_order_of_ideal_optimization.append(["weight_pmech",[0]])

operating_speeds = [100] #first operating point is to determine L and lambda_PM
enable_target_mech_power = [0]
enable_turns_calculation = [1]
enable_automatic_flux_weakening = [0]
mesh_densities = [3] #increase mesh density when local optimisation
steps = [31,31] #increase steps when local optimisation
target_mech_power_kW = [2]

itr = 0.025
round_off_dec = 3
number_of_threads = 6

pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

#add rows, each with unique combination of weigths
w_eff = 0.0
w_tor = 0.0
w_mass = 0.0

w_eff_init = 0.0
w_tor_init = 0.0
w_mass_init = 1.0

div_o = int(1/itr)
div_i = div_o
loop_array = []
i = 0
j = 0

while i<div_o+1:
  j = i
  while j<div_i+1:
    w_tor = w_tor_init + j*itr - w_mass
    w_eff = abs(1 - w_tor - w_mass)
    loop_array.append([w_eff,w_tor,w_mass,0.0,0.0,0.0,0.0])

    j = j + 1

  i = i + 1
  w_mass = w_eff_init + i*itr

#for i in range (0,len(loop_array)):
  #print("w_eff = %.3f, w_tor = %.3f, w_mass = %.3f"%(loop_array[i][0],loop_array[i][1],loop_array[i][2]))



#copy SORSPM.db to optimise.db (overwrite if already exists)
shutil.copy(source_db,'optimise.db')

conn = sqlite3.connect(source_db)
c = conn.cursor()

#attach (existing) database
c.execute("ATTACH DATABASE 'optimise.db' as optimise")

#delete all row entries in optimise.db
c.execute("DELETE FROM optimise.machine_input_parameters")
c.execute("DELETE FROM optimise.machine_semfem_output")

#c.execute("CREATE TABLE optimise.machine_input_parameters")
try:
  c.execute("INSERT INTO optimise.machine_input_parameters SELECT * FROM main.machine_input_parameters WHERE primary_key = ?",(primary_key,))    
  c.execute("INSERT INTO optimise.machine_input_parameters SELECT * FROM main.machine_input_parameters WHERE primary_key = ?",(pk_upper_limit,))
  c.execute("INSERT INTO optimise.machine_input_parameters SELECT * FROM main.machine_input_parameters WHERE primary_key = ?",(pk_lower_limit,))
except:
  pass

#update primary_key for upper_limit to be at -2 and lower_limit to be at -1
c.execute("UPDATE optimise.machine_input_parameters SET primary_key = -2 WHERE _rowid_ = ?",(pk_upper_limit,))
c.execute("UPDATE optimise.machine_input_parameters SET primary_key = -1 WHERE _rowid_ = ?",(pk_lower_limit,))

#update primary_key to start at 0 (which will signify starting point)
c.execute("UPDATE optimise.machine_input_parameters SET primary_key = 0 WHERE _rowid_ = ?",(primary_key,))

#nou ingesit
c.execute("INSERT INTO optimise.machine_input_parameters SELECT * FROM main.machine_input_parameters WHERE primary_key = ?",(primary_key,))  
c.execute("UPDATE optimise.machine_input_parameters SET primary_key = -3 WHERE _rowid_ = ?",(primary_key,))

#conn.commit()
#sys.exit()

c.execute("UPDATE optimise.SQLITE_SEQUENCE SET seq = 0 WHERE NAME = 'machine_semfem_output'")
c.execute("UPDATE optimise.SQLITE_SEQUENCE SET seq = 0 WHERE NAME = 'machine_input_parameters'")

if single_weight_optimisation == 0:
  for i in range (0,len(loop_array)):
    if i+1 != primary_key:
      #print("i+1 = %d, primary_key = %d"%(i+1,primary_key))
      c.execute("INSERT INTO optimise.machine_input_parameters SELECT * FROM main.machine_input_parameters WHERE primary_key = ?",(primary_key,))
      c.execute("UPDATE optimise.machine_input_parameters SET primary_key = ? WHERE _rowid_ = ?",(i+1,primary_key,))

  c.execute("UPDATE optimise.machine_input_parameters SET primary_key = ? WHERE _rowid_ = -3",(primary_key,))
else:
  c.execute("UPDATE optimise.machine_input_parameters SET primary_key = ? WHERE _rowid_ = -3",(1,))
conn.commit()

# add weight_id column, and other weight columns
for i in range (0, len(string_order_of_ideal_optimization)):
  c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN " + string_order_of_ideal_optimization[i][0] + " REAL")

#c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN weight_eff REAL")
#c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN weight_pf REAL")
#c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN weight_mass_total REAL")
#c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN weight_torque_average REAL")
#c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN weight_torque_ripple REAL")
c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN thread_number INTEGER")
c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN iteration_number INTEGER")
c.execute("ALTER TABLE optimise.machine_input_parameters ADD COLUMN completed_optimisation INTEGER")
conn.commit()

###create another database which will only save best values found during optimisation
shutil.copy('optimise.db','best.db')
c.execute("ATTACH DATABASE 'best.db' as best")
c.execute("ALTER TABLE best.machine_semfem_output ADD COLUMN f_value REAL")
c.execute("CREATE TABLE best.info (variable_name TEXT,value_int INTEGER)")
c.execute("INSERT INTO best.info (variable_name,value_int) VALUES (?,?)",('have_already_commited_best_solution',0))

# create table which saves primary_keys of ideal_values
c.execute("CREATE TABLE optimise.ideal_value_keys (weight_name TEXT,primary_key INTEGER)")

# add state machine tables to optimise.db
if enable_state_machine_operating_points:
  for i in range(0,len(operating_speeds)):
    os.system("sqlite3 optimise.db '.schema machine_input_parameters' | sed '1s/machine_input_parameters/machine_input_parameters_rpm_%03d/' | sqlite3 optimise.db"%(operating_speeds[i]))
    os.system("sqlite3 optimise.db '.schema machine_semfem_output' | sed '1s/machine_semfem_output/machine_semfem_output_rpm_%03d/' | sqlite3 optimise.db"%(operating_speeds[i]))

  for i in range(0,len(operating_speeds)):
    c.execute("INSERT INTO optimise.machine_input_parameters_rpm_%03d SELECT * FROM optimise.machine_input_parameters"%(operating_speeds[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET n_rpm = %d"%(operating_speeds[i],operating_speeds[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET enable_target_mech_power = %d"%(operating_speeds[i],enable_target_mech_power[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET enable_turns_calculation = %d"%(operating_speeds[i],enable_turns_calculation[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET enable_automatic_flux_weakening = %d"%(operating_speeds[i],enable_automatic_flux_weakening[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET mesh_scale = %d"%(operating_speeds[i],mesh_densities[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET steps = %d"%(operating_speeds[i],steps[i]))
    c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET target_mech_power_kW = %d"%(operating_speeds[i],target_mech_power_kW[i]))
    
    
  for i in range (0, len(string_order_of_ideal_optimization)):
    for j in range(0,len(operating_speeds)):
      c.execute("UPDATE optimise.machine_input_parameters_rpm_%03d SET %s = %f"%(operating_speeds[j],string_order_of_ideal_optimization[i][0],string_order_of_ideal_optimization[i][1][j]))
      #print("operating_speeds[j] = %d,string_order_of_ideal_optimization[i][0] = %s,string_order_of_ideal_optimization[i][1][j] = %f"%(operating_speeds[j],string_order_of_ideal_optimization[i][0],string_order_of_ideal_optimization[i][1][j]))
 
conn.commit()
#res = conn.execute("SELECT name FROM optimise.sqlite_master WHERE type='table';")
#for name in res:
  #if name[0].startswith("machine_input_parameters_"):
    #print name[0]



keys = []
thread_number = 1
for i in range (0,len(loop_array)):
  #if i == 0:
  #c.execute("INSERT INTO optimise.machine_input_parameters (primary_key,weight_eff,weight_mass_total,weight_torque_average,thread_number) VALUES (%d,%.3f,%.3f,%.3f,%d)"%(i+1,loop_array[i][0],loop_array[i][1],loop_array[i][2],thread_number))
  #c.execute("INSERT INTO optimise.machine_input_parameters (weight_eff,weight_mass_total,weight_torque_average,weight_torque_ripple,weight_pf,thread_number) VALUES (%.3f,%.3f,%.3f,%.3f,%.3f,%d)"%(loop_array[i][0],loop_array[i][1],loop_array[i][2],0.0,0.0,thread_number))
  
  c.execute("UPDATE optimise.machine_input_parameters SET thread_number = %d WHERE primary_key = %d"%(thread_number,i+1))
  c.execute("UPDATE optimise.machine_input_parameters SET completed_optimisation = %d WHERE primary_key = %d"%(0,i+1))
  
  #for j in range (0,len(string_order_of_ideal_optimization)):
    #c.execute("UPDATE optimise.machine_input_parameters SET " + string_order_of_ideal_optimization[j][0] + " = %.3f WHERE primary_key = %d"%(loop_array[i][j],i+1))
  
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_eff = %.3f WHERE primary_key = %d"%(loop_array[i][0],i+1))
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_mass_total = %.3f WHERE primary_key = %d"%(loop_array[i][1],i+1))
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_torque_average = %.3f WHERE primary_key = %d"%(loop_array[i][2],i+1))
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_torque_ripple = %.3f WHERE primary_key = %d"%(loop_array[i][3],i+1))
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_pf = %.3f WHERE primary_key = %d"%(loop_array[i][4],i+1))
  
  #c.execute("UPDATE optimise.machine_input_parameters SET weight_eff = %.3f, weight_mass_total = %.3f, weight_torque_average = %.3f, weight_torque_ripple = %.3f,weight_pf = %.3f,thread_number = %d WHERE primary_key = %d"%(loop_array[i][0],loop_array[i][1],loop_array[i][2],0.0,0.0,thread_number,i+1))
  
  thread_number = thread_number + 1
  
  if np.isclose(1,loop_array[i][0],rtol=1e-03, atol=1e-03, equal_nan=False):
    keys.append(i+1)
    
  if np.isclose(1,loop_array[i][1],rtol=1e-03, atol=1e-03, equal_nan=False):
    keys.append(i+1)
    
  if np.isclose(1,loop_array[i][2],rtol=1e-03, atol=1e-03, equal_nan=False):
    keys.append(i+1)    
  
  if thread_number>number_of_threads:
    thread_number = 1
    
  #print thread_number
  #print loop_array[i]

keys.append(0)
keys.append(0)
keys.append(0)
keys.append(0)

#for i in range(0,len(string_order_of_ideal_optimization)):
  #c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)",(string_order_of_ideal_optimization[i][0],keys[i],))
  ##c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ("+string_order_of_ideal_optimization[i][0]+",?)",(keys[i],))
  
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ('weight_eff',?)",(keys[0],))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ('weight_mass_total',?)",(keys[1],))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ('weight_torque_average',?)",(keys[2],))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ('weight_torque_ripple',?)",(keys[3],))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES ('weight_pf',?)",(keys[4],))

#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)"('weight_eff',keys[0]))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)"('weight_mass_total',keys[1]))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)"('weight_torque_average',keys[2]))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)"('weight_torque_ripple',0))
#c.execute("INSERT INTO optimise.ideal_value_keys (weight_name,primary_key) VALUES (?,?)"('weight_pf',0))


conn.commit()
conn.close()










#conn_optimise = sqlite3.connect('optimise.db')
#c_optimise = conn_optimise.cursor()

#transfer initial starting input parameters from SORSPM.db to optimise.db
#c.execute("INSERT INTO table1 SELECT * FROM table2")
#c_read_initial_input.execute("CREATE TEMP TABLE initial_row AS SELECT * FROM machine_input_parameters WHERE primary_key = ?",(primary_key,))
#start_row = c_read_initial_input.fetchone()[0]
#start_row = c_read_initial_input.fetchall()
#print(start_row)
#c_optimise.execute("INSERT INTO machine_input_parameters FROM")

#def db_insert(table,column,primary_key,value):
  ##conn.commit()
    #c.execute("INSERT INTO "+table+"(input_id,"+column+") "+ "VALUES (?,?)",(primary_key,value))
    
    #if database_verbosity == 1: print "created new row and added some value to a column"
  #else:
    #c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
    #if database_verbosity == 1: print "added new value to column of existing row"
  ##conn.commit()
  
