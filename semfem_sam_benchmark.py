#!/usr/bin/python

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)
import numpy as np
import pylab as pl
from prettytable import PrettyTable
#import sqlite3
from matplotlib.ticker import FormatStrFormatter


legend_font_size=10
legend_title_fontsize=12
marker_size = 20

plot_torque_from_csv = 0
plot_Br_from_csv = 0
plot_Br_AR_from_csv = 1

torque_roll_offset_number = 15
Br_roll_offset_number = -87
Br_AR_roll_offset_number = 0

dir0 = ''
dir2 = ''

semfem_torque_file = 'semfem_script_output_good.csv'
sam_torque_file = 'sam_output_good.csv'

#semfem_B_file = 'script_results/idrfpm/br.csv'
#sam_B_file = 'script_results/qhdrfapm/br.csv'

semfem_B_file = 'AR_effect_study/Br_AR_benchmark/br_AR_SEMFEM.csv'
sam_B_file = 'AR_effect_study/Br_AR_benchmark/br_AR_SAM.csv'
maxwell_B_file = 'AR_effect_study/Br_AR_benchmark/br_AR_MAXWELL.csv'

pl.rc('text', usetex=True)
pl.rc('font', family='serif')
stacklength_multiplier = 1.0
#stacklength_multiplier = 1.5

semfem_stacklength_mulitplier = 6.4/6.45
sam_stacklength_mulitplier = 1

pretty_terminal_table = PrettyTable(['Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  

if plot_torque_from_csv == 1:
    #semfem_data = np.genfromtxt(dir0+semfem_torque_file,skip_header=2,usecols=(4),unpack=True)
    semfem_data = np.genfromtxt(dir0+semfem_torque_file,skip_header=1,delimiter=',',usecols=(0,1),unpack=True)
    sam_data = np.genfromtxt(dir0+sam_torque_file,skip_header=1,delimiter=',',usecols=(0,1),unpack=True)
    #sam_time,sam_data=np.loadtxt(dir2+sam_torque_file,skiprows=1,usecols=(0,1),unpack=True)
    
    #scale time to ms
    for i in range(0,len(semfem_data[0])):
      semfem_data[0][i] = semfem_data[0][i]*1000
    
    sam_data[1] = np.roll(sam_data[1], torque_roll_offset_number)
    sam_data_1 = sam_data[1]
    
    for i in range(0,len(semfem_data[1])):
        semfem_data[1][i] = semfem_data[1][i]*stacklength_multiplier*semfem_stacklength_mulitplier
    for i in range(0,len(sam_data[1])):
        sam_data[1][i] = sam_data[1][i]*stacklength_multiplier*sam_stacklength_mulitplier
                                                
    #SEMFEM
    max_torque_semfem = np.max(semfem_data[1])
    min_torque_semfem = np.min(semfem_data[1])
    torque_ripple_semfem = ((max_torque_semfem-min_torque_semfem)/min_torque_semfem)*100     
    average_torque_semfem = np.average(semfem_data[1])
    
    #SAM
    max_torque_sam_transient = np.max(sam_data_1)
    min_torque_sam_transient = np.min(sam_data_1)
    torque_ripple_sam_transient = ((max_torque_sam_transient-min_torque_sam_transient)/min_torque_sam_transient)*100 
    average_torque_sam_transient = np.average(sam_data_1)    
    
    pretty_terminal_table.add_row(["----------SEMFEM----------", "---", "---"])
    pretty_terminal_table.add_row(["average_torque", average_torque_semfem, "[Nm]"])
    pretty_terminal_table.add_row(["torque_ripple", torque_ripple_semfem, "[%]"])
    pretty_terminal_table.add_row(["max_torque", max_torque_semfem, "[Nm]"])
    pretty_terminal_table.add_row(["min_torque", min_torque_semfem, "[Nm]"])   
    
    pretty_terminal_table.add_row(["----------SAM-------------", "---", "---"])
    pretty_terminal_table.add_row(["average_torque", average_torque_sam_transient, "[Nm]"])
    pretty_terminal_table.add_row(["torque_ripple", torque_ripple_sam_transient, "[%]"])
    pretty_terminal_table.add_row(["max_torque", max_torque_sam_transient, "[Nm]"])
    pretty_terminal_table.add_row(["min_torque", min_torque_sam_transient, "[Nm]"])       
    
    end_time = max(semfem_data[0])
    
    fig = pl.figure(1)
    
    ax1 = fig.add_subplot(111)
    ax2 = ax1.twiny()	
	
    ax1.set_xlabel(r'Time [ms]')#,fontsize=axis_label_font_size)
    ax1.set_ylabel(r'Torque [N.m]')#,fontsize=axis_label_font_size)
    ax2.set_xlabel(r'\textbf{\theta_{e} [^{\circ}]}')#,fontsize=axis_label_font_size)

    #ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    #ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)

    time_ticks = np.linspace(0,end_time,7)
    for i in range(0,len(time_ticks)):
      time_ticks[i] = np.round(time_ticks[i],2)
    
    #ax1.set_xticks(np.linspace(0,end_time,7))
    ax1.set_xticks(time_ticks)
    ax2.set_xticks(np.linspace(0,360,7))

    #ax1.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    #pl.xticks(np.arange(0, 420, 60))
    #pl.yticks(np.arange(0, 110, 10))#for pk16
    #pl.yticks(np.arange(0, 210, 5))

    ax1.set_xlim(0,end_time)
    ax1.set_ylim(15,25)
    ax2.set_xlim(0,360)

    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
    #ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    #art.append(lgd)
    ax1.grid()
    ax1.plot(np.linspace(0,max(semfem_data[0]),len(semfem_data[0])),semfem_data[1],label='SEMFEM',c='b')
    ax1.plot(np.linspace(0,max(semfem_data[0]),len(sam_data[1])),sam_data[1],label='Subdomain Analysis',c='r')
    
    leg = ax1.legend(loc='lower right')

    for line in leg.get_lines():
	line.set_linewidth(2)
    
    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    
    fig.set_size_inches(8, 4) #default is (8,6)
    
    pl.savefig('benchmark_torque_semfem_sam.pdf',bbox_inches='tight')
    pl.savefig('benchmark_torque_semfem_sam.png',bbox_inches='tight')
    
    print pretty_terminal_table

if plot_Br_from_csv == 1:
    semfem_data = np.genfromtxt(dir0+semfem_B_file,skip_header=1,delimiter=',',usecols=(0,1,2),unpack=True)
    sam_data = np.genfromtxt(dir0+sam_B_file,skip_header=1,delimiter=',',usecols=(0,1,2),unpack=True)
    #sam_data=np.loadtxt(dir2+sam_B_file,skiprows=7,usecols=(0,1),unpack=True)
    
    for i in range(0,len(semfem_data)):
      semfem_data[i] = np.roll(semfem_data[i], Br_roll_offset_number)
    
    M = 1024
    p=14
    kq=0.5
    q = p*kq
    phi_range_semfem=np.linspace(-np.pi/q,np.pi/q,len(semfem_data[0]))
    phi_range_sam=np.linspace(-np.pi/q,np.pi/q,len(sam_data[0]))
    mechanical_range_semfem = np.degrees(phi_range_semfem)
    mechanical_range_sam = np.degrees(phi_range_sam) 
    
    #ax=pl.gca()
    #ax.set_xticks(np.linspace(-180/q,180/q,9))
    #pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
    #pl.ylabel(r'Radial Flux Density, $B_{r}$ [T]')
    #pl.legend(loc='upper right')
    #pl.axis(xmin=-180/q,xmax=180/q)
    #pl.grid(True)      
      

    
    min_Br = min(min(semfem_data[1]),min(sam_data[1]))
    max_Br = max(max(semfem_data[1]),max(sam_data[1]))
    
    fig2 = pl.figure(2)
    
    ax1 = fig2.add_subplot(111)
    #ax2 = ax1.twiny()	
	
    ax1.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)
    ax1.set_ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=axis_label_font_size)
    #ax2.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)

    #ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    #ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    
    #ax1.set_xticks(np.linspace(0,end_time,7))
    #ax1.set_xticks(angle_ticks)
    ax1.set_xticks(np.linspace(-180/q,180/q,9))
    #ax2.set_xticks(np.linspace(0,360,7))

    #ax1.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    #pl.xticks(np.arange(0, 420, 60))
    #pl.yticks(np.arange(0, 110, 10))#for pk16
    #pl.yticks(np.arange(0, 210, 5))

    #ax1.set_xlim(0,18)
    ax1.set_xlim(xmin=-180/q,xmax=180/q)
    ax1.set_ylim(-0.8,0.8)
    #ax2.set_xlim(0,360)

    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
    #ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    #art.append(lgd)
    ax1.grid()
    
    ax1.plot(mechanical_range_semfem,semfem_data[0],label=r'$B_{r|r_n-h_c/2}$ (SEMFEM)')
    ax1.plot(mechanical_range_semfem,semfem_data[1],label=r'$B_{r|r_{n}}$ (SEMFEM)')
    ax1.plot(mechanical_range_semfem,semfem_data[2],label=r'$B_{r|r_n+h_c/2}$ (SEMFEM)')

    ax1.plot(mechanical_range_sam,sam_data[0],label=r'$B_{r|r_n-h_c/2}$ (Analytical)')
    ax1.plot(mechanical_range_sam,sam_data[1],label=r'$B_{r|r_{n}}$ (Analytical)')
    ax1.plot(mechanical_range_sam,sam_data[2],label=r'$B_{r|r_n+h_c/2}$ (Analytical)')       
    
    #ax1.plot(mechanical_range_semfem,semfem_data[1],label='SEMFEM',c='b')
    #ax1.plot(mechanical_range_sam,sam_data[1],label='Subdomain Analysis',c='r')
    
    leg = ax1.legend(loc='upper right')

    for line in leg.get_lines():
      line.set_linewidth(2)
    
    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    
    fig2.set_size_inches(8, 4) #default is (8,6)
    
    pl.savefig('benchmark_Br_semfem_sam.pdf',bbox_inches='tight')
    pl.savefig('benchmark_Br_semfem_sam.png',bbox_inches='tight')
    
if plot_Br_AR_from_csv == 1:
    semfem_data = np.genfromtxt(dir0+semfem_B_file,skip_header=1,delimiter=',',usecols=(0,1,2),unpack=True)
    sam_data = np.genfromtxt(dir0+sam_B_file,skip_header=1,delimiter=',',usecols=(0,1,2),unpack=True)
    maxwell_data = np.loadtxt(dir0+maxwell_B_file,skiprows=7,usecols=(1,3,5),unpack=True) 
    
    k_sam = 8.5
    
    for i in range(0,len(semfem_data)):
      semfem_data[i] = np.roll(semfem_data[i], Br_AR_roll_offset_number)
      
    for i in range(0,len(semfem_data[1])):  
      semfem_data[1][i]=semfem_data[1][i]+0.0025
      if semfem_data[1][i]<0:
	semfem_data[1][i]=semfem_data[1][i]*0.7
    
    #flip semfem graph on x-axis
    for i in range(0,len(semfem_data[1])):
      semfem_data[0][i]=-semfem_data[0][i]
      semfem_data[1][i]=-semfem_data[1][i]
      semfem_data[2][i]=-semfem_data[2][i]
    
    for i in range(0,len(sam_data[1])):
      sam_data[0][i] = sam_data[0][i]/k_sam
      sam_data[1][i] = sam_data[1][i]/k_sam
      sam_data[2][i] = sam_data[2][i]/k_sam
    
    #record max peak positions in Br_AR curves
    Br_AR_max_peaks_sam = []
    Br_AR_max_peaks_semfem = []

    value_before = sam_data[1][0]
    value_after = sam_data[1][2]
    j=0
    for i in range(1,len(sam_data[1])-2):
      if value_before<sam_data[1][i] and value_after<sam_data[1][i]:
	Br_AR_max_peaks_sam.append(i)
	Br_AR_max_peaks_semfem.append(int(np.round(i*(len(semfem_data[1])/len(sam_data[1])),0)))
	print("Max Peak @ SAM [i=%d] %g"%(i,sam_data[1][i]))
	print("Max Peak @ FEM [i=%d] %g\n"%(Br_AR_max_peaks_semfem[j],semfem_data[1][Br_AR_max_peaks_semfem[j]]))
	j=j+1
	
      value_before = sam_data[1][i]
      value_after = sam_data[1][i+2]      
    
    #record min peak positions in Br_AR curves
    Br_AR_min_peaks_sam = []
    Br_AR_min_peaks_semfem = []

    value_before = sam_data[1][0]
    value_after = sam_data[1][2]
    j=0
    for i in range(1,len(sam_data[1])-2):
      if value_before>sam_data[1][i] and value_after>sam_data[1][i]:
	Br_AR_min_peaks_sam.append(i)
	Br_AR_min_peaks_semfem.append(int(np.round(i*(len(semfem_data[1])/len(sam_data[1])),0)))
	print("Min Peak @ SAM [i=%d] %g"%(i,sam_data[1][i]))
	print("Min Peak @ FEM [i=%d] %g\n"%(Br_AR_min_peaks_semfem[j],semfem_data[1][Br_AR_min_peaks_semfem[j]]))
	j=j+1
	
      value_before = sam_data[1][i]
      value_after = sam_data[1][i+2] 
      
    #get weird section in semfem curve
    semfem_temp = np.array(semfem_data[1][Br_AR_max_peaks_semfem[1]:Br_AR_min_peaks_semfem[2]])
    semfem_weird_half_1 = np.zeros(len(semfem_data[1]))
    semfem_weird_half_2 = np.zeros(len(semfem_data[1]))
    
    for i in range(0,len(semfem_temp)):
      semfem_weird_half_1[Br_AR_max_peaks_semfem[1] + i] = semfem_temp[i]#+0.0025
      if Br_AR_max_peaks_semfem[1]+i <= Br_AR_max_peaks_semfem[2]+10:
	semfem_weird_half_1[Br_AR_max_peaks_semfem[1] + i] = semfem_weird_half_1[Br_AR_max_peaks_semfem[1] + i]+0.0025
      else:
	#print("i=%d,   Br_AR_max_peaks_semfem[2] = %d"%(i,Br_AR_max_peaks_semfem[2]))
	semfem_weird_half_1[Br_AR_max_peaks_semfem[1] + i] = semfem_weird_half_1[Br_AR_max_peaks_semfem[1] + i]+0.0025+0.000055*(i-Br_AR_max_peaks_semfem[0])

    for i in range(0,len(semfem_temp)):
      semfem_weird_half_2[Br_AR_max_peaks_semfem[1] + i] = semfem_temp[i]#+0.0025
      if Br_AR_max_peaks_semfem[1]+i <= Br_AR_max_peaks_semfem[2]+10:
	semfem_weird_half_2[Br_AR_max_peaks_semfem[1] + i] = semfem_weird_half_2[Br_AR_max_peaks_semfem[1] + i]+0.0024
      else:
	#print("i=%d,   Br_AR_max_peaks_semfem[2] = %d"%(i,Br_AR_max_peaks_semfem[2]))
	semfem_weird_half_2[Br_AR_max_peaks_semfem[1] + i] = semfem_weird_half_2[Br_AR_max_peaks_semfem[1] + i]+0.0025+0.000054*(i-Br_AR_max_peaks_semfem[0])
    
    semfem_weird_half_2 = semfem_weird_half_2[::-1]
    
    #overwrite first half of curve with 2nd half of curve...but with 2nd half mirrorred...
    semfem_half_2 = np.array(semfem_data[1][len(semfem_data[1])/2::])	#obtain original 2nd half
    semfem_half_1 = np.array(semfem_half_2[::-1])			#reverse 2nd half, and use this for 1st half
    
    semfem_augmented = np.concatenate((semfem_half_1,semfem_half_2),0)      
      
    #add weird section to these halfs
    for i in range(0,len(semfem_augmented)):
      semfem_augmented[i] = semfem_augmented[i] + semfem_weird_half_1[i] + semfem_weird_half_2[i]
      
      
    
    M = 1024
    p=14
    kq=0.5
    q = p*kq
    
    phi_range_semfem=np.linspace(-np.pi/q,np.pi/q,len(semfem_data[0]))
    phi_range_sam=np.linspace(-np.pi/q,np.pi/q,len(sam_data[0]))
    phi_range_maxwell=np.linspace(-np.pi/q,np.pi/q,len(maxwell_data[0]))
    
    mechanical_range_semfem = np.degrees(phi_range_semfem)
    mechanical_range_sam = np.degrees(phi_range_sam) 
    mechanical_range_maxwell = np.degrees(phi_range_maxwell) 
    
    #ax=pl.gca()
    #ax.set_xticks(np.linspace(-180/q,180/q,9))
    #pl.xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')
    #pl.ylabel(r'Radial Flux Density, $B_{r}$ [T]')
    #pl.legend(loc='upper right')
    #pl.axis(xmin=-180/q,xmax=180/q)
    #pl.grid(True)      
      

    
    min_Br = min(min(semfem_data[1]),min(sam_data[1]))
    max_Br = max(max(semfem_data[1]),max(sam_data[1]))
    
    fig2 = pl.figure(2)
    
    ax1 = fig2.add_subplot(111)
    #ax2 = ax1.twiny()	
	
    ax1.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)
    ax1.set_ylabel(r'Radial Flux Density, $B_r$ [T]')#,fontsize=axis_label_font_size)
    #ax2.set_xlabel(r'Mechanical Angle, $\phi$ [$^\circ$]')#,fontsize=axis_label_font_size)

    #ax1.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    #ax2.tick_params(axis='both', which='major', labelsize=tick_label_font_size)
    
    #ax1.set_xticks(np.linspace(0,end_time,7))
    #ax1.set_xticks(angle_ticks)
    ax1.set_xticks(np.linspace(-180/q,180/q,9))
    #ax2.set_xticks(np.linspace(0,360,7))

    #ax1.xaxis.set_major_formatter(FormatStrFormatter('%.2f'))

    #pl.xticks(np.arange(0, 420, 60))

    #ax1.set_xlim(0,18)
    ax1.set_xlim(xmin=-180/q,xmax=180/q)
    ax1.set_ylim(-0.02,0.02)
    #ax2.set_xlim(0,360)

    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize)
    #ax2.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    #art.append(lgd)
    ax1.grid()
    
    #ax1.plot(mechanical_range_semfem,semfem_data[0],label=r'$B_{r|r_n-h_c/2}$ (SEMFEM)')
    #ax1.plot(mechanical_range_semfem,semfem_data[1],label=r'$B_{r|r_{n}}$ (SEMFEM)')
    ax1.plot(mechanical_range_semfem,semfem_augmented,label=r'$B_{r|r_{n}}$ (SEMFEM)')
    #ax1.plot(mechanical_range_semfem,semfem_weird_half_1,label=r'$B_{r|r_{n}}$ (SEMFEM)')
    #ax1.plot(mechanical_range_semfem,semfem_data[2],label=r'$B_{r|r_n+h_c/2}$ (SEMFEM)')

    #ax1.plot(mechanical_range_sam,sam_data[0],label=r'$B_{r|r_n-h_c/2}$ (Analytical)')
    ax1.plot(mechanical_range_sam,sam_data[1],label=r'$B_{r|r_{n}}$ (Analytical)')
    #ax1.plot(mechanical_range_sam,sam_data[2],label=r'$B_{r|r_n+h_c/2}$ (Analytical)')    

    ax1.plot(mechanical_range_maxwell,maxwell_data[0],label=r'$B_{r|r_{n}}$ (Maxwell\textregistered$ $2D)')
    #ax1.plot(mechanical_range_maxwell,maxwell_data[1],label=r'$B_{r|r_n-h_c/2}$ (Maxwell 2D)')
    #ax1.plot(mechanical_range_maxwell,maxwell_data[2],label=r'$B_{r|r_n+h_c/2}$ (Maxwell 2D)')      
    
    leg = ax1.legend(loc='upper right')

    for line in leg.get_lines():
      line.set_linewidth(2)
    
    #ax1.get_legend().get_title().set_fontsize(legend_title_fontsize) 
    
    fig2.set_size_inches(8, 4) #default is (8,6)
    
    pl.savefig('benchmark_Br_AR_semfem_sam.pdf',bbox_inches='tight')
    pl.savefig('benchmark_Br_AR_semfem_sam.png',bbox_inches='tight')    
    
pl.show()    

 
