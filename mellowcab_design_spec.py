#!/usr/bin/python
#-*- coding: utf-8 -*-

#import all other necessary modules
from __future__ import division
from math import pi,degrees,sin,cos,sqrt
#import locale
import numpy as np
import pylab as pl
from prettytable import PrettyTable 

allow_wind = False

LINEWIDTH=1
font_size=14
pl.rc('text', usetex=True)
pl.rc('font', family='serif')
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

pretty_table = PrettyTable(['Output Variable','Value','[Unit]'])
pretty_table.align = 'l'
pretty_table.border= True  

max_degrees = 30
mellowcab_max_speed_kmph = 60 #max spoed vanaf Neil
motor_min_eff = 0.7
#gear_ratio = 12.25
gear_ratio = 17.2

gravitational_acc = 9.807
air_density = 1.2041
coeff_drag = 0.5
front_area = 1.2*1.7 #m^2 #dimensies vanaf Neil gekry
unloaded_mass = 250
passenger_mass = 250
battery_max_energy = 10 #kwh
total_mass = unloaded_mass + passenger_mass

wheel_diameter = 0.56 #18 duim profiel (is dit die rim se diameter, of die bande se diameter?)= 0.4572m
wheel_radius = wheel_diameter/2

mellowcab_max_speed_mps  = mellowcab_max_speed_kmph*(1000/(60*60))
motor_max_rpm = (mellowcab_max_speed_mps/wheel_radius)*(60/(2*pi))*gear_ratio


datapoints = 50
speed_range_kmph = np.linspace(0,mellowcab_max_speed_kmph,datapoints)
motor_speed_range_rpm = np.linspace(0,motor_max_rpm,datapoints)


if allow_wind:
  wind_max_speed_mps = 10
else:
  wind_max_speed_mps = 0

coeff_rolling_resistance_max = 0.01*(1+(3.6/100)*mellowcab_max_speed_mps)


def tick_function(X):
    V = 1/(1+X)
    return ["%.3f" % z for z in V]






FORCE_friction = np.zeros(len(speed_range_kmph))
FORCE_drag = np.zeros(len(speed_range_kmph))
FORCE_gravity = np.zeros(len(speed_range_kmph))
FORCE_to_maintain_const_speed = np.zeros(len(speed_range_kmph))

TORQUE_friction = np.zeros(len(speed_range_kmph))
TORQUE_drag = np.zeros(len(speed_range_kmph))
TORQUE_gravity = np.zeros(len(speed_range_kmph))
TORQUE_to_maintain_const_speed = np.zeros(len(speed_range_kmph))
POWER_delivered_kW = np.zeros(len(speed_range_kmph))

#= np.zeros(len(speed_range_kmph))


for v in range(0,len(speed_range_kmph)):
  mps  = speed_range_kmph[v]*(1000/(60*60))

  #coeff_rolling_resistance = 0.015
  coeff_rolling_resistance = 0.01*(1+(3.6/100)*mps)

  FORCE_friction[v] = total_mass*gravitational_acc*coeff_rolling_resistance
  FORCE_drag[v] = 0.5*air_density*coeff_drag*front_area*((mps+wind_max_speed_mps)**2)
  FORCE_gravity[v] = total_mass*gravitational_acc*sin(max_degrees*(pi/180))

  FORCE_to_maintain_const_speed[v] = FORCE_friction[v]+FORCE_drag[v]+FORCE_gravity[v]

  #Torque loads which the motor experiences
  TORQUE_friction[v] = (FORCE_friction[v]*wheel_radius)/gear_ratio
  TORQUE_drag[v]     = (FORCE_drag[v]*wheel_radius)/gear_ratio
  TORQUE_gravity[v]  = (FORCE_gravity[v]*wheel_radius)/gear_ratio
  
  TORQUE_to_maintain_const_speed[v] = TORQUE_friction[v] + TORQUE_drag[v] + TORQUE_gravity[v]

  POWER_delivered_kW[v] = (FORCE_to_maintain_const_speed[v]*mps)/1000

mellowcab_max_torque = FORCE_to_maintain_const_speed[v]*wheel_radius
mellowcab_max_wheel_rpm = (mellowcab_max_speed_mps/wheel_radius)*(60/(2*pi))

motor_max_rpm = mellowcab_max_wheel_rpm*gear_ratio
motor_max_torque = mellowcab_max_torque/gear_ratio

motor_max_power_output = (motor_max_torque*motor_max_rpm*((2*pi)/60))/1000



#range_at_max_speed = 


pretty_table.add_row(["Mellowcab_max_speed",mellowcab_max_speed_mps,"[m/s]"])
pretty_table.add_row(["COEFF_rolling_resistance_max",coeff_rolling_resistance_max,"[p.u.]"])
pretty_table.add_row(["COEFF_drag_resistance",coeff_drag,"[p.u.]"])
pretty_table.add_row(["FORCE_friction",FORCE_friction[v],"[N]"])
pretty_table.add_row(["FORCE_drag",FORCE_drag[v],"[N]"])
pretty_table.add_row(["FORCE_gravity",FORCE_gravity[v],"[N]"])
pretty_table.add_row(["FORCE_total",FORCE_to_maintain_const_speed[v],"[N]"])
pretty_table.add_row(["mellowcab_max_torque",mellowcab_max_torque,"[N.m]"])
pretty_table.add_row(["mellowcab_max_wheel_rpm",mellowcab_max_wheel_rpm,"[rpm]"])
pretty_table.add_row(["motor_torque",motor_max_torque,"[N.m]"])
pretty_table.add_row(["motor_max_wheel_rpm",motor_max_rpm,"[rpm]"])
pretty_table.add_row(["motor_power_output@max_speed",motor_max_power_output,"[kW]"])

print pretty_table

#plotting
colors = ['brown','b', 'c', 'y', 'm', 'r','khaki']

#text box with basic info
#props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
props = dict(boxstyle='square', facecolor='white', alpha=0.5)
#textstr = 'm^{2} = %f'%(coeff_drag)
textstr = 'Total mass [kg] = %.2f\nIncline angle [deg] = %.2f\nGear ratio = %.2f\nWheel diameter [m] = %.2f\nCoef. rolling resistance @ %d km/h = %.4f\nCoef. drag = %.2f\nFrontal area [m^{2}] = %.2f'%(total_mass,max_degrees,gear_ratio,wheel_diameter,mellowcab_max_speed_kmph,coeff_rolling_resistance_max,coeff_drag,front_area)
#textstr = '$\mu=%.2f$\n$\mathrm{median}=%.2f$\n$\sigma=%.2f$'%(mu, median, sigma)


##################
fig = pl.figure(1)
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
pl.title('Vehichle Forces vs Speed',fontsize=16,y=1.08)
line1 = ax1.plot(speed_range_kmph,FORCE_friction,linewidth=LINEWIDTH,label='Rolling Resistance [N]',c='b')
line2 = ax1.plot(speed_range_kmph,FORCE_drag,linewidth=LINEWIDTH,label='Aerodynamic Drag [N]',c='g')
#line3 = ax2.plot(motor_speed_range_rpm,FORCE_gravity,linewidth=LINEWIDTH,label='Gravity',c='r')
line3 = ax1.plot(speed_range_kmph,FORCE_gravity,linewidth=LINEWIDTH,label='Gravity [N]',c='r')
line4 = ax1.plot(speed_range_kmph,FORCE_to_maintain_const_speed,linewidth=LINEWIDTH,label='Total Force Load [N]',c='m')
line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=font_size)
ax1.set_ylabel(r'Force [N]',fontsize=font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=font_size)
ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

# added these three lines
lines = line1+line2+line3+line4+line5
labels = [l.get_label() for l in lines]
ax1.legend(lines, labels, loc='upper left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#ax2.grid()


##################
fig = pl.figure(2)
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
pl.title('Motor Torque Load vs Speed',fontsize=16,y=1.08)
line1 = ax1.plot(speed_range_kmph,TORQUE_friction,linewidth=LINEWIDTH,label='Rolling Resistance [N.m]',c='b')
line2 = ax1.plot(speed_range_kmph,TORQUE_drag,linewidth=LINEWIDTH,label='Aerodynamic Drag [N.m]',c='g')
line3 = ax2.plot(motor_speed_range_rpm,TORQUE_gravity,linewidth=LINEWIDTH,label='Gravity [N.m]',c='r')
line4 = ax1.plot(speed_range_kmph,TORQUE_to_maintain_const_speed,linewidth=LINEWIDTH,label='Total Torque Load [N.m]',c='m')
line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=font_size)
ax1.set_ylabel(r'Torque [N]',fontsize=font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=font_size)
ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)

# added these three lines
lines = line1+line2+line3+line4+line5
labels = [l.get_label() for l in lines]
ax1.legend(lines, labels, loc='upper left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#ax2.grid()

fig = pl.figure(3)
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()
ax3 = ax1.twinx()
pl.title('Traction Torque vs Speed',fontsize=16,y=1.08)
line1 = ax1.plot(speed_range_kmph,TORQUE_friction*gear_ratio,linewidth=LINEWIDTH,label='Rolling Resistance [N.m]',c='b')
line2 = ax1.plot(speed_range_kmph,TORQUE_drag*gear_ratio,linewidth=LINEWIDTH,label='Aerodynamic Drag [N.m]',c='g')
line3 = ax2.plot(motor_speed_range_rpm,TORQUE_gravity*gear_ratio,linewidth=LINEWIDTH,label='Gravity [N.m]',c='r')
line4 = ax1.plot(speed_range_kmph,TORQUE_to_maintain_const_speed*gear_ratio,linewidth=LINEWIDTH,label='Total Torque Load [N.m]',c='m')
line5 = ax3.plot(speed_range_kmph,POWER_delivered_kW,'b--',linewidth=LINEWIDTH,label='Traction Power [kW]')
ax1.set_xlabel(r'Vehicle Speed [km/h]',fontsize=font_size)
ax1.set_ylabel(r'Torque [N]',fontsize=font_size)
ax3.set_ylabel(r'Power [kW]',fontsize=font_size)
ax1.text(0.4, 0.985, textstr, transform=ax1.transAxes, fontsize=14, verticalalignment='top', bbox=props)
#ax1.yaxis.set_ticks(np.arange(0,max(TORQUE_to_maintain_const_speed*gear_ratio)),25)

## added these three lines
lines = line1+line2+line3+line4+line5
labels = [l.get_label() for l in lines]
ax1.legend(lines, labels, loc='upper left')
ax2.set_xlabel(r'Motor Speed [rpm]',fontsize=font_size)
ax2.set_xlim(0,motor_max_rpm)
ax1.grid()
#ax2.grid()

pl.show()