#!/usr/bin/python
#"The line above is needed because this script requires the python shell to execute the following command lines."

#Title	: Simulation of a Double-rotor Ironless Radial Flux Permanent Magnet Machine
#Author : Andreas Joss

#os.system("fpl2 project_idrfpm_semfem/drawing.poly")
#os.system("fpl2 project_idrfpm_semfem/drawing.fpl")
#os.system("fpl2 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl")
#os.system("post.py project_idrfpm_semfem/results/post.res")

from __future__ import division				#ensures division always returns the correct answer (eg. 1/2 = 0.5 instead of 1/2 = 0)

from semfem import *
import pylab as pl
#from SEMFEM_plotter import *
from prettytable import PrettyTable
import time
#pyplot is just a wrapper module to provide a Matlab-style interface to matplotlib. 
#Many plotting functions in Matlab are provided by pyplot with the same names and arguments. 
#This will ease the process of moving from Matlab to Python for scientific computation.
#pylab is basically a mode in which pyplot and numpy are imported in a single namespace, 
#thus making the Python working environment very similar to Matlab.

from numpy import * # * imports all the functions contained within the numpy library
		  # this numpy library is necessary to use the loadtxt() function
import os, sys, csv
import sqlite3
import datetime
import argparse

#input_file = 'input_original.csv' #will always remain the true original input parameters
input_file = 'input_original-gert.csv'
#input_file = 'input_original-copy.csv'

draw_full_machine = 0
locked_rotor_test = 0
plot_all=0 #semfem post plots
save_fpl2pdf=0
save_csv=0
rotate_offset=0
debug=0
show_all=0
air0_iron1_yokes=0
iron_yoke_height_mm=4.5
enable_tangential_magnets=1
enable_inner_magnets=1

#CONSTANTS
READ_INPUT = 1
MANUAL_INPUT = 2

MAGNETS_ONLY = 1
WINDINGS_ONLY = 2
ALL_ACTIVE = 3

input_method = READ_INPUT
use_sql = True 			#variable used to rather use sqlite database than text file for input source


pretty_terminal_table = PrettyTable(['SEMFEM Output Variable','Value','Unit'])
pretty_terminal_table.align = 'l'
pretty_terminal_table.border= True  
has_opened = False
enable_variable_test = False

enable_shell_mass = 1
rotor_shell_height=10.0/1000
stator_shell_height=10.0/1000
tufnol_density=1360 #kg/cm^3

yoke_iron = 250

parser = argparse.ArgumentParser(description="IDRFPM semfem simulation script",formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-pk','--primary_key', dest='primary_key', type=int, default=12, help='Specify the primary_key of the input row of the database that should be used')
parser.add_argument('-db','--database', dest='database', default='IDRFPM.db', help='Specify the database name which should be used')
parser.add_argument('-tb','--table', dest='table', default='', help='Specify the table extension name from which inputs should be read') #for eg. the extension could be '_rpm_050'
parser.add_argument('-pl','--plot', dest='plot', type=int, default=0, help='Reprint and output graphs of previous simulation')

args = parser.parse_args()

def addToTable (variable_string_name, value, unit_string, enable_disable, dir_parent):
  if enable_disable == True:
    pretty_terminal_table.add_row([variable_string_name,value,unit_string])
    variable_string_name = variable_string_name.replace(" ", "_")
    with open(dir_parent+'script_results/idrfpm/output.csv',"a") as output_dat:
      output_dat.write('%s,%f\n'%(variable_string_name,value))

primary_key = args.primary_key
db_name = args.database
input_table_name = "machine_input_parameters"+args.table
output_table_name = "machine_semfem_output"+args.table

store_results_in_database = 1
overwrite_previous_result = 1
database_verbosity = 0

conn = sqlite3.connect(db_name)
c = conn.cursor()

def db_input_select(table,column,primary_key):
  string = "SELECT " +column+ " FROM " +table+ " WHERE primary_key=%d"%(primary_key)
  c.execute(string)
  return c.fetchone()[0]

def db_output_insert(table,column,primary_key,value):
  global already_commited_something_now
  if already_commited_something_now==0:
    try:
      c.execute("INSERT INTO "+table+"(primary_key,"+column+") "+ "VALUES (?,?)",(primary_key,value))
      already_commited_something_now = 1
      if database_verbosity == 1: print "created new row and added some value to a column"
    except:
      #pass
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
  else:
    try:
      c.execute("UPDATE "+table+" SET "+column+" = ? WHERE primary_key=?",(value,primary_key))
      if database_verbosity == 1: print "added new value to column of existing row"
    except:
      pass

#flags
result_already_exists = 1
already_commited_something_now = 0
deleted_row_entry = 0

#check if a result is already stored for this primary_key
if store_results_in_database == 1:
  #try:
  if database_verbosity == 1: print "trying to select"
  c.execute("SELECT magnet_mass FROM machine_semfem_output WHERE primary_key = ?", (primary_key,))
  data=c.fetchone()

  if database_verbosity == 1:
    if data is None:
        print('There is no primary_key named %s'%primary_key)
    else:
        print('primary_key %s found with magnet_mass %s'%(primary_key,data[0]))

  if data is None:
    if database_verbosity == 1: print "No entry found"
    result_already_exists = 0
  else:
    if database_verbosity == 1: print "Entry found"
    result_already_exists = 1

#delete entry if it exists and if we are to overwrite any previous results
if overwrite_previous_result == 1 and result_already_exists == 1 and store_results_in_database == 1:
  #conn.commit()
  c.execute("DELETE FROM machine_semfem_output WHERE primary_key=?",(primary_key,))
  #conn.commit()
  deleted_row_entry = 1
  if database_verbosity == 1: print "deleted previous entry"

	
#def main (*args):   
#if len(args) > 1:
  #number_of_threads = int(args[1])
  
  #if number_of_threads > 1:
    #dir_parent = './mp/p%s/'%args[1]
  #else:
    #dir_parent = ''
#else:
  #number_of_threads = 1
  #dir_parent = ''
  ##dir_parent = './mp/p%s/'%args[1]

#if __name__ != '__main__':
  #dir_parent = './mp/p%s/'%args[1]
  #number_of_threads = int(args[1])
#else:
dir_parent = ''
number_of_threads = 1
dir_temp = dir_parent+'script_results/idrfpm/'
if not os.path.isdir(dir_temp): os.makedirs(dir_temp)
dir_temp = dir_parent+'script_results/main/'
if not os.path.isdir(dir_temp): os.makedirs(dir_temp)    
  
#temp fix
#dir_parent = './mp/p%s/'%args[1]
#number_of_threads = int(args[1])

print("writing to "+dir_parent+"script_results/idrfpm/output.csv")

with open(dir_parent+'script_results/idrfpm/output.csv', 'w') as output_dat:
  output_dat.write('variable,value\n')   

checkpoint = []
checkpoint.append(time.time())
##################
# Script options #
##################

#input_data = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(1),unpack=True)
input_data = genfromtxt(input_file,skip_header=1,delimiter=',',usecols=(1),unpack=True)
input_data_original = genfromtxt(input_file,skip_header=1,delimiter=',',usecols=(1),unpack=True)

#print(args[0])

if __name__ != '__main__':
  VERBOSITY=0		#VERBOSITY setting when this script is invoked by an other script
  display_drawings=0
  #display_drawings=1
  enable_litz_wire_selection = int(input_data[48])
  enable_voltage_leeway = int(input_data[49])
  enable_manual_turnsPerCoil_limit = int(input_data[43])
else:
  VERBOSITY=1		#VERBOSITY setting when this script is executed by itself
  #VERBOSITY=0
  display_drawings=0

  #enable_litz_wire_selection = 1
  #enable_voltage_leeway = 0
  #enable_manual_turnsPerCoil_limit = 1   
  
  enable_voltage_leeway = int(input_data[49])
  enable_manual_turnsPerCoil_limit = int(input_data[43])
  enable_litz_wire_selection = int(input_data[48])

#simulation options
enable_smc_cored_coils = int(input_data[23])
enable_sub_air_gaps = int(input_data[24])
enable_general_calc = int(input_data[25])
enable_eddy_losses_calc = int(input_data[26])
enable_flux_density_harmonic_calc = int(input_data[27])



#output options
#enable_single_csv_output = int(input_data[28])
enable_terminal_output = int(input_data[29]) 
enable_optimization_csv_output = int(input_data[30])
enable_show_iteration_number = int(input_data[44])
enable_show_inputs = int(input_data[45])
enable_current_density = int(input_data[46])

###########################
# Read and Update iteration number #
###########################
if os.path.isfile(dir_parent+'script_results/main/running_flags.csv'):
  run_flags = genfromtxt(dir_parent+'script_results/main/running_flags.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
  iteration_number = int(run_flags)
else:
  iteration_number=0

#################################
# Read dimensions from csv file #
#################################
c110000_resistivity_20 = 1.72e-8
if input_method == READ_INPUT:
  if use_sql:
    print("Using the sqlite3 database, instead of the archaic input text file.")
    
    poles = db_input_select(input_table_name,"poles",primary_key)
    slots = db_input_select(input_table_name,"slots",primary_key)
    J = db_input_select(input_table_name,"current_density_rms",primary_key)*np.sqrt(2)
    system_voltage = db_input_select(input_table_name,"system_voltage",primary_key)
    ro = db_input_select(input_table_name,"outer_radius_in_mm",primary_key)/1000
    hc  = db_input_select(input_table_name,"coil_height_in_mm",primary_key)/1000
    hmi_r = db_input_select(input_table_name,"inner_magnet_height_in_mm",primary_key)/1000
    hmo_r = db_input_select(input_table_name,"outer_magnet_height_in_mm",primary_key)/1000
    km = db_input_select(input_table_name,"radially_magnetized_PM_width_ratio",primary_key)
    kc  = db_input_select(input_table_name,"coil_width_ratio",primary_key)
    kq  = db_input_select(input_table_name,"coils_per_phase_vs_rotor_pole_pair_ratio",primary_key)
    l = db_input_select(input_table_name,"stacklength_in_mm",primary_key)/1000
    g = db_input_select(input_table_name,"airgap_in_mm",primary_key)/1000
    fill_factor = db_input_select(input_table_name,"fill_factor",primary_key)
    n_rpm = db_input_select(input_table_name,"n_rpm",primary_key)
    Brem= db_input_select(input_table_name,"B_rem",primary_key)						#remnant flux density of NdBFe N48 PMs        [T]
    force_this_number_of_turns = db_input_select(input_table_name,"force_this_number_of_turns",primary_key)
    coil_temperature = db_input_select(input_table_name,"coil_temperature",primary_key)
    initial_electrical_angle = db_input_select(input_table_name,"initial_position_defined_by_electrical_angle_deg",primary_key)
    shift_current_electrical_angle = db_input_select(input_table_name,"shift_current_by_electrical_angle_deg",primary_key)
    inverter_squarewave0_sinusoidalPWM1 = db_input_select(input_table_name,"inverter_squarewave0_sinusoidalPWM1",primary_key)
    copper_losses_allowed = db_input_select(input_table_name,"copper_losses",primary_key)
    current_angle_degrees = db_input_select(input_table_name,"current_angle_degrees",primary_key)
    input_is_Jrms0_Pcu1_slotcurrent2 = db_input_select(input_table_name,"input_is_Jrms0_Pcu1_slotcurrent2",primary_key)
    number_of_parallel_circuits = db_input_select(input_table_name,"number_of_parallel_circuits",primary_key)
    three_phase_delta0_wye1 = db_input_select(input_table_name,"three_phase_delta0_wye1",primary_key)
    use_parallel_strands = db_input_select(input_table_name,"use_parallel_strands",primary_key)
    enable_eddy_losses_calc = db_input_select(input_table_name,"enable_eddy_losses",primary_key)
    current_through_slot = db_input_select(input_table_name,"current_through_slot_rms",primary_key)
    target_mech_power_kW = db_input_select(input_table_name,"target_mech_power_kW",primary_key)

    #enable_target_mech_power determines if the script should try to operate machine with a constant power output. It should be disabled when Ls and flm are to be discovered.
    enable_target_mech_power = db_input_select(input_table_name,"enable_target_mech_power",primary_key)

    #should only be enabled when the adequate number of turns should be determined at top speed, and thereafter the turns should be kept constant
    turns_calculation_for_rpm = db_input_select(input_table_name,"turns_calculation_for_rpm",primary_key)
    enable_turns_calculation = db_input_select(input_table_name,"enable_turns_calculation",primary_key)
    
    mesh_scale = db_input_select(input_table_name,"mesh_scale",primary_key)
    steps = db_input_select(input_table_name,"steps",primary_key)
    
    peak_voltage_allowed = system_voltage
    p = int(poles/2)
    kmo = km
    kmi = km
    muR = 1.05 #recoil permeability of the PMs
    active_parts=3
    f_e = (n_rpm*(poles))/120
    w_e = 2*pi*f_e
    copper_resistivity = c110000_resistivity_20*(1+0.0039*(coil_temperature-20)) #copper resistivity is given in [Ohm.m]
    wire_resistivity = copper_resistivity
    T=1/f_e
    t=0*T/12
    gamma=0
    a=number_of_parallel_circuits
    omega_m=n_rpm*pi/30
    omega_m_turns = turns_calculation_for_rpm*np.pi/30
    N=1
    Ls_k = 2.225
    turnsPerCoil=force_this_number_of_turns
    gi=g
    go=g
    ri = ro-hmo_r-g-hc-g-hmi_r
    parallel_stranded_conductors=70
    wire_diameter=0.0002

  else:
    p		= input_data[0]							#pole pairs
    kq		= input_data[1]							#coils per pole pair per phase                           [m]
    l		= input_data[5] 						#axial rotor/stator length                    [m]
    g		= input_data[6] 						#each airgap length
    hc		= input_data[7] 						#coil/winding thickness                       [m]
    kmo		= input_data[8] 						#radial outer PM width / pole pitch
    kmi		= input_data[62] 						#radial inner PM width / pole pitch
    Brem	= input_data[9] 						#remnant flux density of NdBFe N48 PMs        [T]
    muR		= input_data[10] 						#recoil permeability of the PMs

    #f_e		= input_data[11]						#electrical excitation frequency	      [Hz]

    active_parts= int(input_data[19])
    turnsPerCoil= int(input_data[20])
    fill_factor = input_data[21]						#according to Gert's calculations, but we are not sure what value Maxwell actually used (2015/05/27)
    
    J		= input_data[31]*sqrt(2)					#input current density [A/mm^2] (RMS value is given by input file, thus multiply by sqrt(2)
    kc		= input_data[32]						#coil winding ratio range(0-1)
    hmo_r	= input_data[36]						#outer magnet width
    hmi_r	= input_data[37]						#inner magnet width
    ri		= input_data[38]						#most inner radius point on machine
    ro		= input_data[39]						#most outer radius point on machine

    
    go		= input_data[53]
    gi		= input_data[54]
    x_ri	= input_data[55]
    x_hmo_r	= input_data[56]
    #x_go	= input_data[57]
    x_hc	= input_data[58]
    #x_gi	= input_data[59]
    x_hmi_r	= input_data[60]
    n_rpm	= input_data[61]
    f_e		= (n_rpm*p*2)/120						#f_e is really the flux distribution frequency caused by the rotor poles, meaning it is already the second harmonic of the stator current distribution
										#p is number of pole PAIRS
										#this means the frequency of the current injected into the machine is f_e/2
  
  if enable_eddy_losses_calc == 1:
    print "Note that f_e = 70Hz for eddy current calculations. And that source current is zero because we want to simulate open circuit conditions."
    #f_e = 70
    n_rpm = 300
    #n_rpm = 100
    f_e = (n_rpm*p*2)/120
    J = 0
  
  x_hmo_rt	= input_data[63]
  x_hmi_rt	= input_data[64]
  
  if use_sql == False:
    variable_no = input_data[52]
    perturb_count = zeros(int(variable_no))
    mesh_scale = input_data[47]

if enable_inner_magnets == 0:
  hmi_r = 0
  gi = 0

if enable_show_inputs == 1:
  #extra_input_info = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)
  #ri_min  =  extra_input_info[1][38]
  #hmo_r_min =  extra_input_info[1][36]
  #go_min  =  extra_input_info[1][53]
  #hc_min  =  extra_input_info[1][7]
  #gi_min  =  extra_input_info[1][54]
  #hmi_r_min =  extra_input_info[1][37]


  ###DEBUGGING
  if enable_variable_test == True:
    if __name__ != '__main__':	#when this script is invoked by an other script
      print("hallo daar")
      #these variable values are decided by main.py
      g	= input_data[6] 						#each airgap length
      hc	= input_data[7] 						#magnet thickness   
      hmo_r	= input_data[36]						#outer magnet width
      hmi_r	= input_data[37]						#inner magnet width
      ri	= input_data[38]						#most inner radius point on machine
    else:				#when this script is executed by itself

      ri  = (ro - ri_min - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_ri  + ri_min
      hmo_r = (ro - ri     - hmo_r_min - go_min - hc_min - gi_min - hmi_r_min)*x_hmo_r + hmo_r_min
      go  = (ro - ri     - hmo_r     - go_min - hc_min - gi_min - hmi_r_min)*x_go  + go_min
      hc  = (ro - ri     - hmo_r     - go     - hc_min - gi_min - hmi_r_min)*x_hc  + hc_min
      gi  = (ro - ri     - hmo_r     - go     - hc     - gi_min - hmi_r_min)*x_gi  + gi_min
      hmi_r = (ro - ri     - hmo_r     - go     - hc     - gi     - hmi_r_min)*x_hmi_r + hmi_r_min
      
      print go
      
  ###
  
  if active_parts == WINDINGS_ONLY or active_parts == ALL_ACTIVE:
    #id 	= input_data[12]
    #iq 	= input_data[13]
    #these current variables will be determined by current density J
    id = 0
    iq = 0
  else:
    id = 0
    iq = 0       
  
  if use_sql == False:
    steps 		 = int(input_data[22])
    coil_temperature	 = input_data[34]
    peak_voltage_allowed = (input_data[33]*0.95*1.15)/sqrt(3)			#The input file refers to a peak Line-to-Line (LL) voltage, 0.95 takes into account voltage source drop, 1.15 takes into account THD of the sinusoidal voltage source
  
  
  if enable_optimization_csv_output == 1 and iteration_number == 0:
    with open(dir_parent+'script_results/idrfpm/optimization_output.csv', 'w') as opt_output_file:
      opt_output_file.write('p,kq,l,g,hmi_r,hc,hmo_r,kmo,kc,ri,ro,number_of_layers_eddy,wire_diameter,parallel_stranded_conductors,turnsPerCoil,fill_factor,J,max_input_phase_current,max_induced_phase_voltage,max_input_phase_voltage,average_torque_dq,magnet_mass,copper_mass,Ra_analytical,efficiency,S,Q,P,P_out,P_conductive_analytical,P_eddy,P_wf,P_core_loss,Total_losses,power_factor,coil_side_area,max_Br_1,Br1,Br_THD*100\n')

  if use_sql == False:
    number_of_layers_eddy        = int(input_data[14])
    max_harmonics_eddy           = int(input_data[15])
    wire_diameter                = input_data[16]
    parallel_stranded_conductors = int(input_data[17])
    wire_resistivity             = input_data[18]


#############################
# Enter dimensions manually #
#############################
elif input_method == MANUAL_INPUT:
  p = 14						#pole pairs
  kq = 0.5						#coils per pole pair per phase
  rn=120E-3 						#nominal radius                               [m]
  hy= 50E-3 						#dummy air cored yoke thickness               [m]
  hm=  6E-3 						#magnet thickness                             [m]
  l=  40E-3 						#axial rotor/stator length                    [m]
  g=   1E-3 						#each airgap length
  hc=   9E-3 						#coil/winding thickness                       [m]

  km=   0.5 						#radial PM width / pole pitch (describes the radial facing PM's width as a fraction of the width of the axial facing PMs)
  Brem= 1.4 						#remnant flux density of NdBFe N48 PMs        [T]
  muR= 1.05 						#recoil permeability of the PMs

  f_e = 67.667
  

  turnsPerCoil = 22
  fill_factor = 0.5 					#according to Gert's calculations, but we are not sure what value Maxwell actually used (2015/05/27)
  active_parts == ALL_ACTIVE

  if active_parts == WINDINGS_ONLY or active_parts == ALL_ACTIVE:
    id = 0
    iq = 20
  else:
    id = 0
    iq = 0

  if active_parts == MAGNETS_ONLY:
    steps = 1
  else:
    steps = int(input_data[22])   
  
  if enable_eddy_losses_calc == 1:
    number_of_layers_eddy        = 5
    max_harmonics_eddy           = 15
    wire_diameter                = 0.0016
    parallel_stranded_conductors = 1
    wire_resistivity             = c110000_resistivity_20
  
  #simulation options
  enable_smc_cored_coils = 0
  enable_sub_air_gaps = 1
  enable_general_calc = 0
  enable_eddy_losses_calc = 0
  enable_flux_density_harmonic_calc = 0

  #output options
  enable_csv_output = 1
  enable_terminal_output = 0

if active_parts == WINDINGS_ONLY:
  Brem = 0
  Br = 0

#############################################
# Calculate remaining dimensions and ratios #
#############################################

#x_hmo_rt --> gives ratio between hight of outer radially magnetized PMs compared to outer tangentially magnetized PMs
hmo_t = x_hmo_rt*hmo_r
#x_hmi_rt --> gives ratio between hight of inner radially magnetized PMs compared to inner tangentially magnetized PMs
hmi_t = x_hmi_rt*hmi_r


#convert these parameters to my script's parameters
magnetTotal = 4*p
innerMagnetRadiusA = ri
innerMagnetRadiusB = ri + hmi_r
outerMagnetRadiusA = ri + hmi_r + gi + hc + go
outerMagnetRadiusB = ro

CoilRadiusA 	= ri + hmi_r + gi
CoilRadiusB 	= ri + hmi_r + gi + hc


innerBlockRadius = CoilRadiusA				#assuming the coil resin has no thickness
outerBlockRadius = CoilRadiusB				#assuming the coil resin has no thickness
coilBlockWidthRadial = outerBlockRadius - innerBlockRadius
stack_length = l
Br = Brem
mur = muR
coilsTotal = int(kq * 3 * p)


magnetPitch = (2*pi)/magnetTotal
axial_outer_PM_pitch = (1-kmo)*2*magnetPitch
axial_inner_PM_pitch = (1-kmi)*2*magnetPitch
radial_outer_PM_pitch = kmo*2*magnetPitch
radial_inner_PM_pitch = kmi*2*magnetPitch
angle_inc = 0.5*magnetPitch

innerYoke = 0.9*ri
outerYoke = 1.1*ro

magnetPitch = (2*pi)/magnetTotal
poles = int(magnetTotal/2)					#number of poles in rotor1, and number of poles in rotor2

if draw_full_machine == 0:
  mechRotation = (4*pi)/poles					#only rotate one electrical full period (amount of radians the machine should mechanically rotate) ##mechRotation = 2*pi * (2/poles)
else:
  mechRotation = 2*pi

if rotate_offset == 1:
  rotate_all_radians = -0.5*mechRotation
else:
  rotate_all_radians = 0

#if draw_full_machine == 0:
rotor_symmetry_factor = int(4*magnetTotal/poles)
#else:
  
symmetry_angle = (magnetTotal/(poles/4))*magnetPitch
rotorDAxisOffset = 1.5*magnetPitch				#this angle is used to move rotor d-axis inline with the x-axis (assuming the transformation angle = 0 because we assume the phase-A axis also to be there)
coilBlockPitch = (2*pi)/coilsTotal
meanBlockRadius = innerBlockRadius + 0.5*coilBlockWidthRadial


coil_side_radians = 0.5*kc*coilBlockPitch
blockAngle = (1-kc)*coilBlockPitch
coil_side_area = (CoilRadiusB**2 - CoilRadiusA**2)*(coil_side_radians/2)	#[m^2]
single_turn_area = parallel_stranded_conductors*pi*((wire_diameter/2)**2)

#print("J = %f"%(J))
current_through_slot = J*(1000**2)*coil_side_area*fill_factor

#if enable_manual_turnsPerCoil_limit == 1:
  #turnsPerCoil = int(input_data[20])
  #iq = current_through_slot/turnsPerCoil
  
#else:# enable_current_density == True:
  ##current_through_slot = J*(1000**2)*coil_side_area*fill_factor			#[A]
  #turnsPerCoil = 1
  #iq = current_through_slot/turnsPerCoil					#[A]
##else:
  ##current_through_slot = iq*turnsPerCoil

windingRadians = coil_side_radians

N_L = 2							#Number of layer stack windings (if N_L == 1, then an error is given)
N_S = 1							#Overlap number. This is a non-overlapping coil structure, thus N_S = 1
phase = [1,-1,3,-3,2,-2]

end_winding_radius = ((blockAngle+windingRadians)*meanBlockRadius)/2

#Air-Gaps
innerGap = innerBlockRadius - innerMagnetRadiusB
outerGap = outerMagnetRadiusA - outerBlockRadius

mesh_multiplier1 = (ro-ri)/(0.131-0.109)
mesh_multiplier2 = 14/p

#mesh_multiplier1 = (input_data_original[39]-input_data_original[38])/(ro-ri)
#mesh_multiplier2 = input_data_original[0]/p

#print "NEW MACHINE"
#print (mesh_multiplier1)
#print (mesh_multiplier2)

#mesh_scale = int(mesh_scale*mesh_multiplier1*mesh_multiplier2)

#if mesh_scale <= 1: mesh_scale=1

mesh_scale = mesh_scale*mesh_multiplier1*mesh_multiplier2
#print (mesh_scale)
if mesh_scale <1: mesh_scale=1

#Mesh size settings
#magnet_mesh = 2e-3
magnet_mesh = 1e-3*mesh_scale
#gmesh = 2e-3
gmesh = 1e-3*mesh_scale	#temporary value for csv Br exportation

#Arc length settings
arc_len = 5e-4*mesh_scale
gap_len = 0.5e-3*mesh_scale					#Stiaan added this parameter
boundary_len = 1e-3*mesh_scale

#temporary, trying to get nice magnetic vector potential plot on outskirts of machine
gap_len = arc_len
boundary_len = arc_len
outskirts_mesh = 5*gmesh #was this previously
#outskirts_mesh = gmesh

#if enable_show_inputs == 1:
  #pretty_input_table = PrettyTable(['SEMFEM Input Variable','Perturb Count','Minimum Value','Maximum Value','Initial Value','Present Value','Percentage Change','Unit'])
  #pretty_input_table.align = 'l'
  #pretty_input_table.border= True
  ##extra_input_info = genfromtxt(args[0],skip_header=1,delimiter=',',usecols=(2,3),unpack=True)

  #if iteration_number == 0 or __name__ == '__main__':
    #for i in range(0,size(perturb_count)):
      #perturb_count[i] = 0
  #else:
    #perturb_count = genfromtxt(dir_parent+'script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)
    ##perturb_count = np.loadtxt('./script_results/main/perturb_count.csv',skip_header=1,usecols=(1),delimiter=",",unpack=True)

  #pretty_input_table.add_row(["ABSOLUTE VALUES","","","","","","",""])
  #pretty_input_table.add_row(["ro",0,extra_input_info[1][39],extra_input_info[0][39],input_data_original[39],ro,"%.3f"%(((ro-input_data_original[39])/input_data_original[39])*100),"[m]"])
  #pretty_input_table.add_row(["ri",int(perturb_count[0]),extra_input_info[1][38],extra_input_info[0][38],input_data_original[38],ri,"%.3f"%(((ri-input_data_original[38])/input_data_original[38])*100),"[m]"])
  #pretty_input_table.add_row(["hmo_r",int(perturb_count[1]),extra_input_info[1][36],extra_input_info[0][36],input_data_original[36],hmo_r,"%.3f"%(((hmo_r-input_data_original[36])/input_data_original[36])*100),"[m]"])
  #pretty_input_table.add_row(["hc",int(perturb_count[2]),extra_input_info[1][7],extra_input_info[0][7],input_data_original[7],hc,"%.3f"%(((hc-input_data_original[7])/input_data_original[7])*100),"[m]"])
  #pretty_input_table.add_row(["hmi_r",int(perturb_count[3]),extra_input_info[1][37],extra_input_info[0][37],input_data_original[37],hmi_r,"%.3f"%(((hmi_r-input_data_original[37])/input_data_original[37])*100),"[m]"])    
  #pretty_input_table.add_row(["g",0,extra_input_info[1][6],extra_input_info[0][6],input_data_original[6],g,"%.3f"%(((g-input_data_original[6])/input_data_original[6])*100),"[m]"])    
  #pretty_input_table.add_row(["l",0,extra_input_info[1][5],extra_input_info[0][5],input_data_original[5],l,"%.3f"%(((l-input_data_original[5])/input_data_original[5])*100),"[m]"])        
  #pretty_input_table.add_row(["kmo",int(perturb_count[4]),extra_input_info[1][8],extra_input_info[0][8],input_data_original[8],kmo,"%.3f"%(((kmo-input_data_original[8])/input_data_original[8])*100),"[p.u.]"])
  #pretty_input_table.add_row(["kc",int(perturb_count[5]),extra_input_info[1][32],extra_input_info[0][32],input_data_original[32],kc,"%.3f"%(((kc-input_data_original[32])/input_data_original[32])*100),"[p.u.]"])
  #pretty_input_table.add_row(["p",int(perturb_count[6]),extra_input_info[1][0],extra_input_info[0][0],input_data_original[0],p,"%.3f"%(((p-input_data_original[0])/input_data_original[0])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["kmi",int(perturb_count[7]),extra_input_info[1][62],extra_input_info[0][62],input_data_original[62],kmi,"%.3f"%(((kmi-input_data_original[62])/input_data_original[62])*100),"[p.u.]"])     
  ##pretty_input_table.add_row(["go",int(perturb_count[10]),extra_input_info[1][53],extra_input_info[0][53],input_data_original[53],go,"%.3f"%(((go-input_data_original[53])/input_data_original[53])*100),"[m]"])
  ##pretty_input_table.add_row(["gi",int(perturb_count[11]),extra_input_info[1][54],extra_input_info[0][54],input_data_original[54],gi,"%.3f"%(((gi-input_data_original[54])/input_data_original[54])*100),"[m]"])
  
  #pretty_input_table.add_row(["RELATIVE VALUES","","","","","","",""])
  #pretty_input_table.add_row(["x_ri" ,int(perturb_count[0]),extra_input_info[1][55],extra_input_info[0][55],input_data_original[55],x_ri,"%.3f"%(((x_ri-input_data_original[55])/input_data_original[55])*100),"[p.u.]"])
  #pretty_input_table.add_row(["x_hmo_r",int(perturb_count[1]),extra_input_info[1][56],extra_input_info[0][56],input_data_original[56],x_hmo_r,"%.3f"%(((x_hmo_r-input_data_original[56])/input_data_original[56])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_go" ,int(perturb_count[10]),extra_input_info[1][57],extra_input_info[0][57],input_data_original[57],x_go,"%.3f"%(((x_go-input_data_original[57])/input_data_original[57])*100),"[p.u.]"])
  #pretty_input_table.add_row(["x_hc" ,int(perturb_count[2]),extra_input_info[1][58],extra_input_info[0][58],input_data_original[58],x_hc,"%.3f"%(((x_hc-input_data_original[58])/input_data_original[58])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_gi" ,int(perturb_count[11]),extra_input_info[1][59],extra_input_info[0][59],input_data_original[59],x_gi,"%.3f"%(((x_gi-input_data_original[59])/input_data_original[59])*100),"[p.u.]"])
  #pretty_input_table.add_row(["x_hmi_r" ,int(perturb_count[3]),extra_input_info[1][60],extra_input_info[0][60],input_data_original[60],x_hmi_r,"%.3f"%(((x_hmi_r-input_data_original[60])/input_data_original[60])*100),"[p.u.]"])
  ##pretty_input_table.add_row(["x_hmo_rt",int(perturb_count[8]),extra_input_info[1][63],extra_input_info[0][63],input_data_original[63],x_hmo_rt,"%.3f"%(x_hmo_rt*100),"[p.u.]"]) 
  ##pretty_input_table.add_row(["x_hmi_rt",int(perturb_count[9]),extra_input_info[1][64],extra_input_info[0][64],input_data_original[64],x_hmi_rt,"%.3f"%(x_hmi_rt*100),"[p.u.]"])
  
  #save_stdout = sys.stdout
  #sys.stdout = open(dir_parent+"pretty_input_table.txt", "wb")
  #print pretty_input_table
  #sys.stdout = save_stdout    

  #if enable_terminal_output == 1:
    #print pretty_input_table

#########################
# SEMFEM Setup settings #
#########################

sf = SEMFEM()	#initialize object

sf.semfem_begin(dir_parent+"project_idrfpm_semfem")
sf.semfem_set_problem_steps(steps)

#air gaps between inner rotor and coil, and outer rotor and coil
if enable_inner_magnets:
  sf.semfem_set_air_gap_method(2)			#band solver (was used) (2 denotes 2 airgaps!!)
else:
  sf.semfem_set_air_gap_method(1)
  
sf.semfem_set_problem_type(sf.ROTATING)
#sf.semfem_current_input_method = sf.PHASE_CURRENT
sf.semfem_current_input_method = sf.PHASE_CURRENT_DENSITY
sf.semfem_set_stack_length(stack_length)
sf.semfem_set_bh_curve("/usr/share/SEMFEM/data/Laminations/Magnet_M-19_29_Ga.bh")	#apparantly this is the default magnet BH curve data
sf.semfem_set_verbosity(VERBOSITY)			#no print-outs

sf.semfem_set_output(2)	#will generate all the fpl files (for all the steps) #1-- would generate only the fpl file for the first step #0-- would generate 0 fpl files

####################
# Drawing commands #
####################
print "Awe my larney, active_parts = %d"%(active_parts)
if enable_inner_magnets:
  # Inner magnets
  ###############
  if draw_full_machine == 1:
    inner_magnet_arc_len_A = arc_len*0.5
    inner_magnet_arc_len_B = arc_len*0.5 
  elif active_parts != MAGNETS_ONLY and steps != 1:
    inner_magnet_arc_len_A = (innerMagnetRadiusA*mechRotation)/(steps-1)
    inner_magnet_arc_len_B = (innerMagnetRadiusB*mechRotation)/(steps-1)
  else:
    inner_magnet_arc_len_A = arc_len
    inner_magnet_arc_len_B = arc_len

  #print "whole_number calc"
  #print(innerMagnetRadiusB)
  #print(innerMagnetRadiusA)
  #print(boundary_len)
  #print((innerMagnetRadiusB-innerMagnetRadiusA)/boundary_len)


  
  pitstop = innerMagnetRadiusA+((1-x_hmi_rt)*hmi_r)
  whole2 = (pitstop-innerMagnetRadiusA)/magnet_mesh
  if whole2 < 1:
    whole2 = 1
  else:
    whole2 = int(round(whole2))
  
  #whole_number = int((innerMagnetRadiusB-innerMagnetRadiusA)/boundary_len)
  whole_number = (innerMagnetRadiusB-pitstop)/boundary_len
  #print(whole_number)
  if whole_number < 1:
    whole_number = 1
  else:
    whole_number = int(round(whole_number))

  sf.pnline(innerMagnetRadiusA, 0,pitstop, 0, whole2)
  sf.pnline(pitstop, 0,innerMagnetRadiusB, 0, whole_number)
  #sf.pmline(innerMagnetRadiusA, 0,innerMagnetRadiusB, 0, boundary_len)
  
  ###tangential facing PMs###
  sf.begin_part()    
  sf.oarc(0, axial_inner_PM_pitch, innerMagnetRadiusA, inner_magnet_arc_len_A)		#arc length would not matter to much here 
  sf.oarc(0, axial_inner_PM_pitch, pitstop, inner_magnet_arc_len_B) 			#this has to be correct arc length
  sf.oarc(0, axial_inner_PM_pitch, innerMagnetRadiusB, inner_magnet_arc_len_B) 		#this has to be correct arc length
  sf.pline(innerMagnetRadiusA, axial_inner_PM_pitch,pitstop, axial_inner_PM_pitch)
  sf.pline(pitstop, axial_inner_PM_pitch,innerMagnetRadiusB, axial_inner_PM_pitch)

  if rotate_offset == 1:
    sf.rotate(0,0,rotate_all_radians)

  #make copies of the tangential facing PM 
  if draw_full_machine == 0:
    sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2))
  else:
    sf.nrotate(0, 0, 2*magnetPitch, int(poles))
  sf.end_part()

  ###radial facing PMs###
  sf.begin_part()    
  sf.oarc(axial_inner_PM_pitch, axial_inner_PM_pitch + radial_inner_PM_pitch, innerMagnetRadiusA, inner_magnet_arc_len_A)		#arc length would not matter to much here 
  sf.oarc(axial_inner_PM_pitch, axial_inner_PM_pitch + radial_inner_PM_pitch, innerMagnetRadiusB, inner_magnet_arc_len_B) 	#this has to be correct arc length
  sf.pline(innerMagnetRadiusA, axial_inner_PM_pitch + radial_inner_PM_pitch,pitstop, axial_inner_PM_pitch + radial_inner_PM_pitch)
  sf.pline(pitstop, axial_inner_PM_pitch + radial_inner_PM_pitch,innerMagnetRadiusB, axial_inner_PM_pitch + radial_inner_PM_pitch)

  if rotate_offset == 1:
    sf.rotate(0,0,rotate_all_radians)

  #make copies of the radial facing PM 
  if draw_full_machine == 0:
    sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2) -1)
  else:
    sf.nrotate(0, 0, 2*magnetPitch, int(poles))
  sf.end_part()
  
  ###
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_inner_PM_pitch + rotate_all_radians, rotor_symmetry_factor*magnetPitch + rotate_all_radians, innerMagnetRadiusA, inner_magnet_arc_len_A)	#arc length would not matter to much here 
  sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_inner_PM_pitch + rotate_all_radians, rotor_symmetry_factor*magnetPitch + rotate_all_radians, innerMagnetRadiusB, inner_magnet_arc_len_B) 	#this has to be correct arc length
  
  if draw_full_machine == 0:
    sf.pnline(innerMagnetRadiusA, symmetry_angle + rotate_all_radians,pitstop, symmetry_angle + rotate_all_radians, whole2)
    sf.pnline(pitstop, symmetry_angle + rotate_all_radians,innerMagnetRadiusB, symmetry_angle + rotate_all_radians, whole_number)
  #sf.pmline(innerMagnetRadiusA, symmetry_angle,innerMagnetRadiusB, symmetry_angle, boundary_len)  
  ###
  
  #give polarity for each inner magnet
  angle = 0+rotate_all_radians
  orientation = 1
  inner_magnet_centre_point_r = 0.5*(innerMagnetRadiusA+innerMagnetRadiusB)
  inner_magnet_centre_point_t = 0.5*(innerMagnetRadiusA+pitstop)
  inner_magnet_centre_point_t_air = 0.5*(pitstop+innerMagnetRadiusB)

  if draw_full_machine == 0:
    pole_copies = rotor_symmetry_factor
  else:
    pole_copies = int(poles*2)

  for i in range (pole_copies): #orientate inner magnets in Halbach formation
    
    if orientation == 1:	#tangential clockwise direction (yellow)
      if x_hmi_rt != 1:
	if enable_tangential_magnets:
	  sf.cpmagnet(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, "rt", 0, Br, mur, magnet_mesh, 1, 1, 0)
	else:
	  sf.pregion(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, 0, gmesh) #air
      if x_hmi_rt != 0:
	sf.pregion(inner_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh) #air

    elif orientation == 2:	#radially outward direction (red)
      sf.cpmagnet(inner_magnet_centre_point_r, angle + 0.5*radial_inner_PM_pitch, "rt", Br, 0, mur, magnet_mesh, 1, 0, 0)
      
    elif orientation == 3:	#tangential anti-clockwise direction (light blue)
      if x_hmi_rt != 1:
	if enable_tangential_magnets:
	  sf.cpmagnet(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, "rt", 0, -Br, mur, magnet_mesh, 0, 1, 1)
	else:
	  sf.pregion(inner_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, 0, gmesh) #air
	  
      if x_hmi_rt != 0:
	sf.pregion(inner_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
  
    else:			#radially inward direction (blue)
      sf.cpmagnet(inner_magnet_centre_point_r, angle + 0.5*radial_inner_PM_pitch, "rt", -Br, 0, mur, magnet_mesh, 0, 0, 1)
      
    if orientation%2 == 1:
      angle = angle + axial_inner_PM_pitch
    else:
      angle = angle + radial_inner_PM_pitch
    
    orientation = orientation + 1 
    if orientation > 4:
      orientation = 1


# Outer magnets
###############
if draw_full_machine == 1:
  outer_magnet_arc_len_A = arc_len
  outer_magnet_arc_len_B = arc_len
elif active_parts != MAGNETS_ONLY and steps != 1:
  outer_magnet_arc_len_A = (outerMagnetRadiusA*mechRotation)/(steps-1)
  outer_magnet_arc_len_B = (outerMagnetRadiusB*mechRotation)/(steps-1)
else:
  outer_magnet_arc_len_A = arc_len
  outer_magnet_arc_len_B = arc_len

test1 = innerMagnetRadiusA + hmi_r + gi + hc + go
test4 = outerMagnetRadiusB - test1
#test2 = outerMagnetRadiusB - hmo_r
#test3 = test2-test1
#print("test1 = %f"%(test1))
#print("test2 = %f"%(test2))
#print("test3 = %f"%(test3))
#print("test4 = %f"%(test4))
#print("x_hmo_rt = %f"%(x_hmo_rt))
#print("hmo_r = %f"%(hmo_r))


#pitstop = innerMagnetRadiusA + hmi_r + gi + hc + go + x_hmo_rt*hmo_r
#pitstop = outerMagnetRadiusB-((1-x_hmo_rt)*hmo_r)
#pitstop = test1 + 0.5*test3 + x_hmo_rt*hmo_r
pitstop = test1 + x_hmo_rt*test4

#print hmo_r
#print x_hmo_rt
#print pitstop
whole2 = (pitstop-outerMagnetRadiusA)/magnet_mesh
if whole2 < 1:
  whole2 = 1
else:
  whole2 = int(round(whole2))

whole_number = int(round((outerMagnetRadiusB-pitstop)/boundary_len))
if whole_number <= 0: whole_number = 1

#print outerMagnetRadiusA
#print outerMagnetRadiusB
#print pitstop
#print whole2
#print magnet_mesh

sf.pnline(outerMagnetRadiusA, 0,pitstop, 0, whole2)
sf.pnline(pitstop, 0,outerMagnetRadiusB, 0, whole_number)
#sf.pmline(outerMagnetRadiusA, 0,outerMagnetRadiusB, 0, boundary_len)

###tangential facing PMs###
sf.begin_part()
sf.oarc(0, axial_outer_PM_pitch, pitstop, outer_magnet_arc_len_A) #this has to be correct arc length
sf.oarc(0, axial_outer_PM_pitch, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
sf.oarc(0, axial_outer_PM_pitch, outerMagnetRadiusA, outer_magnet_arc_len_A) #arc length would not matter to much here
sf.pline(outerMagnetRadiusA, axial_outer_PM_pitch,pitstop, axial_outer_PM_pitch)
sf.pline(pitstop, axial_outer_PM_pitch,outerMagnetRadiusB, axial_outer_PM_pitch)

if rotate_offset == 1:
  sf.rotate(0,0,rotate_all_radians)  

#make copies of the tangential facing PM
if draw_full_machine == 0:
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2))
else:
  sf.nrotate(0, 0, 2*magnetPitch, int(poles))
sf.end_part()

###radial facing PMs###
sf.begin_part()
sf.oarc(axial_outer_PM_pitch, axial_outer_PM_pitch + radial_outer_PM_pitch, outerMagnetRadiusA, outer_magnet_arc_len_A) #this has to be correct arc length
sf.oarc(axial_outer_PM_pitch, axial_outer_PM_pitch + radial_outer_PM_pitch, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
sf.pline(outerMagnetRadiusA, axial_outer_PM_pitch + radial_outer_PM_pitch,pitstop, axial_outer_PM_pitch + radial_outer_PM_pitch)
sf.pline(pitstop, axial_outer_PM_pitch + radial_outer_PM_pitch,outerMagnetRadiusB, axial_outer_PM_pitch + radial_outer_PM_pitch)

if rotate_offset == 1:
  sf.rotate(0,0,rotate_all_radians)  

#make copies of the radial facing PM 
if draw_full_machine == 0:
  sf.nrotate(0, 0, 2*magnetPitch, int((rotor_symmetry_factor - 2)/2)-1)
else:
  sf.nrotate(0, 0, 2*magnetPitch, int(poles))
sf.end_part()

###
sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_outer_PM_pitch + rotate_all_radians, rotor_symmetry_factor*magnetPitch + rotate_all_radians, outerMagnetRadiusA, outer_magnet_arc_len_A) #this has to be correct arc length
sf.oarc((rotor_symmetry_factor*magnetPitch) - radial_outer_PM_pitch + rotate_all_radians, rotor_symmetry_factor*magnetPitch + rotate_all_radians, outerMagnetRadiusB, outer_magnet_arc_len_B) #arc length would not matter to much here
if draw_full_machine == 0:
  sf.pnline(outerMagnetRadiusA, symmetry_angle+rotate_all_radians,pitstop, symmetry_angle+rotate_all_radians, whole2)
  sf.pnline(pitstop, symmetry_angle+rotate_all_radians,outerMagnetRadiusB, symmetry_angle+rotate_all_radians, whole_number)
#sf.pmline(outerMagnetRadiusA, symmetry_angle,outerMagnetRadiusB, symmetry_angle, boundary_len)
###

angle = 0+rotate_all_radians
orientation = 1
outer_magnet_centre_point_r = 0.5*(outerMagnetRadiusA+outerMagnetRadiusB)
outer_magnet_centre_point_t = 0.5*(outerMagnetRadiusB+pitstop)
outer_magnet_centre_point_t_air = 0.5*(outerMagnetRadiusA+pitstop)

if draw_full_machine == 0:
  pole_copies = rotor_symmetry_factor
else:
  pole_copies = int(poles*2)

for i in range (pole_copies):  
  if orientation == 1:	#tangetial anti-clockwise direction (yellow)
      if x_hmo_rt != 1:
	if enable_tangential_magnets:
	  sf.cpmagnet(outer_magnet_centre_point_t, angle + 0.5*axial_outer_PM_pitch, "rt", 0, -Br, mur, magnet_mesh, 0, 1, 1)
	else:
	  sf.pregion(outer_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
	  
      if x_hmo_rt != 0:
	sf.pregion(outer_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)

  elif orientation == 2:	#radially outward direction (red)
      sf.cpmagnet(outer_magnet_centre_point_r, angle + 0.5*radial_outer_PM_pitch, "rt", Br, 0, mur, magnet_mesh, 1, 0, 0)
      
  elif orientation == 3:	#tangential clockwise direction  (light blue)
      if x_hmo_rt != 1:
	if enable_tangential_magnets:
	  sf.cpmagnet(outer_magnet_centre_point_t, angle + 0.5*axial_outer_PM_pitch, "rt", 0, Br, mur, magnet_mesh, 1, 1, 0)
	else:
	  sf.pregion(outer_magnet_centre_point_t, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
	  
      if x_hmo_rt != 0:
	sf.pregion(outer_magnet_centre_point_t_air, angle + 0.5*axial_inner_PM_pitch, 0, gmesh)
  
  else:			#radially inward direction (blue)
      sf.cpmagnet(outer_magnet_centre_point_r, angle + 0.5*radial_outer_PM_pitch, "rt", -Br, 0, mur, magnet_mesh, 0, 0, 1)
    
  if orientation%2 == 1:
    angle = angle + axial_outer_PM_pitch
  else:
    angle = angle + radial_outer_PM_pitch

  orientation = orientation + 1 
  if orientation > 4:
    orientation = 1


# Stator coil cores (blocks) and windings
#########################################
sf.begin_part()
angle_temp1 = windingRadians + blockAngle
angle_temp2 = int(coilsTotal/(coilsTotal/3)-1)*coilBlockPitch
angle_temp3 = angle_temp2 + angle_temp1

if draw_full_machine == 1:
  stator_arc_len_inner = arc_len*0.5
  stator_arc_len_outer = arc_len*0.5
elif active_parts != MAGNETS_ONLY and steps != 1:
  stator_arc_len_inner = (innerBlockRadius*mechRotation)/(steps-1)
  stator_arc_len_outer = (outerBlockRadius*mechRotation)/(steps-1)
else:
  stator_arc_len_inner = arc_len
  stator_arc_len_outer = arc_len  

sf.oarc(0, windingRadians, innerBlockRadius, stator_arc_len_inner)		#coil block inside arc
sf.oarc(0, windingRadians, outerBlockRadius, stator_arc_len_outer)		#coil block inside arc
sf.pline(innerBlockRadius, windingRadians, outerBlockRadius, windingRadians)

sf.oarc(windingRadians, angle_temp1, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
sf.oarc(windingRadians, angle_temp1, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
sf.pline(innerBlockRadius, angle_temp1, outerBlockRadius, angle_temp1)

sf.oarc( angle_temp1, coilBlockPitch, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
sf.oarc( angle_temp1, coilBlockPitch, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
sf.pline(innerBlockRadius, coilBlockPitch, outerBlockRadius, coilBlockPitch)

if rotate_offset == 1:
  sf.rotate(0,0,rotate_all_radians)

###
if draw_full_machine == 0:
  sf.nrotate(0, 0, coilBlockPitch, int(coilsTotal/(coilsTotal/3)-2))
else:
  sf.nrotate(0, 0, coilBlockPitch, int(coilsTotal))

sf.oarc(angle_temp2+rotate_all_radians, angle_temp2 + windingRadians+rotate_all_radians, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
sf.oarc(angle_temp2+rotate_all_radians, angle_temp2 + windingRadians+rotate_all_radians, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
sf.pline(innerBlockRadius, angle_temp2 + windingRadians+rotate_all_radians, outerBlockRadius, angle_temp2 + windingRadians+rotate_all_radians)

sf.oarc(angle_temp2 + windingRadians+rotate_all_radians, angle_temp3+rotate_all_radians, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
sf.oarc(angle_temp2 + windingRadians+rotate_all_radians, angle_temp3+rotate_all_radians, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc
sf.pline(innerBlockRadius, angle_temp3+rotate_all_radians, outerBlockRadius, angle_temp3+rotate_all_radians)

sf.oarc(angle_temp3+rotate_all_radians, angle_temp2 + coilBlockPitch+rotate_all_radians, innerBlockRadius, stator_arc_len_inner)	#coil block inside arc
sf.oarc(angle_temp3+rotate_all_radians, angle_temp2 + coilBlockPitch+rotate_all_radians, outerBlockRadius, stator_arc_len_outer)	#coil block inside arc

whole_number = int((outerBlockRadius-innerBlockRadius)/boundary_len)
if whole_number <= 0: whole_number = 1

if draw_full_machine == 0:
  sf.pnline(innerBlockRadius, angle_temp2 + coilBlockPitch+rotate_all_radians, outerBlockRadius, angle_temp2 + coilBlockPitch+rotate_all_radians, whole_number)
  sf.pnline(innerBlockRadius, 0+rotate_all_radians, outerBlockRadius, 0+rotate_all_radians, whole_number)


###

#assign stator coil windings 
winding = zeros((coilsTotal,2))
phase_A_offset = sf.assign_winding2(3, int(poles/2), coilsTotal, N_L, N_S, winding)			#This gives the correct answer as it correlates with fpl viewer's theta (t) reading

#declare coils and specify which windings they belong to
present_phase = 0
angle = 0.5*windingRadians+rotate_all_radians
if draw_full_machine == 0:
  coil_count = int(coilsTotal/(coilsTotal/3)*2)
else:
  coil_count = coilsTotal*2

for i in range (coil_count):  
  if(present_phase > 5):
    present_phase = 0
  sf.pregion(0.5*(innerBlockRadius+outerBlockRadius), angle, phase[present_phase], gmesh)		#coils has same permeability as air
  if(i%2 == 0):
    angle = angle + angle_temp1
  else:
    angle = angle + windingRadians
    
  present_phase = present_phase + 1

angle = windingRadians + 0.5*blockAngle

if draw_full_machine == 0:
  coil_count = int(coilsTotal/(coilsTotal/3))
else:
  coil_count = coilsTotal

for i in range (coil_count):
  if enable_smc_cored_coils == 0:
    sf.pregion(0.5*(innerBlockRadius+outerBlockRadius), angle, 0, gmesh)				#air-cored coil
  else:
    #SMC material is dummied by creating a magnet region with Br=0, but with mur = 850
    Br = 0
    mur = 850
    sf.cpmagnet(0.5*(innerBlockRadius+outerBlockRadius),angle, "rt", Br, Br, mur, gmesh, 0.2, 0.4, 1)	#smc-cored coil
    
  angle += 2*windingRadians + blockAngle

sf.end_part()


#Additional drawing commands
############################
# Add layers inside yoke
insideYokeLayer  = innerMagnetRadiusA*0.98
outsideYokeLayer = outerMagnetRadiusB*1.02
  
if active_parts != MAGNETS_ONLY and steps != 1:
  #add yoke material (essentially the same permeability as air due to ironless design)
  #this is necessary to simulate flux paths moving through the yoke and the air outside of the yoke
  
  if air0_iron1_yokes == 0:
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, insideYokeLayer, (insideYokeLayer*mechRotation)/(steps-1))
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outsideYokeLayer, (outsideYokeLayer*mechRotation)/(steps-1)) 

    #yoke outside nodes dont have to be this fine, it just wastes computational time
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerYoke, outskirts_mesh*(innerYoke/outerYoke))
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerYoke, outskirts_mesh)
    
  elif air0_iron1_yokes == 1:
    #print(ri-iron_yoke_height_mm/1000)
    #print(ro+iron_yoke_height_mm/1000)
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, ri-iron_yoke_height_mm/1000, ((ri-iron_yoke_height_mm/1000)*mechRotation)/(steps-1))
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, ro+iron_yoke_height_mm/1000, ((ro+iron_yoke_height_mm/1000)*mechRotation)/(steps-1))
    sf.pregion(ri-0.5*(iron_yoke_height_mm/1000), 0.5*mechRotation+rotate_all_radians,yoke_iron, magnet_mesh)
    sf.pregion(ro+0.5*(iron_yoke_height_mm/1000), 0.5*mechRotation+rotate_all_radians,yoke_iron, magnet_mesh)

else:
  sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, insideYokeLayer, magnet_mesh)
  sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outsideYokeLayer, magnet_mesh) 

  #yoke outside nodes dont have to be this fine, it just wastes computational time
  sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerYoke, outskirts_mesh*(innerYoke/outerYoke))
  sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerYoke, outskirts_mesh)


# Multilayer air-gaps
if draw_full_machine == 0:
  if active_parts != MAGNETS_ONLY and steps != 1:
    if enable_inner_magnets:
      sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerMagnetRadiusB + innerGap/3, ((innerMagnetRadiusB + innerGap/3)*mechRotation)/(steps-1))
      sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerBlockRadius - innerGap/3, ((innerBlockRadius - innerGap/3)*mechRotation)/(steps-1))
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerBlockRadius + outerGap/3, ((outerBlockRadius + outerGap/3)*mechRotation)/(steps-1))
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerMagnetRadiusA - outerGap/3, ((outerMagnetRadiusA - outerGap/3)*mechRotation)/(steps-1))
  else:
    if enable_inner_magnets:
      sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerMagnetRadiusB + innerGap/3, gap_len)
      sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, innerBlockRadius - innerGap/3, gap_len)
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerBlockRadius + outerGap/3, gap_len)
    sf.oarc(0+rotate_all_radians, 2*mechRotation+rotate_all_radians, outerMagnetRadiusA - outerGap/3, gap_len)    

# Boundary nodes
if draw_full_machine == 0:
  if air0_iron1_yokes == 0:
    whole_number = int((innerMagnetRadiusA-insideYokeLayer)/magnet_mesh)
    if whole_number <= 0: whole_number = 1
    if enable_inner_magnets:
      sf.pnline(insideYokeLayer, 0+rotate_all_radians, innerMagnetRadiusA, 0+rotate_all_radians, whole_number)
      sf.pnline(insideYokeLayer, symmetry_angle+rotate_all_radians, innerMagnetRadiusA, symmetry_angle+rotate_all_radians, whole_number)  
    
    whole_number = int((outsideYokeLayer-outerMagnetRadiusB)/magnet_mesh)
    if whole_number <= 0: whole_number = 1
    sf.pnline(outerMagnetRadiusB, 0+rotate_all_radians,outsideYokeLayer, 0+rotate_all_radians, whole_number)
    sf.pnline(outerMagnetRadiusB, symmetry_angle+rotate_all_radians,outsideYokeLayer, symmetry_angle+rotate_all_radians, whole_number)

    whole_number = int((insideYokeLayer-innerYoke)/(outskirts_mesh*(innerYoke/outerYoke)))
    if whole_number <= 0: whole_number = 1
    if enable_inner_magnets:
      sf.pnline(innerYoke, 0+rotate_all_radians,insideYokeLayer, 0+rotate_all_radians, whole_number)
      sf.pnline(innerYoke, symmetry_angle+rotate_all_radians, insideYokeLayer, symmetry_angle+rotate_all_radians, whole_number)
    
    whole_number = int((outerYoke-outsideYokeLayer)/(outskirts_mesh))
    if whole_number <= 0: whole_number = 1
    sf.pnline(outsideYokeLayer, 0+rotate_all_radians,outerYoke, 0+rotate_all_radians, whole_number)
    sf.pnline(outsideYokeLayer, symmetry_angle+rotate_all_radians,outerYoke, symmetry_angle+rotate_all_radians, whole_number)

  elif air0_iron1_yokes == 1:
    whole_number = int((innerMagnetRadiusA-(ri-iron_yoke_height_mm/1000))/magnet_mesh)
    if whole_number <= 0: whole_number = 1
    if enable_inner_magnets:
      sf.pnline((ri-iron_yoke_height_mm/1000), 0+rotate_all_radians, innerMagnetRadiusA, 0+rotate_all_radians, whole_number)
      sf.pnline((ri-iron_yoke_height_mm/1000), symmetry_angle+rotate_all_radians, innerMagnetRadiusA, symmetry_angle+rotate_all_radians, whole_number)  
    else:
      sf.pnline((ri-iron_yoke_height_mm/1000), 0+rotate_all_radians, ri, 0+rotate_all_radians, whole_number)
      sf.pnline((ri-iron_yoke_height_mm/1000), symmetry_angle+rotate_all_radians, ri, symmetry_angle+rotate_all_radians, whole_number) 
    
    whole_number = int(((ro+iron_yoke_height_mm/1000)-outerMagnetRadiusB)/magnet_mesh)
    if whole_number <= 0: whole_number = 1
    sf.pnline(outerMagnetRadiusB, 0+rotate_all_radians,(ro+iron_yoke_height_mm/1000), 0+rotate_all_radians, whole_number)
    sf.pnline(outerMagnetRadiusB, symmetry_angle+rotate_all_radians,(ro+iron_yoke_height_mm/1000), symmetry_angle+rotate_all_radians, whole_number)
    

#draw lines to close sub-air-gap sections (also boundary nodes)
if draw_full_machine == 0:
  if (enable_sub_air_gaps == 1):
    if enable_inner_magnets:
      sf.pline(innerMagnetRadiusB, 0+rotate_all_radians, innerMagnetRadiusB + innerGap/3, 0+rotate_all_radians)
      sf.pline(innerBlockRadius - innerGap/3, 0+rotate_all_radians, innerBlockRadius, 0+rotate_all_radians)
    sf.pline(outerBlockRadius, 0+rotate_all_radians, outerBlockRadius + outerGap/3, 0+rotate_all_radians)
    sf.pline(outerMagnetRadiusA - outerGap/3, 0+rotate_all_radians, outerMagnetRadiusA, 0+rotate_all_radians)

    if enable_inner_magnets:
      sf.pline(innerMagnetRadiusB, symmetry_angle+rotate_all_radians, innerMagnetRadiusB + innerGap/3, symmetry_angle+rotate_all_radians)
      sf.pline(innerBlockRadius - innerGap/3, symmetry_angle+rotate_all_radians, innerBlockRadius, symmetry_angle+rotate_all_radians)
    sf.pline(outerBlockRadius, symmetry_angle+rotate_all_radians, outerBlockRadius + outerGap/3, symmetry_angle+rotate_all_radians)
    sf.pline(outerMagnetRadiusA - outerGap/3, symmetry_angle+rotate_all_radians, outerMagnetRadiusA, symmetry_angle+rotate_all_radians)
    
#############################
# Final simulation settings #
#############################
#os.system("fpl2 project_idrfpm_semfem/drawing.poly")
#os.system("fpl2 project_idrfpm_semfem/drawing.fpl")
#sys.exit()

if enable_sub_air_gaps == 1:
  if enable_inner_magnets:
    sf.semfem_set_cvw_gaps(2, [innerMagnetRadiusB + innerGap/3, outerBlockRadius + outerGap/3], [innerBlockRadius - innerGap/3, outerMagnetRadiusA - outerGap/3])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation
  else:
    #print(outerBlockRadius + outerGap/3)
    #print(outerMagnetRadiusA - outerGap/3)
    sf.semfem_set_cvw_gaps(1, [outerBlockRadius + outerGap/3], [outerMagnetRadiusA - outerGap/3])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation
else:
  if enable_inner_magnets:
    sf.semfem_set_cvw_gaps(2, [innerMagnetRadiusB, outerBlockRadius], [innerBlockRadius, outerMagnetRadiusA])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation
  else:
    sf.semfem_set_cvw_gaps(1, [outerBlockRadius], [outerMagnetRadiusA])	#coulomb vector w.. method used to calculate torque in air gaps. this function does not effect an age_solver simulation

###FROM OE_MAC.py periodic example
if draw_full_machine == 0:
  sf.semfem_set_left_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the left boundary)
  sf.semfem_set_right_boundary(sf.EVEN_PERIODIC)		#because this section repeats itself by an even number (on the right boundary)
sf.semfem_set_top_boundary(sf.DIRICHLET)			#top section Dirichlet boundary (determines in which directions the flux can be solved)
sf.semfem_set_bottom_boundary(sf.DIRICHLET)			#bottom section Dirichlet boundary (determines in which directions the flux can be solved)
###

#sf.semfem_coil_turns[:] = turnsPerCoil			#define number of turns for each coil (22) (this line not needed if using sf.PHASE_CURRENT_DENSITY)
sf.semfem_coil_turns[:] = 1
sf.semfem_coil_sides[:] = N_L*coilsTotal/3	   		#define number of coils for each phase (21/3)


		
#specify fill factor
for i in range(size(sf.semfem_ff_vec)):			#Although it is only necessary to assign values to the first 3 elements of the array (3 phase machine) the other elements are assigned these values just for contingency.
  sf.semfem_ff_vec[i] = fill_factor
		
#iq_density = current_through_slot/(coil_side_area*1000**2)
iq_density = J
iq = iq_density*(coil_side_area*1000**2)*fill_factor
id = 0

current_angle_degrees = np.arctan(id/iq)*180/np.pi

for i in range(steps):					#simulates an entire electrical period in the allocated number of steps
  #if active_parts != MAGNETS_ONLY:
  if active_parts != MAGNETS_ONLY and steps != 1:
    sf.semfem_time_vec[i] = ((i+1)/steps)/f_e			#this is necessary to setup the electrical operational frequency (f_e) for the motor
							      #it also adjusts how fast (number of steps) the simulation runs through a single electrical period

    if locked_rotor_test == 0:
    #Controls the position of different components at each step. Components are seperated by air-gaps and are numbered from the inside out.
      if enable_inner_magnets:
	sf.semfem_p_vec[i,0] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the inner rotor [0]
	sf.semfem_p_vec[i,2] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the outer rotor [2]
      else:
	sf.semfem_p_vec[i,1] = (i/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the outer rotor [2]
    else:
      sf.semfem_p_vec[i,0] = (0/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the inner rotor [0]
      sf.semfem_p_vec[i,2] = (0/(steps-1))*(mechRotation) - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the outer rotor [2]      

    [ia, ib, ic] = [0, 0, 0]
    ia, ib, ic = sf.dq_abc(id, iq_density, (i*2.*pi)/(steps-1))	#3rd function argument is the transformation angle of the dq transformation
    #use the following three lines for phase current quantities
    #sf.semfem_i_vec[i,0] = ia
    #sf.semfem_i_vec[i,1] = ib
    #sf.semfem_i_vec[i,2] = ic
    #use the following three lines for phase current density quantities
    #print("ia = %.3f, ib = %.3f, ic = %.3f"%(ia,ib,ic))
    sf.semfem_idens_vec[i,0] = ia
    sf.semfem_idens_vec[i,1] = ib
    sf.semfem_idens_vec[i,2] = ic
    #print("sf.semfem_idens_vec[i,0] = %f"%(sf.semfem_idens_vec[i,0]))
  
  else:
    if enable_inner_magnets:
      sf.semfem_p_vec[i,0] = 0 - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the inner rotor [0]
      sf.semfem_p_vec[i,2] = 0 - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the outer rotor [2]
    else:
      sf.semfem_p_vec[i,1] = 0 - rotorDAxisOffset + phase_A_offset+rotate_all_radians		#the second array index indicates rotation of the outer rotor [2]
    
    [ia, ib, ic] = [0, 0, 0]
    #ia, ib, ic = sf.dq_abc(id, iq, (i*2.*pi)/(steps))
    ia, ib, ic = sf.dq_abc(id, iq_density, 0)
    #sf.semfem_i_vec[i,0] = ia
    #sf.semfem_i_vec[i,1] = ib
    #sf.semfem_i_vec[i,2] = ic      
    #print "jabaas 0"
    sf.semfem_idens_vec[i,0] = ia
    sf.semfem_idens_vec[i,1] = ib
    sf.semfem_idens_vec[i,2] = ic    

#symmetry settings
if draw_full_machine == 0:
  sf.semfem_set_symmetry_multiplier(int(poles/4))		#number of periodic occurances of rotor structure
  sf.semfem_set_parallel_circuits(int(coilsTotal/3)) 		#number of parallel circuits for each stator phase winding
else:
  sf.semfem_set_symmetry_multiplier(1)		#number of periodic occurances of rotor structure
  sf.semfem_set_parallel_circuits(1) 		#number of parallel circuits for each stator phase winding  

#print(sf.semfem_p_vec)

##################    
# Construct mesh #
##################
#os.system("fpl2 mp/p1/project_idrfpm_semfem/drawing.poly")
if display_drawings == 1:
  os.system("fpl2 project_idrfpm_semfem/drawing.poly")
#sys.exit()
m = sf.py_make_mesh(sf.NO_FIX)          			#previously used sf.FIX, why? Because polygon was not accurately constructed

if draw_full_machine == 1:
  svg_file = 'project_idrfpm_semfem/drawing'
  #os.system("fpl2svg %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(svg_file+"_plain"+".pdf",svg_file+".svg"))
  #os.system("inkscape -e --export-dpi=400 %s %s"%(svg_file+"_plain"+".png",svg_file+".svg"))  

#just to view the drawing before actually running the solver
#os.system("fpl2 project_idrfpm_semfem/drawing.poly")
if display_drawings == 1:
  os.system("fpl2 project_idrfpm_semfem/drawing.fpl")
#sys.exit()

##########
# Solver #
##########
#m = sf.age_solver(m, sf.ICCG)				#takes much longer to simulate
#m = sf.band_solver(m, sf.GAUSS, 1)				#was so, is dieselfde as in Bola se script
m = sf.band_solver(m, sf.GAUSS, 1,sf.NONLINEAR_PROBLEM)				#was so, is dieselfde as in Bola se script

#############################
# View fieldplots or graphs #
#############################
if display_drawings == 1:
  os.system("fpl2 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl")
#os.system("fpl2 project_idrfpm_semfem/results/fieldplots/age_machine_field001.fpl") 
if plot_all == 1:
  os.system("post.py project_idrfpm_semfem/results/post.res")	#brings up thousands of plots

#checkpoint.append(time.time())
####################################
# Calculate flux density harmonics #
####################################
if enable_flux_density_harmonic_calc == 1:
  M = 1024					#M should be even number to optimally use the FFT algorithm
  fplfile = dir_parent+"project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl"
  Br = []
  Br.append([])
  Br.append([])
  Br.append([])
  #get_arc_B(self, fpl_file, theta1, theta2, r, npoints) #function prototype from semfem.py
  #Bm, Br[0], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn - hc/2, 2*M)		#the last argument specifies the number of data points one desires
  #Bm, Br[1], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn, 2*M)
  #Bm, Br[2], Bt = sf.get_arc_B(fplfile, 0,2*mechRotation, rn + hc/2, 2*M)

  Bm, Br[0], Bt = sf.get_arc_B(fplfile, 0+rotate_all_radians,2*mechRotation+rotate_all_radians, CoilRadiusA, 2*M)		#the last argument specifies the number of data points one desires
  Bm, Br[1], Bt = sf.get_arc_B(fplfile, 0+rotate_all_radians,2*mechRotation+rotate_all_radians, CoilRadiusA + 0.5*(CoilRadiusB-CoilRadiusA), 2*M)
  Bm, Br[2], Bt = sf.get_arc_B(fplfile, 0+rotate_all_radians,2*mechRotation+rotate_all_radians, CoilRadiusB, 2*M)

  #get first harmonic of Br @ rn
  pl.figure(3)

  Br_fft = abs(np.fft.rfft(Br[1],M)*2/M)	#bereken fft en normaliseer (gebruik NumPy library) (kan rfft gebruik omdat sein net reel is) (gebruik n/2 punte omdat ons net positiewe frekwensies wil bereken)    
  Br1 = Br_fft[1]
  max_Br_1 = max(Br[1])

  #my_range = 20
  #k = np.arange(my_range)			#maak aanname dat elektriese periode van die sein 1 Hz is (dan sal 1st harmoniek by k = 1 le, en dus verteenwoordig k ook dan sommer die frekwensie in Hz)
  #pl.plot(k,abs(Br_fft[:my_range]))
  #pl.xlabel('Freq (Hz)')
  #pl.ylabel('|$B_{r|r_{n}}$|')
  #pl.show()

  Br_THD = sqrt(sum(Br_fft[2:int((M-1)/2)]**2))/Br1
  

#checkpoint.append(time.time()) 
#############################
# Calculate general resutls #
#############################
if use_sql == False:
  wire_resistivity = wire_resistivity*(1+0.0039*(coil_temperature-20)) 

if enable_general_calc == 1:
  fld = zeros(steps)
  flq = zeros(steps)
  torque_dq = zeros(steps)
  P_wf = 0
  vd_arr = zeros(steps)
  vq_arr = zeros(steps)

  #checkpoint.append(time.time())
  #CALCULATE DQ VOLTAGES
  if steps != 1:
    sfm_post = SEMFEM_postfile(dir_parent+'project_idrfpm_semfem/results/post.res', 1.0/f_e)
    vd = zeros(steps)
    vq = zeros(steps)      
    for i in range(steps):
      vd[i], vq[i] = sf.abc_dq(sfm_post.voltage[0,i], sfm_post.voltage[1,i], sfm_post.voltage[2,i], i/steps*2.*pi)    
  #####################

  #checkpoint.append(time.time())
  #CALCULATE TORQUE
  for i in range(steps):
    fld[i], flq[i] = sf.abc_dq(sf.semfem_flink_vec[i,0], sf.semfem_flink_vec[i,1], sf.semfem_flink_vec[i,2], i/steps*2.*pi)
    torque_dq[i] = -(3/2)*p*(fld[i]*iq - flq[i]*id)
    
  savetxt(dir_parent+'script_results/idrfpm/torque_dq.csv', torque_dq, delimiter = ',',newline='\n',header='torque_dq',fmt='%f')    
  average_torque_dq = average(torque_dq)

  if active_parts != MAGNETS_ONLY and steps != 1:
    average_torque = abs(average(sf.semfem_torque_vec[:,1]))
  else:
    average_torque = -1    
  #####################
  
  id_new = np.zeros(steps)
  iq_new = np.zeros(steps)

  for i in range(steps):
    if steps == 1:
      id_new[i],iq_new[i] = sf.abc_dq(sf.semfem_i_vec[i,0], sf.semfem_i_vec[i,1], sf.semfem_i_vec[i,2], 0)   
    else:
      id_new[i],iq_new[i] = sf.abc_dq(sf.semfem_i_vec[i,0], sf.semfem_i_vec[i,1], sf.semfem_i_vec[i,2], i/steps*2.*np.pi)   
  
  #calculate vd and vq here as the values seem more constant as opposed to determining vd and vq from sf.abd_dq 
  w_e = 2*np.pi*f_e
  vd_calculated = np.zeros(steps)
  vq_calculated = np.zeros(steps)
  for i in range(0,steps):
    vd_calculated[i] = -w_e*flq[i]#/turnsPerCoil #minus is present with vd, see Umans p578
    vq_calculated[i] = w_e*fld[i]#/turnsPerCoil    
  
  #####################
  
  #calculate inductance
  #determine synchronous inductance Ls, and magnet flux linkage flm if a pure q-axis current is injected, or if we are not yet at stage in which the target kW should be attepted (we first need the Ls and flm quantities)
  if locked_rotor_test == 0:
    if np.isclose(current_angle_degrees, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
      #Ced)/(np.sqrt(2)*w_e*iq_new[0])) #I think the current used here is an rms value
      synchronous_inductance_single_turn = abs(max(vd_calculated)/(w_e*current_through_slot)) #current through slot is already a peak value in this script
      #flm_single_turn = (abs(average(vq_calculated))-w_e*synchronous_inductance_single_turn*id_new[0])/w_e
      #flm_single_turn = (abs(average(vq_calculated))-w_e*synchronous_inductance_single_turn*current_through_slot)/w_e
  
  #calculate inductance when only AR effect is in place
  if active_parts == WINDINGS_ONLY:
    synchronous_inductance_single_turn_AR = max(sf.semfem_flink_vec[:,0])/(current_through_slot) #current through slot is already a peak value in this script
  
  
  #####################
  
  #checkpoint.append(time.time())
  #CALCULATE MAGNET AND COPPER MASS
  magnet_mass = sf.get_magnet_mass(m, sf.NDFEB_DENSITY)
  copper_mass = 0.
  aluminium_mass = 0
  tufnol_mass = 0
  for i in range(1, 4):
      coil_mass = sf.get_mass(m, i, sf.COPPER_DENSITY)
      copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]
      coil_mass = sf.get_mass(m, -i, sf.COPPER_DENSITY)
      copper_mass = copper_mass + coil_mass*sf.semfem_ff_vec[i-1]

  if enable_shell_mass == 1:
    outer_rotor_shell_mass = np.pi*((rotor_shell_height+ro)**2 - ro**2)*l*sf.ALUM_DENSITY
    inner_rotor_shell_mass = np.pi*(ri**2 - (ri-stator_shell_height)**2)*l*sf.ALUM_DENSITY
    aluminium_mass = outer_rotor_shell_mass + inner_rotor_shell_mass
    
    #print("blockAngle = %f"%(blockAngle))
    #print("CoilRadiusB = %f"%(CoilRadiusB))
    #print("CoilRadiusA = %f"%(CoilRadiusA))
    tufnol_mass = tufnol_density*l*coilsTotal*pi*(CoilRadiusB**2 - CoilRadiusA**2)*(blockAngle/(2*pi))


  total_mass = magnet_mass + copper_mass + aluminium_mass + tufnol_mass

  #print("ri = %f"%(ri))
  #print("ro = %f"%(ro))

  #####################

  #DETERMINE MAX INPUT CURRENT
  #if steps != 1:
    #print "jabaas 2"
    #print(sfm_post.current)
    #max_input_phase_current = max(sf.semfem_i_vec[:,0])
  #else:
    #max_input_phase_current = iq

  '''
  NB: Instantaneous power word bereken, menend die stroom op daardie oomblik word gemaal met die spanning op daardie oomblik. Hierdie stroom en spanning kan inderdaad uit fase wees, ons maal maar net hul waardes waarokal hulle op hul
  eie kurwe sit. Dan, as ons die gemiddeld van hierdie oombliklike drywing neem, dan kry ons P (reele drywing). Daarna kan ons S bereken, deur die RMS waardes vnan stroom en spanning met mekaar te maal (dus nie inagneem of hulle uit
  fase is of nie). Dan kan ons p.f. bereken nou dat ons P en S het.
  '''
  
  #checkpoint.append(time.time())
  #CALCULATE VOLTAGE, S,P,Q AND POWER FACTOR
  Ia_instantaneous = sf.semfem_i_vec[:,0]
  #print(Ia_instantaneous)
  #print(sf.semfem_i_vec[:,0])
  #print(sf.semfem_i_vec[5,0])
  #print(sf.semfem_i_vec[5,1])
  ewf = (pi*end_winding_radius)/l		#end winding factor
  #Ra = 0#m.rcoil[0]*(1+ewf)			#phase resistance of coil winding. it is a mesh attribute
  Ia_rms = iq/sqrt(2)    
  
  area_available_per_turn = (coil_side_area*fill_factor*1000**2)
  l_coil = 2*(l+(pi*end_winding_radius))
  l_total = (coilsTotal/3)*l_coil
  Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)    
  
  if steps != 1:
    Ea_instantaneous = sfm_post.voltage[0]
    Va_instantaneous = zeros(steps)
    P_instantaneous = zeros(steps)
    
    single_turn_back_emf_rms_dq = 0
    #print("here")
    for i in range(steps):
      #Ia_instantaneous[i] = Ia_instantaneous[i]/(coil_side_area*1000**2 * fill_factor)
      #print(Ia_instantaneous[i])
      Va_instantaneous[i] = Ea_instantaneous[i] + Ia_instantaneous[i]*Ra_analytical
      vd_arr[i] = -2*pi*f_e*flq[i]
      vq_arr[i] = -2*pi*f_e*fld[i] 
      tmp = sqrt((vd_arr[i]**2 + vq_arr[i]**2)/2)
      if tmp > single_turn_back_emf_rms_dq:
	single_turn_back_emf_rms_dq = tmp
	
	
    single_turn_max_induced_phase_voltage = max(Ea_instantaneous)
    single_turn_max_input_phase_voltage = max(Va_instantaneous)
    single_turn_max_input_phase_current = max(Ia_instantaneous)
    
    #now calculate all with correct number of turns
    if enable_manual_turnsPerCoil_limit == 1:
      turnsPerCoil = int(input_data[20])
    
    area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
    l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
    l_total = (coilsTotal/3)*l_coil
    Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
    
    max_induced_phase_voltage = single_turn_max_induced_phase_voltage*turnsPerCoil
    max_input_phase_current = single_turn_max_input_phase_current/turnsPerCoil    
    max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
    back_emf_rms_dq = single_turn_back_emf_rms_dq*turnsPerCoil
    iq = iq/turnsPerCoil
    
    if locked_rotor_test == 0:
      Ls = synchronous_inductance_single_turn*turnsPerCoil**2
    else:
      #determine inductance when rotor is standstill
      if not np.isclose(max_input_phase_current*w_e, 0, rtol=1e-02, atol=1e-02, equal_nan=False):
	Ls = max_induced_phase_voltage/(max_input_phase_current*w_e)  
	synchronous_inductance_single_turn = Ls/(turnsPerCoil**2)
    if active_parts == WINDINGS_ONLY:
      Ls_AR = synchronous_inductance_single_turn_AR*turnsPerCoil**2    
    
    #print("jabba")
    for i in range(steps):
      Ia_instantaneous[i] = Ia_instantaneous[i]/turnsPerCoil
      #print(Ia_instantaneous[i])	  
      Ea_instantaneous[i] = Ea_instantaneous[i]*turnsPerCoil
      Va_instantaneous[i] = Ea_instantaneous[i] + Ia_instantaneous[i]*Ra_analytical
      P_instantaneous[i] = Va_instantaneous[i]*Ia_instantaneous[i]      
      flq[i] = flq[i]*turnsPerCoil
      fld[i] = fld[i]*turnsPerCoil
      vd_arr[i] = -2*pi*f_e*flq[i]
      vq_arr[i] = -2*pi*f_e*fld[i]
    
    P = abs(mean(3*P_instantaneous))
    P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical
    
    V_rms=sqrt(mean(Va_instantaneous**2))
    I_rms=sqrt(mean(Ia_instantaneous**2))
    #fl_rms=sqrt(mean(fla**2))
    S= 3*(V_rms*I_rms)   
    
  #else:     
    ##vd = Ra*id - 2*pi*f_e*flq[0]
    ##vq = Ra*iq - 2*pi*f_e*fld[0]
    #vd = -2*pi*f_e*flq[0]
    #vq = -2*pi*f_e*fld[0]      
    ##P = (3/2)*(vd*id + vq*iq) 		#inlcudes P_out and P_conductive (does not include other losses)
    #back_emf_rms = sqrt((vd**2 + vq**2)/2)		#that means that this Va_rms, would need to be slightly higher if for instance eddy current losses were included (however eddy current losses are present even when Ia = 0, meaning it should rather be modeled 
      
    #if enable_current_density == True:
      #turnsPerCoil = int(peak_voltage_allowed/(back_emf_rms*sqrt(2)))	#get integer number which is rounded down so as to not exceed voltage design limit
      #area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
      #max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
      #max_input_phase_current = current_through_slot/turnsPerCoil
      #if debug == 1:
	#print("\n(0) Wire_resistivity = %f [Ohm.m]  @ T = %d [Celsius Deg]"%(wire_resistivity,coil_temperature))
	#print("(0) Maximum allowed peak voltage for this design V_a(ln) = %f [V]"%peak_voltage_allowed)
	#print("(0) Peak voltage induced in 1 turn V_a(ln) = %f [V/t]"%(back_emf_rms*sqrt(2)))
	#print("\n(1) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	#print("(1) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	#print("(1) Peak input phase current Ia = %f [A]"%max_input_phase_current)	
	      
  #if steps == 1:
    ##CALCULATE I^2R CONDUCTION LOSSES (ANALYTICAL) 
    #l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
    #l_total = (coilsTotal/3)*l_coil
    #Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
    #max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
    #if debug == 1:
      #print("(1) Ra_analytical = %f [Ohm]"%Ra_analytical)
      #print("(1) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)
  
    ##CHECK that max_input_phase_voltage is small enough, adjust turns until limits are adhered to
    
    #if enable_manual_turnsPerCoil_limit == 1:
      #turnsPerCoil = int(input_data[20])
      #area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
      #max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
      #max_input_phase_current = current_through_slot/turnsPerCoil
      #l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
      #l_total = (coilsTotal/3)*l_coil
      #Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)

      #max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical      
    #else:   
      
      #while max_input_phase_voltage > peak_voltage_allowed:
	#turnsPerCoil = turnsPerCoil - 1
	
	#area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	#max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	#max_input_phase_current = current_through_slot/turnsPerCoil

	#l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	#l_total = (coilsTotal/3)*l_coil

	#Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	#max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
	
      #if debug == 1:  
	#print("\n(2) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	#print("(2) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	#print("(2) Peak input phase current Ia = %f [A]"%max_input_phase_current)
	#print("(2) Ra_analytical = %f [Ohm]"%Ra_analytical)
	#print("(2) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)	  
      
      ##if the available area per turn is not adequate to use the finest type of Litz wire (0.2mm with 70 strands), then the turns per coil need to be reduced. If the available area is more than required by this Litz wire, then we can use thicker wire, or use more parallel strands, or insert another turn during manufacturing
      #if enable_litz_wire_selection == 1:
	#while area_available_per_turn < single_turn_area*1e6:
	  #turnsPerCoil = turnsPerCoil - 1
	
	  #area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	  #max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	  #max_input_phase_current = current_through_slot/turnsPerCoil	

	  #l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	  #l_total = (coilsTotal/3)*l_coil

	  #Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	  #max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical
    
      ##introduce contingency for 1 less turn during construction, thereby reducing the voltage.
      #if enable_voltage_leeway == 1:
	#turnsPerCoil = turnsPerCoil - 1
      
	#area_available_per_turn = (coil_side_area*fill_factor*1000**2)/turnsPerCoil
	#max_induced_phase_voltage = turnsPerCoil*back_emf_rms*sqrt(2)
	#max_input_phase_current = current_through_slot/turnsPerCoil	

	#l_coil = 2*(l+(pi*end_winding_radius))*turnsPerCoil
	#l_total = (coilsTotal/3)*l_coil

	#Ra_analytical = (wire_resistivity*l_total)/(area_available_per_turn/1e6)
	#max_input_phase_voltage = max_induced_phase_voltage + max_input_phase_current*Ra_analytical	
    
	#if debug == 1:
	  #print("\n(3) Maximum number of turns allowed N = %d turns"%turnsPerCoil)
	  #print("(3) Peak voltage induced by all turns V_a(ln) = %f [V]"%max_induced_phase_voltage)
	  #print("(3) Peak input phase current Ia = %f [A]"%max_input_phase_current)
	  #print("(3) Ra_analytical = %f [Ohm]"%Ra_analytical)
	  #print("(3) Peak system input phase voltage V_a(ln) = %f [V]"%max_input_phase_voltage)       
    
    #current_through_slot = max_input_phase_current*turnsPerCoil
    #P_conductive_analytical = (3/2)*(max_input_phase_current**2)*Ra_analytical
    #if debug == 1:
      #print("(3) Total analytical conductive (copper) losses P_cu = %f [W]\n"%(P_conductive_analytical)) 
    
    ##S = 3*Va_rms*Ia_rms
    #S = (3/2)*max_input_phase_voltage*max_input_phase_current
    #P = (3/2)*(vd*id + vq*iq)  + P_conductive_analytical
    ##P = (3/2)*(vd*id + vq*iq) 		#inlcudes P_out and P_conductive (does not include other losses)
    #Q= sqrt(S**2 - P**2)
    #if S !=0:
	#power_factor=(P/S)*100
    #else:
	#power_factor=0
    ######################   

checkpoint.append(time.time())
#################################
# Calculate eddy current losses #
#################################
#As of yet, eddy current losses only take into account Br,Bt, harmonics, and variation of Br and Bt at various positions in coil height
if enable_eddy_losses_calc == 1:
  pretty_eddy = PrettyTable(['Eddy Current Variable','Value','Unit'])
  pretty_eddy.align = 'l'
  pretty_eddy.border= True  
  #pretty_eddy.add_row(["INPUTS",0,""])
  pretty_eddy.add_row(["INPUTS","------------","------------"])
  pretty_eddy.add_row(["Turns per coil per phase (N)",turnsPerCoil,"turns"])
  pretty_eddy.add_row(["Total number of stator coils (Q)",coilsTotal,""])
  pretty_eddy.add_row(["Number of parallel strands (nc)",parallel_stranded_conductors,""])
  pretty_eddy.add_row(["Active conductor length",l,"[m]"])
  pretty_eddy.add_row(["Wire diameter",wire_diameter,"[m]"])
  pretty_eddy.add_row(["Peak fundamental flux density (B_1_pk)",Br,"[T]"])
  pretty_eddy.add_row(["Fundamental electrical angular frequency (We)",2*pi*f_e,"[rad/s]"])
  #wire_resistivity = 1.7e-8
  pretty_eddy.add_row(["Copper resistivity (p)",wire_resistivity,"[Ohm.m]"])
  
  M = 1024
  fplfile = dir_parent+"project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl"
  layer_range = linspace(ri+hmi_r+gi, ri+hmi_r+gi+hc, num=number_of_layers_eddy, endpoint=True)
  Bm, Br_middle,Bt_middle = sf.get_arc_B(fplfile, 0+rotate_all_radians,2*mechRotation+rotate_all_radians, ri+hmi_r+gi+(0.5*hc), 2*M) #Br_middle is the Instantaneous (or rather spacial) value of Br_middle. Br_middle can thus be used to draw sinusoidal-like curve.
  Br_middle_fft = abs(np.fft.rfft(Br_middle,M)*2/M) #get the peak values of all the harmonics
  Bt_middle_fft = abs(np.fft.rfft(Bt_middle,M)*2/M) #get the peak values of all the harmonics
  #print("Br_middle_fft[1] = %f"%Br_middle_fft[1])
  #print("Bt_middle_fft[1] = %f"%Bt_middle_fft[1])
  
  #print "layer range"
  #print layer_range
  #print "NQa"
  NQa = turnsPerCoil * coilsTotal * parallel_stranded_conductors
  #print(NQa)
  #wire_resistivity = 1.678e-8 #the larger the wire resistivity, the smaller the eddy currents, and thus less losses
  #thus heat is good for reducing eddy current losses
  piLd_div_16rho = (pi*l*(wire_diameter**4))/(16*wire_resistivity)
  #print(pi)
  #print(l)
  #print(wire_diameter**4)
  #print(wire_resistivity)
  #print(piLd_div_16rho)
  
  pretty_layer = PrettyTable(['Layer number','Br_layer_fft(max)','Bt_layer_fft(max)'])#,'Layer 3','Layer 4','Layer 5'])
  pretty_layer.align = 'l'
  pretty_layer.border= True      
  
  
  P_eddy = 0
  number_of_conductors_in_layer = (2*turnsPerCoil*parallel_stranded_conductors)/number_of_layers_eddy		#the factor 2 is inserted because each turn has a wire facing the upward axial direction and a wire facing the downward axial direction within a single layer
													      #this is also why Stegmann's 2011 paper had a divide by 16 factor instead of a 32
  total = 0
  conductors_per_layer_per_coil_side = (turnsPerCoil*parallel_stranded_conductors)/number_of_layers_eddy
  hos = 0
  for j in range (1, number_of_layers_eddy+1): #for each layer
    Bm, Br_layer,Bt_layer = sf.get_arc_B(fplfile, 0+rotate_all_radians,2*mechRotation+rotate_all_radians, layer_range[j-1], 2*M)
    #print max(Bt_layer)
    #print("\nlayer number = %d"%j)
    Br_layer_fft = abs(np.fft.rfft(Br_layer,M)*2/M)
    Bt_layer_fft = abs(np.fft.rfft(Bt_layer,M)*2/M)
    
    #print("Br_layer_fft(max) = %f"%max(Br_layer_fft))
    #print("Bt_layer_fft(max) = %f"%max(Bt_layer_fft))
    
    #pretty_layer.add_row(['%d'%j,max(Br_layer_fft),max(Bt_layer_fft)])
    
    B_component = 0
    for i in range(1, max_harmonics_eddy+1): #for each harmonic
      B_component = B_component + (i**2)*((Br_layer_fft[i]/sqrt(2))**2 + (Bt_layer_fft[i]/sqrt(2))**2)
    
    hos = hos + conductors_per_layer_per_coil_side*B_component
    
  P_eddy = ((pi * l * (wire_diameter**4) * (2*pi*f_e)**2)/(64*wire_resistivity))*hos 		#this is then the eddy current losses for a single coil side of only one phase  
											      #now P_eddy includes the losses from both coil sides, of all the total coil sides in the machine
  #P_eddy = 2*coilsTotal*P_eddy #correct according to literature, but gives answer that is 2x bigger than measured result
  
  P_eddy = coilsTotal*P_eddy
  
  #print(coilsTotal)
  #print(wire_diameter)
  #print(f_e)
  #print(wire_resistivity)
  #print(hos)
  #print(B_component)
  print("Eddy current losses (P_eddy) = %.2f [W]"%(P_eddy))
  print("Wire resistivity (Eddy Calc) = %f e-8"%(wire_resistivity*1e8))
    ##print("layer = %d"%j)
    #for i in range (1, max_harmonics_eddy+1):  #for each harmonic
      ##print("harmonic number = %d"%i)
      ##print "P_eddy_temp"
      ##print piLd_div_16rho*(i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)
      #squared_sum = (Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)
      ##print("squared_sum = %f"%squared_sum)
      #squared_sum_times_squared_freq = ((2*pi*f_e*i)**2)*squared_sum
      ##print("squared_sum_times_squared_freq = %f"%squared_sum_times_squared_freq)
      #times_everything_else = (NQa/number_of_layers_eddy) * piLd_div_16rho * squared_sum_times_squared_freq
      ##print("Power dissipated by this harmonic and this layer = %f [W]"%times_everything_else)
      #total = total + times_everything_else
      #P_eddy = P_eddy + (i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)

  #print("Total eddy losses = %f [W]"%total)

  #P_eddy = coilsTotal * number_of_conductors_in_layer * ((pi*l*(wire_diameter**4)*(2*pi*f_e)**2)/(32*wire_resistivity)) * P_eddy
  #pretty_eddy.add_row(["OUTPUTS","------------","------------"])
  
  #p_eddy_1 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*(Br_middle_fft[1]**2))/(wire_resistivity)
  
  #pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/16",p_eddy_1/16,"[W]"])
  #pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/32",p_eddy_1/32,"[W]"])
  #pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/64",p_eddy_1/64,"[W]"])
  #pretty_eddy.add_row(["p_eddy_1 (no harmonics, only radial flux, no layers)/128",p_eddy_1/128,"[W]"])
  #pretty_eddy.add_row(["------------","------------","------------"])
  
  #p_eddy_2 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*(Br_middle_fft[1]**2 + Bt_middle_fft[1]**2))/(wire_resistivity)
  
  #pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/16",p_eddy_2/16,"[W]"])
  #pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/32",p_eddy_2/32,"[W]"])
  #pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/64",p_eddy_2/64,"[W]"])
  #pretty_eddy.add_row(["p_eddy_2 (no harmonics, no layers)/128",p_eddy_2/128,"[W]"])    
  #pretty_eddy.add_row(["------------","------------","------------"])
  
  #squared_sum = 0
  #for i in range (1, max_harmonics_eddy+1):
    #squared_sum = squared_sum + (i**2)*(Br_middle_fft[i]**2 + Bt_middle_fft[i]**2)
    #print("Br_middle_fft[%d] = %f"%(i,Br_middle_fft[i]))
    #print("Bt_middle_fft[%d] = %f"%(i,Bt_middle_fft[i]))
  
  #p_eddy_3 = (2*turnsPerCoil * coilsTotal * parallel_stranded_conductors * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2)*squared_sum)/(wire_resistivity)
  
  #pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/16"%(max_harmonics_eddy),p_eddy_3/16,"[W]"])
  #pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/32"%(max_harmonics_eddy),p_eddy_3/32,"[W]"])
  #pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/64"%(max_harmonics_eddy),p_eddy_3/64,"[W]"])
  #pretty_eddy.add_row(["p_eddy_3 (%d harmonics, no layers)/128"%(max_harmonics_eddy),p_eddy_3/128,"[W]"])    
  #pretty_eddy.add_row(["------------","------------","------------"])
  
  #total = 0
  #layer_range = linspace(ri+hmi_r+gi, ri+hmi_r+gi+hc, num=number_of_layers_eddy, endpoint=True)
  #conductors_per_layer = (2*turnsPerCoil*coilsTotal*parallel_stranded_conductors)/number_of_layers_eddy
  #p_eddy_4 = 0
  #for j in range (1, number_of_layers_eddy+1): #for each layer
    #Bm, Br_layer,Bt_layer = sf.get_arc_B(fplfile, 0,2*mechRotation, layer_range[j-1], 2*M)
    #Br_layer_fft = abs(np.fft.rfft(Br_layer,M)*2/M)
    #Bt_layer_fft = abs(np.fft.rfft(Bt_layer,M)*2/M)
    
    #squared_sum = 0
    #for i in range (1, max_harmonics_eddy+1):  #for each harmonic
      #squared_sum = squared_sum + (i**2)*(Br_layer_fft[i]**2 + Bt_layer_fft[i]**2)

    #p_eddy_4 = conductors_per_layer * squared_sum

  #p_eddy_4 = (p_eddy_4 * pi * l * (wire_diameter**4) * ((2*pi*f_e)**2))/(wire_resistivity)

  #pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/16"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/16,"[W]"])
  #pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/32"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/32,"[W]"])
  #pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/64"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/64,"[W]"])
  #pretty_eddy.add_row(["p_eddy_4 (%d harmonics, %d layers)/128"%(max_harmonics_eddy,number_of_layers_eddy),p_eddy_4/128,"[W]"])    
  #pretty_eddy.add_row(["------------","------------","------------"])    
  
else:
  P_eddy = 0
  

  
if enable_general_calc == 1:
  checkpoint.append(time.time())
  #CALCULATE I^2R CONDUCTION LOSSES (SEMFEM)
  if steps != 1:
    P_conductive_semfem=3*sf.semfem_copper_loss_vec[:,0]	#multiplying with 3, assuming that each phase has the exact same copper losses
    P_conductive_semfem=sum(P_conductive_semfem)/steps
    P_core_loss=sum(sf.semfem_avg_core_loss_vec)
  else:
    P_conductive_semfem = sf.semfem_copper_loss_vec[0,0] + sf.semfem_copper_loss_vec[0,1] + sf.semfem_copper_loss_vec[0,2]	#power of each phase is added. 3phase power is constant.
    P_core_loss=0
    
  P_conductive = P_conductive_semfem*(1+ewf)	#scale the losses calculated by SEMFEM using only the stack_length  
  #####################   
  
  checkpoint.append(time.time())
  #CALCULATE EFFICIENCY
  #machine_speed = (60*f_e)/p # 60*f_e/Qr   (Qr = 10)		#should be 290rpm
  P_out = (n_rpm*(pi/30))*average_torque_dq
  P_losses = P_conductive_analytical + P_core_loss + P_eddy + P_wf
  #if steps == 1:
    #P_losses = P_conductive_analytical + P_core_loss + P_eddy + P_wf
  #else:
    #P_losses = P_conductive_semfem* + P_core_loss + P_eddy + P_wf
  P_in = P_out + P_losses
  
  if P_in != 0:
      efficiency = (P_out/P_in) * 100
  else:
      efficiency = 0
  #####################      

############################
# Output data to CSV files #
############################

checkpoint.append(time.time())

if enable_flux_density_harmonic_calc == 1:
  savetxt(dir_parent+'script_results/idrfpm/br.csv', transpose([Br[0],Br[1],Br[2]]), delimiter = ',',newline='\n',header='Br @(rn-h/2), Br @(rn), Br @(rn+h/2)',fmt='%f')
else:
  max_Br_1=0
  Br1=0
  Br_THD=0
  
if active_parts != MAGNETS_ONLY and steps != 1:
  savetxt(dir_parent+'script_results/idrfpm/voltage.csv', transpose([sfm_post.voltage[0],sfm_post.voltage[1],sfm_post.voltage[2]]), delimiter = ',',newline='\n',header='Va, Vb, Vc',fmt='%f')

    
checkpoint.append(time.time())
#this section should only execute if optimization_output.csv already exists and we just want to APPEND to the file
if enable_optimization_csv_output == 1:
  with open(dir_parent+'script_results/idrfpm/optimization_output.csv',"a") as opt_output_file:
    if active_parts != MAGNETS_ONLY and steps != 1:
      opt_output_file.write('%g, %g, %g, %g, %g, %g, %g ,%g ,%g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g, %g\n'%(p, kq, l, g, hc, kmo, Brem, muR, f_e, id, iq, number_of_layers_eddy, max_harmonics_eddy, wire_diameter, parallel_stranded_conductors, wire_resistivity, active_parts, turnsPerCoil, fill_factor,max_Br_1,Br1,Br_THD*100,P_eddy,magnet_mass,copper_mass,P_core_loss,max_input_phase_current,max_input_phase_voltage,max_induced_phase_voltage,max(sf.semfem_flink_vec[:,0]),efficiency,average_torque_dq,P,P_out,P_losses,coil_side_area))
    else:
      opt_output_file.write('%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g,%g\n'%(p,kq,l,g,hmi_r,hc,hmo_r,kmo,kc,ri,ro,number_of_layers_eddy,wire_diameter,parallel_stranded_conductors,turnsPerCoil,fill_factor,J,max_input_phase_current,max_induced_phase_voltage,max_input_phase_voltage,average_torque_dq,magnet_mass,copper_mass,Ra_analytical,efficiency,S,Q,P,P_out,P_conductive_analytical,P_eddy,P_wf,P_core_loss,P_losses,power_factor,coil_side_area,max_Br_1,Br1,Br_THD*100))

checkpoint.append(time.time())
#################
# Print results #
#################
if enable_terminal_output == 1:
  if enable_general_calc == 1 and active_parts != MAGNETS_ONLY:# and steps != 1:
    addToTable("Peak current through single slot",current_through_slot,"[A]",True,dir_parent)
    addToTable("J_peak",J,"[A/mm^2]",True,dir_parent)      
    addToTable("iq", iq, "[A]", True,dir_parent)
    addToTable("id", id, "[A]", True,dir_parent)
    addToTable("RMS induced phase voltage (dq)", back_emf_rms_dq, "[V]", True,dir_parent)
    addToTable("Peak induced phase voltage (dq)", back_emf_rms_dq*sqrt(2), "[V]", True,dir_parent)
    addToTable("Peak input phase current", max_input_phase_current, "[A]", True,dir_parent)
    addToTable("Peak induced phase voltage",max_induced_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
    addToTable("Peak input phase voltage",max_input_phase_voltage, "[V]", True,dir_parent) 	#phase A max induced voltage
    addToTable("Peak flux linkage",max(sf.semfem_flink_vec[:,0])*1000, "[mWb]", True,dir_parent)
    #if active_parts == WINDINGS_ONLY:
    addToTable("Synchronous Inductance single turn",synchronous_inductance_single_turn,"[uH]", True ,dir_parent)
    addToTable("Synchronous Inductance (Ls)",Ls*1e6,"[uH]", True ,dir_parent)
    if active_parts == WINDINGS_ONLY:
      addToTable("Synchronous Inductance single turn (AR)",synchronous_inductance_single_turn_AR,"[uH]", True ,dir_parent)
      addToTable("Synchronous Inductance (Ls_AR)",Ls_AR*1e6,"[uH]", True ,dir_parent)    
    addToTable("Average torque produced (semfem)",average_torque,"[Nm]", steps != 1,dir_parent)
    addToTable("*********Average torque produced (dq quantities)********",average_torque_dq,"[Nm]", True,dir_parent)
    addToTable("*******************Magnet mass**************************",magnet_mass,"[kg]", True,dir_parent)
    addToTable("Copper mass",copper_mass,"[kg]", True,dir_parent)
    addToTable("Aluminium mass",aluminium_mass,"[kg]", True,dir_parent)
    addToTable("---Outer rotor shell mass",outer_rotor_shell_mass,"[kg]", True,dir_parent)
    addToTable("---Inner rotor shell mass",inner_rotor_shell_mass,"[kg]", True,dir_parent)
    addToTable("Tufnol mass",tufnol_mass,"[kg]", True,dir_parent)
    addToTable("Total mass",total_mass,"[kg]", True,dir_parent)
    #addToTable("Phase resistance",Ra,"[Ohm]", True ,dir_parent)
    #if steps == 1:
    addToTable("Phase resistance (analytical)",Ra_analytical,"[Ohm]", True,dir_parent)
    #addToTable("Cogging torque",cogging,"[Nm]", True,dir_parent)
    addToTable("Electrical frequency",f_e,"[Hz]", True,dir_parent)
    addToTable("Machine speed",n_rpm,"[rpm]", True,dir_parent)
    addToTable("**********************Efficiency************************",efficiency,"[%]", True,dir_parent)
    addToTable("Output Torque/Mass Ratio",(average_torque_dq/(total_mass)),"[Nm/kg]", True,dir_parent)
    addToTable("Output Torque/PM Mass Ratio",(average_torque_dq/magnet_mass),"[Nm/kg]", True,dir_parent)
    #addToTable("Input Apparant Power (S)",S,"[VA]", True,dir_parent)
    #if steps == 1:
      #addToTable("Input Reactive Power (Q)",Q,"[VAR]", True,dir_parent)
    addToTable("Input Real Power (mean value of instantaneous power over full period) (P)",P,"[W]", steps != 1,dir_parent)
    addToTable("Input Real Power (P_in=P_out_dq+P_losses)",P_in,"[W]", True,dir_parent)
    #addToTable("Input Real Power (P)",P,"[W]", True,dir_parent)
    addToTable("Output Mechanical Power",P_out,"[W]", True,dir_parent)
    #if steps == 1:
    addToTable("Conductive loss (analytical with end-windings)",P_conductive_analytical,"[W]", True,dir_parent)
    #addToTable("Conductive loss (SEMFEM)",P_conductive_semfem,"[W]", True,dir_parent)
    #addToTable("Conductive loss (with end-windings)",P_conductive,"[W]", True,dir_parent)
    addToTable('Eddy current losses',P_eddy,'[W]', enable_eddy_losses_calc == 1,dir_parent)
    addToTable("Wind & Friction losses",P_wf,"[W]", True,dir_parent)
    addToTable("Core loss",P_core_loss,"[W]", True,dir_parent)					#core loss will be zero because there is no B-H hysteresis effect for an ironless machine  
    addToTable("Total losses",P_losses,"[W]", True,dir_parent)
    if steps == 1:
      addToTable("Power factor",power_factor,"[%]", True,dir_parent)
    addToTable("Fill factor",fill_factor,"[p.u.]",True,dir_parent)
    addToTable("Coil side area",coil_side_area*1000**2,"[mm^2]",True,dir_parent)
    #if steps == 1:
    addToTable("Area available per Turn (including fill factor)",area_available_per_turn,"[mm^2]",True,dir_parent)
    addToTable("Area required per Turn (Litz with parallel strands)",single_turn_area*1000**2,"[mm^2]",True,dir_parent)
    addToTable("Turns per coil (N)",turnsPerCoil,"[Turns]",True,dir_parent)
    addToTable("Coil temperature",coil_temperature,"[Deg. C]",True,dir_parent)
    if steps == 1:
      addToTable("Coil winding length",l_coil,"[m]",True,dir_parent)
      addToTable("Phase winding length",l_total,"[m]",True,dir_parent)
    addToTable("Copper resistivity",wire_resistivity,"[Ohm.m]",True,dir_parent)
    
    #addToTable("Active slot area",active_slot_area*1000**2,"[mm^2]",True,dir_parent)
    #addToTable("Achieved fill factor",active_slot_area/coil_side_area,"[p.u.]",True,dir_parent)
	
  else:
    addToTable("Estimated torque produced (dq quantities)",average_torque_dq,"[Nm]", True,dir_parent)
    
  
  if enable_flux_density_harmonic_calc == 1:
    addToTable('Brmax|rn',max_Br_1,'[T]', True,dir_parent)
    addToTable('Br1|rn',Br1,'[T]', True,dir_parent)
    addToTable('THD(Br|rn)',(Br_THD*100),'[%]', True,dir_parent)


if enable_terminal_output == 1:
  save_stdout = sys.stdout
  sys.stdout = open(dir_parent+"pretty_terminal_table.txt", "wb")
  print pretty_terminal_table
  sys.stdout = save_stdout    
  
  if VERBOSITY == 1:
    print pretty_terminal_table
    if enable_eddy_losses_calc == 1:
      print pretty_eddy
      print pretty_layer
      pretty_eddy.clear_rows()
      pretty_layer.clear_rows()

  pretty_terminal_table.clear_rows()

  #pretty_terminal_table = None


#os.system("post.py --voltage project_idrfpm_semfem/results/post.res -f %f" % (f_e))	#brings up all the voltage plots, for more usage and options to use this command, see
											    #SEMFEM_git/SEMFEM/core/plotter/post.py
								
#os.system("post.py project_idrfpm_semfem/results/post.res -f %f" % (f_e))  

######################
#SAVE PDF FILE OF FPL FILES
use_this_fpl_number = 0
fplfiles = []
for i in range(0,steps):  
  fplfiles.append("project_idrfpm_semfem/results/fieldplots/band_machine_field%003d.fpl"%(i+1))

  #os.system("fpl2svg --help")
  #fpl2svg -- convert SEMFEM fpl files to svg

    #-d, --fluxdensity          Show flux density colormap
    #-l, --fluxlines            Show flux lines
    #-m, --mesh                 Show the mesh
    #-n, --nlines=<value>       Specify the number of flux lines
    #-s, --strokewidth=<value>  Specify the line width [mm] used for drawing triangles
    #-w, --fluxwidth=<value>    Specify the line width [mm] used for drawing flux lines
    #-?, --help                 Give this help list
    #    --usage                Give a short usage message
if save_fpl2pdf == 1:
  fpl2pdf_time_start = time.time()
  #use_this_fpl_number = steps-1
  
  os.system("fpl2svg %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_plain"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -e --export-dpi=400 %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_plain"+".png",fplfiles[use_this_fpl_number][:-3]+"svg"))
  print "HOS HOS HOS"
  os.system("fpl2svg -l -n 25 -w 0.15 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_plain_flux_lines"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))  

  #os.system("fpl2svg 0 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("fpl2svg -m -s 0.01 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_mesh"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))

  os.system("fpl2svg -ld -n 25 -w 0.15 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_flux"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))  

  os.system("fpl2svg -lo -n 25 -w 0.15 %s %s"%(fplfiles[use_this_fpl_number],fplfiles[use_this_fpl_number][:-3]+"svg"))
  os.system("inkscape -A %s %s"%(fplfiles[use_this_fpl_number][:-4]+"_A_flux_lines"+".pdf",fplfiles[use_this_fpl_number][:-3]+"svg"))   

checkpoint.append(time.time())  

if save_csv:
  np.savetxt('semfem_script_output.csv', transpose([sf.semfem_time_vec,sf.semfem_torque_vec[:,1]]),delimiter = ',',newline='\n',header='time_stamp,semfem_torque',fmt='%8f')

sf.semfem_end()

#os.system("fpl2 project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl")
if show_all:
  os.system("post.py project_idrfpm_semfem/results/post.res")

#if enable_show_iteration_number == True:
  #sys.stdout.write("SEMFEM simulations executed: %4d\r" %(iteration_number))
  #print("SEMFEM simulations executed: %4d\r" %(iteration_number))


with open(dir_parent+'script_results/main/running_flags.csv', 'wb') as running_flags_file:
  running_flags_file.write("iteration_number,%d"%(iteration_number+1)) 

#return 1

if store_results_in_database == 1 and overwrite_previous_result == 1:
  conn.commit()
conn.close()

#if __name__ == '__main__': #when executed by itself
  ##main('input_original.csv')
  #main(input_file)
