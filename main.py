#!/usr/bin/python

#Title : Main script which controls the semfem script, analytical script and optimization processes.
#Author: Andreas Joss

from __future__ import division

import numpy as np
import pylab as pl
import idrfpm_semfem
import qhdrfapm
import time
import os,sys
import shutil
from prettytable import PrettyTable
import cairosvg
import threading

reload(idrfpm_semfem)
reload(qhdrfapm)

#########################
# Variable declarations #
#########################

enable_animate_optimization = False
enable_ideal_value_calculation = False

#function_evaluation_script = 'idrfpm'
function_evaluation_script = 'qhdrfapm'

if function_evaluation_script == 'qhdrfapm':
  enable_animate_optimization = False

file_input_name = 'input_edited.csv'
#file_output_name = './script_results/'+function_evaluation_script+'/optimization_output.csv'



itr = 0.025
processes = 1


LINEWIDTH=1

SEMFEM = 1
ANALYTIC = 2
BOTH = 3

#define constants, using Python Tuples (Tuples are immutable)
P  = 0,"p","[pairs]"
KQ = 1,"kq","[ratio]"
#RN = 2,"rn","[m]"	#this parameter has become obsolete
#HY = 3,"hy","[m]"
#HM = 4,"hm","[m]"	#this parameter has become obsolete
L  = 5,"l","[m]"
G  = 6,"g","[m]"
HC  = 7,"hc","[m]"
KMO = 8,"kmo","[ratio]"
BR = 9,"Brem","[T]"
MUR= 10,"muR","[ur]"

F_E= 11,"f_e","[Hz]"
ID = 12,"id","[A]"
IQ = 13,"iq","[A]"
NUMBER_OF_LAYERS_EDDY = 14,"number_of_layers_eddy","[unitless]"
MAX_HARMONICS_EDDY = 15,"max_harmonics_eddy","[unitless]"
WIRE_DIAMETER = 16,"wire_diameter","[m]"
PARALLEL_STRANDED_CONDUCTORS = 17,"parallel_stranded_conductors","[unitless]"
WIRE_RESISTIVITY = 18,"wire_resistivity","[Ohm.m]"
ACTIVE_PARTS = 19,"active_parts","[option]"
TURNS_PER_COIL = 20,"turns_per_coil","[turns]"
FILL_FACTOR = 21,"fill_factor","[unitless]"
STEPS = 22,"steps","[unitless]"
#ENABLE_SMC_CORED_COILS = 23,"enable_smc_cored_coils","[option]"
#ENABLE_SUB_AIR_GAPS = 24,"enable_sub_air_gaps","[option]"
#ENABLE_GENERAL_CALC = 25,"enable_general_calc","[option]"
#ENABLE_EDDY_LOSSES_CALC = 26,"enable_eddy_losses_calc","[option]"
#ENABLE_FLUX_DENSITY_HARMONIC_CALC = 27,"enable_flux_density_harmonic_calc","[option]"
#ENABLE_CSV_OUTPUT = 28,"enable_csv_output","[option]"
#ENABLE_TERMINAL_OUTPUT = 29,"enable_terminal_output","[option]"
#ENABLE_OPTIMIZATION_CSV_OUTPUT = 30,"enable_optimization_csv_output","[option]"
J = 31,"J","[A/mm^2]"
KC = 32,"kc","[unitless]"
PEAK_VOLTAGE_ALLOWED = 33,"peak_voltage_allowed","[V]"
COIL_TEMPERATURE = 34,"coil_temperature","[Deg. C]"
#MIN_WIDTH = 35,"min_width","[m]"
HMO_R = 36,"hmo_r","[m]"
HMI_R = 37,"hmi_r","[m]"
RI = 38,"ri","[m]"
RO = 39,"ro","[m]"
#X3 = 40,"x3","[unitless]"
#X2 = 41,"x2","[unitless]"
#X1 = 42,"x1","[unitless]"
GO = 53,"go","[m]"
GI = 54,"gi","[m]"
X_RI  = 55,"x_ri", "[unitless]"
X_HMO_R = 56,"x_hmo_r","[unitless]"
X_GO  = 57,"x_go", "[unitless]"
X_HC  = 58,"x_hc", "[unitless]"
X_GI  = 59,"x_gi", "[unitless]"
X_HMI_R = 60,"x_hmi_r","[unitless]"
KMI = 62,"kmi","[ratio]"
X_HMO_RT = 63,"x_hmo_rt","[ratio]"
X_HMI_RT = 64,"x_hmi_rt","[ratio]"

i = 0


input_original = np.genfromtxt('./input_original.csv',skip_header=1,delimiter=',',usecols=(1),unpack=True)


#running flag variables
ITERATION_NUMBER = 0

PRINT_READY = [0,0,0,0]
progress_string = []

pretty_multi_proc_table = PrettyTable(['Process number','Progress count','SEMFEM simulations for this count'])
pretty_multi_proc_table.align = 'l'
pretty_multi_proc_table.border = True  


for i in range(1,processes+1):
  progress_string.append([])

#if processes != 1:
dir_temp = './mp/p%d/'%(i)
if not os.path.isdir(dir_temp): os.makedirs(dir_temp)
shutil.copy('input_original.csv',dir_temp+file_input_name)

if enable_animate_optimization == True:
  dir = dir_temp+'script_results/main/optimization_animation/svg/'
  if not os.path.isdir(dir): os.makedirs(dir)
  delete_dir = dir+'*'
  os.system("rm %s"%(delete_dir))
  
  dir = dir_temp+'script_results/main/optimization_animation/png/'
  if not os.path.isdir(dir): os.makedirs(dir)  
  delete_dir = dir+'*'
  os.system("rm %s"%(delete_dir))

dir2 = dir_temp+'script_results/main/'
if not os.path.isdir(dir2): os.makedirs(dir2)
dir2 = dir_temp+'script_results/'+function_evaluation_script+'/'
if not os.path.isdir(dir2): os.makedirs(dir2)

with open(dir_temp+'script_results/main/running_flags.csv', 'w') as running_flags_file:
  running_flags_file.write("iteration_number,%d"%ITERATION_NUMBER)
#else:
  #shutil.copy('input_original.csv',file_input_name)

  #if enable_animate_optimization == True:
    #dir = 'script_results/main/optimization_animation/svg/'
    #if not os.path.isdir(dir): os.makedirs(dir)
    #delete_dir = dir+'*'
    #os.system("rm %s"%(delete_dir))
    
    #dir = 'script_results/main/optimization_animation/png/'
    #if not os.path.isdir(dir): os.makedirs(dir)  
    #delete_dir = dir+'*'
    #os.system("rm %s"%(delete_dir))

  #dir2 = 'script_results/main/'
  #if not os.path.isdir(dir2): os.makedirs(dir2)
  #dir2 = 'script_results/'+function_evaluation_script+'/'
  #if not os.path.isdir(dir2): os.makedirs(dir2)

  #with open('script_results/main/running_flags.csv', 'w') as running_flags_file:
    #running_flags_file.write("iteration_number,%d"%ITERATION_NUMBER)  


## with is like your try .. finally block in this case
#with open(file_input_name, 'r') as file:
    ## read a list of lines into data
    #data = file.readlines()

#with open(file_input_name, 'r') as file:
    ## read a list of lines into data
    #backup_file = file.readlines()

#hmo_r = np.array(map(float, data[HMO_R[0]+1].split(",")[1:]))
#hmi_r = np.array(map(float, data[HMI_R[0]+1].split(",")[1:]))
##x3 = np.array(map(float, data[RI[0]+1].split(",")[1:]))
##x2 = np.array(map(float, data[RI[0]+1].split(",")[1:]))
##x1 = np.array(map(float, data[RI[0]+1].split(",")[1:]))
#ro = np.array(map(float, data[RO[0]+1].split(",")[1:]))
#ri = np.array(map(float, data[RI[0]+1].split(",")[1:]))
#p = np.array(map(float, data[P[0]+1].split(",")[1:]))
#kq = np.array(map(float, data[KQ[0]+1].split(",")[1:]))
#l = np.array(map(float, data[L[0]+1].split(",")[1:]))
#g = np.array(map(float, data[G[0]+1].split(",")[1:]))
#hc = np.array(map(float, data[HC[0]+1].split(",")[1:]))
#kmo = np.array(map(float, data[KMO[0]+1].split(",")[1:]))
#kmi = np.array(map(float, data[KMI[0]+1].split(",")[1:]))
#kc = np.array(map(float, data[KC[0]+1].split(",")[1:]))
##min_width = np.array(map(float, data[MIN_WIDTH[0]+1].split(",")[1:]))


def replaceLine(key,newValue):
  if type(newValue) == int:
    # now change the 2nd line, note that you have to add a newline
    data[key[0]+1] = '%s,%d,%d,%d\n'%(key[1],newValue,float(data[key[0]+1].split(",")[2]),float(data[key[0]+1].split(",")[3]))
  elif type(newValue) == float:
    # now change the 2nd line, note that you have to add a newline
    data[key[0]+1] = '%s,%s,%s,%s\n'%(key[1],newValue,float(data[key[0]+1].split(",")[2]),float(data[key[0]+1].split(",")[3])) #should use string because otherwise value of small decimal numbers can be lost

def replaceFile():    
  # and write everything back
  with open(file_input_name, 'w') as file:
      file.writelines( data )    

#determine entire loop_array structure
w_eff = 0
w_tor = 0
w_pm = 0

w_pm_init = 0
w_tor_init = 0
w_eff_init = 1

div_o = int(1/itr)
div_i = div_o
loop_array = []
i = 0
j = 0
while i<div_o+1:
  
  j = i
  while j<div_i+1:
    w_tor = w_tor_init + j*itr - w_pm
    w_eff = abs(1 - w_tor - w_pm)  
    loop_array.append([w_eff,w_tor,w_pm])
    #print("w_eff = %.3f, w_tor = %.3f, w_pm = %.3f"%(w_eff,w_tor,w_pm))
    
    j = j + 1

  i = i + 1
  w_pm = w_pm_init + i*itr


#determine start and stop index for each process
start_end_index = []
end_num = -1
for p in range (1,processes+1):
  #start_num = int(((p-1)/processes) *len(loop_array))
  start_num = end_num+1
  end_num = int((p/processes) *len(loop_array))
  
  start_end_index.append([start_num,end_num])

#print start_end_index



#thread function prototype
def optimizeThread(p,loop_array,start_end_index):
  all_ready = 0
  print("thread %d initiated, thread active count %d"%(p,threading.active_count()))
 
  start_num = start_end_index[p-1][0]
  end_num = start_end_index[p-1][1]
  count = 0
  dir_parent = './mp/p%d/'%(p)

  #print("thread %d:: start at %d and end at %d"%(p,start_num,end_num))
  
  for i in range(start_num, end_num+1):
    w_eff = loop_array[i][0]
    w_tor = loop_array[i][1]
    w_pm = loop_array[i][2]
    
    #check if this datapoint was already evaluated
    dir = 'optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    if not os.path.isdir(dir):
      os.makedirs(dir)
      
      #write the weight values to text file
      np.savetxt(dir_parent+'objective_weights.csv', np.transpose([[abs(w_eff)],[abs(w_tor)],[abs(w_pm)]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')
      
      print("thread num: %d"%p)
      progress_string[p-1] = "%d/%d"%(count,end_num-start_num)
      
      #run optimization process
      os.system("./optimization.py -t %d"%(p))
      os.system("./post_script.py 14 -t %d"%(p))      
      
      count = count + 1
      #pretty_multi_proc_table.add_row([p,progress_string])
      #PRINT_READY[p-1] = 1
      
      
      #for k in range (0, len(start_end_index)):
	#if PRINT_READY[k] == 1:
	  #all_ready = all_ready + 1
      
      #if all_ready == len(start_end_index):
	#for k in range (0, len(start_end_index)):
	  #PRINT_READY[k] = 0
	#all_ready = 0
	#print pretty_multi_proc_table
	#pretty_multi_proc_table.clear_rows()
      
      shutil.copy('input_original.csv',dir+'/input_original.csv')
      #shutil.copy(dir_parent+'script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
      #shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
      #shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
      shutil.copy(dir_parent+'optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
      shutil.copy(dir_parent+'f_data.csv',dir+'/f_data.csv')
      #shutil.copy(dir_parent+'ideal_solutions.csv',dir+'/ideal_solutions.csv')
      shutil.copy(dir_parent+'solution.txt',dir+'/solution.txt')

      #copy all the png animation files to a new folder
      if enable_animate_optimization == True:
	dest = 'optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
	if not os.path.isdir(dest): os.makedirs(dest)  
	src = dir_parent+'script_results/main/optimization_animation/png/'
	src_files = os.listdir(src)
	for file_name in src_files:
	    full_file_name = os.path.join(src, file_name)
	    if (os.path.isfile(full_file_name)):
		shutil.copy(full_file_name, dest)  

      #delete all old animation files
	dir = dir_parent+'script_results/main/optimization_animation/svg/'
	if not os.path.isdir(dir): os.makedirs(dir)
	delete_dir = dir+'*'
	os.system("rm %s"%(delete_dir))
	
	dir = dir_parent+'script_results/main/optimization_animation/png/'
	if not os.path.isdir(dir): os.makedirs(dir)  
	delete_dir = dir+'*'
	os.system("rm %s"%(delete_dir))

    #reset iteration number to zero
    with open(dir_parent+'script_results/main/running_flags.csv', 'w') as running_flags_file:
      running_flags_file.write("iteration_number,%d"%0)  
  
  print("thread %d finished, thread active count %d"%(p,threading.active_count()))
    
########################
# Function definitions #
########################
def perturb(parameter,perturb_range,calc_type):
  #updateInputFile()
  
  #declare arrays used for graphs
  if calc_type == SEMFEM or calc_type == ANALYTIC:
    Brpeak = []
    Br1 = []
    Br_THD = []
  elif calc_type == BOTH:
    Brpeak_semfem = []
    Br1_semfem = []
    Br_THD_semfem = []  
    Brpeak_analytic = []
    Br1_analytic = []
    Br_THD_analytic = []      
  
  #simulate/calculate the idrfpm machine for variations in a specific parameter
  for i in range (0,np.size(perturb_range)):
    replaceLine(parameter,perturb_range[i])
    replaceFile()
    #var[parameter[0]-1] = perturb_range[i]
    #updateInputFile()
    
    #provoke simulation/calculation process
    if calc_type == SEMFEM or calc_type == ANALYTIC:
      if calc_type == SEMFEM:
	idrfpm_semfem.main()
	#get results from this iteration
	results = np.loadtxt('./script_results/idrfpm/output.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
	
      elif calc_type == ANALYTIC:
	qhdrfapm.main(**{'read_input':True, 'andreas':1})
	#get results from this iteration
	results = np.loadtxt('./script_results/qhdrfapm/output.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)      
      
      Brpeak.append(results[0])
      Br1.append(results[1])
      Br_THD.append(results[2])
      
    elif calc_type == BOTH:
      idrfpm_semfem.main()
      qhdrfapm.main(**{'read_input':True, 'andreas':1})
      
      results = np.loadtxt('./script_results/idrfpm/output.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
      
      Brpeak_semfem.append(results[0])
      Br1_semfem.append(results[1])
      Br_THD_semfem.append(results[2])        
      
      results = np.loadtxt('./script_results/qhdrfapm/output.csv',skip_header=1,usecols=(0,1,2),delimiter=",",unpack=True)
      
      Brpeak_analytic.append(results[0])
      Br1_analytic.append(results[1])
      Br_THD_analytic.append(results[2])      
    
    i = i + 1
    print "\nPerturb counter = %d"%i
    

  #plot data for variation in parameter
  fig, ax1 = pl.subplots()

  pl.figure(1)
  pl.title(r'$B_{r|r_n (peak)}, B_{r1|r_n (peak)}, THD_{Br|r_n}$ due to the Permanent Magnets, for variation in $%s$'%parameter[1],y=1.02)#fontsize = 26,

  ax2 = ax1.twinx()
  
  if calc_type == SEMFEM or calc_type == ANALYTIC:
    ax1.plot(perturb_range,Brpeak,'b',label=r'$B_{r|r_n (peak)}$')
    ax1.plot(perturb_range,Br1,'g',label=r'$B_{r1|r_n (peak)}$')
    ax2.plot(perturb_range,Br_THD,'r',label=r'$THD_{Br|rn}$')
  elif calc_type == BOTH:
    ax1.plot(perturb_range,Brpeak_semfem,'b',label=r'$B_{r|r_n (peak)}$ (SEMFEM)',linestyle = '-')
    ax1.plot(perturb_range,Br1_semfem,'g',label=r'$B_{r1|r_n (peak)}$ (SEMFEM)',linestyle = '-')
    ax2.plot(perturb_range,Br_THD_semfem,'r',label=r'$THD_{Br|rn}$ (SEMFEM)',linestyle = '-')
    
    ax1.plot(perturb_range,Brpeak_analytic,'b',label=r'$B_{r|r_n (peak)}$')
    ax1.plot(perturb_range,Br1_analytic,'g',label=r'$B_{r1|r_n (peak)}$')
    ax2.plot(perturb_range,Br_THD_analytic,'r',label=r'$THD_{Br|rn}$')    

  ax1.set_xlabel(r'$%s$ %s'%(parameter[1],parameter[2]))#,fontsize = 20
  ax1.set_ylabel(r'Radial flux density $B_{r|r_n (peak)}, B_{r1|r_n (peak)}$ [T]')#,fontsize = 20
  ax2.set_ylabel(r'Percentage $THD_{Br|r_n}$')#,fontsize = 20

  l1 = ax1.legend(loc='upper right',fontsize = 16)
  l2 = ax2.legend(loc='upper center',fontsize = 16)

  # set the linewidth of each legend object
  for legobj in l1.legendHandles:
    legobj.set_linewidth(2.0)

  for legobj in l2.legendHandles:
    legobj.set_linewidth(2.0)

  if calc_type == ANALYTIC:
    pl.savefig('./script_results/main/analytic_%s.pdf'%parameter[1],transparent=True, bbox_inches='tight')
  elif calc_type == SEMFEM:
    pl.savefig('./script_results/main/semfem_%s.pdf'%parameter[1],transparent=True, bbox_inches='tight')
  elif calc_type == BOTH:
    pl.savefig('./script_results/main/both_%s.pdf'%parameter[1],transparent=True, bbox_inches='tight')

  pl.show()
  
def proximityEffect():
  stringy = data[IQ[0]+1]
  iq = stringy.split(",")
  iq = float(iq[1])
  
  stringy = data[BR[0]+1]
  br = stringy.split(",")
  br = float(br[1])
  
  stringy = data[MUR[0]+1]
  mur = stringy.split(",")
  mur = float(mur[1])    
  
  #simulate machine with only the PMs being active
  ################################################
  print "Simulating machine with only the PMs being active"
  replaceLine(BR,br)
  replaceLine(MUR,mur)
  replaceLine(ID,0)
  replaceLine(IQ,0)
  replaceLine(ACTIVE_PARTS,1)
  replaceFile()
  
  idrfpm_semfem.main()
  os.rename('./script_results/'+function_evaluation_script+'/br.csv', './script_results/'+function_evaluation_script+'/br_sim1.csv')
  
  #simulate machine with only the windings being active
  #####################################################
  print "\nSimulating machine with only the windings being active"
  
  replaceLine(BR,0)
  replaceLine(MUR,1)
  replaceLine(ID,0)
  replaceLine(IQ,iq)
  replaceLine(ACTIVE_PARTS,2)
  replaceFile()  
  
  idrfpm_semfem.main()
  os.rename('./script_results/'+function_evaluation_script+'/br.csv', './script_results/'+function_evaluation_script+'/br_sim2.csv')
  
  #simulate machine with both PMs and windings being active
  #########################################################
  print "\nSimulating machine with both PMs and windings being active"
  
  replaceLine(BR,br)
  replaceLine(MUR,mur)
  replaceLine(ID,0)
  replaceLine(IQ,iq)
  replaceLine(ACTIVE_PARTS,3)
  replaceFile()  
  
  idrfpm_semfem.main()
  os.rename('./script_results/'+function_evaluation_script+'/br.csv', './script_results/'+function_evaluation_script+'/br_sim3.csv')


# the goal of this function is to compare the initial design of Gert's machine, with the altered more practical machine (which has smaller magnet width and uses Litz wire)
# 1. compare torque of the two machines (both PMs and windings active)
# 2. compare eddy current losses (both PMs and windings active) (need Br curves for that)

def compareTwoMachines():
  #temporary save initial values in text file
  #stringy = data[HM[0]+1]
  #hm = stringy.split(",")
  #hm = float(hm[1])
  
  #stringy = data[TURNS_PER_COIL[0]+1]
  #turns_per_coil = stringy.split(",")
  #turns_per_coil = float(turns_per_coil[1])
  
  #stringy = data[FILL_FACTOR[0]+1]
  #fill_factor = stringy.split(",")
  #fill_factor = float(fill_factor[1])    
  
  
  print "\nSimulating machine (1)"
  
  replaceLine(HM,0.006)
  replaceLine(TURNS_PER_COIL,22)
  replaceLine(FILL_FACTOR,0.5)
  replaceLine(NUMBER_OF_LAYERS_EDDY,5)
  replaceLine(MAX_HARMONICS_EDDY,15)
  replaceLine(WIRE_DIAMETER,0.0016)
  replaceLine(PARALLEL_STRANDED_CONDUCTORS,1)
  
  replaceFile()
  
  idrfpm_semfem.main()
  
  #check if directory exists, if not, create the directory
  dir = './script_results/main/compareTwoMachines'
  if not os.path.isdir(dir): os.makedirs(dir)  
  
  os.rename('./script_results/'+function_evaluation_script+'/br.csv', './script_results/main/compareTwoMachines/br_machine1.csv')  
  os.rename('./script_results/'+function_evaluation_script+'/output.csv', './script_results/main/compareTwoMachines/output_machine1.csv')  
  
  print "\nSimulating machine (2)"
  
  replaceLine(HM,0.0055)
  replaceLine(TURNS_PER_COIL,20)
  replaceLine(FILL_FACTOR,0.3)
  replaceLine(NUMBER_OF_LAYERS_EDDY,5)
  replaceLine(MAX_HARMONICS_EDDY,15)
  replaceLine(WIRE_DIAMETER,0.0002)
  replaceLine(PARALLEL_STRANDED_CONDUCTORS,70)  
  
  replaceFile()  
  
  idrfpm_semfem.main()
  
  os.rename('./script_results/'+function_evaluation_script+'/br.csv', './script_results/main/compareTwoMachines/br_machine2.csv') 
  os.rename('./script_results/'+function_evaluation_script+'/output.csv', './script_results/main/compareTwoMachines/output_machine2.csv')  
  
  #replace initial values back into text file
  #replaceLine(HM,hm)
  #replaceLine(TURNS_PER_COIL,turns_per_coil)
  #replaceLine(FILL_FACTOR,fill_factor)
  #replaceFile()  
  



############################
# Start main control process #
############################

start_time = time.time()

#for i in range(0,5):
  #idrfpm_semfem.main(1)

# test commands
###############
#perturb(H,np.arange(0.006,0.013,0.001),ANALYTIC)
#perturb(KM,np.arange(0.5,0.8,0.05),ANALYTIC)
#perturb(KM,np.arange(0.2,0.8,0.05),ANALYTIC)
#perturb(RN, np.arange(0.08, 0.14, 0.01), ANALYTIC)
#perturb(HM, np.arange(0.003,0.009,0.001),ANALYTIC)
#perturb(G, np.arange(0.0002,0.002,0.0002),ANALYTIC)
#perturb(KM,np.arange(0.2,0.8,0.05),ANALYTIC)
#perturb(KM,np.arange(0.2,0.8,0.05),SEMFEM)

#roximityEffect()
#replaceLine(BR,2.068)
#replaceLine(MUR,0.89)
#replaceLine(WIRE_RESISTIVITY,1.678e-8)
#replaceFile()

#compareTwoMachines()

##############################
# Start optimization process #
##############################

#determine ideal values
if enable_ideal_value_calculation == True:  
  dir_parent='./mp/p1/'
  ################################
  ###CALCULATE IDEAL EFFICIENCY###
  ################################
  #write ideal objective function values to text files, these values are dummy values that will simply be used to calculate the actual values
  np.savetxt(dir_parent+'ideal_solutions.csv', np.transpose([[1],[1],[1]]), delimiter = ',',newline='\n',header='f_eff_ideal, f_tor_ideal, f_pm_ideal',fmt='%f')

  w_eff = 1
  w_tor = 0
  w_pm  = 0

  p = 1 #assume that when we calculate ideal values, we are only using a single process
  dir_process = './mp/p%d/'%p
  if not os.path.isdir(dir_process): os.makedirs(dir_process)

  #write the weight values to text file
  #dir_parent = './mp/p%d/'%args.thread
  #weights_objective_data = np.genfromtxt(dir_parent+'objective_weights.csv',skip_header=1,delimiter=',',unpack=False)
  np.savetxt(dir_process+'objective_weights.csv', np.transpose([[w_eff],[w_tor],[w_pm]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')

  #now find the real ideal value
  os.system("./optimization.py -t %d"%(p))
  os.system("./post_script.py 14 -t %d"%(p))
  
  #read single ideal value (before it is overwritten by the next ideal value calculation)
  ideal_objective_data = np.genfromtxt(dir_process+'f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
  f_eff_ideal = abs(ideal_objective_data[1]) #minus is here because we are trying to maximize this quantity
   #minus is here because we are trying to maximize this quantity
  
  #pack all the most important results in a folder
  dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
  if not os.path.isdir(dir): os.makedirs(dir)

  shutil.copy('input_original.csv',dir+'/input_original.csv')
  #shutil.copy('./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
  #shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
  #shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
  shutil.copy(dir_parent+'optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
  shutil.copy(dir_parent+'f_data.csv',dir+'/f_data.csv')
  shutil.copy(dir_parent+'ideal_solutions.csv',dir+'/ideal_solutions.csv')
  shutil.copy(dir_parent+'solution.txt',dir+'/solution.txt')

  #copy all the png animation files to a new folder
  if enable_animate_optimization == True:
    dest = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
    if not os.path.isdir(dest): os.makedirs(dest)  
    src = './script_results/main/optimization_animation/png/'
    src_files = os.listdir(src)
    for file_name in src_files:
	full_file_name = os.path.join(src, file_name)
	if (os.path.isfile(full_file_name)):
	    shutil.copy(full_file_name, dest)  

  #delete all old animation files
    dir = './script_results/main/optimization_animation/svg/'
    if not os.path.isdir(dir): os.makedirs(dir)
    delete_dir = dir+'*'
    os.system("rm %s"%(delete_dir))
    
    dir = './script_results/main/optimization_animation/png/'
    if not os.path.isdir(dir): os.makedirs(dir)  
    delete_dir = dir+'*'
    os.system("rm %s"%(delete_dir))


  #reset iteration number to zero
  with open(dir_parent+'./script_results/main/running_flags.csv', 'w') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%0)    
  
  ############################
  ###CALCULATE IDEAL TORQUE###
  ############################
  #write ideal objective function values to text files, these values are dummy values that will simply be used to calculate the actual values
  np.savetxt('ideal_solutions.csv', np.transpose([[1],[1],[1]]), delimiter = ',',newline='\n',header='f_eff_ideal, f_tor_ideal, f_pm_ideal',fmt='%f')

  w_eff = 0
  w_tor = 1
  w_pm  = 0

  #write the weight values to text file
  np.savetxt(dir_process+'objective_weights.csv', np.transpose([[w_eff],[w_tor],[w_pm]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')

  #now find the real ideal value
  os.system("./optimization.py -t %d"%(p))
  os.system("./post_script.py 14 -t %d"%(p))
  
  #read single ideal value (before it is overwritten by the next ideal value calculation)
  
  ideal_objective_data = np.genfromtxt(dir_parent+'f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
  f_tor_ideal = abs(ideal_objective_data[2]) #minus is here because we are trying to maximize this quantity
  f_pm_ideal = abs(ideal_objective_data[3])
  
  print("f_eff_ideal = %f"%f_eff_ideal)
  print("f_tor_ideal = %f"%f_tor_ideal)
  print("f_pm_ideal = %f"%f_pm_ideal)
  
  #pack all the most important results in a folder
  dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
  if not os.path.isdir(dir): os.makedirs(dir)

  shutil.copy('input_original.csv',dir+'/input_original.csv')
  #shutil.copy('./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
  #shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
  #shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
  shutil.copy(dir_parent+'optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
  shutil.copy(dir_parent+'f_data.csv',dir+'/f_data.csv')
  shutil.copy(dir_parent+'ideal_solutions.csv',dir+'/ideal_solutions.csv')
  shutil.copy(dir_parent+'solution.txt',dir+'/solution.txt')

  #copy all the png animation files to a new folder
  if enable_animate_optimization == True:
    dest = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
    if not os.path.isdir(dest): os.makedirs(dest)  
    src = './script_results/main/optimization_animation/png/'
    src_files = os.listdir(src)
    for file_name in src_files:
	full_file_name = os.path.join(src, file_name)
	if (os.path.isfile(full_file_name)):
	    shutil.copy(full_file_name, dest)  

  #delete all old animation files
    dir = './script_results/main/optimization_animation/svg/'
    if not os.path.isdir(dir): os.makedirs(dir)
    delete_dir = dir+'*'
    os.system("rm %s"%(delete_dir))
    
    dir = './script_results/main/optimization_animation/png/'
    if not os.path.isdir(dir): os.makedirs(dir)  
    delete_dir = dir+'*'
    os.system("rm %s"%(delete_dir))


  #reset iteration number to zero
  with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
    running_flags_file.write("iteration_number,%d"%0)    
  
  #############################
  ###CALCULATE IDEAL PM MASS###
  #############################
  #write ideal objective function values to text files, these values are dummy values that will simply be used to calculate the actual values
  #np.savetxt('ideal_solutions.csv', np.transpose([[1],[1],[1]]), delimiter = ',',newline='\n',header='f_eff_ideal, f_tor_ideal, f_pm_ideal',fmt='%f')

  #w_eff = 0
  #w_tor = 0
  #w_pm  = 1

  ##write the weight values to text file
  #np.savetxt('objective_weights.csv', np.transpose([[w_eff],[w_tor],[w_pm]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')

  ##now find the real ideal value
  #os.system("./optimization.py")
  #os.system("./post_script.py 14")
  
  ##read single ideal value (before it is overwritten by the next ideal value calculation)
  #ideal_objective_data = np.genfromtxt('f_data.csv',skip_header=0,delimiter=',',usecols=(2),unpack=True)
  #f_pm_ideal = abs(ideal_objective_data[0])
  
  #now, finally, write all the ideal values that were obtained back into the text file
  np.savetxt(dir_parent+'ideal_solutions.csv', np.transpose([[f_eff_ideal],[f_tor_ideal],[f_pm_ideal]]), delimiter = ',',newline='\n',header='f_eff_ideal, f_tor_ideal, f_pm_ideal',fmt='%f')

  ##pack all the most important results in a folder
  #dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
  #if not os.path.isdir(dir): os.makedirs(dir)

  #shutil.copy('input_original.csv',dir+'/input_original.csv')
  #shutil.copy('./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
  #shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
  #shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
  #shutil.copy('optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
  #shutil.copy('ideal_solutions.csv',dir+'/ideal_solutions.csv')
  #shutil.copy('f_data.csv',dir+'/f_data.csv')
  #shutil.copy('solution.txt',dir+'/solution.txt')

  ##copy all the png animation files to a new folder
  #if enable_animate_optimization == True:
    #dest = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
    #if not os.path.isdir(dest): os.makedirs(dest)  
    #src = './script_results/main/optimization_animation/png/'
    #src_files = os.listdir(src)
    #for file_name in src_files:
	#full_file_name = os.path.join(src, file_name)
	#if (os.path.isfile(full_file_name)):
	    #shutil.copy(full_file_name, dest)  

  ##delete all old animation files
    #dir = './script_results/main/optimization_animation/svg/'
    #if not os.path.isdir(dir): os.makedirs(dir)
    #delete_dir = dir+'*'
    #os.system("rm %s"%(delete_dir))
    
    #dir = './script_results/main/optimization_animation/png/'
    #if not os.path.isdir(dir): os.makedirs(dir)  
    #delete_dir = dir+'*'
    #os.system("rm %s"%(delete_dir))


  ##reset iteration number to zero
  #with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
    #running_flags_file.write("iteration_number,%d"%0)  

#iterate over weight values



'''
#hot_start = True

#active_outer = True
while active_outer == True:
  
  #if hot_start == False:
  w_pm = 1 - w_eff - w_tor #so that the sum of all three weights are equal to 1
  
  #dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
  #if os.path.isdir(dir):
    #print "dir does exist"
    ##skip this run
    #### OUTER LOOP
    #w_eff = w_eff+itr
    #w_tor = 1-w_eff
    ##reset iteration number to zero
    #with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
      #running_flags_file.write("iteration_number,%d"%0)  
  #else:
  active_inner = True
  while active_inner == True:
    #chek if this datapoint was already evaluated
    #write the weight values to text file
    np.savetxt('objective_weights.csv', np.transpose([[w_eff],[w_tor],[w_pm]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')
    
    #run optimization process
    #print "poep"
    os.system("./optimization.py")
    os.system("./post_script.py 14")
    #print "arrives here"

    #pack all the most important results in a folder
    #dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    if not os.path.isdir(dir): os.makedirs(dir)

    shutil.copy('input_original.csv',dir+'/input_original.csv')
    shutil.copy('./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
    shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
    shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
    shutil.copy('optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
    shutil.copy('f_data.csv',dir+'/f_data.csv')
    shutil.copy('ideal_solutions.csv',dir+'/ideal_solutions.csv')
    shutil.copy('solution.txt',dir+'/solution.txt')

    #copy all the png animation files to a new folder
    if enable_animate_optimization == True:
      dest = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
      if not os.path.isdir(dest): os.makedirs(dest)  
      src = './script_results/main/optimization_animation/png/'
      src_files = os.listdir(src)
      for file_name in src_files:
	  full_file_name = os.path.join(src, file_name)
	  if (os.path.isfile(full_file_name)):
	      shutil.copy(full_file_name, dest)  

    #delete all old animation files
      dir = './script_results/main/optimization_animation/svg/'
      if not os.path.isdir(dir): os.makedirs(dir)
      delete_dir = dir+'*'
      os.system("rm %s"%(delete_dir))
      
      dir = './script_results/main/optimization_animation/png/'
      if not os.path.isdir(dir): os.makedirs(dir)  
      delete_dir = dir+'*'
      os.system("rm %s"%(delete_dir))

    #w_eff = w_eff + itr
    #w_pm  = 1 - w_eff
    
    #w_eff = w_eff + itr
    #w_tor  = 1 - w_eff

    #w_tor = w_tor + itr
    #w_pm  = 1 - w_tor

    #if w_eff > 0.05:
      #active_inner = False
      
    #if w_tor > 0.05:
      #active_inner = False
    

    #if w_eff >= 1+(0.5*itr):
      
      
      ##active_inner = False
      
    #if w_tor >= 1+(0.5*itr):
      #active_inner = False
    
    #if w_pm >= 1+(0.5*itr):
      #active_inner = False

    

    w_pm = w_pm + itr
    if w_pm > 1+(0.5*itr): active_inner = False

    #reset iteration number to zero
    with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
      running_flags_file.write("iteration_number,%d"%0)  

    hot_start = False
    ### OUTER LOOP
    w_eff = w_eff+itr
    w_tor = 1-w_eff

    if w_eff >= 1+(0.5*itr): active_outer = False
'''
    











#w_eff = 0
#w_tor = 0
#w_pm = 0

#w_pm_init = 0
#w_tor_init = 0
#w_eff_init = 1

#i = 0
#j = 0

#itr = 0.025

#div = int(1/itr)


#WHILE I<DIV+1:
  
  #j = i
  #while j<div+1:
    #w_tor = w_tor_init + j*itr - w_pm
    #w_eff = abs(1 - w_tor - w_pm)
    
    ##check if this datapoint was already evaluated
    #dir = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f'%(abs(w_eff),abs(w_tor),abs(w_pm))
    #if not os.path.isdir(dir):
      #os.makedirs(dir)
      
      ##write the weight values to text file
      #np.savetxt('objective_weights.csv', np.transpose([[abs(w_eff)],[abs(w_tor)],[abs(w_pm)]]), delimiter = ',',newline='\n',header='w_eff, w_tor, w_pm',fmt='%f')
      
      ##run optimization process
      #os.system("./optimization.py")
      #os.system("./post_script.py 14")      
      
      #shutil.copy('input_original.csv',dir+'/input_original.csv')
      #shutil.copy('./script_results/'+function_evaluation_script+'/optimization_output.csv',dir+'/optimization_output.csv')
      ##shutil.copy('./project_idrfpm_semfem/results/fieldplots/band_machine_field001.fpl',dir+'/band_machine_field001.fpl')
      ##shutil.copy('./project_idrfpm_semfem/results/post.res',dir+'/post.res')
      #shutil.copy('optimization_results_summary.csv',dir+'/optimization_results_summary.csv')
      #shutil.copy('f_data.csv',dir+'/f_data.csv')
      #shutil.copy('ideal_solutions.csv',dir+'/ideal_solutions.csv')
      #shutil.copy('solution.txt',dir+'/solution.txt')

      ##copy all the png animation files to a new folder
      #if enable_animate_optimization == True:
	#dest = './optimization_database/w1=%.3f,w2=%.3f,w3=%.3f/png/'%(abs(w_eff),abs(w_tor),abs(w_pm))
	#if not os.path.isdir(dest): os.makedirs(dest)  
	#src = './script_results/main/optimization_animation/png/'
	#src_files = os.listdir(src)
	#for file_name in src_files:
	    #full_file_name = os.path.join(src, file_name)
	    #if (os.path.isfile(full_file_name)):
		#shutil.copy(full_file_name, dest)  

      ##delete all old animation files
	#dir = './script_results/main/optimization_animation/svg/'
	#if not os.path.isdir(dir): os.makedirs(dir)
	#delete_dir = dir+'*'
	#os.system("rm %s"%(delete_dir))
	
	#dir = './script_results/main/optimization_animation/png/'
	#if not os.path.isdir(dir): os.makedirs(dir)  
	#delete_dir = dir+'*'
	#os.system("rm %s"%(delete_dir))

    ##reset iteration number to zero
    #with open('./script_results/main/running_flags.csv', 'w') as running_flags_file:
      #running_flags_file.write("iteration_number,%d"%0)  

    #j = j + 1

  #i = i + 1
  #w_pm = w_pm_init + i*itr









#initiate each thread
p = 1
while p <= processes:
  #create working directories for each thread
  dir = './mp/p%d/'%p
  if not os.path.isdir(dir): os.makedirs(dir)
  
  shutil.copy('input_original.csv',dir+'/input_original.csv')
  shutil.copy('input_original.csv',dir+'/input_edited.csv')
  
  t = threading.Thread(target=optimizeThread, args=(p,loop_array,start_end_index,))
  t.start()


  p = p + 1



      #progress_string = "%d/%d"%(count,end_num-start_num)
      #count = count + 1
      
      #PRINT_READY[p-1] = 1
      
      
      #for k in range (0, len(start_end_index)):
	#if PRINT_READY[k] == 1:
	  #all_ready = all_ready + 1
      
      #if all_ready == len(start_end_index):
	#for k in range (0, len(start_end_index)):
	  #PRINT_READY[k] = 0
	#all_ready = 0
	
	

  
  #print("thread active count %d"%threading.active_count())
  
time_temp = time.time()  
while threading.active_count() > 1:
  #if time.time()-time_temp > 3:
  #time.sleep(4)
  for i in range(1,processes+1):
    dir_parent = './mp/p%d/'%(i)
    if (os.path.isfile(dir_parent+'script_results/main/running_flags.csv')):
      run_flags = np.genfromtxt(dir_parent+'script_results/main/running_flags.csv',skip_header=0,delimiter=',',usecols=(1),unpack=True)
      iterations = int(run_flags)
    else:
      iterations = 0
    pretty_multi_proc_table.add_row([i,progress_string[i-1],iterations])
    #pretty_multi_proc_table.add_row([i,progress_string[i-1],0])
  
  print pretty_multi_proc_table
  pretty_multi_proc_table.clear_rows()
  time_temp = time.time()
  

